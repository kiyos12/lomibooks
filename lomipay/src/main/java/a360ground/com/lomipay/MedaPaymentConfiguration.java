package a360ground.com.lomipay;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kiyos on 8/22/2017.
 */

public class MedaPaymentConfiguration implements Parcelable{

    private String title;
    private String description;
    private String message;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MedaPaymentConfiguration(Parcel in) {
        this.title = in.readString();
        this.description = in.readString();
        this.message = in.readString();
    }
    public MedaPaymentConfiguration() {
    }

    public static final Creator<MedaPaymentConfiguration> CREATOR = new Creator<MedaPaymentConfiguration>() {
        @Override
        public MedaPaymentConfiguration createFromParcel(Parcel in) {
            return new MedaPaymentConfiguration(in);
        }

        @Override
        public MedaPaymentConfiguration[] newArray(int size) {
            return new MedaPaymentConfiguration[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(message);
    }


}
