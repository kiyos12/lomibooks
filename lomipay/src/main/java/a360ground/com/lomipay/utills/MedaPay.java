package a360ground.com.lomipay.utills;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import java.util.UUID;

import a360ground.com.lomipay.MedaPaymentConfiguration;
import a360ground.com.lomipay.dialog.MedaPayment;
import a360ground.com.lomipay.logic.SMS;

/**
 * Created by Kiyos on 8/22/2017.
 */

public class MedaPay {

    public static final int MEDA_BILLED = 1;

    public static final int MEDA_PENDING = 2;

    public static final int MEDA_FAILED = 3;

    public static final int MEDA_PAY_REQUEST_CODE = 1221;

    public static final int PERMISSION_SEND_SMS = 123;

    public static final String MEDA_CONF = "meda_conf";

    public static final String MEDA_PASS = "meda_pass";

    public static String getMedaKey(Context context) {

        if (PreferenceUtills.readFromPreferences(context, MEDA_PASS, null) != null) {
            return PreferenceUtills.readFromPreferences(context, MEDA_PASS, null);
        } else {
            String id = generateID();
            PreferenceUtills.saveToPreferences(context, MEDA_PASS, id);
            return id;
        }
    }

    private static String generateID() {
        return UUID.randomUUID().toString().substring(0, 5);
    }

    public static class MedaPaymentRequestBuilder implements SMS.IPaymentStatus {

        public SMS.IPaymentStatus paymentStatus;
        Context context;
        MedaPaymentConfiguration medaPaymentConfiguration;

        public MedaPaymentRequestBuilder(Context context, MedaPaymentConfiguration medaPaymentConfiguration) {

            this.context = context;

            this.medaPaymentConfiguration = medaPaymentConfiguration;
        }

        public void setPaymentStatus(SMS.IPaymentStatus paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public void build(FragmentManager fragmentManager) {

            MedaPayment medaPayment = new MedaPayment();

            medaPayment.setTargetFragment(medaPayment, MEDA_PAY_REQUEST_CODE);

            medaPayment.setPaymentStatus(this);

            medaPayment.show(fragmentManager, MedaPay.class.getSimpleName(), medaPaymentConfiguration);
        }

        @Override
        public void onPaymentSuccessfull(int id) {
            paymentStatus.onPaymentSuccessfull(id);
        }

        @Override
        public void onPaymentUnSuccessfull(int id, String message) {
            paymentStatus.onPaymentUnSuccessfull(id, message);
        }
    }
}
