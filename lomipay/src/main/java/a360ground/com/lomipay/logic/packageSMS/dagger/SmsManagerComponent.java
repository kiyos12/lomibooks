package a360ground.com.lomipay.logic.packageSMS.dagger;



import javax.inject.Singleton;

import a360ground.com.lomipay.logic.packageSMS.SmsHandler;
import dagger.Component;

@Singleton
@Component(modules ={
        SmsManagerModule.class
} )
public interface SmsManagerComponent {
    void inject(SmsHandler handler);
}
