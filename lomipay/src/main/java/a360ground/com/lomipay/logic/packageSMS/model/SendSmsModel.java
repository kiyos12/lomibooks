package a360ground.com.lomipay.logic.packageSMS.model;


public interface SendSmsModel {

    void generateSMSForMultiSimCards(int smsId, String body, int carrierSlotCount, int carrierSlutNumber,
                                     String carrierName, String carrierNameFilter,
                                     MySmsManager.SMSManagerCallBack callBack);

    void generateSMSForSingleSimCard(int smsId, String body, String carrierNameFilter,
                                     MySmsManager.SMSManagerCallBack callBack);
}

