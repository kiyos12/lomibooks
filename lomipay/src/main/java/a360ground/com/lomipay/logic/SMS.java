package a360ground.com.lomipay.logic;

import android.content.Context;
import android.util.Base64;

import a360ground.com.lomipay.logic.packageSMS.SmsHandler;
import a360ground.com.lomipay.logic.packageSMS.model.MySmsManager;


/**
 * Created by Kiyos on 8/22/2017.
 */

public class SMS {



    public static void send(Context context, String message, final IPaymentStatus paymentStatus){

        MySmsManager mySmsManager = new MySmsManager("8073", context);

        mySmsManager.generateSMS(1221, message, new MySmsManager.SMSManagerCallBack() {
            @Override
            public void afterSuccessfulSMS(int smsId) {
                paymentStatus.onPaymentSuccessfull(smsId);
            }

            @Override
            public void afterDelivered(int smsId) {

            }

            @Override
            public void afterUnSuccessfulSMS(int smsId, String message) {
                paymentStatus.onPaymentUnSuccessfull(smsId, message);
            }

            @Override
            public void onCarrierNameNotMatch(int smsId, String message) {

            }
        });
    }

    public interface IPaymentStatus{
        void onPaymentSuccessfull(int id);
        void onPaymentUnSuccessfull(int id, String message);
    }

}
