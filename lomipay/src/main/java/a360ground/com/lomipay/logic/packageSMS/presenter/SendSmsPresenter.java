package a360ground.com.lomipay.logic.packageSMS.presenter;

import a360ground.com.lomipay.logic.packageSMS.model.MySmsManager;
import a360ground.com.lomipay.logic.packageSMS.view.SendSmsView;


public interface SendSmsPresenter {

    void closeDialog();

    void startWiringUp(String dialogMessage, String smsBody, MySmsManager.SMSManagerCallBack callback);

    void prepareSendSms();

    void sendSmsFromSubscriptionIdIndex(int id);

    void cancelSendSms();

    void removeView();

    void setView(SendSmsView view);

    void setCarrierNameFilter(String filter);

    void setNeedDialog(boolean ifNeed);

}
