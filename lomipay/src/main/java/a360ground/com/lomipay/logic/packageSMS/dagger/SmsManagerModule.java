package a360ground.com.lomipay.logic.packageSMS.dagger;


import android.content.Context;

import javax.inject.Singleton;

import a360ground.com.lomipay.logic.packageSMS.model.MySmsManager;
import a360ground.com.lomipay.logic.packageSMS.model.SendSmsModel;
import a360ground.com.lomipay.logic.packageSMS.model.SendSmsModelImpl;
import a360ground.com.lomipay.logic.packageSMS.presenter.SendSmsPresenter;
import a360ground.com.lomipay.logic.packageSMS.presenter.SendSmsPresenterImpl;
import a360ground.com.lomipay.logic.packageSMS.view.SendSmsView;
import a360ground.com.lomipay.logic.packageSMS.view.SendSmsViewImpl;
import dagger.Module;
import dagger.Provides;

@Module
public class SmsManagerModule {


    private String smsNumber;
    private Context context;


    public SmsManagerModule(String smsNumber, Context context) {
        this.context = context;
        this.smsNumber = smsNumber;
    }


    @Singleton
    @Provides
    Context provideContext() {
        return this.context;
    }

    @Singleton
    @Provides
    MySmsManager providesMySmsManager(Context context) {
        return new MySmsManager(smsNumber, context);
    }

    @Singleton
    @Provides
    SendSmsModel providesSendSmsModel(MySmsManager mySmsManager) {
        return new SendSmsModelImpl(mySmsManager);
    }

    @Singleton
    @Provides
    SendSmsPresenter providesSendSmsPresenter(SendSmsModel model, Context context) {
        return new SendSmsPresenterImpl(model, context);
    }

    @Singleton
    @Provides
    SendSmsView providesSendSmsView(SendSmsPresenter presenter) {
        return new SendSmsViewImpl(presenter);
    }
}
