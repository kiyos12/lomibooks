package a360ground.com.lomipay.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import a360ground.com.lomipay.MedaPaymentConfiguration;
import a360ground.com.lomipay.R;
import a360ground.com.lomipay.logic.SMS;
import a360ground.com.lomipay.utills.MedaPay;

/**
 * Created by Kiyos on 8/23/2017.
 */

public class MedaPayment extends DialogFragment implements View.OnClickListener, SMS.IPaymentStatus {

    public TextView paymentTitle;
    public Button paymentAccept;
    public RelativeLayout poweredByWrapper;
    private View view;
    private MedaPaymentConfiguration medaPaymentConfiguration;
    private SMS.IPaymentStatus paymentStatus;
    private ProgressDialog progressDialog;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder aBuilder = new AlertDialog.Builder(getActivity());

        view = LayoutInflater.from(getActivity()).inflate(R.layout.activity_meda_payment, null);

        progressDialog = new ProgressDialog(getActivity());

        progressDialog.setMessage("Completing payment Please wait . . .");

        initViews(view);

        aBuilder.setView(view);

        aBuilder.setCancelable(false);

        return aBuilder.create();
    }

    public void setPaymentStatus(SMS.IPaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public void show(FragmentManager fragmentManager, String tag, MedaPaymentConfiguration medaPaymentConfiguration) {
        this.medaPaymentConfiguration = medaPaymentConfiguration;
        show(fragmentManager, tag);
    }

    private void initViews(View view) {
        paymentTitle = (TextView) view.findViewById(a360ground.com.lomipay.R.id.payment_title);
        paymentAccept = (Button) view.findViewById(a360ground.com.lomipay.R.id.payment_accept);
        poweredByWrapper = (RelativeLayout) view.findViewById(a360ground.com.lomipay.R.id.powered_by_wrapper);
        paymentAccept.setOnClickListener(this);
        poweredByWrapper.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.payment_accept) {
            requestPermissionAndSend();
            progressDialog.show();
            dismiss();
        }
        if (view.getId() == R.id.powered_by_wrapper) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://meda.im"));
            startActivity(browserIntent);
        }
    }

    private void requestPermissionAndSend() {
        SMS.send(getActivity(), new String(Base64.encode(medaPaymentConfiguration.getMessage().getBytes(), 1)), this);
    }

    @Override
    public void onPaymentSuccessfull(int id) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        paymentStatus.onPaymentSuccessfull(id);
    }

    @Override
    public void onPaymentUnSuccessfull(int id, String message) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        paymentStatus.onPaymentUnSuccessfull(id, message);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case MedaPay.PERMISSION_SEND_SMS: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    SMS.send(getActivity(), new String(Base64.encode(medaPaymentConfiguration.getMessage().getBytes(), 1)), this);
                } else {
                    // permission denied
                }
                return;
            }
        }
    }
}
