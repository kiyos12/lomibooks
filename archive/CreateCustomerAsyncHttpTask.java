package com.a360ground.lomibook.asyncTask;

import android.content.Context;
import android.os.AsyncTask;

import com.a360ground.lomibook.Constants;
import com.a360ground.lomibook.LomiApplication;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.WooCommerceError;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Converter;
import retrofit2.Response;

public class CreateCustomerAsyncHttpTask extends AsyncTask<String, Void, Integer> {

    private Customer customer;

    private WooCommerceError error;

    LomiApplication application;

    LomiAsyncTaskCallback mLomiAsyncTaskCallback;

    public CreateCustomerAsyncHttpTask(Context context, LomiAsyncTaskCallback lomiAsyncTaskCallback) {

        mLomiAsyncTaskCallback = lomiAsyncTaskCallback;

        application = (LomiApplication) context.getApplicationContext();
    }

    @Override
    protected void onPreExecute() {
        mLomiAsyncTaskCallback.onAsyncTaskStart();
    }

    @Override
    protected Integer doInBackground(String... params) {

        Integer result = 0;

        String phoneNumber = params[0] + params[1];

        String email = params[2];

        String password = params[3];

        String hashedPassword;

        try {

            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");

            hashedPassword = digest.digest(password.getBytes()).toString();

            HashMap<String, String> customerData = new HashMap<>();

            customerData.put(Constants.PHONE_NUMBER_IDENTIFIER, phoneNumber);

            customerData.put(Constants.EMAIL_IDENTIFIER, email);

            customerData.put(Constants.PASSWORD_IDENTIFIER, hashedPassword);

            Call<Customer> call = application.getCustomerClient().createCustomer(null/*customerData*/);

            Response<Customer> response = call.execute();

            if(response.isSuccessful()) {

                customer = response.body();

                result = 1;

            } else {

                error = (WooCommerceError) application.getErrorResponse(response.errorBody());
            }

        } catch (NoSuchAlgorithmException | IOException e) {

            error = new WooCommerceError(WooCommerceError.ErrorCode.CONNECTION_ERROR);

            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(Integer result) {

        if(result == 1) {

            mLomiAsyncTaskCallback.onAsyncTaskEndWithSuccess(customer);

        } else {

            mLomiAsyncTaskCallback.onAsyncTaskEndWithFailure(error);
        }

    }
}
