package com.a360ground.lomibook.asyncTask;

import android.content.Context;
import android.os.AsyncTask;

import com.a360ground.lomibook.LomiApplication;
import com.a360ground.lomibook.client.CategoryClient;
import com.a360ground.lomibook.domain.woocommerce.Category;
import com.a360ground.lomibook.domain.woocommerce.WooCommerceError;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class GetCategoryDetailsBySlugAsyncTask extends AsyncTask<Void, Void, Integer> {

    private String categorySlug;

    private Category category;

    private WooCommerceError error;

    private final LomiAsyncTaskCallback lomiAsyncTaskCallback;

    LomiApplication application;

    public GetCategoryDetailsBySlugAsyncTask(Context context, LomiAsyncTaskCallback lomiAsyncTaskCallback, String categorySlug) {

        this.lomiAsyncTaskCallback = lomiAsyncTaskCallback;

        this.categorySlug = categorySlug;

        application = (LomiApplication) context.getApplicationContext();
    }


    @Override
    protected Integer doInBackground(Void... params) {

        Integer result = 0;

        Call<List<Category>> call = application.getCategoryClient().getCategoriesBySlug(categorySlug);

        try {

            Response<List<Category>> response = call.execute();

            if(response.isSuccessful()) {

                List<Category> categories = response.body();

                category = categories.get(0);

                result = 1;

            } else {

                error = (WooCommerceError) application.getErrorResponse(response.errorBody());
            }

        } catch (IOException e) {

            error = new WooCommerceError(WooCommerceError.ErrorCode.CONNECTION_ERROR);

            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(Integer result) {

        if(result == 1) {

            lomiAsyncTaskCallback.onAsyncTaskEndWithSuccess(category);

        } else {

            lomiAsyncTaskCallback.onAsyncTaskEndWithFailure(error);
        }
    }
}
