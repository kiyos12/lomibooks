package com.a360ground.lomibook.asyncTask;

import android.content.Context;
import android.os.AsyncTask;

import com.a360ground.lomibook.LomiApplication;
import com.a360ground.lomibook.client.CustomerClient;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.WooCommerceError;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetCustomerByPhoneNumberAsyncHttpTask extends AsyncTask<Void, Void, Integer> {

    private static final String TAG = GetCustomerByPhoneNumberAsyncHttpTask.class.getSimpleName();

    LomiAsyncTaskCallback lomiAsyncTaskStartCallback;

    List<Customer> customers;

    WooCommerceError error;

    LomiApplication application;

    public GetCustomerByPhoneNumberAsyncHttpTask(Context context, LomiAsyncTaskCallback callback) {

        lomiAsyncTaskStartCallback = callback;

        application = (LomiApplication) context.getApplicationContext();
    }

    @Override
    protected void onPreExecute() {
        lomiAsyncTaskStartCallback.onAsyncTaskStart();
    }

    @Override
    protected Integer doInBackground(Void... params) {

        Integer result = 0;

        Call<List<Customer>> call = application.getCustomerClient().customers();

        try {

            Response<List<Customer>> response = call.execute();

            if(response.isSuccessful()) {

                customers = response.body();

                result = 1;

            } else {

                error = (WooCommerceError) application.getErrorResponse(response.errorBody());
            }


        } catch (IOException e) {

            error = new WooCommerceError(WooCommerceError.ErrorCode.CONNECTION_ERROR);

            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(Integer result) {

        if(result == 1) {

            lomiAsyncTaskStartCallback.onAsyncTaskEndWithSuccess(customers);

        } else {

            lomiAsyncTaskStartCallback.onAsyncTaskEndWithFailure(error);
        }
    }
}
