package com.a360ground.lomibook;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.a360ground.lomibook.asyncTask.ConfirmVerificationCode;
import com.a360ground.lomibook.asyncTask.CreateCustomerAsyncHttpTask;
import com.a360ground.lomibook.asyncTask.LomiAsyncTaskCallback;
import com.a360ground.lomibook.client.Confirmation;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.WooCommerceError;

import java.util.concurrent.ExecutionException;

public class ConfirmationActivity extends AppCompatActivity {

    private static final String TAG = ConfirmationActivity.class.getSimpleName();

    // This constant was defined to prevent the confirmation block
    // from being again and again when the broadcast receiver
    // keeps receiving broadcast multiple times for the duration
    // of the missed call
    // i.e. Call broadcasts are received every some milliseconds as long as the
    // call is active
    /*private static boolean CONFIRMATION_BLOCK_CALLED = false;

    private BroadcastReceiver missedCallBroadcastReceiver;*/

    public String phoneNumber;

    public String countryCode;

    public String uuid;

    private BroadcastReceiver smsBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_confirmation);

        Intent intent = getIntent();

        /*final String keyMatch = intent.getStringExtra(Constants.KEYMATCH_IDENTIFIER);

        final String otpStart = intent.getStringExtra(Constants.OTP_START_IDENTIFIER);*/

        phoneNumber = intent.getStringExtra(Constants.PHONE_NUMBER_IDENTIFIER);

        countryCode = intent.getStringExtra(Constants.COUNTRY_CODE_IDENTIFIER);

        uuid = intent.getStringExtra(Constants.UUID_IDENTIFIER);

        // Listen to the broadcast here
        /*missedCallBroadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                telephonyManager.listen(new PhoneStateListener() {
                    @Override
                    public void onCallStateChanged(int state, String incomingNumber) {

                        super.onCallStateChanged(state, incomingNumber);

                        Log.d(TAG, "Incoming Number: " + incomingNumber);

                        if(!CONFIRMATION_BLOCK_CALLED && incomingNumber.startsWith(otpStart)) {

                            try {

                                confirmPhoneNumber(keyMatch, incomingNumber);

                            } catch (ExecutionException | InterruptedException e) {

                                e.printStackTrace();

                            }

                        } else {
                            Log.d(TAG, "Trying to confirm again!");
                        }

                    }
                }, PhoneStateListener.LISTEN_CALL_STATE);
            }
        };*/

        smsBroadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                if(Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(intent.getAction())) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

                        for(SmsMessage smsMessage : Telephony.Sms.Intents.getMessagesFromIntent(intent)) {

                            String messageBody = smsMessage.getMessageBody();

                            // get code from message
                            String confirmationCode = messageBody.replaceAll("\\D+", "");

                            Log.d(TAG, messageBody);

                            try {

                                confirmPhoneNumber(countryCode, phoneNumber, confirmationCode);

                            } catch (ExecutionException e) {

                                e.printStackTrace();

                            } catch (InterruptedException e) {

                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        };

        // registerReceiver(missedCallBroadcastReceiver, new IntentFilter("android.intent.action.PHONE_STATE"));
        registerReceiver(smsBroadcastReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));

        setSupportActionBar((Toolbar) findViewById(R.id.toolbarConfirmation));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle("Verification");

        final EditText verificationCodeEditText = (EditText) findViewById(R.id.verification_code_edit_text);

        Button verifyButton = (Button) findViewById(R.id.verify_button);

        verifyButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // unregister the missed call broadcast receiver
                // unregisterReceiver(missedCallBroadcastReceiver);
                unregisterReceiver(smsBroadcastReceiver);

                // missedCallBroadcastReceiver = null;
                smsBroadcastReceiver = null;

                try {

                    confirmPhoneNumber(countryCode, phoneNumber, verificationCodeEditText.getText().toString());

                } catch (ExecutionException e) {

                    e.printStackTrace();

                } catch (InterruptedException e) {

                    e.printStackTrace();
                }
            }
        });
    }

    public void confirmPhoneNumber(String countryCode, final String phoneNumber, String confirmationCode) throws ExecutionException, InterruptedException {

        /*CONFIRMATION_BLOCK_CALLED = true;*/

        final ProgressDialog progressDialog = ProgressDialog.show(ConfirmationActivity.this, "Authy...ing", "Your number is being confirmed", true);

        ConfirmVerificationCode sendConfirmationCodeAsyncTask = new ConfirmVerificationCode(ConfirmationActivity.this, new LomiAsyncTaskCallback<Confirmation, Error>() {

            @Override
            public void onAsyncTaskStart() {

                progressDialog.show();
            }

            @Override
            public void onAsyncTaskProgressUpdate(Integer... values) {}

            @Override
            public void onAsyncTaskEndWithSuccess(Confirmation confirmation) {

                if(confirmation != null) {

                    // create the customer on WooCommerce
                    new CreateCustomerAsyncHttpTask(ConfirmationActivity.this, new LomiAsyncTaskCallback<Customer, WooCommerceError>() {

                        @Override
                        public void onAsyncTaskStart() {

                            progressDialog.setTitle("Creating Customer");

                            progressDialog.setMessage("Your customer information is being created. Please wait.");
                        }

                        @Override
                        public void onAsyncTaskProgressUpdate(Integer... values) {}

                        @Override
                        public void onAsyncTaskEndWithSuccess(Customer customer) {

                            progressDialog.dismiss();

                            //todo save the customer information in shared preferences
                            startActivity(new Intent(ConfirmationActivity.this, LomiBooksActivity.class));
                        }

                        @Override
                        public void onAsyncTaskEndWithFailure(WooCommerceError error) {

                            progressDialog.dismiss();

                            Toast.makeText(ConfirmationActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }).execute(phoneNumber, uuid);

                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onAsyncTaskEndWithFailure(Error error) {

                progressDialog.dismiss();

                Toast.makeText(ConfirmationActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

                finish();
            }

        });

        sendConfirmationCodeAsyncTask.execute(countryCode, phoneNumber, confirmationCode).get();
    }

    @Override
    protected void onDestroy() {

        /*if(missedCallBroadcastReceiver != null) {

            unregisterReceiver(missedCallBroadcastReceiver);

            missedCallBroadcastReceiver = null;
        }*/
        if(smsBroadcastReceiver != null) {

            unregisterReceiver(smsBroadcastReceiver);

            smsBroadcastReceiver = null;
        }
        super.onDestroy();
    }
}
