package com.a360ground.lomibook.asyncTask;

import android.content.Context;
import android.os.AsyncTask;

import com.a360ground.lomibook.LomiApplication;
import com.a360ground.lomibook.domain.woocommerce.WooCommerceError;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.client.ProductClient;
import com.a360ground.lomibook.domain.woocommerce.Product;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class GetProductsByCategoryAsyncHttpTask extends AsyncTask<Void, Void, Integer> {

    private static final String FRAGMENT_ASYNC_TAG = "FragmentAsync";

    List<Product> products;

    WooCommerceError error;

    Long categoryId;

    LomiAsyncTaskCallback lomiAsyncTaskCallback;

    LomiApplication application;

    public GetProductsByCategoryAsyncHttpTask(Context context, LomiAsyncTaskCallback callback, Long id) {

        lomiAsyncTaskCallback = callback;

        categoryId = id;

        application = (LomiApplication) context.getApplicationContext();
    }

    @Override
    protected void onPreExecute() {
        lomiAsyncTaskCallback.onAsyncTaskStart();
    }

    @Override
    protected Integer doInBackground(Void... params) {

        Integer result = 0;

        Call<List<Product>> call = application.getProductClient().productsByCategory(categoryId);

        try {

            Response<List<Product>> response = call.execute();

            if(response.isSuccessful()) {

                products = response.body();

                result = 1;
            } else {

                error = (WooCommerceError) application.getErrorResponse(response.errorBody());
            }

        } catch (IOException e) {

            error = new WooCommerceError(WooCommerceError.ErrorCode.CONNECTION_ERROR);

            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(Integer result) {

        if(result == 1) {

            lomiAsyncTaskCallback.onAsyncTaskEndWithSuccess(products);

        } else {

            lomiAsyncTaskCallback.onAsyncTaskEndWithFailure(error);
        }
    }
}