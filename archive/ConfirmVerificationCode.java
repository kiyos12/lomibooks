package com.a360ground.lomibook.asyncTask;

import android.content.Context;
import android.os.AsyncTask;

import com.a360ground.lomibook.LomiApplication;
import com.a360ground.lomibook.client.Confirmation;
import com.a360ground.lomibook.domain.authy.AuthyConfirmation;
import com.a360ground.lomibook.domain.woocommerce.WooCommerceError;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class ConfirmVerificationCode extends AsyncTask<String, Integer, Integer> {

    public final LomiAsyncTaskCallback lomiAsyncTaskCallback;

    private WooCommerceError error;

    private LomiApplication application;

    public Confirmation confirmation = null;

    public ConfirmVerificationCode(Context context, LomiAsyncTaskCallback lomiAsyncTaskCallback) {

        this.lomiAsyncTaskCallback = lomiAsyncTaskCallback;

        application = (LomiApplication) context.getApplicationContext();
    }

    @Override
    protected void onPreExecute() {

        super.onPreExecute();

        lomiAsyncTaskCallback.onAsyncTaskStart();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {

        super.onProgressUpdate(values);

        lomiAsyncTaskCallback.onAsyncTaskProgressUpdate(values);
    }

    @Override
    protected Integer doInBackground(String... params) {

        Integer result = 0;

        String countryCode = params[0];

        String phoneNumber = params[1];

        String code = params[2];

        Call<AuthyConfirmation> call = application.getAuthyClient().confirmPhoneNumber(countryCode, phoneNumber, code);

        try {

            Response response = call.execute();

            if(response.isSuccessful()) {

                confirmation = call.execute().body();

                result = 1;

            } else {

                error = (WooCommerceError) application.getErrorResponse(response.errorBody());
            }

        } catch (IOException e) {

            error = new WooCommerceError(WooCommerceError.ErrorCode.CONNECTION_ERROR);

            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(Integer result) {

        if(result == 1) {

            lomiAsyncTaskCallback.onAsyncTaskEndWithSuccess(confirmation);

        } else {

            lomiAsyncTaskCallback.onAsyncTaskEndWithFailure(error);
        }
    }
}