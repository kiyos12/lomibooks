package com.a360ground.lomibook.asyncTask;

import android.content.Context;
import android.os.AsyncTask;

import com.a360ground.lomibook.LomiApplication;
import com.a360ground.lomibook.client.Verification;
import com.a360ground.lomibook.domain.authy.AuthyVerification;
import com.a360ground.lomibook.domain.cognalys.CognalysVerification;
import com.a360ground.lomibook.domain.woocommerce.WooCommerceError;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

public class SendVerificationCode extends AsyncTask<String, Integer, Integer> {

    public final LomiAsyncTaskCallback lomiAsyncTaskCallback;

    public Verification verification = null;

    Error error;

    LomiApplication application;

    public SendVerificationCode(Context context, LomiAsyncTaskCallback lomiAsyncTaskCallback) {

        this.lomiAsyncTaskCallback = lomiAsyncTaskCallback;

        application = (LomiApplication) context.getApplicationContext();
    }

    @Override
    protected void onPreExecute() {

        super.onPreExecute();

        lomiAsyncTaskCallback.onAsyncTaskStart();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {

        super.onProgressUpdate(values);

        lomiAsyncTaskCallback.onAsyncTaskProgressUpdate(values);
    }

    @Override
    protected Integer doInBackground(String... params) {

        Integer result = 0;

        String phoneNumber = params[0];

        String country_code = params[1];

        String via = "sms";

        String locale = "en";

        /*Call<CognalysVerification> call = application.getCognalysClient().sendVerificationCode(phoneNumber);*/
        Call<AuthyVerification> call = application.getAuthyClient().sendVerificationCode(via, phoneNumber, country_code, locale);

        // Check if there is internet connection before trying to connect
        if(application.isNetworkAvailable()) {

            try {

                Response<AuthyVerification> response = call.execute();

                if(response.isSuccessful()) {

                    verification = response.body();

                } else {

                    error = application.getErrorResponse(response.errorBody());
                }
            } catch (IOException e) {

                error = new WooCommerceError(WooCommerceError.ErrorCode.CONNECTION_ERROR);

                e.printStackTrace();

            }
        }

        return result;
    }

    @Override
    protected void onPostExecute(Integer result) {

        if(result == 1) {

            // todo remove this
//            verification = new CognalysVerification("success", "e5260991f02d4ace98e67e4", "+251914153366", "+177421");

            lomiAsyncTaskCallback.onAsyncTaskEndWithSuccess(verification);

        } else {

            lomiAsyncTaskCallback.onAsyncTaskEndWithFailure(error);
        }
    }
}
