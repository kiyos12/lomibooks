package com.a360ground.lomibook.asyncTask;

import android.content.Context;
import android.os.AsyncTask;

import com.a360ground.lomibook.LomiApplication;
import com.a360ground.lomibook.domain.woocommerce.WooCommerceError;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.client.CategoryClient;
import com.a360ground.lomibook.domain.woocommerce.Category;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * GetCategoriesAsyncHttpTask
 * This class was created to avoid having to define the AsyncTask
 * as an inner class inside the main activity.
 */
public class GetCategoriesAsyncHttpTask extends AsyncTask<Void, Void, Integer> {

    private static final String TAG = GetCategoriesAsyncHttpTask.class.getSimpleName();

    LomiAsyncTaskCallback lomiAsyncTaskStartCallback;

    List<Category> categories;

    WooCommerceError error;

    LomiApplication application;

    public GetCategoriesAsyncHttpTask(Context context, LomiAsyncTaskCallback callback) {

        lomiAsyncTaskStartCallback = callback;

        application = (LomiApplication) context.getApplicationContext();
    }

    @Override
    protected void onPreExecute() {
        lomiAsyncTaskStartCallback.onAsyncTaskStart();
    }

    @Override
    protected Integer doInBackground(Void... params) {

        Integer result = 0;

        Call<List<Category>> call = application.getCategoryClient().categories();

        try {

            Response<List<Category>> response = call.execute();

            if(response.isSuccessful()) {

                categories = response.body();

                result = 1;

            } else {

                error = (WooCommerceError) application.getErrorResponse(response.errorBody());
            }

        } catch (IOException e) {

            error = new WooCommerceError(WooCommerceError.ErrorCode.CONNECTION_ERROR);

            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(Integer result) {

        if(result == 1) {

            lomiAsyncTaskStartCallback.onAsyncTaskEndWithSuccess(categories);

        } else {

            lomiAsyncTaskStartCallback.onAsyncTaskEndWithFailure(error);
        }
    }
}
