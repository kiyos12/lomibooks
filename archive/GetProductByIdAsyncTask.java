package com.a360ground.lomibook.asyncTask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.a360ground.lomibook.LomiApplication;
import com.a360ground.lomibook.domain.woocommerce.WooCommerceError;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.client.ProductClient;
import com.a360ground.lomibook.domain.woocommerce.Product;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

public class GetProductByIdAsyncTask extends AsyncTask<Integer, Void, Integer> {

    private static final String TAG = GetProductByIdAsyncTask.class.getSimpleName();

    private final LomiAsyncTaskCallback callback;

    private Product product;

    WooCommerceError error;

    LomiApplication application;

    public GetProductByIdAsyncTask(Context context, LomiAsyncTaskCallback lomiAsyncTaskCallback) {

        this.callback = lomiAsyncTaskCallback;

        application = (LomiApplication) context.getApplicationContext();
    }

    @Override
    protected void onPreExecute() {
        callback.onAsyncTaskStart();
    }

    @Override
    protected Integer doInBackground(Integer... params) {

        Integer result = 0;

        Call<Product> call = application.getProductClient().productById(params[0]);

        try {

            Response<Product> response =call.execute();

            if(response.isSuccessful()) {

                product = response.body();

                result = 1;

            } else {

                error = (WooCommerceError) application.getErrorResponse(response.errorBody());
            }
        } catch (IOException e) {

            error = new WooCommerceError(WooCommerceError.ErrorCode.CONNECTION_ERROR);

            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(Integer result) {

        if(result == 1) {

            callback.onAsyncTaskEndWithSuccess(product);

        } else {

            callback.onAsyncTaskEndWithFailure(error);
        }
    }
}
