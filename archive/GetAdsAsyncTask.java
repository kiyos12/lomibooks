package com.a360ground.lomibook.asyncTask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.a360ground.lomibook.LomiApplication;
import com.a360ground.lomibook.client.PostClient;
import com.a360ground.lomibook.domain.woocommerce.Post;
import com.a360ground.lomibook.domain.woocommerce.WooCommerceError;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class GetAdsAsyncTask extends AsyncTask<Void, Integer, Integer> {

    private final LomiAsyncTaskCallback lomiAsyncTaskCallback;

    private final Context mContext;

    List<Post> ads;

    WooCommerceError error;

    LomiApplication application;

    public GetAdsAsyncTask(Context context, LomiAsyncTaskCallback lomiAsyncTaskCallback) {

        this.lomiAsyncTaskCallback = lomiAsyncTaskCallback;

        this.mContext = context;

        application = (LomiApplication) context.getApplicationContext();
    }

    @Override
    protected void onPreExecute() {

        super.onPreExecute();

        lomiAsyncTaskCallback.onAsyncTaskStart();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {

        super.onProgressUpdate(values);

        lomiAsyncTaskCallback.onAsyncTaskProgressUpdate(values);
    }

    @Override
    protected Integer doInBackground(Void... params) {

        Integer result = 0;

        Call<List<Post>> call = application.getPostClient().getAds();

        try {

            Response<List<Post>> response = call.execute();

            if(response.isSuccessful()) {

                ads = response.body();

                result = 1;

            } else {

                error = (WooCommerceError) application.getErrorResponse(response.errorBody());
            }

        } catch (IOException e) {

            error = new WooCommerceError(WooCommerceError.ErrorCode.CONNECTION_ERROR);

            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(Integer result) {

        if(result == 1) {

            lomiAsyncTaskCallback.onAsyncTaskEndWithSuccess(ads);

        } else {

            lomiAsyncTaskCallback.onAsyncTaskEndWithFailure(error);
        }
    }
}
