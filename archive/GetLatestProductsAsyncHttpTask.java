package com.a360ground.lomibook.asyncTask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.a360ground.lomibook.LomiApplication;
import com.a360ground.lomibook.client.ProductClient;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.domain.woocommerce.WooCommerceError;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class GetLatestProductsAsyncHttpTask extends AsyncTask<Void, Void, Integer> {

    private List<Product> products;

    private Integer maximumProducts;

    private LomiAsyncTaskCallback lomiAsyncTaskCallback;

    private LomiApplication application;

    private WooCommerceError error;

    public GetLatestProductsAsyncHttpTask(Context context, LomiAsyncTaskCallback callback, Integer i) {

        lomiAsyncTaskCallback = callback;

        maximumProducts = i;

        application = (LomiApplication) context.getApplicationContext();
    }

    @Override
    protected void onPreExecute() {
        lomiAsyncTaskCallback.onAsyncTaskStart();
    }

    @Override
    protected Integer doInBackground(Void... params) {

        Integer result = 0;

        Call<List<Product>> call = application.getProductClient().latestProducts(maximumProducts);

        try {

            Response<List<Product>> response = call.execute();

            if(response.isSuccessful()) {

                products = response.body();

                result = 1;
            } else {

                error = (WooCommerceError) application.getErrorResponse(response.errorBody());
            }
        } catch (IOException e) {

            error = new WooCommerceError(WooCommerceError.ErrorCode.CONNECTION_ERROR);

            e.printStackTrace();

            this.cancel(false);
        }

        return result;
    }

    @Override
    protected void onPostExecute(Integer result) {

        if(result == 1) {

            lomiAsyncTaskCallback.onAsyncTaskEndWithSuccess(products);
        } else {
            Toast.makeText(application.getBaseContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            lomiAsyncTaskCallback.onAsyncTaskEndWithFailure(error);
        }
    }

    // This callback was overriden and task was canceled
    // on exception above to prevent repetition
    // (The AsyncTask retries again and again)
    @Override
    protected void onCancelled() {
        onPostExecute(0);
    }
}