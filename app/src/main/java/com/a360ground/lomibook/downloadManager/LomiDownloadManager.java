package com.a360ground.lomibook.downloadManager;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.client.OrderClient;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.LineItem;
import com.a360ground.lomibook.domain.woocommerce.Order;
import com.a360ground.lomibook.domain.woocommerce.OrderDownload;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.domain.woocommerce.WooCommerceError;
import com.a360ground.lomibook.service.DownloadTrackerIntentService;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.utils.LomiFileUtils;
import com.a360ground.lomibook.utils.PreferenceHelper;
import com.koolearn.klibrary.core.util.Native;

import java.io.File;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class LomiDownloadManager {

    public static final String LOMIDOWNLOAD_INTENT_FILTER = "Intent.Filter.LomiDownload";

    public static final String DOWNLOAD_STATUS_EXTRA_IDENTIFIER = "status";

    public static final String DOWNLOAD_PRODUCT_ID_EXTRA_IDENTIFIER = "downloading-product-id";

    public static final String DOWNLOAD_IDS_EXTRA_IDENTIFIER = "downloading-id";

    public static final String DOWNLOAD_MESSAGE_EXTRA_IDENTIFIER = "download-message";

    public static final String COMBINE_DOWNLOAD_IDS_EXTRA_IDENTIFIER = "combine";

    public static final String DOWNLOAD_PROGRESS_EXTRA_IDENTIFIER = "bytes-downloaded";

    public static final String FILE_URL_EXTRA_IDENTIFIER = "file-url";

    public static final int DOWNLOAD_STARTED = 1;

    public static final int PREVIEW_DOWNLOAD_STARTED = 2;

    public static final int DOWNLOAD_SUCCESS = 3;

    public static final int PREVIEW_DOWNLOAD_SUCCESS = 4;

    public static final int DOWNLOAD_PROGRESS = 5;

    public static final int PREVIEW_DOWNLOAD_PROGRESS = 6;

    public static final int DOWNLOAD_FAILURE = 7;

    public static final int PREVIEW_DOWNLOAD_FAILURE = 8;

    public static final int FAILED_BEFORE_DOWNLOAD_START = 9;

    public static final int DOWNLOAD_PAUSED = 10;

    public static final int PREVIEW_DOWNLOAD_PAUSED = 11;

    private Context context;

    private Product product;

    private boolean inEthiopia;

    private LocalBroadcastManager localBroadcastManager;

    private DownloadManager downloadManager;

    private Realm realm;

    public LomiDownloadManager(Context context, final Product product, Realm realm) {

        this.context = context;

        this.product = product;

        this.realm = realm;

        downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);

        localBroadcastManager = LocalBroadcastManager.getInstance(context);

        inEthiopia = PreferenceHelper.inEthiopia(context);
    }

    public void downloadPreview() throws LomiDownloadError {

        if (product == null) {

            throw new LomiDownloadError("Invalid book");
        }

        if (product.getSampleDownloadUrl() == null) {

            throw new LomiDownloadError("The book does not have associated preview file");
        }

        downloadOperation(true);

        notifyDownloadStarted(PREVIEW_DOWNLOAD_STARTED);
    }

    public DownloadStatus download() throws LomiDownloadError {

        final Customer customer = realm.where(Customer.class).findFirst();

        if (customer == null) {

            throw new RuntimeException("You need to login to be able to download");
        }

        if (product == null) {

            throw new LomiDownloadError("Invalid book");
        }

        if (product.getDownloadUrl() == null) {

            throw new LomiDownloadError("The book does not have associated file");
        }

        realm.executeTransaction(new Realm.Transaction() {

            @Override
            public void execute(Realm realm) {

                if (!customer.getBooks().contains(product))
                    customer.addBook(product);
            }

        });

        final Order order = createOrder();

        sendOrder(order, new PostOrderTask() {

            @Override
            public void run(String downloadingUrl) {

                notifyDownloadStarted(DOWNLOAD_STARTED);

                downloadOperation(false);
            }

            @Override
            public void orderFailed() {

                Log.d(TAG, "download: order failed");

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        product.setOrderSuccesful(false);
                    }
                });

                notifyDownloadStarted(FAILED_BEFORE_DOWNLOAD_START);
            }
        });

        return DownloadStatus.SUCCESSFUL;
    }

    private void downloadOperation(boolean preview) {

        String url;

        url = preview ? product.getSampleDownloadUrl() : product.getDownloadingUrl();

        Uri URI = Uri.parse(url);

        DownloadManager.Request request = new DownloadManager.Request(URI);

        request.addRequestHeader("Header1", Native.getHeaders()[0].toString());

        request.addRequestHeader("Header1", Native.getHeaders()[1].toString());

        request.addRequestHeader("Header1", Native.getHeaders()[2].toString());

        request.setAllowedOverMetered(true);

        request.setTitle(String.format(context.getString(R.string.downloading_book), product.getName()));

        request.setVisibleInDownloadsUi(false);

        if (preview) {

            request.setDescription("preview");
        }

        LomiFileUtils.createLomiFolder();

        Uri filePath = Uri.parse("file://"

                + LomiFileUtils.LOMI_FOLDER_PATH

                + File.separator + product.getId()

                + (preview ? Constants.SAMPLE_FILE_EXTENSION : Constants.LOMI_FILE_EXTENSION));

        if (new File(filePath.getPath()).exists()) new File(filePath.getPath()).delete();

        request.setDestinationUri(filePath);

        final long downloadId = downloadManager.enqueue(request);

        Realm realm = null;

        final long productId = product.getId();

        try {

            realm = Realm.getDefaultInstance();

            realm.beginTransaction();

            Product product = realm.where(Product.class).equalTo("id", productId).findFirst();

            if (preview) {

                product.setPreviewDownloadId(downloadId);

                product.setPreviewBytesDownloaded(0);

            } else {

                product.setDownloadId(downloadId);

                product.setBytesDownloaded(0);
            }

            realm.commitTransaction();

        } finally {
            if (realm != null) {
                realm.close();
            }
        }

        Intent downloadTrackerIntent = new Intent(context, DownloadTrackerIntentService.class);

        ArrayList<Long> downloadIds = new ArrayList<>();

        downloadIds.add(downloadId);

        downloadTrackerIntent.putExtra(DOWNLOAD_IDS_EXTRA_IDENTIFIER, downloadIds);

        downloadTrackerIntent.putExtra(COMBINE_DOWNLOAD_IDS_EXTRA_IDENTIFIER, true);

        context.startService(downloadTrackerIntent);
    }

    private Order createOrder() {

        Customer customer = realm.where(Customer.class).findFirst();

        if (customer == null) {

            throw new RuntimeException("Customer has to be logged in to be able to create and send orders");
        }

        final Order order = new Order();

        RealmList<LineItem> lineItems = new RealmList<>();

        LineItem item = new LineItem();

        item.setProductId(inEthiopia ? product.getEtbVariationId() : product.getUsdVariationId());

        item.setQuantity(1);

        lineItems.add(item);

        order.setLineItems(lineItems);

        order.setSetPaid(true);

        order.setCoinsSpent(customer.getCoinsSpent());

        order.setCurrency(inEthiopia ? Constants.ETB : Constants.USD);

        order.setCustomerId(customer.getId());

        order.setPaymentMethod(inEthiopia ? Constants.ETC_PAYMENT_METHOD : Constants.PAYPAL_PAYMENT_METHOD);

        order.setPaymentMethodTitle(inEthiopia ? Order.PaymentMethod.ETHIO_TELECOM.name() : Order.PaymentMethod.PAYPAL.name());

        order.setTotalPrice(product.getPrice(context));

        return order;
    }

    public void sendOrder(final Order order, final PostOrderTask task) {

        final WooCommerceServiceGenerator generator = new WooCommerceServiceGenerator(context);

        OrderClient client = generator.createService(OrderClient.class);

        Call<OrderDownload> createOrderCall = client.createOrder(Native.getOrder() + "new/", order);

        createOrderCall.enqueue(new Callback<OrderDownload>() {

            Realm asyncRealm = null;

            @Override
            public void onResponse(Call<OrderDownload> call, Response<OrderDownload> response) {

                if (response.isSuccessful()) {

                    try {

                        asyncRealm = Realm.getDefaultInstance();

                        if (response.isSuccessful()) {

                            final OrderDownload orderDownload = response.body();

                            asyncRealm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {

                                    if(product.isValid()){
                                        product.setDownloadingUrl(orderDownload.getDownloadUrl());

                                        product.setOrderSuccesful(true);
                                    }

                                }
                            });

                            task.run(orderDownload.getDownloadUrl());


                        } else {

                            task.orderFailed();

                            WooCommerceError error = new WooCommerceError(WooCommerceError.ErrorCode.DOWNLOAD_LIMIT_EXCEEDED);

                        }

                    } finally {

                        if (asyncRealm != null) {

                            asyncRealm.close();
                        }
                    }

                } else {

                    task.orderFailed();

                }
            }

            @Override
            public void onFailure(Call<OrderDownload> call, Throwable t) {

                task.orderFailed();

                t.printStackTrace();
            }
        });
    }

    private void notifyDownloadStarted(int downloadExtra) {

        Intent intent = new Intent(LOMIDOWNLOAD_INTENT_FILTER);

        intent.putExtra(DOWNLOAD_STATUS_EXTRA_IDENTIFIER, downloadExtra);

        intent.putExtra(DOWNLOAD_PRODUCT_ID_EXTRA_IDENTIFIER, (int) product.getId());

        localBroadcastManager.sendBroadcast(intent);
    }

    private enum DownloadStatus {

        SUCCESSFUL,

        FAILED,

        SENDING_ORDER,

        GETTING_CUSTOM_DOWNLOADS,

        DOWNLOADING
    }

    private interface PostOrderTask {

        void run(String downloadingUrl);

        void orderFailed();
    }

    public class LomiDownloadError extends Throwable {
        public LomiDownloadError(String message) {
            super(message);
        }
    }
}

