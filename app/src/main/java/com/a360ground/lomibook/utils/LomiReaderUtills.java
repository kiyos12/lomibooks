package com.a360ground.lomibook.utils;

import android.content.Context;
import android.widget.Toast;

import com.amse.ys.zip.StreamUtil;
import com.koolearn.android.kooreader.KooReader;
import com.koolearn.android.kooreader.libraryService.BookCollectionShadow;
import com.koolearn.kooreader.book.Book;

import java.io.File;
import java.io.IOException;

/**
 * Created by Kiyos on 8/14/2017.
 */

public class LomiReaderUtills {

    static BookCollectionShadow myCollection = new BookCollectionShadow();

    public static void openBookEvent(final Context context, final String path, final boolean isOnline) {

        boolean service = myCollection.bindToService(context, new Runnable() {
            public void run() {
                try {
                    final Book book = myCollection.getBookByFile(StreamUtil.getDecrypted(context, new File(path), isOnline));
                    if (book != null) {
                        openBook(book, context);
                    } else {
                        Toast.makeText(context, "Sorry there is an error", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private static void openBook(com.koolearn.kooreader.book.Book data, Context context) {
        KooReader.openBookActivity(context, data, null);
    }

    private static void onDestroy() {
        myCollection.unbind();
    }

}
