package com.a360ground.lomibook.utils;

public class Constants {
    public static final String FACEBOOK_PROFILE_EMAIL_IDENTIFIER = "email";

    public static final String FACEBOOK_PROFILE_NAME_IDENTIFIER = "name";

    public static final String GRAPH_FACEBOOK_BASE_URL = "https://graph.facebook.com/";

    public static final String FIELDS_IDENTIFIER = "fields";

    public static final String FACEBOOK_ID_IDENTIFIER = "id";

    public static final String FACEBOOK_PROFILE_GENDER_IDENTIFIER = "gender";

    public static final String FACEBOOK_PROFILE_BIRTHDAY_IDENTIFIER = "birthday";

    public static final String FACEBOOK_PERMISSION_EMAIL_IDENTIFIER = "email";
    public static final String PRODUCT_ID_IDENTIFIER = "product_id";
    public static final String VERSION = "version";
    public static final String LOMI_LOGIN_ACTION = "com.a360ground.lomibooksrecycler.login";
    public static final String ERROR_MESSAGE_IDENTIFIER = "error_message";
    public static final int MY_BOOKS_COLUMNS_COUNT = 3;
    public static final String SEARCH_QUERY = "search_query";
    public static final long TRIAL_END_INTERVAL = 1800000;
    public static final String LOMI_FILE_EXTENSION = ".lomi";
    public static final String PAYPAL_PAYMENT_METHOD = "paypal";
    public static final String SAMPLE_FILE_EXTENSION = ".sample.lomi";
    public static final String DEFAULT_REMOTE_URI = "https://api.lomistore.com";
    public static final String TOP_SELLERS_PERIOD_YEAR_IDENTIFIER = "year";
    public static final String CATEGORY_ID_IDENTIFIER = "category_id";
    public static final String RECOMMENDED_BOOKS_SLUG = "recommended";
    public static final String WOOCOMMERCE_REST_API_PREFIX = "/wp-json/wc/v1/";
    public static final String LOMI_REST_API_PREFIX = "/wp-json/lomi/v1/";
    public static final String ACTION_SHOW_UPDATE_AVAILABLE = "show_update_available";
    public static final String APK_URL = "http://api.lomistore.com/apk/lomi.apk";
    public static final String CATEGORY_NAME_IDENTIFIER = "category_name";
    public static final int LANGUAGE_REQUEST_CODE = 12;
    public static final int CAMERA_REQUEST = 63;
    public static final int GALLERY_REQUEST = 64;
    public static final int CAMERA_PERMISSION_REQUEST = 63;
    public static final int MY_PERMISSION_WRITE_TO_EXTERNAL_STORAGE = 639;
    public static final String ETB = "ETB";
    public static final String COIN = "Coin";
    public static final String USD = "USD";
    public static final String SAMPLE_TEXT = "sample";
    public static final String FACEBOOK_PROFILE_PIC = "facebook_profile_pic";
    public static final String ETC_PAYMENT_METHOD = "etc";
    public static String[] fieldsFromFacebook = new String[]{
            FACEBOOK_ID_IDENTIFIER,
            FACEBOOK_PROFILE_NAME_IDENTIFIER,
            FACEBOOK_PROFILE_EMAIL_IDENTIFIER,
            FACEBOOK_PROFILE_GENDER_IDENTIFIER,
            FACEBOOK_PROFILE_BIRTHDAY_IDENTIFIER
    };
}
