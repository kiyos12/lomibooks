package com.a360ground.lomibook.utils;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.graphics.drawable.DrawableCompat;

import com.a360ground.lomibook.R;

/**
 * Created by Kiyos on 7/25/2017.
 */

public class NotificationUtils {

    public static void sendNotification(Context context, String content, String title){

        Uri soundUri = Uri.parse("android.resource://com.a360ground.lomibook/raw/demonstrative");;

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.gift)
                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.gift))
                        .setContentTitle(title)
                        .setSound(soundUri)
                        .setContentText(content);


        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(1, mBuilder.build());
    }

    public static long count = 0;
}
