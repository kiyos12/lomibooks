package com.a360ground.lomibook.utils;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class LomiFileUtils {

    public static final String LOMI_FOLDER_PATH = Environment.getExternalStorageDirectory() + File.separator + ".lomi";

    public static void deleteRecursive(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory ()) {

            for (File child : fileOrDirectory.listFiles ()) {

                deleteRecursive (child);
            }
        }

        fileOrDirectory.delete ();
    }

    public static void deleteFilesInFolder(File folder) {
        if (folder.isDirectory ()) {
            File[] files = folder.listFiles ();
            if (files != null && files.length > 0) {
                for (int i = 0; i < files.length; i++) {
                    File child = files[i];
                    child.delete ();
                }
            }
        }
    }

    public static void createLomiFolder() {

        File dir = new File (LOMI_FOLDER_PATH);

        if (!dir.exists ()) {

            dir.mkdir ();
        }
    }

    public static String getFileNameFromUrl(String url) {

        String fileName = url.substring (url.lastIndexOf ("/") + 1);

        return fileName;
    }

    public static String getImagePathInCache(Context context, String url) {
        return context.getCacheDir ().getAbsolutePath () + File.separator + getFileNameFromUrl (url);
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read (buffer)) != -1) {
            output.write (buffer, 0, bytesRead);
        }
    }

}
