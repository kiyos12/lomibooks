package com.a360ground.lomibook.utils;

import android.content.Context;
import android.telephony.TelephonyManager;

import com.a360ground.lomibook.R;

/**
 * Created by Kiyos on 3/23/2017.
 */

public class SIMUtils {

    public static String isSimReady(Context context) {
        String state = null;
        TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                state = context.getString(R.string.no_sim);
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                state = context.getString(R.string.sim_locked);
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                state = context.getString(R.string.sim_pin_required);
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                state = context.getString(R.string.sim_puk_required);
                break;
            case TelephonyManager.SIM_STATE_READY:
                state = context.getString(R.string.sim_ready);
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                state = context.getString(R.string.sim_state_unknown);
                break;
        }
        return state;
    }

}
