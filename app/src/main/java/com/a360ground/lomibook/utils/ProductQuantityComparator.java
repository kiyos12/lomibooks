package com.a360ground.lomibook.utils;

import com.a360ground.lomibook.domain.woocommerce.Product;

import java.util.Comparator;

public class ProductQuantityComparator implements Comparator<Product> {

    @Override
    public int compare(Product product1, Product product2) {

        return ((Long) product2.getQuantity()).compareTo(product1.getQuantity());
    }
}
