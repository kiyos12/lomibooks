package com.a360ground.lomibook.utils;

import android.content.res.Resources;

public class DimensionUtils {

    public static int dpToPx(int dp) {

        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxTodp(int widthPixels) {

        return (int) (widthPixels / Resources.getSystem().getDisplayMetrics().density);
    }
}
