package com.a360ground.lomibook.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;

import com.a360ground.lomibook.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PreferenceHelper {

    public static final String SELECTED_LANGUAGE = "Locale.Helper.Selected.Language";
    public static final String TRIAL_COUNTER_END_TIME  = "trial_counter_end_time";
    public static final String HOME_LAST_UPDATED = "Preference.Helper.Home.LastUpdated";
    public static final String ADS_LAST_UPDATED = "Preference.Helper.Ads.LastUpdated";
    public static final String TOP_SELLERS_LAST_UPDATED = "Preference.Helper.TopSellers.LastUpdated";
    public static final String CATEGORY_LAST_UPDATED = "Preference.Helper.Category.LastUpdated";
    public static final String CUSTOMER_COUNTRY = "Preference.Helper.Customer.Country";
    public static final String ETHIOPIA_COUNTRY_CODE = "ET";
    public static final String OTHER_COUNTRY_CODE = "EN";
    private static final String REQUIRES_FORCED_UPDATE_PREFERENCE_IDENTIFIER = "requires_forced_update";
    private static List<OnLocaleChangedListener> listeners = new ArrayList<> ();
    public static long updateInterval = 24 * 60 * 60 * 1000;

    public static void onCreate(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences (context);

        String lang = preferences.getString(SELECTED_LANGUAGE, (new LomiLanguage(context, "am")).getShortCode());

        setLocale(context, lang);
    }

    public static void profilePicFromFB(Context context, String path) {

        persist(context, Constants.FACEBOOK_PROFILE_PIC, path);
    }

    public static void saveVersion(Context context, String version) {

        persist(context, Constants.VERSION, version);
    }

    public static String getVersion(Context context, String version) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getString(Constants.VERSION, null);
    }

    public static String getLanguage(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getString(SELECTED_LANGUAGE, (new LomiLanguage(context, "am")).getShortCode());
    }

    public static void setLocale(Context context, String language) {

        persist (context, SELECTED_LANGUAGE, language);

        updateResources(context, language);

        for (OnLocaleChangedListener listener : listeners) {

            listener.localeChanged(language);
        }
    }

    public static void setTrialEndTime(Context context, long count){
        persist (context, TRIAL_COUNTER_END_TIME, count);
    }

    public static String getProfilePicFromFB(Context context){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getString(Constants.FACEBOOK_PROFILE_PIC, null);
    }

    public static long getTrialEndTime(Context context){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getLong(TRIAL_COUNTER_END_TIME, 0);
    }

    public static long getLastUpdated(Context context, String identifier) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getLong(identifier, -1);
    }

    public static void setLastUpdated(Context context, String identifier, long lastUpdated) {

        persist(context, identifier, lastUpdated);
    }

    public static long getCategoryLastUpdated(Context context, String categoryName) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getLong(CATEGORY_LAST_UPDATED + categoryName, -1);
    }

    public static void setCountryCode(Context context, String country) {
        persist(context, CUSTOMER_COUNTRY, country);
    }

    public static String getCountryCode(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getString(CUSTOMER_COUNTRY, null);
    }

    public static boolean inEthiopia(Context context) {

        String countryCode = getCountryCode(context);

        return countryCode != null && countryCode.equals(ETHIOPIA_COUNTRY_CODE);
    }

    public static void addListener(OnLocaleChangedListener listener) {

        listeners.add(listener);
    }

    private static void persist(Context context, String preferenceName, long value) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = preferences.edit();

        editor.putLong(preferenceName, value);

        editor.apply();
    }

    public static void clearPreferences(Context context) {

        persist(context, SELECTED_LANGUAGE, null);

        persist(context, HOME_LAST_UPDATED, -1);

        persist(context, Constants.FACEBOOK_PROFILE_PIC, null);

        persist(context, ADS_LAST_UPDATED, -1);

        persist(context, TOP_SELLERS_LAST_UPDATED, -1);

        persist(context, CATEGORY_LAST_UPDATED, -1);
    }

    private static void persist(Context context, String preferenceName, String value) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(preferenceName, value);

        editor.apply();
    }

    private static void updateResources(Context context, String language) {

        Locale locale = new Locale(language);

        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();

        configuration.locale = locale;

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }

    public static void setRequiresForcedUpdate(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = preferences.edit();

        editor.putBoolean(REQUIRES_FORCED_UPDATE_PREFERENCE_IDENTIFIER, true);

        editor.apply();
    }

    public static class LomiLanguage {

        private String language;

        private String shortCode;

        public LomiLanguage(Context context, String shortCode) {

            this.shortCode = shortCode;

            switch(shortCode) {

                case "am":

                    Configuration conf = context.getResources().getConfiguration();

                    conf.setLocale(new Locale("am"));

                    DisplayMetrics metrics = new DisplayMetrics();

                    ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metrics);

                    Resources resources = new Resources(context.getAssets(), metrics, conf);

                    language = resources.getString(R.string.language_amharic);

                    break;

                case "en":

                    language = "English";
            }
        }

        public String getLanguage() {
            return language;
        }

        public String getShortCode() {
            return shortCode;
        }
    }

    public interface OnLocaleChangedListener {

        void localeChanged(String language);
    }
}