package com.a360ground.lomibook.utils;

import android.content.Context;
import android.util.Log;

import com.a360ground.lomibook.client.SearchesClient;
import com.a360ground.lomibook.domain.algolia.SearchSuggestions;
import com.a360ground.lomibook.domain.algolia.TopSearches;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.koolearn.klibrary.core.util.Native;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kiyos on 9/29/2017.
 */

public class SearchHelper {

    public static void getSearchSuggestions(Context context, String q, final ISearchSuggestions searchSuggestions){

        WooCommerceServiceGenerator generator = new WooCommerceServiceGenerator(context);

        SearchesClient client = generator.createService(SearchesClient.class);

        Call<List<SearchSuggestions>> searchSuggestionsCall = client.searchSuggestions(new String(Native.getSearchProducts(q))+"&typing=true");

        searchSuggestionsCall.enqueue(new Callback<List<SearchSuggestions>>() {
            @Override
            public void onResponse(Call<List<SearchSuggestions>> call, Response<List<SearchSuggestions>> response) {
                searchSuggestions.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<List<SearchSuggestions>> call, Throwable throwable) {
            }
        });
    }

    public static void  getTopSearchesCall(Context context, final ITopSearches topSearches) {

        WooCommerceServiceGenerator generator = new WooCommerceServiceGenerator(context);

        SearchesClient client = generator.createService(SearchesClient.class);

        Call<List<TopSearches>> topSearchesCall = client.topSearches(Native.getTopSearchs());

        topSearchesCall.enqueue(new Callback<List<TopSearches>>() {
            @Override
            public void onResponse(Call<List<TopSearches>> call, Response<List<TopSearches>> response) {
                topSearches.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<List<TopSearches>> call, Throwable throwable) {

            }
        });
    }

    public interface ITopSearches{
        void onResponse(List<TopSearches> topSearches);
    }

    public interface ISearchSuggestions{
        void onResponse (List<SearchSuggestions> searchSuggestions);
    }
}
