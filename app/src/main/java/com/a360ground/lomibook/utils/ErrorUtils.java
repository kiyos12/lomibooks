package com.a360ground.lomibook.utils;


import com.a360ground.lomibook.domain.woocommerce.CustomerError;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by kiyos on 9/30/2017.
 */

public class ErrorUtils {

    public static CustomerError parseError(Response<?> response, Retrofit retrofit) {
        Converter<ResponseBody, CustomerError> converter =
                retrofit
                        .responseBodyConverter(CustomerError.class, new Annotation[0]);

        CustomerError customerError;

        try {
            customerError = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new CustomerError();
        }

        return customerError;
    }
}