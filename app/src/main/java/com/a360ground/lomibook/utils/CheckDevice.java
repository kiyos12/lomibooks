package com.a360ground.lomibook.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.activity.ChooseCountryActivity;
import com.a360ground.lomibook.client.CustomerClient;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.koolearn.klibrary.core.util.Native;

import java.io.File;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kiyos on 10/1/2017.
 */

public class CheckDevice {

    public static void checkAndClear(final Context context) {
        WooCommerceServiceGenerator wooCommerceServiceGenerator = new WooCommerceServiceGenerator(context);
        CustomerClient customerClient = wooCommerceServiceGenerator.createService(CustomerClient.class);
        Call<String> deviceCall = customerClient.checkDevice(new String(Native.checkDevice(String.valueOf(getCustomer().getId()))));
        deviceCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (!getDeviceId(context).equals(response.body())) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setCustomTitle(LayoutInflater.from(context).inflate(R.layout.error_view, null));
                    builder.setCancelable(false);
                    builder.setMessage("በዚህ ስልክ ላይ ይጠቀሙበት የነበረው የሎሚ አካውንት ወደ ሌላ ተዘዋውሯል።እንደገና ለመግባት ይፈልጋሉ");
                    builder.setIcon(R.drawable.ic_error_black_24dp);
                    builder.setPositiveButton("አዎ", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            CheckDevice.clearRealm();
                            CheckDevice.clearApplicationData(context);
                            context.startActivity(new Intent(context, ChooseCountryActivity.class));
                        }
                    });
                    builder.setNegativeButton("አይ", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            CheckDevice.clearRealm();
                            CheckDevice.clearApplicationData(context);
                            ((Activity) context).finishAffinity();
                        }
                    });
                    AlertDialog a = builder.create();
                    a.show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable throwable) {

            }
        });
    }

    public static Customer getCustomer() {
        Realm realm = getRealm();
        return realm.where(Customer.class).findFirst();

    }

    public static Realm getRealm() {
        Realm realm = null;
        try {
            return Realm.getDefaultInstance();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void clearRealm() {
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }


    public static String getDeviceId(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        return telephonyManager.getDeviceId();
    }

    public static void clearApplicationData(Context context) {
        File cache = context.getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                if (!s.equals("lib")) {
                    deleteDir(new File(appDir, s));
                    deleteDir(new File(LomiFileUtils.LOMI_FOLDER_PATH));
                }
            }
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

}
