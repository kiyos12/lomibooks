package com.a360ground.lomibook.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PhotoHelper {

    private static final String SELECTED_PHOTO = "PhotoHelper.Selected.Photo";

    public static String getPersistedData(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getString(SELECTED_PHOTO, null);
    }

    public static void setProfilePicture(Context context, String pathToProfilePicture) {

        persist(context, pathToProfilePicture);
    }

    private static void persist(Context context, String pathToPhoto) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(SELECTED_PHOTO, pathToPhoto);

        editor.apply();
    }
}
