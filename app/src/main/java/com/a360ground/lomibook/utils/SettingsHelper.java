package com.a360ground.lomibook.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class SettingsHelper {

    private static final String REMOTE_URI = "Settings.Helper.RemoteURI";

    private static final String LAST_UPDATE_CHECK = "Settings.Helper.LastUpdateCheck";

    private static final String APK_LAST_MODIFIED = "Settings.Helper.ApkLastModified";

    public static void onCreate(Context context) {

        setRemoteUrl(context, Constants.DEFAULT_REMOTE_URI);
    }

    public static String getRemoteURI(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getString(REMOTE_URI, Constants.DEFAULT_REMOTE_URI);
    }

    public static void setRemoteUrl(Context context, String remoteUrl) {

        persist(context, remoteUrl);
    }

    private static void persist(Context context, String remoteURI) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(REMOTE_URI, remoteURI);

        editor.commit();
    }

    public static long getLastUpdateCheck(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getLong(LAST_UPDATE_CHECK, -1);
    }

    /*public static boolean isBackgroundDataRestricted(){
        ConnectivityManager mgr = (ConnectivityManager)Context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo bgData = mgr.getActiveNetworkInfo();
    }
*/
    public static void setLastUpdateCheck(Context context, long lastUpdateCheck) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = preferences.edit();

        editor.putLong(LAST_UPDATE_CHECK, lastUpdateCheck);

        editor.commit();
    }

    public static long getApkLastModified(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getLong(APK_LAST_MODIFIED, 0);
    }

    public static void setApkLastModified(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = preferences.edit();

        editor.putLong(APK_LAST_MODIFIED, System.currentTimeMillis());

        editor.commit();
    }

    public static boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if(status != ConnectionResult.SUCCESS) {
            if(googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
            }
            return false;
        }
        return true;
    }
}
