package com.a360ground.lomibook.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.client.CouponClient;
import com.a360ground.lomibook.domain.woocommerce.Coupon;
import com.a360ground.lomibook.domain.woocommerce.Coupon;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.koolearn.klibrary.core.util.Native;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by Kiyos on 6/15/2017.
 */

public class ScratchCardTracker {

    public void startTracker(String code, Context context,long cid,  final OnCouponReceived onCouponReceived){

        final ProgressDialog progressDialog = new ProgressDialog(context);

        progressDialog.setMessage(context.getString(R.string.wait));

        progressDialog.show();

        WooCommerceServiceGenerator wooCommerceServiceGenerator = new WooCommerceServiceGenerator(context);

        CouponClient couponClient = wooCommerceServiceGenerator.createService(CouponClient.class);

        Log.d(TAG, String.valueOf(cid));

        Call<Long> couponCall = couponClient.getCouponByCode(new String(Native.getCouponByCode(code))+cid);

        couponCall.enqueue(new Callback<Long>() {
            @Override
            public void onResponse(Call<Long> call, Response<Long> response) {
                onCouponReceived.onCouponSuccessful(response.body(), response.isSuccessful());
                if(progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<Long> call, Throwable t) {
                onCouponReceived.onCouponFailed(t);
                if(progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
            }
        });
    }

    public interface OnCouponReceived{
        void onCouponSuccessful(Long Coupon, boolean isSuccessful);
        void onCouponFailed(Throwable t);
    }

}
