package com.a360ground.lomibook.utils;

import android.content.Context;
import android.util.Log;
import com.a360ground.lomibook.domain.LomiModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LomiNetworkOperation<R, M extends LomiModel> {

    private Context context;

    private Call<R> call;

    public LomiNetworkOperation(Context context, Call<R> call) {

        this.call = call;

        this.context = context;
    }

    public void send(final LaunchesNetworkOperation networkOperation) {

        networkOperation.onStart();

        call.enqueue(new Callback<R>() {

            Realm realm = null;

            @Override
            public void onResponse(Call<R> call, final Response<R> response) {

                if(response.isSuccessful()) {

                    try {

                        realm = Realm.getDefaultInstance();

                        if (response.body() instanceof List) {

                            List<M> realmList = (List<M>) response.body();

                            final List<Long> idsSaved = new ArrayList<>();

                            for (int i = 0; i < realmList.size(); i++) {

                                final LomiModel realmObject = realmList.get(i);

                                realm.executeTransaction(new Realm.Transaction() {

                                    @Override
                                    public void execute(Realm realm) {

                                        LomiModel modelForSave = realmObject.beforeSave(context);

                                        if(modelForSave != null) {

                                            realm.insertOrUpdate((RealmModel) modelForSave);

                                            idsSaved.add(realmObject.getId());
                                        }
                                    }
                                });
                            }

                            if(idsSaved.size() > 0 || realmList.isEmpty()) {

                                networkOperation.onSuccess(idsSaved);
                            }

                        } else {

                            final M realmObject = (M) response.body();

                            final List<Long> fetchedIds = new ArrayList<>();

                            fetchedIds.add(realmObject.getId());

                            realm.executeTransaction(new Realm.Transaction() {

                                @Override
                                public void execute(Realm realm) {

                                    LomiModel modelForSave = realmObject.beforeSave(context);

                                    if(modelForSave != null) {

                                        realm.insertOrUpdate((RealmModel) modelForSave);
                                    }
                                }
                            });

                            networkOperation.onSuccess(fetchedIds);
                        }

                    } finally {

                        if(realm != null) {

                            realm.close();
                        }
                    }

                } else {

                    try {

                        networkOperation.onFailure(response.errorBody().string());

                    } catch (IOException e) {

                        Log.d(LomiNetworkOperation.class.getSimpleName(), "Lomi IOException: Error getting error message");

                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<R> call, Throwable t) {

                networkOperation.onFailure(t.getMessage());

                t.printStackTrace();
            }
        });
    }

    public interface LaunchesNetworkOperation {

        void onStart();

        void onSuccess(List<Long> fetchedIds);

        void onFailure(String errorMessage);
    }
}
