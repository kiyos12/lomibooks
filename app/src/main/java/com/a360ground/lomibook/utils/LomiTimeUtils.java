package com.a360ground.lomibook.utils;

import android.content.Context;

import com.a360ground.lomibook.R;

public class LomiTimeUtils {

    public static String getTimeIntervalFromNow(Context context, long timeInMillis) {

        long timeDifferenceMilliSeconds = System.currentTimeMillis() - timeInMillis;

        long diffSeconds = timeDifferenceMilliSeconds / 1000;

        long diffMinutes = timeDifferenceMilliSeconds / (60 * 1000);

        long diffHours = timeDifferenceMilliSeconds / (60 * 60 * 1000);

        long diffDays = timeDifferenceMilliSeconds / (24 * 60 * 60 * 1000);

        long diffWeeks = timeDifferenceMilliSeconds / (7 * 24 * 60 * 60 * 1000);

        long diffMonths = (long) (timeDifferenceMilliSeconds / (30.41666666 * 24 * 60 * 60 * 1000));

        long diffYears = timeDifferenceMilliSeconds / (365 * 24 * 60 * 60 * 1000);

        String timeString;

        if(diffSeconds < 1) {

            timeString = context.getString(R.string.one_second_ago);

        } else if(diffMinutes < 1) {

            timeString = String.format(context.getString(R.string.seconds_ago), diffSeconds);

        } else if(diffHours < 1) {

            timeString = String.format(context.getString(R.string.minutes_ago), diffMinutes);

        } else if(diffDays < 1) {

            timeString = String.format(context.getString(R.string.hours_ago), diffHours);

        } else if(diffWeeks < 1) {

            timeString = String.format(context.getString(R.string.days_ago), diffDays);

        } else if(diffMonths < 1) {

            timeString = String.format(context.getString(R.string.weeks_ago), diffWeeks);

        } else if(diffYears < 1) {

            timeString = String.format(context.getString(R.string.months_ago), diffMonths);

        } else {

            timeString = String.format(context.getString(R.string.years_ago), diffYears);
        }

        return String.format(context.getString(R.string.last_updated), timeString);
    }
}
