package com.a360ground.lomibook.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.a360ground.lomibook.R;

/**
 * Created by Kiyos on 7/25/2017.
 */

public class SuccessDialogFragment{


    void showMessage(Context context, double amount)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.success_dialog_layout, null);
        final AlertDialog.Builder builder=new AlertDialog.Builder(context, R.style.DialogCustomTheme3);
        TextView mainText = (TextView) view.findViewById(R.id.success_main_text);
        mainText.setText("በተሳካ ሁኔታ "+amount+" የ ሎሚ ሳንቲሞችን በ "+amount*5+" ብር ገዝተዋል");
        view.findViewById(R.id.success_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        builder.setCancelable(true);
        builder.setView(view);
        builder.show();
    }
}
