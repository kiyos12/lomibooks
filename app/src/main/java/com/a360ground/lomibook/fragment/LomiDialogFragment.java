package com.a360ground.lomibook.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;

public class LomiDialogFragment extends AppCompatDialogFragment {

    private static final java.lang.String NUMBER_OF_DIALOGS_IDENTIFIER = "num";
    public static final String FRAGMENT_TAG = LomiDialogFragment.class.getSimpleName();

    private int mNum;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setCancelable(false);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        ProgressDialog dialog = new ProgressDialog(getActivity(), getTheme());

        dialog.setTitle("Please Wait");

        dialog.setMessage("File being downloaded");

        dialog.setIndeterminate(true);

        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
    }
}
