package com.a360ground.lomibook.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.a360ground.lomibook.application.LomiApplication;
import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.activity.MainActivity;
import com.a360ground.lomibook.R;
import com.a360ground.lomibook.adapter.MyBooksAdapter;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.view.BookshelfView;
import com.a360ground.lomibook.view.ExtendedToast;

import java.util.List;

import io.realm.Realm;

public class MyBooksFragment extends Fragment {

    ProgressBar progressBar;

    BookshelfView recyclerView;

    private Realm realm;

    private Customer customer;

    public MyBooksFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        realm = Realm.getDefaultInstance();

        customer = realm.where(Customer.class).findFirst();

        getActivity().setTitle(getString(R.string.my_books));

        ((MainActivity) getActivity()).setActiveBottomNavigationItem(3);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(customer == null) {

            ExtendedToast.showToast (getActivity (),ExtendedToast.INFO, getString (R.string.login_required));

            LomiApplication lomiApplication = (LomiApplication)getActivity().getApplicationContext();

            lomiApplication.launchSignupWithError();
        }

        View view = inflater.inflate(R.layout.fragment_my_books, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);

        progressBar.setVisibility(View.GONE);

        recyclerView = (BookshelfView) view.findViewById(R.id.myBooksRecycler);

        List<Product> cachedMyBooks = customer.getBooks();

        final MyBooksAdapter myBooksAdapter = new MyBooksAdapter(getActivity(), cachedMyBooks);

        recyclerView.setAdapter(myBooksAdapter);

        recyclerView.setNumColumns(Constants.MY_BOOKS_COLUMNS_COUNT);

        return view;
    }

    @Override
    public void onResume() {
        realm = Realm.getDefaultInstance();
        super.onResume();
    }

    @Override
    public void onPause() {
        realm.close();
        super.onPause();
    }
}