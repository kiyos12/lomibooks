package com.a360ground.lomibook.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.a360ground.lomibook.activity.MainActivity;
import com.a360ground.lomibook.R;
import com.a360ground.lomibook.adapter.SearchResultsRecyclerAdapter;
import com.a360ground.lomibook.domain.woocommerce.Product;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class TopSellerFragment extends Fragment {

    private SearchResultsRecyclerAdapter adapter;

    private RecyclerView recyclerView;

    private RealmResults<Product> products;

    private Realm realm;

    public TopSellerFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        ((MainActivity) getActivity()).setActiveBottomNavigationItem(2);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_top_seller, container, false);

        realm = Realm.getDefaultInstance();

        recyclerView = (RecyclerView) view.findViewById(R.id.top_sellers_recycler);

        products = realm.where(Product.class)

                .greaterThan ("quantity", 0)

                .findAllSorted("quantity", Sort.DESCENDING);

        adapter = new SearchResultsRecyclerAdapter(getActivity(), realm.copyFromRealm(products));

        recyclerView.setAdapter(adapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        adapter.notifyDataSetChanged();

        recyclerView.setNestedScrollingEnabled(true);

        return view;
    }

    @Override
    public void onResume() {

        super.onResume();

        realm = Realm.getDefaultInstance();
    }

    @Override
    public void onPause() {

        realm.close();

        super.onPause();
    }
}
