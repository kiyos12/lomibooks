package com.a360ground.lomibook.fragment;

import android.app.Dialog;
import android.content.Context;

import com.a360ground.lomibook.R;

/**
 * Created by Kiyos on 2/28/2017.
 */

public class CheckoutLoadingFragment {

    private Dialog dialog;

    public CheckoutLoadingFragment(Context context) {

        dialog = new Dialog(context, R.style.DialogCustomTheme2);

        dialog.setContentView(R.layout.loading_checkout);

        dialog.setCancelable(false);
    }

    public void showDialog() {

        dialog.show();
    }

    public void dismissDialog() {

        dialog.dismiss();
    }
}
