package com.a360ground.lomibook.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.a360ground.lomibook.R;

/**
 * Created by Kiyos on 2/28/2017.
 */

public class QuoteLoadingFragment extends DialogFragment{
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder (getActivity ());

        View view = LayoutInflater.from (getActivity ()).inflate (R.layout.layout_quote_loading,null,false);

        alertDialog.setView (view);

        return alertDialog.create ();

    }
}
