package com.a360ground.lomibook.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.application.LomiApplication;
import com.a360ground.lomibook.activity.MainActivity;
import com.a360ground.lomibook.R;
import com.a360ground.lomibook.adapter.SearchResultsRecyclerAdapter;
import com.a360ground.lomibook.domain.woocommerce.Category;
import com.a360ground.lomibook.domain.woocommerce.Product;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectedFragment extends Fragment {

    private LomiApplication application;

    private long categoryId = -1;

    RealmResults<Product> products;

    private SearchResultsRecyclerAdapter searchResultsRecyclerAdapter;

    private RecyclerView recyclerView;

    private Realm realm;

    public SelectedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((MainActivity) getActivity()).setActiveBottomNavigationItem(1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        realm = Realm.getDefaultInstance();

        Category category = realm.where(Category.class).equalTo ("slug", Constants.RECOMMENDED_BOOKS_SLUG).findFirst();

        categoryId = category != null ? category.getId() : -1;

        application = (LomiApplication) getActivity().getApplicationContext();

        View view = inflater.inflate(R.layout.fragment_selected, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.selected_recycler);

        updateUI();

        return view;
    }

    private void updateUI() {

        Realm realm = Realm.getDefaultInstance();

        products = realm.where (Product.class)

                .equalTo ("categories.slug", Constants.RECOMMENDED_BOOKS_SLUG)

                .findAll ();

        searchResultsRecyclerAdapter = new SearchResultsRecyclerAdapter(getActivity(), realm.copyFromRealm(products));

        recyclerView.setAdapter(searchResultsRecyclerAdapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        recyclerView.scrollToPosition(1);
    }

    @Override
    public void onResume() {

        super.onResume();

        realm = Realm.getDefaultInstance();
    }

    @Override
    public void onPause() {

        realm.close();

        super.onPause();
    }
}
