package com.a360ground.lomibook.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.activity.TermsAndConditions;
import com.a360ground.lomibook.application.LomiApplication;
import com.a360ground.lomibook.client.CustomerClient;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.WooCommerceError;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.utils.PreferenceHelper;
import com.a360ground.lomibook.view.ExtendedToast;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.koolearn.klibrary.core.util.Native;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.internal.FacebookDialogFragment.TAG;

public class SignupFragment extends Fragment {

    private static final String FACEBOOK_PROFILE_EMAIL_IDENTIFIER = "email";

    private static final String FACEBOOK_PROFILE_NAME_IDENTIFIER = "name";

    private static final String GRAPH_FACEBOOK_BASE_URL = "https://graph.facebook.com/";

    private static final String FIELDS_IDENTIFIER = "fields";

    private static final String FACEBOOK_ID_IDENTIFIER = "id";

    private static final String FACEBOOK_PROFILE_GENDER_IDENTIFIER = "gender";

    private static final String FACEBOOK_PROFILE_BIRTHDAY_IDENTIFIER = "birthday";

    private static final String FACEBOOK_PERMISSION_EMAIL_IDENTIFIER = "email";

    EditText emailEditText;

    EditText fullNameEditText;

    TextInputLayout emailTextInputLayout;

    LoginButton fbLoginButton;

    TextInputLayout fullNameTextInputLayout;

    NestedScrollView nestedScrollView;

    RelativeLayout signUpWrapper;

    CheckBox termsAndConditionsCheckBox;

    CallbackManager callbackManager;

    Map<TextInputLayout, String> errorMap;

    private LomiApplication application;

    private String[] fieldsFromFacebook = new String[]{
            FACEBOOK_ID_IDENTIFIER,
            FACEBOOK_PROFILE_NAME_IDENTIFIER,
            FACEBOOK_PROFILE_EMAIL_IDENTIFIER,
            FACEBOOK_PROFILE_GENDER_IDENTIFIER,
            FACEBOOK_PROFILE_BIRTHDAY_IDENTIFIER
    };

    public SignupFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_signup, container, false);

        application = (LomiApplication) getActivity().getApplicationContext();

        emailEditText = (EditText) view.findViewById(R.id.registration_email_edit_text);

        nestedScrollView = (NestedScrollView) view.findViewById(R.id.scrollViewSignUp);

        signUpWrapper = (RelativeLayout) view.findViewById(R.id.signup_wrapper_relativelayout);

        final TextView terms_and_conditions_error_msg = (TextView) view.findViewById(R.id.terms_and_conditions_error_msg);

        fullNameEditText = (EditText) view.findViewById(R.id.registration_fullname_edit_text);

        fullNameTextInputLayout = (TextInputLayout) view.findViewById(R.id.registration_fullname_text_input_layout);

        final Button button = (Button) view.findViewById(R.id.register_button);

        emailTextInputLayout = (TextInputLayout) view.findViewById(R.id.email_text_input_layout);

        emailTextInputLayout.setErrorEnabled(true);

        fbLoginButton = (LoginButton) view.findViewById(R.id.fb_login_btn);

        fbLoginButton.setReadPermissions(FACEBOOK_PERMISSION_EMAIL_IDENTIFIER);

        fbLoginButton.setFragment(this);

        Button custom_fb_btn = (Button) view.findViewById(R.id.custom_fb_button);

        custom_fb_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                fbLoginButton.performClick();
            }
        });

        emailTextInputLayout = (TextInputLayout) view.findViewById(R.id.email_text_input_layout);

        emailTextInputLayout.setErrorEnabled(true);

        emailEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                    inputMethodManager.hideSoftInputFromWindow(emailEditText.getWindowToken(), 0);
                }
            }
        });

        fullNameEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                    inputMethodManager.hideSoftInputFromWindow(fullNameEditText.getWindowToken(), 0);
                }
            }
        });

        termsAndConditionsCheckBox = (CheckBox) view.findViewById(R.id.terms_and_conditions_check_box);

        fbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {

                loginResult.getAccessToken();

                GraphRequest request = GraphRequest.newMeRequest(

                        loginResult.getAccessToken(),

                        new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {

                                try {

                                    final String name = object.getString(FACEBOOK_PROFILE_NAME_IDENTIFIER);

                                    final String email = object.getString(FACEBOOK_PROFILE_EMAIL_IDENTIFIER);

                                    final Profile profile = Profile.getCurrentProfile();

                                    if (profile != null) {

                                        String profilePicturePath = GRAPH_FACEBOOK_BASE_URL + profile.getId() + "/picture?type=large";

                                        Log.d(TAG, "onCompleted: " + profilePicturePath);

                                        PreferenceHelper.profilePicFromFB(getActivity(), profilePicturePath);

                                        new AlertDialog.Builder(getContext())

                                                .setTitle(getString(R.string.country))

                                                .setMessage(getString(R.string.country_question))

                                                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                    }
                                                })
                                                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                    }
                                                }).show();

                                    } else {

                                        Toast.makeText(getContext(), getString(R.string.facebook_profile_error), Toast.LENGTH_SHORT).show();
                                    }

                                } catch (JSONException e) {

                                    e.printStackTrace();
                                }
                            }
                        });

                Bundle parameters = new Bundle();

                parameters.putString(FIELDS_IDENTIFIER, TextUtils.join(",", fieldsFromFacebook));

                request.setParameters(parameters);

                request.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });


        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                errorMap = new HashMap<>();

                clearErrors();

                if (!application.isNetworkAvailable()) {

                    Toast.makeText(getActivity(), getString(R.string.connect_to_internet), Toast.LENGTH_SHORT).show();

                    return;
                }

                final String fullName = fullNameEditText.getText().toString();

                final String email = emailEditText.getText().toString();

                if (fullName.isEmpty()) {

                    errorMap.put(fullNameTextInputLayout, getString(R.string.full_name_empty));
                }

                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    errorMap.put(emailTextInputLayout, getString(R.string.email_invalid));
                }
                if (email.isEmpty()) {
                    errorMap.put(emailTextInputLayout, getString(R.string.email_empty));
                }
                if (!areFieldsValid()) {

                    showError();

                    return;
                }
                if (!termsAndConditionsCheckBox.isChecked()) {

                    terms_and_conditions_error_msg.setVisibility(View.VISIBLE);

                    return;

                } else {

                    terms_and_conditions_error_msg.setVisibility(View.INVISIBLE);
                }
            }
        });

        TextView termsAndConditionsLink = (TextView) view.findViewById(R.id.terms_and_conditions);

        termsAndConditionsLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(), TermsAndConditions.class));
            }
        });

        return view;
    }

    private void clearError(TextInputLayout textInputLayout) {

        textInputLayout.setError(null);
    }

    private void clearErrors() {

        fullNameTextInputLayout.setError(null);
    }

    private void showError() {

        Iterator it = errorMap.entrySet().iterator();

        while (it.hasNext()) {

            Map.Entry<TextInputLayout, String> pair = (Map.Entry) it.next();

            pair.getKey().setError(pair.getValue());
        }
    }

    private boolean areFieldsValid() {

        return errorMap.isEmpty();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
