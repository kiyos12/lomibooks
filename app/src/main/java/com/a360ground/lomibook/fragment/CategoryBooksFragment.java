package com.a360ground.lomibook.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.adapter.SearchResultsRecyclerAdapter;
import com.a360ground.lomibook.client.ProductClient;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.view.ExtendedToast;
import com.a360ground.lomibook.view.WrapContentLinearLayoutManager;
import com.koolearn.klibrary.core.util.Native;

import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryBooksFragment extends Fragment {

    private static String TAG = SearchResultsRecyclerAdapter.class.getSimpleName();
    long categoryId = -1;
    String categoryName;
    int loadCounter = 1;
    RecyclerView recyclerView;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    WrapContentLinearLayoutManager mLayoutManager;
    ProgressBar progressBar;
    private SearchResultsRecyclerAdapter adapter;
    private Realm realm;
    private boolean loading = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        categoryId = getActivity().getIntent().getLongExtra(Constants.CATEGORY_ID_IDENTIFIER, -1);

        categoryName = getActivity().getIntent().getStringExtra(Constants.CATEGORY_NAME_IDENTIFIER);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_category_books, container, false);

        realm = Realm.getDefaultInstance();

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        progressBar = (ProgressBar) view.findViewById(R.id.detail_progress);

        if (categoryId != -1) {
            fetchData(20);

            /*recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) {

                        visibleItemCount = mLayoutManager.getChildCount();
                        totalItemCount = mLayoutManager.getItemCount();
                        pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                        if (loading) {

                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                loading = false;
                                ++loadCounter;
                                fetchData(loadCounter);
                            }
                        }
                    }

                }
            });*/


        }

        return view;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    @Override
    public void onResume() {

        realm = Realm.getDefaultInstance();

        super.onResume();
    }

    private void fetchData(int page) {

        final WooCommerceServiceGenerator generator = new WooCommerceServiceGenerator(getActivity());

        ProductClient productClient = generator.createService(ProductClient.class);

        Call<List<Product>> productsCall = productClient.productsByCategoryWithPagination(Native.getProducts(),
                categoryId, page);

        productsCall.enqueue(new Callback<List<Product>>() {

            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {

                if(progressBar.isShown()){
                    progressBar.setVisibility(View.GONE);
                }

                adapter = new SearchResultsRecyclerAdapter(getActivity(), response.body());

                recyclerView.setAdapter(adapter);

                mLayoutManager = new WrapContentLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

                recyclerView.setLayoutManager(mLayoutManager);
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                if(progressBar.isShown()){
                    progressBar.setVisibility(View.GONE);
                }
                ExtendedToast.showToast(getActivity(), ExtendedToast.FAILED, getString(R.string.sorry_error));
            }
        });
    }

    @Override
    public void onPause() {

        realm.close();

        super.onPause();
    }
}
