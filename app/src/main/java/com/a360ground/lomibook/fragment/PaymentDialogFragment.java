package com.a360ground.lomibook.fragment;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.client.CustomerClient;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.utils.NotificationUtils;
import com.a360ground.lomibook.utils.PreferenceHelper;
import com.a360ground.lomibook.utils.ScratchCardTracker;
import com.a360ground.lomibook.view.ExtendedToast;
import com.a360ground.lomibook.view.LomiBuyButtonView;
import com.a360ground.lomibook.view.ProgressButton;
import com.koolearn.klibrary.core.util.Native;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.nio.charset.Charset;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.login.widget.ProfilePictureView.TAG;

public class PaymentDialogFragment extends DialogFragment {

    public static final String PAYPAL_CLIENT_ID = "AUWVvDRriqhpNUAyatBXKlsDqLvJ0lwPEToXyWvZ5F7IOgs4rhgzLwutmZ5ymNlWenb3zCPjmVQn6v_K";

    public static final int REQUEST_CODE_FOR_PAYPAL = 1222;

    public final int REQUEST_CODE_FORTUMO = 1221;
    private TextView balanceTextView;
    private TextView balanceMessageTextView;
    private ProgressButton progressButton;
    private Context context;
    private LomiBuyButtonView buyerButton;
    private Product product;
    private TextView bgNotice;
    private TextView giftCard;

    public PaymentDialogFragment() {
    }

    public static PaymentDialogFragment newInstance(LomiBuyButtonView button) {

        PaymentDialogFragment fragment = new PaymentDialogFragment();

        fragment.setBuyerButton(button);

        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        this.context = getActivity();

        product = buyerButton.getProduct();

        setCancelable(false);

        boolean inEthiopia = PreferenceHelper.inEthiopia(context);

        String currency = inEthiopia ? context.getResources().getString(R.string.coin_text) : context.getResources().getString(R.string.dollar_text);

        String priceText = String.format(currency, product.getPrice(context));

        final Customer customer = getCustomer();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.pay, null);

        TextView bookPriceTextView = (TextView) v.findViewById(R.id.book_price);

        TextView bookTitleTextView = (TextView) v.findViewById(R.id.pay_book_title);

        balanceMessageTextView = (TextView) v.findViewById(R.id.pay_balance_message);

        bgNotice = (TextView) v.findViewById(R.id.pay_bg_data_notice);

        //final TextView amountPayableTextView = (TextView) v.findViewById(R.id.pay_amount_payable);

        balanceTextView = (TextView) v.findViewById(R.id.pay_balance);

        ImageView bookThumbNail = (ImageView) v.findViewById(R.id.pay_book_thumbnail);

        final ImageView payMessageIcon = (ImageView) v.findViewById(R.id.payment_message_icon);

        balanceTextView.setText(String.format(currency, customer.getLomiCoins()));

        //amountPayableTextView.setText("10 " + context.getResources().getString(R.string.birr));

        bookTitleTextView.setText(product.getName());

        Picasso.with(context).load(product.getThumbnailSrc()).into(bookThumbNail);

        progressButton = (ProgressButton) v.findViewById(R.id.pay_progress_button);

        giftCard = (TextView) v.findViewById(R.id.pay_gift_card);

        progressButton.setCustomer(getCustomer());

        bgNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$DataUsageSummaryActivity"));
                    context.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        progressButton.setOnProgressFinished(new ProgressButton.onProgressFinished() {

            @Override
            public void onFinished(boolean isClicked, boolean isFinished) {
                if (isFinished) {
                    payMessageIcon.setImageResource(R.drawable.ic_check_circle_black_24dp);
                    giftCard.setVisibility(View.GONE);
                    bgNotice.setVisibility(View.VISIBLE);
                    balanceMessageTextView.setText(context.getResources().getString(R.string.enough_amount));
                    if (isClicked) {
                        finishDialog();
                    }
                }
            }

            @Override
            public void onPayment(boolean isSuccessful) {
                if (isSuccessful) {
                    addCoinOnline(customer, context, 10);
                }
            }
        });


        bookPriceTextView.setText(priceText);

        v.findViewById(R.id.pay_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PaymentDialogFragment.this.dismiss();
            }
        });


        final PayPalConfiguration configuration = new PayPalConfiguration().

                environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION).

                clientId(PAYPAL_CLIENT_ID);

        final LinearLayout pay_balance_wrapper = (LinearLayout) v.findViewById(R.id.pay_balance_wrapper);

        //final LinearLayout pay_amount_payable_wrapper = (LinearLayout) v.findViewById(R.id.pay_amount_payable_wrapper);

        final LinearLayout paypalPanel = (LinearLayout) v.findViewById(R.id.paypalPanel);

        final RelativeLayout balanceIndicator = (RelativeLayout) v.findViewById(R.id.balance_indicator_wrapper);

        final Button done = (Button) v.findViewById(R.id.card_done);

        final EditText cardEditText = (EditText) v.findViewById(R.id.id_input_card);

        final TextInputLayout inputLayout = (TextInputLayout) v.findViewById(R.id.id_input_layout_card);

        progressButton.setMax(product.getPrice(context));

        progressButton.setProgress((int) customer.getLomiCoins());

        if (PreferenceHelper.inEthiopia(context)) {
            paypalPanel.setVisibility(View.GONE);
        } else {
            pay_balance_wrapper.setVisibility(View.GONE);
            //pay_amount_payable_wrapper.setVisibility(View.GONE);
            balanceIndicator.setVisibility(View.GONE);
            progressButton.setVisibility(View.GONE);
        }

        paypalPanel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                PayPalPayment payPalPayment = new PayPalPayment(new BigDecimal(product.getPrice(context)), "USD", "Lomi Books Store", PayPalPayment.PAYMENT_INTENT_SALE);

                Intent intent = new Intent(getActivity(), PaymentActivity.class);

                intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, configuration);

                intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payPalPayment);

                startActivityForResult(intent, REQUEST_CODE_FOR_PAYPAL);
            }
        });

        giftCard.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                progressButton.setVisibility(View.GONE);

                done.setVisibility(View.VISIBLE);

                pay_balance_wrapper.setVisibility(View.GONE);

                inputLayout.setVisibility(View.VISIBLE);
            }
        });

        final ScratchCardTracker scratchCardTracker = new ScratchCardTracker();

        done.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (cardEditText.getText().toString().length() < 16 || cardEditText.getText().toString().length() > 16) {

                    cardEditText.setError(getString(R.string.invalid_number));
                } else if (NotificationUtils.count >= 5) {
                    ExtendedToast.showToast(context, ExtendedToast.FAILED, "Your account has been suspended try after 30 minute");

                    PreferenceHelper.setTrialEndTime(getActivity(), System.currentTimeMillis());

                    NotificationUtils.count = 0;

                } else if (PreferenceHelper.getTrialEndTime(context) != 0 &&
                        System.currentTimeMillis() - PreferenceHelper.getTrialEndTime(getActivity()) < Constants.TRIAL_END_INTERVAL) {

                    long minute = 30 - (System.currentTimeMillis() - PreferenceHelper.getTrialEndTime(getActivity())) / 60000;

                    ExtendedToast.showToast(context, ExtendedToast.FAILED, "Your account has been suspended try after " + minute + " minute");

                    PaymentDialogFragment.this.dismiss();
                } else {

                    PreferenceHelper.setTrialEndTime(context, 0);

                    Realm realm = null;

                    try {

                        realm = Realm.getDefaultInstance();

                        final Customer customerCached = realm.where(Customer.class).findFirst();

                        final Realm finalRealm = realm;

                        scratchCardTracker.startTracker(cardEditText.getText().toString(), getActivity(), customerCached.getId(), new ScratchCardTracker.OnCouponReceived() {
                            @Override
                            public void onCouponSuccessful(final Long response, boolean isSuccessful) {

                                if (isSuccessful) {

                                    finalRealm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            realm.where(Customer.class).findFirst().setLomiCoins(response);
                                        }
                                    });
                                    ExtendedToast.showToast(getActivity(), ExtendedToast.SUCCESS, getString(R.string.successfully_coupon));
                                    buyerButton.onButtonClicked();
                                    PaymentDialogFragment.this.dismiss();
                                }
                            }

                            @Override
                            public void onCouponFailed(Throwable t) {

                            }
                        });
                    } finally {
                        if (realm != null) {
                            realm.close();
                        }
                    }


                }
            }
        });


        if (PreferenceHelper.inEthiopia(context)) {

            //paypalPanel.setVisibility(View.GONE);

        } else {


        }

        v.setBackgroundResource(R.drawable.alert_round);

        builder.setView(v);

        return builder.create();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_FORTUMO) {

            if (resultCode == Activity.RESULT_OK) {

                finishDialog();
            }

            if (resultCode == Activity.RESULT_CANCELED) {

                dismiss();
            }
        }

        if (requestCode == REQUEST_CODE_FOR_PAYPAL) {

            if (data == null) {

                Toast.makeText(getActivity(), "Null", Toast.LENGTH_SHORT).show();
            }

            if (resultCode == Activity.RESULT_OK) {

                assert data != null;

                PaymentConfirmation paymentConfirmation = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                if (paymentConfirmation != null) {

                    try {

                        JSONObject jsonObject = new JSONObject(paymentConfirmation.toJSONObject().getJSONObject("response").toString());

                        if (jsonObject.getString("state").equalsIgnoreCase("approved")) {

                            Toast.makeText(getActivity(), "Successfully paid", Toast.LENGTH_SHORT).show();

                            finishDialog();
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                } else {

                    Toast.makeText(getActivity(), "Null Confirmation", Toast.LENGTH_SHORT).show();
                }
            }
        } else {

            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void addCoinOnline(final Customer customer, final Context context, int numCoins) {

        if (customer != null) {

            WooCommerceServiceGenerator generator = new WooCommerceServiceGenerator(context);

            CustomerClient client = generator.createService(CustomerClient.class);

            Call<Customer> customerCall = client.putCustomerById(new String(Native.putCustomerById(String.valueOf(customer.getId())), Charset.defaultCharset()) + "/" + numCoins + "/");

            customerCall.enqueue(new Callback<Customer>() {

                @Override
                public void onResponse(Call<Customer> call, final Response<Customer> response) {

                    Realm realm = null;

                    try {

                        realm = Realm.getDefaultInstance();

                        realm.executeTransaction(new Realm.Transaction() {

                            @Override
                            public void execute(Realm realm) {

                                customer.setLomiCoins(response.body().getLomiCoins());
                            }
                        });

                        balanceTextView.setText(String.valueOf(response.body().getLomiCoins()));

                        animateTextView((int) customer.getLomiCoins(), (int) response.body().getLomiCoins(), balanceTextView);

                        progressButton.setProgress(10);

                    } finally {
                        if (realm != null) {

                            realm.close();
                        }
                    }
                }

                @Override
                public void onFailure(Call<Customer> call, Throwable t) {

                }
            });
        }
    }

    private void finishDialog() {

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            final int productPrice = buyerButton.getProduct().getPrice(context);

            realm.executeTransaction(new Realm.Transaction() {

                @Override
                public void execute(Realm realm) {

                    Customer customer = realm.where(Customer.class).findFirst();

                    if (customer != null) {

                        if (PreferenceHelper.inEthiopia(getContext())) {

                            customer.setCoinsSpent(customer.getCoinsSpent() + productPrice);

                        } else {

                            customer.setUsdSpent(customer.getUsdSpent() + productPrice);
                        }
                    }
                }
            });

            buyerButton.onFinishPayment(true);

            this.dismiss();

        } finally {

            if (realm != null) {

                realm.close();
            }
        }
    }

    public void animateTextView(int initialValue, int finalValue, final TextView textview) {
        Log.d(TAG, "animateTextView: " + initialValue + " " + finalValue);
        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(1500);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                textview.setText(valueAnimator.getAnimatedValue().toString() + " " + context.getResources().getString(R.string.birr));
            }
        });
        valueAnimator.start();

    }

    private void setBuyerButton(LomiBuyButtonView button) {
        this.buyerButton = button;
    }

    @Override
    public void show(FragmentManager manager, String tag) {

        try {

            FragmentTransaction ft = manager.beginTransaction();

            ft.add(this, tag);

            ft.commitAllowingStateLoss();

        } catch (IllegalStateException e) {

            Log.d(PaymentDialogFragment.class.getSimpleName(), "Exception", e);
        }
    }

    public Customer getCustomer() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            return realm.where(Customer.class).findFirst();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }
}
