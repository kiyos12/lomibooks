package com.a360ground.lomibook.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.activity.MainActivity;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.service.SyncCustomerBooks;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.utils.PreferenceHelper;
import com.a360ground.lomibook.utils.SettingsHelper;
import com.a360ground.lomibook.view.ExtendedToast;
import com.jaychang.slm.SocialLoginManager;
import com.jaychang.slm.SocialUser;

import java.util.regex.Pattern;

import rx.functions.Action1;

public class Intro2Login extends Fragment implements View.OnClickListener {

    public static String TAG = Intro2Login.class.getSimpleName();

    RelativeLayout facebookLogin;

    RelativeLayout googleLogin;

    EditText fullNameEditText;

    EditText emailEditText;

    Button signOn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_intro2_login, container, false);

        initViews(view);

        return view;
    }

    public void initViews(View view) {

        facebookLogin = (RelativeLayout) view.findViewById(R.id.facebook_login_button_wrapper);

        googleLogin = (RelativeLayout) view.findViewById(R.id.google_login_button_wrapper);

        fullNameEditText = (EditText) view.findViewById(R.id.registration_fullname_edit_text);

        emailEditText = (EditText) view.findViewById(R.id.registration_email_edit_text);

        signOn = (Button) view.findViewById(R.id.register_button);

        signOn.setOnClickListener(this);

        facebookLogin.setOnClickListener(this);

        googleLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.facebook_login_button_wrapper) {

            loginByFacebook();

        }

        if (view.getId() == R.id.google_login_button_wrapper) {

            if (SettingsHelper.isGooglePlayServicesAvailable(getActivity()))

                loginByGoogle();

        }

        if (view.getId() == R.id.register_button) {

            if(TextUtils.isEmpty(fullNameEditText.getText())){
                fullNameEditText.setError(getString(R.string.full_name_empty));
                return;
            }

            if(TextUtils.isEmpty(emailEditText.getText())){
                emailEditText.setError(getString(R.string.email_empty));
                return;
            }

            if(!Patterns.EMAIL_ADDRESS.matcher(emailEditText.getText()).matches()){
                emailEditText.setError(getString(R.string.email_invalid));
                return;
            }

            else{
                completeSignup(emailEditText.getText().toString(), fullNameEditText.getText().toString());
            }
        }
    }

    private void loginByFacebook() {

        SocialLoginManager.getInstance(getActivity())

                .facebook()

                .login()

                .subscribe(new Action1<SocialUser>() {

                    @Override
                    public void call(SocialUser socialUser) {

                        PreferenceHelper.profilePicFromFB(getActivity(), socialUser.photoUrl);
                        completeSignup(socialUser.profile.email, socialUser.profile.name);
                        Log.d(TAG, "call: "+socialUser.profile.fullName);
                    }


                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        ExtendedToast.showToast(getActivity(), ExtendedToast.FAILED, getString(R.string.error_please_try_again)+throwable.getMessage());
                    }
                });
    }

    private void loginByGoogle() {

        SocialLoginManager.getInstance(getActivity())

                .google(getString(R.string.default_web_client_id))

                .login()

                .subscribe(new Action1<SocialUser>() {
                    @Override
                    public void call(SocialUser socialUser) {
                        PreferenceHelper.profilePicFromFB(getActivity(), socialUser.photoUrl);
                        completeSignup(socialUser.profile.email, socialUser.profile.name);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        ExtendedToast.showToast(getActivity(), ExtendedToast.FAILED, getString(R.string.error_please_try_again));
                    }
                });
    }

    private void completeSignup(String email, String name){

        new WooCommerceServiceGenerator(getActivity()).createCustomer(getActivity(), email, name, new WooCommerceServiceGenerator.OnSignupCompleted() {
            @Override
            public void isSignupFinished(final Customer customer) {
                final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Syncing your books");
                progressDialog.show();
                SyncCustomerBooks.getBooks(getActivity(), customer, new SyncCustomerBooks.OnFinishedSyncing() {
                    @Override
                    public void OnFinishedLoading(boolean isFinished) {
                        progressDialog.dismiss();
                        startActivity(new Intent(getActivity(), MainActivity.class));
                    }
                });
            }
        });
    }
}
