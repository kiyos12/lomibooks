package com.a360ground.lomibook.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.application.LomiApplication;
import com.a360ground.lomibook.client.ProductClient;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.service.DownloadTrackerIntentService;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.view.LomiBuyButtonView;
import com.a360ground.lomibook.view.LomiProductView;
import com.facebook.share.widget.LikeView;
import com.koolearn.klibrary.core.util.Native;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookDetailsFragment extends Fragment {

    long id = 0;
    Product product;
    private LomiBuyButtonView buyButton;
    private LomiBuyButtonView previewButton;
    private Long productId;
    private RealmResults<Product> products;
    private TextView bookDetailTextView;
    private Realm realm;
    private LomiProductView productView;
    private Toolbar toolbar;
    private int mLongAnimationDuration;
    private ScrollView scrollView;
    private ImageView backButton;
    private LikeView likeView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_book_details, container);

        realm = Realm.getDefaultInstance();

        likeView = (LikeView) view.findViewById(R.id.like_view);

        likeView.setLikeViewStyle(LikeView.Style.STANDARD);

        likeView.setAuxiliaryViewPosition(LikeView.AuxiliaryViewPosition.INLINE);

        buyButton = (LomiBuyButtonView) view.findViewById(R.id.buy_book);

        backButton = (ImageView) view.findViewById(R.id.back_button);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        previewButton = (LomiBuyButtonView) view.findViewById(R.id.preview_book);

        productView = (LomiProductView) view.findViewById(R.id.product_view);

        bookDetailTextView = (TextView) view.findViewById(R.id.book_details_description);

        scrollView = (ScrollView) view.findViewById(R.id.scrollview);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        mLongAnimationDuration = getResources().getInteger(android.R.integer.config_longAnimTime);

        final Intent intent = ((Activity) getContext()).getIntent();

        if(getArguments()!=null){
            productId = getArguments().getLong(Constants.PRODUCT_ID_IDENTIFIER);
        }

        else{
            productId = intent.getLongExtra(Constants.PRODUCT_ID_IDENTIFIER, -1);
        }

        if (productId != -1) {

            products = realm.where(Product.class).equalTo("id", productId).findAll();

            if (!products.isEmpty()) {

                product = products.get(0);

                likeView.setObjectIdAndType( "http://www.google.com/", LikeView.ObjectType.OPEN_GRAPH);

                ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();

                if (actionBar != null) {

                    actionBar.setTitle(product.getName());
                }

                bookDetailTextView.setText(Html.fromHtml(product.getDescription()));

                buyButton.setProduct(product);

                previewButton.setProduct(product);

                productView.setProduct(product);

                if (!product.fileExistsLocally(false)) {
                    DownloadTrackerIntentService.startTrackerService(getContext(), products, true);
                }

                scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

                    private boolean scrolled = false;

                    @Override
                    public void onScrollChanged() {

                        if (scrollView.getScrollY() > productView.getHeight() - 10) {

                            if (!scrolled) {

                                crossFade();

                                backButton.setColorFilter(ContextCompat.getColor(getContext(), R.color.white));

                                scrolled = true;
                            }

                        } else if (scrollView.getScrollY() < productView.getHeight() + 10) {

                            if (scrolled) {

                                resetCrossFade();

                                backButton.setColorFilter(ContextCompat.getColor(getContext(), R.color.mainColor));

                                scrolled = false;
                            }
                        }
                    }
                });

            } else {

                WooCommerceServiceGenerator serviceGenerator = new WooCommerceServiceGenerator(getActivity());

                final ProgressDialog progressDialog = new ProgressDialog(getActivity());

                progressDialog.setMessage(getString(R.string.wait));

                progressDialog.show();

                ProductClient productClient = serviceGenerator.createService(ProductClient.class);

                Call<Product> productCall = productClient.productById(new String(Native.getProductsById(String.valueOf(productId))) + "/");

                productCall.enqueue(new Callback<Product>() {

                    @Override
                    public void onResponse(Call<Product> call, Response<Product> response) {

                        progressDialog.dismiss();

                        final Product product = response.body();

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(product);
                            }
                        });

                        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();

                        if (actionBar != null) {

                            actionBar.setTitle(product.getName());
                        }

                        bookDetailTextView.setText(Html.fromHtml(product.getDescription()));

                        buyButton.setProduct(product);

                        previewButton.setProduct(product);

                        productView.setProduct(product);

                        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

                            private boolean scrolled = false;

                            @Override
                            public void onScrollChanged() {

                                if (scrollView.getScrollY() > productView.getHeight() - 10) {

                                    if (!scrolled) {

                                        crossFade();

                                        backButton.setColorFilter(ContextCompat.getColor(getContext(), R.color.white));

                                        scrolled = true;
                                    }

                                } else if (scrollView.getScrollY() < productView.getHeight() + 10) {

                                    if (scrolled) {

                                        resetCrossFade();

                                        backButton.setColorFilter(ContextCompat.getColor(getContext(), R.color.mainColor));

                                        scrolled = false;
                                    }
                                }
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<Product> call, Throwable t) {

                    }
                });
            }
            // bring from online
        } else {

            Toast.makeText(getContext(), "You need to pass the id to the view", Toast.LENGTH_SHORT).show();

            ((Activity) getContext()).finish();
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {

        super.onResume();

        realm = Realm.getDefaultInstance();

        if (buyButton.getState() == LomiBuyButtonView.StateEnum.DOWNLOADING || buyButton.getState() == LomiBuyButtonView.StateEnum.CONNECTING) {

            previewButton.setState(getContext(), null);
        }

        if (product != null) {

            if (!product.fileExistsLocally(false) || !product.fileExistsLocally(true)) {
                DownloadTrackerIntentService.startTrackerService(getContext(), products, true);
            }
        }

    }

    @Override
    public void onPause() {

        super.onPause();

        realm.close();

        DownloadTrackerIntentService.stopTrackerService(getContext());
    }

    public void crossFade() {

        ViewGroup.LayoutParams layoutParams = toolbar.getLayoutParams();

        layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;

        toolbar.setLayoutParams(layoutParams);

        toolbar.setAlpha(0);

        toolbar.setVisibility(View.VISIBLE);

        toolbar.animate()

                .alpha(1f)

                .setDuration(mLongAnimationDuration)

                .setListener(null);

        productView.animate()

                .alpha(0f)

                .setDuration(mLongAnimationDuration)

                .setListener(new AnimatorListenerAdapter() {

                    @Override
                    public void onAnimationEnd(Animator animation) {

                        productView.setVisibility(View.INVISIBLE);
                    }
                });
    }

    public void resetCrossFade() {

        toolbar.animate()

                .alpha(0f)

                .setDuration(mLongAnimationDuration)

                .setListener(new AnimatorListenerAdapter() {

                    @Override
                    public void onAnimationEnd(Animator animation) {

                        ViewGroup.LayoutParams layoutParams = toolbar.getLayoutParams();

                        layoutParams.height = 0;

                        toolbar.setLayoutParams(layoutParams);
                    }
                });

        productView.setVisibility(View.VISIBLE);

        productView.setAlpha(0f);

        productView.animate()

                .alpha(1f)

                .setDuration(mLongAnimationDuration)

                .setListener(null);
    }
}
