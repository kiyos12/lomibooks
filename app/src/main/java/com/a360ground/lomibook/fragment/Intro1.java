package com.a360ground.lomibook.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.utils.PreferenceHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class Intro1 extends Fragment {


    public Intro1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_intro1, container, false);

        final RelativeLayout birr_wrapper = (RelativeLayout) view.findViewById(R.id.birr_wrapper);

        final RelativeLayout dollar_wrapper = (RelativeLayout) view.findViewById(R.id.dollar_wrapper);

        final ImageView paypal_onboard = (ImageView) view.findViewById(R.id.paypal_logo_onboard);

        final TextView great_to_let = (TextView) view.findViewById(R.id.great_to_let);

        final TextView paymentOption = (TextView) view.findViewById(R.id.payment_option_txt);

        final ImageView ethio_telecom_onboard = (ImageView) view.findViewById(R.id.ethiotelecom_logo_onboard);

        birr_wrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ethio_telecom_onboard.setVisibility(View.VISIBLE);
                paypal_onboard.setVisibility(View.GONE);
                birr_wrapper.setBackgroundResource(R.drawable.country_bg_selected);
                dollar_wrapper.setBackgroundResource(R.drawable.country_bg_default);
                paymentOption.setText(getString(R.string.payment_is_processed_by_ethio_telecom));
                PreferenceHelper.setCountryCode(getActivity(), PreferenceHelper.ETHIOPIA_COUNTRY_CODE);
                great_to_let.setText(getString(R.string.great_to_let_you_know_you_can_pay_with_your_sim_card_from_ethiopia));
            }
        });

        dollar_wrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ethio_telecom_onboard.setVisibility(View.GONE);
                paypal_onboard.setVisibility(View.VISIBLE);
                birr_wrapper.setBackgroundResource(R.drawable.country_bg_default);
                paymentOption.setText(getString(R.string.payment_is_processed_by_paypal));
                PreferenceHelper.setCountryCode(getActivity(), PreferenceHelper.OTHER_COUNTRY_CODE);
                dollar_wrapper.setBackgroundResource(R.drawable.country_bg_selected);
                great_to_let.setText(getString(R.string.hey_traveler));
            }
        });

        return view;
    }

}
