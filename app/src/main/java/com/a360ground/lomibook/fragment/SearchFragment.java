package com.a360ground.lomibook.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.a360ground.lomibook.adapter.TopSearchAdapter;
import com.a360ground.lomibook.client.SearchesClient;
import com.a360ground.lomibook.domain.algolia.AlgoliaSearchResult;
import com.a360ground.lomibook.domain.algolia.TopSearches;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.R;
import com.a360ground.lomibook.adapter.SearchResultsRecyclerAdapter;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.utils.Constants;
import com.arlib.floatingsearchview.suggestions.SearchSuggestionsAdapter;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.koolearn.klibrary.core.util.Native;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends Fragment {

    ListView topSearchCategoriesListView;

    LinearLayout topSearchResultsContainerLinearLayout;

    RecyclerView searchResultsRecyclerView;

    SearchResultsRecyclerAdapter searchResultsRecyclerAdapter;

    RelativeLayout searchNotFound;

    TextView searchNotFoundTextView;

    private String searchQueryString;

    private ProgressBar progressImage;

    private List<Product> searchResults = new ArrayList<>();

    public static SearchFragment newInstance(String searchQuery) {
        SearchFragment fragment = new SearchFragment();
        Bundle arguments = new Bundle();
        arguments.putString(Constants.SEARCH_QUERY, searchQuery);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_search, container, false);

        searchResultsRecyclerView = (RecyclerView) view.findViewById(R.id.search_results_recycler);

        topSearchCategoriesListView = (ListView) view.findViewById(R.id.top_search_categories_list);

        searchNotFoundTextView = (TextView) view.findViewById(R.id.search_not_found_text);

        searchNotFound = (RelativeLayout) view.findViewById(R.id.search_not_found);

        topSearchResultsContainerLinearLayout = (LinearLayout) view.findViewById(R.id.top_search_categories_list_container);

        progressImage = (ProgressBar) view.findViewById(R.id.loading_spinner);

        handleIntent(getArguments().getString(Constants.SEARCH_QUERY));

        return view;
    }

    private void handleIntent(String query) {

        searchQueryString = query;

        searchResults.clear();

        searchResultsRecyclerAdapter = new SearchResultsRecyclerAdapter(getActivity(), searchResults);

        searchResultsRecyclerView.setAdapter(searchResultsRecyclerAdapter);

        searchResultsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        search();
    }

    public void search() {

        progressImage.setVisibility(View.VISIBLE);

        WooCommerceServiceGenerator wooCommerceServiceGenerator = new WooCommerceServiceGenerator(getActivity());

        SearchesClient searchesClient = wooCommerceServiceGenerator.createService(SearchesClient.class);

        Call<List<Product>> listCallSearchProducts = searchesClient.search(new String(Native.getSearchProducts(searchQueryString)));

        listCallSearchProducts.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {

                progressImage.setVisibility(View.GONE);

                if(response.body().size() != 0){

                    searchNotFound.setVisibility(View.INVISIBLE);

                    searchResultsRecyclerAdapter = new SearchResultsRecyclerAdapter(getActivity(), response.body());

                    searchResultsRecyclerView.setAdapter(searchResultsRecyclerAdapter);

                    searchResultsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

                    searchResultsRecyclerView.setVisibility(View.VISIBLE);

                    topSearchCategoriesListView.setVisibility(View.GONE);
                }

                else{
                    searchNotFound.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable throwable) {
                progressImage.setVisibility(View.GONE);
            }
        });
    }
}