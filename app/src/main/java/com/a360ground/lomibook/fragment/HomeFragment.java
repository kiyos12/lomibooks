package com.a360ground.lomibook.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.activity.MainActivity;
import com.a360ground.lomibook.adapter.HomeCategoryRecyclerAdapter;
import com.a360ground.lomibook.application.LomiApplication;
import com.a360ground.lomibook.client.CategoryClient;
import com.a360ground.lomibook.client.CustomerClient;
import com.a360ground.lomibook.client.PostClient;
import com.a360ground.lomibook.client.ProductClient;
import com.a360ground.lomibook.domain.woocommerce.Category;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.Post;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.utils.LomiNetworkOperation;
import com.a360ground.lomibook.utils.PreferenceHelper;
import com.koolearn.klibrary.core.util.Native;

import java.nio.charset.Charset;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements PreferenceHelper.OnLocaleChangedListener {

    WooCommerceServiceGenerator generator;
    HomeCategoryRecyclerAdapter categoryRecyclerAdapter;
    CategoryClient categoryClient;
    ProgressBar progressBar;
    SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private RealmResults<Category> categories;
    private Context context;
    private Realm realm;

    public HomeFragment() {
        PreferenceHelper.addListener(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        context = getContext();

        realm = Realm.getDefaultInstance();

        ((MainActivity) context).setActiveBottomNavigationItem(0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        realm = Realm.getDefaultInstance();

        categories = realm.where(Category.class).findAllSorted("menuOrder");

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.category_recycler_view);

        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeToRefresh);

        generator = new WooCommerceServiceGenerator(context);

        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.mainColor));

        recyclerView.setNestedScrollingEnabled(false);

        categoryRecyclerAdapter = new HomeCategoryRecyclerAdapter(context, categories);

        recyclerView.setAdapter(categoryRecyclerAdapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

        recyclerView.setHasFixedSize(true);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                refreshHomeData();

                refreshAds();

                verifyAuthor();

                refreshTopSellers();

            }
        });

        if (realm.isEmpty()) {

            refreshHomeData();

            refreshAds();

            refreshTopSellers();

        }

        long homeLastUpdated = PreferenceHelper.getLastUpdated(context, PreferenceHelper.HOME_LAST_UPDATED);

        if (homeLastUpdated == -1 || System.currentTimeMillis() - homeLastUpdated > PreferenceHelper.updateInterval) {

            refreshHomeData();
        }

        long adsLastUpdated = PreferenceHelper.getLastUpdated(context, PreferenceHelper.ADS_LAST_UPDATED);

        if (adsLastUpdated == -1 || System.currentTimeMillis() - adsLastUpdated > PreferenceHelper.updateInterval) {

            refreshAds();
        }
        final long topSellersLastUpdated = PreferenceHelper.getLastUpdated(context, PreferenceHelper.TOP_SELLERS_LAST_UPDATED);

        if (topSellersLastUpdated == -1 || System.currentTimeMillis() - topSellersLastUpdated > PreferenceHelper.updateInterval) {

            refreshTopSellers();
        }

        return view;
    }

    @Override
    public void localeChanged(String language) {
    }

    public void verifyAuthor() {

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            Customer customer = realm.where(Customer.class).findFirst();

            if (customer != null) {


                WooCommerceServiceGenerator generator = new WooCommerceServiceGenerator(getActivity());

                CustomerClient client = generator.createService(CustomerClient.class);

                Call<Customer> verifyAuthorCall = client.getCustomerById(new String(Native.getCustomerById(String.valueOf(customer.getId())), Charset.defaultCharset()) + "/");

                new LomiNetworkOperation<Customer, Customer>(getActivity(), verifyAuthorCall).send(new LomiNetworkOperation.LaunchesNetworkOperation() {

                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onSuccess(List<Long> fetchedIds) {

                        Realm realm = null;

                        try {

                        } finally {

                            if (realm != null) {

                                realm.close();
                            }
                        }
                    }

                    @Override
                    public void onFailure(String errorMessage) {

                    }
                });
            }

        } finally {

            if (realm != null) {

                realm.close();
            }
        }
    }

    public void refreshHomeData() {

        Log.d("Cats Path", Native.getCategories());

        swipeRefreshLayout.setRefreshing(true);

        categoryClient = generator.createService(CategoryClient.class);

        Call<List<Category>> getCategoriesCall = categoryClient.categories(Native.getCategories());

        new LomiNetworkOperation<List<Category>, Category>(context, getCategoriesCall).send(new LomiNetworkOperation.LaunchesNetworkOperation() {

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(List<Long> fetchedIds) {

                progressBar.setVisibility(View.GONE);

                for (final long categoryId : fetchedIds) {

                    long categoryProductsLastUpdated = PreferenceHelper.getCategoryLastUpdated(context, PreferenceHelper.CATEGORY_LAST_UPDATED + categoryId);

                    if (categoryProductsLastUpdated == -1 || System.currentTimeMillis() - categoryProductsLastUpdated > PreferenceHelper.updateInterval) {

                        ProductClient productClient = generator.createService(ProductClient.class);

                        Call<List<Product>> productsCall = productClient.productsByCategory(Native.getProducts(), categoryId);

                        final long categoryIdCopy = categoryId;

                        new LomiNetworkOperation<List<Product>, Product>(context, productsCall).send(new LomiNetworkOperation.LaunchesNetworkOperation() {

                            @Override
                            public void onStart() {

                            }

                            @Override
                            public void onSuccess(List<Long> fetchedIds) {

                                progressBar.setVisibility(View.GONE);

                                if (swipeRefreshLayout.isRefreshing()) {

                                    swipeRefreshLayout.setRefreshing(false);

                                }

                                categoryRecyclerAdapter = new HomeCategoryRecyclerAdapter(context, categories);

                                recyclerView.setAdapter(categoryRecyclerAdapter);

                                PreferenceHelper.setLastUpdated(context, PreferenceHelper.CATEGORY_LAST_UPDATED + categoryIdCopy, System.currentTimeMillis());

                            }


                            @Override
                            public void onFailure(String errorMessage) {
                                recyclerView.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }

                PreferenceHelper.setLastUpdated(context, PreferenceHelper.HOME_LAST_UPDATED, System.currentTimeMillis());
            }

            @Override
            public void onFailure(String errorMessage) {

                progressBar.setVisibility(View.GONE);

                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }

                recyclerView.setVisibility(View.VISIBLE);
            }
        });
    }

    private void refreshTopSellers() {

        WooCommerceServiceGenerator generator = new WooCommerceServiceGenerator(getActivity());

        final ProductClient client = generator.createService(ProductClient.class);

        Call<List<Product>> getTopSellersCall = client.getTopSellers(Native.getTopSellers(), Constants.TOP_SELLERS_PERIOD_YEAR_IDENTIFIER);

        new LomiNetworkOperation<List<Product>, Product>(context, getTopSellersCall).send(new LomiNetworkOperation.LaunchesNetworkOperation() {

            @Override
            public void onStart() {
            }

            @Override
            public void onSuccess(List<Long> fetchedIds) {

                Realm realm = null;

                try {

                    realm = Realm.getDefaultInstance();

                    Long[] primitiveFetchedIds = new Long[fetchedIds.size()];

                    for (int i = 0; i < fetchedIds.size(); i++) {

                        primitiveFetchedIds[i] = fetchedIds.get(i);
                    }

                    if (primitiveFetchedIds.length != 0) {
                        RealmResults<Product> topSellers = realm.where(Product.class).in("id", primitiveFetchedIds).findAll();

                        if (topSellers.size() < fetchedIds.size()) {

                            int idsToBeRequestedIndex = 0;

                            Long[] idsToBeRequested = new Long[fetchedIds.size() - topSellers.size()];

                            for (int i = 0; i < fetchedIds.size(); i++) {

                                int j = 0;

                                for (; j < topSellers.size(); j++) {

                                    if (topSellers.get(i).getId() == fetchedIds.get(i)) break;
                                }

                                if (j == topSellers.size()) {

                                    idsToBeRequested[idsToBeRequestedIndex] = fetchedIds.get(i);

                                    idsToBeRequestedIndex++;
                                }
                            }

                            Call<List<Product>> productCall = client.productsByIds(Native.getProducts(), TextUtils.join(",", idsToBeRequested));

                            new LomiNetworkOperation<List<Product>, Product>(context, productCall).send(new LomiNetworkOperation.LaunchesNetworkOperation() {

                                @Override
                                public void onStart() {
                                }

                                @Override
                                public void onSuccess(List<Long> fetchedIds) {

                                    PreferenceHelper.setLastUpdated(context, PreferenceHelper.TOP_SELLERS_LAST_UPDATED, System.currentTimeMillis());
                                }

                                @Override
                                public void onFailure(String errorMessage) {
                                }
                            });

                        } else {

                            PreferenceHelper.setLastUpdated(context, PreferenceHelper.TOP_SELLERS_LAST_UPDATED, System.currentTimeMillis());
                        }
                    }
                } finally {

                    if (realm != null) {

                        realm.close();
                    }
                }
            }

            @Override
            public void onFailure(String errorMessage) {
            }
        });
    }

    private void refreshAds() {

        PostClient adsClient = generator.createService(PostClient.class);

        Call<List<Post>> call = adsClient.getAds();

        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, final Response<List<Post>> response) {
                
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        realm.where(Post.class).findAll().deleteAllFromRealm();

                        realm.insert(response.body());

                        PreferenceHelper.setLastUpdated(context, PreferenceHelper.ADS_LAST_UPDATED, System.currentTimeMillis());
                    }
                });

            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onDestroy() {
        if (realm != null) {
            realm.close();
        }
        super.onDestroy();
    }
}
