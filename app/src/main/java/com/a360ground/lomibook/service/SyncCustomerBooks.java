package com.a360ground.lomibook.service;

import android.content.Context;

import com.a360ground.lomibook.client.CustomerClient;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.koolearn.klibrary.core.util.Native;

import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Kiyos on 8/17/2017.
 */

public class SyncCustomerBooks {

    public static void getBooks(Context context, final Customer customer, final OnFinishedSyncing onFinishedSyncing) {

        WooCommerceServiceGenerator serviceGenerator = new WooCommerceServiceGenerator(context);

        CustomerClient productSyncClient = serviceGenerator.createService(CustomerClient.class);

        Call<List<Product>> productSyncCall = productSyncClient.getDownloads(new String(Native.getCustomerDownloads(String.valueOf(customer.getId()))));

        productSyncCall.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, final Response<List<Product>> response) {

                Realm realm = null;

                try {
                    realm = Realm.getDefaultInstance();

                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            customer.addBooks(response.body());
                        }
                    });
                    onFinishedSyncing.OnFinishedLoading(true);
                } finally {
                    realm.close();
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable throwable) {
                onFinishedSyncing.OnFinishedLoading(false);
            }
        });


    }

    public interface OnFinishedSyncing {
        void OnFinishedLoading(boolean isFinished);
    }
}
