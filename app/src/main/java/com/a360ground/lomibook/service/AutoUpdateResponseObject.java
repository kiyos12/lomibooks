package com.a360ground.lomibook.service;

import com.a360ground.lomibook.BuildConfig;
import com.google.gson.annotations.SerializedName;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class AutoUpdateResponseObject extends RealmObject {

    @PrimaryKey
    int id = 0; // initialized to zero to keep this as a single object and prevent multiple version entries

    private static final java.lang.String DOT_SPLITTER_REGEX = "\\.";
    @SerializedName("version")
    String version;

    @SerializedName("releaseDate")
    String releaseDate;

    @SerializedName("versionName")
    String versionName;

    @SerializedName("downloadLink")
    String downloadLink;

    public static int[] getUpdateVersion() {

        AutoUpdateResponseObject updateObject = getInstance();

        if(updateObject != null) {

            String[] parts = updateObject.version.split(DOT_SPLITTER_REGEX);

            int[] version = new int[3];

            version[0] = parts.length > 0 ? Integer.parseInt(parts[0]) : 0;

            version[1] = parts.length > 1 ? Integer.parseInt(parts[1]) : 0;

            version[2] = parts.length > 2 ? Integer.parseInt(parts[2]) : 0;

            return version;
        }

        return new int[] { 0, 0, 0 };
    }

    public static int[] getBuildVersion() {

        int[] version = new int[3];

        String[] currentApkVersionParts = BuildConfig.VERSION_NAME.split(DOT_SPLITTER_REGEX);

        version[0] = currentApkVersionParts.length > 0 ? Integer.parseInt(currentApkVersionParts[0]) : 0;

        version[1] = currentApkVersionParts.length > 1 ? Integer.parseInt(currentApkVersionParts[1]) : 0;

        version[2] = currentApkVersionParts.length > 2 ? Integer.parseInt(currentApkVersionParts[2]) : 0;

        return version;
    }

    public static boolean requiresForcedUpdate() {

        AutoUpdateResponseObject updateObject = getInstance();

        int[] buildVersion = getBuildVersion();

        int[] updateVersion = updateObject.getUpdateVersion();

        return updateVersion[0] > buildVersion[0];
    }

    public static boolean requiresRecommendedUpdate() {

        AutoUpdateResponseObject updateObject = getInstance();

        int[] buildVersion = getBuildVersion();

        int[] updateVersion = updateObject.getUpdateVersion();

        return updateVersion[0] == buildVersion[0] && updateVersion[1] > buildVersion[1];
    }

    public static AutoUpdateResponseObject getInstance() {

        AutoUpdateResponseObject instance = null;

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            instance = realm.where(AutoUpdateResponseObject.class).findFirst();

        } finally {

            if (realm != null) {

                realm.close();
            }
        }

        return instance;
    }

    public static void resetUpdateVersionToBuildVersion() {

        final int[] buildVersion = getBuildVersion();

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            realm.executeTransaction(new Realm.Transaction() {

                @Override
                public void execute(Realm realm) {

                    AutoUpdateResponseObject updateObject = realm.where(AutoUpdateResponseObject.class).findFirst();

                    updateObject.version = String.valueOf(buildVersion[0])

                            + String.valueOf(buildVersion[1])

                            + String.valueOf(buildVersion[2]);
                }
            });

        } finally {

            if(realm != null) {

                realm.close();
            }
        }
    }
}
