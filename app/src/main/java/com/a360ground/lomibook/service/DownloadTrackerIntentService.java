package com.a360ground.lomibook.service;

import android.app.DownloadManager;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.content.LocalBroadcastManager;

import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.downloadManager.LomiDownloadManager;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class DownloadTrackerIntentService extends IntentService {

    ArrayList<Long> downloadIds;
    private boolean destroyed = false;

    public DownloadTrackerIntentService() {

        super(DownloadTrackerIntentService.class.getName());
    }

    public static void startTrackerServiceByDownloadIds(Context context, ArrayList<Long> downloadIds) {

        Intent downloadTrackerIntent = new Intent(context, DownloadTrackerIntentService.class);

        downloadTrackerIntent.putExtra(LomiDownloadManager.DOWNLOAD_IDS_EXTRA_IDENTIFIER, downloadIds);

        context.startService(downloadTrackerIntent);
    }

    public static void startTrackerService(Context context, RealmResults<Product> products, boolean trackPreview) {

        ArrayList<Long> downloadIds = new ArrayList<>();

        for (Product product : products) {

            long downloadId, previewDownloadId;

            if ((downloadId = product.getDownloadId()) > 0) {

                downloadIds.add(downloadId);
            }

            if (trackPreview && (previewDownloadId = product.getPreviewDownloadId()) > 0) {

                downloadIds.add(previewDownloadId);
            }
        }

        Intent downloadTrackerIntent = new Intent(context, DownloadTrackerIntentService.class);

        downloadTrackerIntent.putExtra(LomiDownloadManager.DOWNLOAD_IDS_EXTRA_IDENTIFIER, downloadIds);

        context.startService(downloadTrackerIntent);
    }

    public static void stopTrackerService(Context context) {

        context.stopService(new Intent(context, DownloadTrackerIntentService.class));
    }

    @Override
    public void onCreate() {

        super.onCreate();

        if (destroyed) destroyed = false;

    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (!intent.hasExtra(LomiDownloadManager.DOWNLOAD_IDS_EXTRA_IDENTIFIER)) {

            throw new RuntimeException(DownloadTrackerIntentService.class.getName() + "You need to pass array of download ids to the service as intent");
        }

        ArrayList<Long> ids = (ArrayList<Long>) intent.getSerializableExtra(LomiDownloadManager.DOWNLOAD_IDS_EXTRA_IDENTIFIER);

        final boolean combine = intent.getBooleanExtra(LomiDownloadManager.COMBINE_DOWNLOAD_IDS_EXTRA_IDENTIFIER, false);

        if (combine && downloadIds != null) {

            downloadIds.addAll(ids);

        } else {

            downloadIds = ids;
        }

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            ArrayList<Long> toRemove = new ArrayList<>();

            while (!destroyed) {

                if (downloadIds.isEmpty()) break;

                for (long downloadId : downloadIds) {

                    DownloadManager dm = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

                    DownloadManager.Query q = new DownloadManager.Query();

                    q.setFilterById(downloadId);

                    Cursor cursor = dm.query(q);

                    if (cursor.moveToFirst()) {

                        final int bytesTotal = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));

                        final int bytesDownloaded = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));

                        final boolean preview = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_DESCRIPTION)).contains("preview");

                        String index = preview ? "previewDownloadId" : "downloadId";

                        final Product product = realm.where(Product.class).equalTo(index, downloadId).findFirst();

                        long productId = product != null ? product.getId() : -1;

                        switch (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))) {

                            case DownloadManager.STATUS_SUCCESSFUL: {

                                destroyed = true;

                                toRemove.add(downloadId);

                                String fileUrl = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI)).replace("file://", "");

                                sendDownloadSuccessLocalBroadcast((int) productId, fileUrl, preview);

                                break;
                            }

                            case DownloadManager.STATUS_RUNNING:

                                int progress = bytesDownloaded * 100 / bytesTotal;

                                sendDownloadProgressLocalBroadcast((int) productId, progress, preview);

                                break;

                            case DownloadManager.STATUS_PENDING:


                                break;

                            case DownloadManager.STATUS_PAUSED:

                                sendDownloadPausedLocalBroadcast((int) productId, preview);


                                break;

                            case DownloadManager.STATUS_FAILED:

                                destroyed = true;

                                int reason = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_REASON));

                                String message = "";

                                switch (reason) {

                                    case DownloadManager.ERROR_CANNOT_RESUME:

                                        message = "cannot resume";

                                        break;

                                    case DownloadManager.ERROR_DEVICE_NOT_FOUND:

                                        message = "device not found";

                                        break;

                                    case DownloadManager.ERROR_FILE_ALREADY_EXISTS:

                                        message = "file already exists";

                                        break;

                                    case DownloadManager.ERROR_FILE_ERROR:

                                        message = "file error";

                                        break;

                                    case DownloadManager.ERROR_HTTP_DATA_ERROR:

                                        message = "http data error";

                                        break;

                                    case DownloadManager.ERROR_INSUFFICIENT_SPACE:

                                        message = "insufficient space";

                                        break;

                                    case DownloadManager.ERROR_TOO_MANY_REDIRECTS:

                                        message = "too many redirects";

                                        break;

                                    case DownloadManager.ERROR_UNHANDLED_HTTP_CODE:

                                        message = "unhandled http";

                                        break;

                                    case DownloadManager.ERROR_UNKNOWN:

                                        message = "unknown error";

                                        break;

                                    case HttpURLConnection.HTTP_FORBIDDEN:

                                        message = "forbidden";

                                        break;
                                }

                                final String reasonMessage = message;

                                sendDownloadFailedLocalBroadcast((int) productId, reasonMessage, preview);

                                toRemove.add(downloadId);

                                break;
                        }

                        if (product != null) {

                            realm.executeTransaction(new Realm.Transaction() {

                                @Override
                                public void execute(Realm realm) {

                                    if (preview) {

                                        product.setPreviewBytesTotal(bytesTotal);

                                        product.setPreviewBytesDownloaded(bytesDownloaded);

                                    } else {

                                        product.setBytesTotal(bytesTotal);

                                        product.setBytesDownloaded(bytesDownloaded);
                                    }
                                }
                            });
                        }

                        if (!cursor.isClosed()) {

                            cursor.close();
                        }

                    } else {

                        toRemove.add(downloadId);
                    }
                }

                downloadIds.removeAll(toRemove);
            }
        } finally {

            if (realm != null) {

                realm.close();
            }
        }
    }

    private void sendDownloadPausedLocalBroadcast(int productId, boolean preview) {

        Intent intent = new Intent(LomiDownloadManager.LOMIDOWNLOAD_INTENT_FILTER);

        intent.putExtra(LomiDownloadManager.DOWNLOAD_STATUS_EXTRA_IDENTIFIER, preview ? LomiDownloadManager.PREVIEW_DOWNLOAD_PAUSED : LomiDownloadManager.DOWNLOAD_PAUSED);

        intent.putExtra(LomiDownloadManager.DOWNLOAD_PRODUCT_ID_EXTRA_IDENTIFIER, productId);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendDownloadFailedLocalBroadcast(int productId, String message, boolean preview) {

        Intent intent = new Intent(LomiDownloadManager.LOMIDOWNLOAD_INTENT_FILTER);

        intent.putExtra(LomiDownloadManager.DOWNLOAD_STATUS_EXTRA_IDENTIFIER, preview ? LomiDownloadManager.PREVIEW_DOWNLOAD_FAILURE : LomiDownloadManager.DOWNLOAD_FAILURE);

        intent.putExtra(LomiDownloadManager.DOWNLOAD_PRODUCT_ID_EXTRA_IDENTIFIER, productId);

        intent.putExtra(LomiDownloadManager.DOWNLOAD_MESSAGE_EXTRA_IDENTIFIER, message);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendDownloadProgressLocalBroadcast(int productId, int progress, boolean preview) {

        Intent intent = new Intent(LomiDownloadManager.LOMIDOWNLOAD_INTENT_FILTER);

        intent.putExtra(LomiDownloadManager.DOWNLOAD_STATUS_EXTRA_IDENTIFIER, preview ? LomiDownloadManager.PREVIEW_DOWNLOAD_PROGRESS : LomiDownloadManager.DOWNLOAD_PROGRESS);

        intent.putExtra(LomiDownloadManager.DOWNLOAD_PRODUCT_ID_EXTRA_IDENTIFIER, productId);

        intent.putExtra(LomiDownloadManager.DOWNLOAD_PROGRESS_EXTRA_IDENTIFIER, progress);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendDownloadSuccessLocalBroadcast(int productId, String fileUrl, boolean preview) {

        Intent intent = new Intent(LomiDownloadManager.LOMIDOWNLOAD_INTENT_FILTER);

        intent.putExtra(LomiDownloadManager.DOWNLOAD_STATUS_EXTRA_IDENTIFIER, preview ? LomiDownloadManager.PREVIEW_DOWNLOAD_SUCCESS : LomiDownloadManager.DOWNLOAD_SUCCESS);

        intent.putExtra(LomiDownloadManager.FILE_URL_EXTRA_IDENTIFIER, fileUrl);

        intent.putExtra(LomiDownloadManager.DOWNLOAD_PRODUCT_ID_EXTRA_IDENTIFIER, productId);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onDestroy() {

        destroyed = true;

        super.onDestroy();
    }
}
