package com.a360ground.lomibook.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;

import com.a360ground.lomibook.downloadManager.LomiDownloadManager;

public class DownloadTrackerBroadcastReceiver extends BroadcastReceiver {

    DownloadTrackingInfoInterface receiver;

    Context context;

    public DownloadTrackerBroadcastReceiver(DownloadTrackingInfoInterface receiver) {
        this.receiver = receiver;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;

        int productId = intent.getIntExtra(LomiDownloadManager.DOWNLOAD_PRODUCT_ID_EXTRA_IDENTIFIER, -1);

        int progress = intent.getIntExtra(LomiDownloadManager.DOWNLOAD_PROGRESS_EXTRA_IDENTIFIER, -1);

        String fileUrl = intent.getStringExtra(LomiDownloadManager.FILE_URL_EXTRA_IDENTIFIER);

        switch(intent.getIntExtra(LomiDownloadManager.DOWNLOAD_STATUS_EXTRA_IDENTIFIER, -1)) {

            case LomiDownloadManager.DOWNLOAD_STARTED:

                receiver.onDownloadStarted(productId);

                break;

            case LomiDownloadManager.PREVIEW_DOWNLOAD_STARTED:

                receiver.onPreviewDownloadStarted(productId);

                break;

            case LomiDownloadManager.DOWNLOAD_SUCCESS:

                receiver.onDownloadSuccess(productId, fileUrl);

                break;

            case LomiDownloadManager.PREVIEW_DOWNLOAD_SUCCESS:

                receiver.onPreviewDownloadSuccess(productId);

                break;

            case LomiDownloadManager.DOWNLOAD_PROGRESS:

                receiver.onDownloadProgress(productId, progress);

                break;

            case LomiDownloadManager.PREVIEW_DOWNLOAD_PROGRESS:

                receiver.onPreviewDownloadProgress(productId, progress);

                break;

            case LomiDownloadManager.DOWNLOAD_FAILURE:

                receiver.onDownloadFailure(productId);

                break;

            case LomiDownloadManager.PREVIEW_DOWNLOAD_FAILURE:

                receiver.onPreviewDownloadFailure(productId);

                break;

            case LomiDownloadManager.FAILED_BEFORE_DOWNLOAD_START:

                receiver.onFailedBeforeDownloadStarted(productId);

                break;

            case LomiDownloadManager.DOWNLOAD_PAUSED:

                receiver.onDownloadPaused(productId);

                break;

            case LomiDownloadManager.PREVIEW_DOWNLOAD_PAUSED:

                receiver.onPreviewDownloadPaused(productId);

                break;
        }
    }
}