package com.a360ground.lomibook.service;

import android.os.Bundle;

public interface Receiver {
    void onReceiveResult(int resultCode, Bundle resultData);
}