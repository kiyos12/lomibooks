package com.a360ground.lomibook.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.os.ResultReceiver;
import android.util.Log;

import java.io.IOException;

import io.realm.Realm;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AutoUpdateService extends IntentService {

    public static final String RECEIVER_TAG = "receiver-tag";

    public static final String EXIST_IN_DATABASE = "exists-in-database";

    public static final int UPDATE_JSON_RECEIVED = 0;

    public static final String UPDATE_JSON_CONTENT = "json-content";

    private static final String TAG = AutoUpdateService.class.getSimpleName();
    private String jsonContent = "";

    public AutoUpdateService() {
        super(AutoUpdateService.class.getSimpleName());
    }

    public static void startTrackerService(Context context) {

        Intent autoUpdateIntent = new Intent(context, AutoUpdateService.class);

        context.startService(autoUpdateIntent);

        Log.i(AutoUpdateService.class.getSimpleName(), "Auto Update service started");
    }

    public static void stopAutoUpdateService(Context context) {

        context.stopService(new Intent(context, AutoUpdateService.class));

        Log.i(AutoUpdateService.class.getSimpleName(), "Stopped Auto Update service");
    }

    @Override
    public void onCreate() {

        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        ResultReceiver receiver = intent.getParcelableExtra(RECEIVER_TAG);

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            Bundle b = new Bundle();

            String urlString = "";

            Request request = new Request.Builder().url(urlString).build();

            OkHttpClient okHttpClient = new OkHttpClient.Builder().build();

            Response response = okHttpClient.newCall(request).execute();

            String responsetxt = response.body().string();

            Log.d(TAG, "onHandleIntent: " + responsetxt);

            b.putString(UPDATE_JSON_CONTENT, responsetxt);
            // the intent service gets called twice for some reason
            // and receiver is null in the second null


            if (receiver != null) {

                receiver.send(UPDATE_JSON_RECEIVED, b);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            realm.close();
        }
    }
}
