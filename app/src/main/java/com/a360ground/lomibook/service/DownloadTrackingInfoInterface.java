package com.a360ground.lomibook.service;

public interface DownloadTrackingInfoInterface {
    void onDownloadStarted(int productId);
    void onPreviewDownloadStarted(int productId);
    void onDownloadSuccess(int productId, String fileUrl);
    void onPreviewDownloadSuccess(int productId);
    void onDownloadProgress(int productId, int progress);
    void onPreviewDownloadProgress(int productId, int progress);
    void onDownloadFailure(int productId);
    void onPreviewDownloadFailure(int productId);
    void onFailedBeforeDownloadStarted(int productId);
    void onDownloadPaused(int productId);
    void onPreviewDownloadPaused(int productId);
}
