package com.a360ground.lomibook.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.a360ground.lomibook.activity.MainActivity;
import com.a360ground.lomibook.R;
import com.a360ground.lomibook.utils.PreferenceHelper;

import java.util.List;

public class LanguageRecyclerAdapter extends RecyclerView.Adapter<LanguageRecyclerAdapter.CustomViewHolder> {

    private final Context context;

    private final List<PreferenceHelper.LomiLanguage> languages;

    public LanguageRecyclerAdapter(Context context, List<PreferenceHelper.LomiLanguage> languages) {

        this.context = context;

        this.languages = languages;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(this.context);

        View view = inflater.inflate(R.layout.language_list_item, parent, false);

        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, int position) {

        PreferenceHelper.LomiLanguage language = this.languages.get(position);

        String currentLanguage = PreferenceHelper.getLanguage(this.context);

        if(language != null) {

            holder.languageTextView.setText(language.getLanguage());

            if(language.getShortCode().equals(currentLanguage)) {

                holder.checkmarkImageView.setVisibility(View.VISIBLE);
            }
        }

        holder.view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                PreferenceHelper.setLocale(context, languages.get(holder.getAdapterPosition()).getShortCode());

                Intent intent = new Intent(context, MainActivity.class);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {

        return languages.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        private View view;

        private TextView languageTextView;

        private ImageView checkmarkImageView;

        public CustomViewHolder(View itemView) {

            super(itemView);

            itemView.setClickable(true);

            view = itemView;

            languageTextView = (TextView) itemView.findViewById(R.id.item);

            checkmarkImageView = (ImageView) itemView.findViewById(R.id.checkmark);
        }
    }
}
