package com.a360ground.lomibook.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.domain.algolia.TopSearches;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kiyos on 9/28/2017.
 */

public class TopSearchAdapter extends ArrayAdapter<TopSearches> {
    public TopSearchAdapter(Context context, List<TopSearches> topSearchesArrayList) {
        super(context, 0, topSearchesArrayList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        TopSearches topSearches = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.search_category_list_item, parent, false);
        }
        TextView itemTxt = (TextView) convertView.findViewById(R.id.item);
        // Populate the data into the template view using the data object
        itemTxt.setText(topSearches.getQuery());
        // Return the completed view to render on screen
        return convertView;
    }
}