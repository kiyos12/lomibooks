package com.a360ground.lomibook.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.activity.BookDetailsActivity;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.view.LomiProductView;

import java.util.List;

public class SearchResultsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Product> searchResults;

    private Context context;

    private int VIEW_PRODUCT = 0;

    private int VIEW_PROGRESS = 1;

    public SearchResultsRecyclerAdapter(final Context context, List<Product> data) {

        this.searchResults = data;

        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(context).inflate(R.layout.single_search_result, parent, false);
        return new SearchResultsRecyclerAdapter.CustomViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof CustomViewHolder) {
            final Product product = searchResults.get(position);

            CustomViewHolder viewHolder = (CustomViewHolder) holder;

            viewHolder.productView.setProduct(product);

            viewHolder.bullet.setText(String.format(context.getString(R.string.bullet), position + 1));

            holder.itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, BookDetailsActivity.class);

                    intent.putExtra(Constants.PRODUCT_ID_IDENTIFIER, product.getId());

                    context.startActivity(intent);
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return searchResults.size();
    }

    private class CustomViewHolder extends RecyclerView.ViewHolder {

        LomiProductView productView;

        TextView bullet;

        private CustomViewHolder(View itemView) {

            super(itemView);

            productView = (LomiProductView) itemView.findViewById(R.id.product_view);

            bullet = (TextView) itemView.findViewById(R.id.bullet);
        }
    }

    private class CustomViewHolder2 extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        private CustomViewHolder2(View itemView) {

            super(itemView);

            progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar_below);

        }
    }
}
