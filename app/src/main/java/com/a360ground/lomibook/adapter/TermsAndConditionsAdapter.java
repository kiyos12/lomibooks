package com.a360ground.lomibook.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.a360ground.lomibook.R;

import java.util.ArrayList;

public class TermsAndConditionsAdapter extends RecyclerView.Adapter<TermsAndConditionsAdapter.ViewHolder> {

    private ArrayList<String> indices;

    private ArrayList<String> articles;

    Context context;

    public TermsAndConditionsAdapter(Context context, ArrayList<String> indices, ArrayList<String> articles) {

        if(indices.size() != articles.size()) {

            throw new IllegalArgumentException("Size of indices must equal size of articles");
        }

        this.context = context;

        this.indices = indices;

        this.articles = articles;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.terms_and_conditions_recycler_single, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String index = indices.get(position);

        String article = articles.get(position);

        holder.headerTextView.setText(index);

        holder.contentTextView.setText(article);
    }

    @Override
    public int getItemCount() {

        return articles.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView headerTextView;

        TextView contentTextView;

        public ViewHolder(View itemView) {

            super(itemView);

            headerTextView = (TextView) itemView.findViewById(R.id.header);

            contentTextView = (TextView) itemView.findViewById(R.id.content);
        }
    }
}
