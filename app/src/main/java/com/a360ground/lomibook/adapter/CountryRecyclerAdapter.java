package com.a360ground.lomibook.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.a360ground.lomibook.R;
import com.a360ground.lomibook.utils.PreferenceHelper;

import java.util.List;
import java.util.Locale;

public class CountryRecyclerAdapter extends RecyclerView.Adapter<CountryRecyclerAdapter.CustomViewHolder> {

    public static final String COUNTRY_EXTRA = "country";

    private final Context context;

    private final List<String> countryCodes;

    public CountryRecyclerAdapter(Context context, List<String> countryCodes) {

        this.context = context;

        this.countryCodes = countryCodes;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(this.context);

        View view = inflater.inflate(R.layout.language_list_item, parent, false);

        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {

        final String countryCode = this.countryCodes.get(position);

        final String country = (new Locale("", countryCode)).getDisplayCountry();

        holder.countryTextView.setText(country);

        holder.view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String currentCountryCode = PreferenceHelper.getCountryCode(context);

                if(currentCountryCode != null && !currentCountryCode.equals(countryCode)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);

                    builder.setTitle(context.getString(R.string.confirm_label));

                    builder.setMessage(context.getString(R.string.confirm_change_country_text));

                    builder.setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            PreferenceHelper.setCountryCode(context, countryCode);

                            Intent returnIntent = new Intent();

                            returnIntent.putExtra(COUNTRY_EXTRA, country);

                            ((Activity) context).setResult(Activity.RESULT_OK, returnIntent);

                            ((Activity) context).finish();
                        }
                    });

                    builder.setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            ((Activity) context).finish();
                        }
                    });

                    builder.setIcon(android.R.drawable.ic_dialog_alert);

                    builder.show();

                } else {

                    PreferenceHelper.setCountryCode(context, countryCode);

                    Intent returnIntent = new Intent();

                    returnIntent.putExtra(COUNTRY_EXTRA, country);

                    ((Activity) context).setResult(Activity.RESULT_OK, returnIntent);

                    ((Activity) context).finish();
                }
            }
        });
    }

    @Override
    public int getItemCount() {

        return countryCodes.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        private View view;

        private TextView countryTextView;

        private ImageView checkmarkImageView;

        public CustomViewHolder(View itemView) {

            super(itemView);

            itemView.setClickable(true);

            view = itemView;

            countryTextView = (TextView) itemView.findViewById(R.id.item);

            checkmarkImageView = (ImageView) itemView.findViewById(R.id.checkmark);
        }
    }
}
