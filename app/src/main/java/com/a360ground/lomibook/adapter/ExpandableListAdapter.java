package com.a360ground.lomibook.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import com.a360ground.lomibook.R;
import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;

    private List<String> listDataHeader;

    private HashMap<String, String> listDataChild;

    public ExpandableListAdapter(Context context, List<String> listDataHeader, HashMap<String, String> listDataChild) {

        this.context = context;

        this.listDataHeader = listDataHeader;

        this.listDataChild = listDataChild;
    }

    @Override
    public int getGroupCount() {

        return this.listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        return 1;
    }

    @Override
    public String getGroup(int groupPosition) {

        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public String getChild(int groupPosition, int childPosition) {

        return this.listDataChild.get(this.listDataHeader.get(groupPosition));
    }

    @Override
    public long getGroupId(int groupPosition) {

        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {

        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        String headerTitle = getGroup(groupPosition);

        if(convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.help_and_feedback_list_item, null);
        }

        TextView listHeader = (TextView) convertView.findViewById(R.id.title);

        listHeader.setTypeface(null, Typeface.BOLD);

        listHeader.setText(headerTitle);

        if(isExpanded) {

            listHeader.setTextColor(context.getResources().getColor(R.color.splash_blue));

        } else {

            listHeader.setTextColor(context.getResources().getColor(R.color.text_color));
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = getChild(groupPosition, childPosition);

        if(convertView == null) {

            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.expandable_list_item, null);
        }

        TextView textListChild = (TextView) convertView.findViewById(R.id.list_item);

        textListChild.setText(childText);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
