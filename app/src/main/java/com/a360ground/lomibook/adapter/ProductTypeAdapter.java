package com.a360ground.lomibook.adapter;

import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.utils.Constants;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ProductTypeAdapter extends CustomizedTypeAdapterFactory<Product> {

    public static final String ATTRIBUTES_IDENTIFIER = "attributes";

    public static final String ATTRIBUTE_NAME_IDENTIFIER = "name";

    public static final String ATTRIBUTE_OPTIONS_IDENTIFIER = "options";

    public static final String ATTRIBUTE_OPTION_IDENTIFIER = "option";

    public static final String ATTRIBUTE_PUBLICATION_YEAR_IDENTIFIER = "publication-year";

    public static final String ATTRIBUTE_PUBLISHER_IDENTIFIER = "publisher";

    public static final String ATTRIBUTE_ISBN_IDENTIFIER = "isbn";

    public static final String DOLLAR_IDENTIFIER = "usd";

    public static final String BIRR_IDENTIFIER = "etb";

    public static final String CURRENCY_IDENTIFIER = "currency";

    public static final String DOLLAR_PRICE_IDENTIFIER = "dollar-price";

    public static final String PRICE_IDENTIFIER = "price";

    public static final String VARIATION_IDENTIFIER = "variations";

    private static final String DOWNLOADS_IDENTIFIER = "downloads";

    public static final String DOWNLOADABLE_VARIATION_ID = "downloadable_variation_id";

    public static final String PRODUCT_ID_IDENTIFIER = "id";

    public static final String VARIATION_ID_IDENTIFIER = "variation_id";

    public static final String DOWNLOAD_URL_IDENTIFIER = "download_url";

    public static final String SAMPLE_DOWNLOAD_URL_IDENTIFIER = "sample_download_url";

    private static final String DOWNLOAD_FILE_IDENTIFIER = "file";

    public static final String USD_VARIATION_ID_IDENTIFIER = "usd_variation";

    public static final String ETB_VARIATION_ID_IDENTIFIER = "etb_variation";

    public ProductTypeAdapter() {
        super(Product.class);
    }

    @Override
    protected void afterRead(JsonElement deserialized) {

        JsonObject productJsonObject = deserialized.getAsJsonObject();

        JsonElement attributesElement = productJsonObject.get(ATTRIBUTES_IDENTIFIER);

        JsonElement priceElement = productJsonObject.get(PRICE_IDENTIFIER);

        JsonElement variationElement = productJsonObject.get(VARIATION_IDENTIFIER);

        if(attributesElement != null) {

            JsonArray attributes = attributesElement.getAsJsonArray();

            for(JsonElement a : attributes) {

                JsonObject attribute = a.getAsJsonObject();

                switch (attribute.get(ATTRIBUTE_NAME_IDENTIFIER).getAsString()) {

                    case ATTRIBUTE_PUBLICATION_YEAR_IDENTIFIER:

                        String publicationYear = attribute.get(ATTRIBUTE_OPTIONS_IDENTIFIER).getAsJsonArray().get(0).getAsString();

                        productJsonObject.addProperty(ATTRIBUTE_PUBLICATION_YEAR_IDENTIFIER, publicationYear);

                        break;

                    case ATTRIBUTE_ISBN_IDENTIFIER:

                        String isbn = attribute.get(ATTRIBUTE_OPTIONS_IDENTIFIER).getAsJsonArray().get(0).getAsString();

                        productJsonObject.addProperty(ATTRIBUTE_ISBN_IDENTIFIER, isbn);

                        break;

                    case ATTRIBUTE_PUBLISHER_IDENTIFIER:

                        String publisher = attribute.get(ATTRIBUTE_OPTIONS_IDENTIFIER).getAsJsonArray().get(0).getAsString();

                        productJsonObject.addProperty(ATTRIBUTE_PUBLISHER_IDENTIFIER, publisher);

                        break;
                }
            }
        }

        if(variationElement != null && variationElement.isJsonArray()) {

            JsonArray variationArray = variationElement.getAsJsonArray();

            JsonArray variationIds = new JsonArray();

            for(int i = 0; i < variationArray.size(); i++) {

                JsonObject variableProduct = variationArray.get(i).getAsJsonObject();

                JsonArray variableProductAttributes = variableProduct.get(ATTRIBUTES_IDENTIFIER).getAsJsonArray();

                JsonObject variationId = new JsonObject();

                variationId.addProperty(VARIATION_ID_IDENTIFIER, variableProduct.get(PRODUCT_ID_IDENTIFIER).getAsInt());

                variationIds.add(variationId);

                for(int j = 0; j < variableProductAttributes.size(); j++) {

                    JsonObject attribute = variableProductAttributes.get(j).getAsJsonObject();

                    if (attribute.get(ATTRIBUTE_NAME_IDENTIFIER).getAsString().equals(CURRENCY_IDENTIFIER)) {

                        String currency = attribute.get(ATTRIBUTE_OPTION_IDENTIFIER).getAsString();

                        long price = variableProduct.get(PRICE_IDENTIFIER).getAsString().equals("")?0:variableProduct.get(PRICE_IDENTIFIER).getAsLong();

                        switch (currency) {

                            case DOLLAR_IDENTIFIER:

                                try {

                                    if (productJsonObject.has(DOLLAR_PRICE_IDENTIFIER)) {

                                        productJsonObject.remove(DOLLAR_PRICE_IDENTIFIER);
                                    }

                                    productJsonObject.addProperty(DOLLAR_PRICE_IDENTIFIER, price);

                                } catch (NumberFormatException e) {

                                    productJsonObject.addProperty(DOLLAR_PRICE_IDENTIFIER, 0);
                                }

                                if(productJsonObject.has(USD_VARIATION_ID_IDENTIFIER)) {

                                    productJsonObject.remove(USD_VARIATION_ID_IDENTIFIER);

                                } else {

                                    productJsonObject.addProperty(USD_VARIATION_ID_IDENTIFIER, variableProduct.get(PRODUCT_ID_IDENTIFIER).getAsInt());
                                }

                                break;

                            case BIRR_IDENTIFIER:

                                try {

                                    if (productJsonObject.has(PRICE_IDENTIFIER)) {

                                        productJsonObject.remove(PRICE_IDENTIFIER);
                                    }

                                    productJsonObject.addProperty(PRICE_IDENTIFIER, price);

                                } catch (NumberFormatException e) {

                                    productJsonObject.addProperty(PRICE_IDENTIFIER, 0);
                                }

                                if(productJsonObject.has(ETB_VARIATION_ID_IDENTIFIER)) {

                                    productJsonObject.remove(ETB_VARIATION_ID_IDENTIFIER);

                                } else {

                                    productJsonObject.addProperty(ETB_VARIATION_ID_IDENTIFIER, variableProduct.get(PRODUCT_ID_IDENTIFIER).getAsInt());
                                }

                                break;
                        }
                    }
                }

                JsonArray variableProductDownloads = variableProduct.get(DOWNLOADS_IDENTIFIER).getAsJsonArray();

                if(variableProductDownloads != null && variableProductDownloads.size() > 0) {

                    if(productJsonObject.has(DOWNLOADS_IDENTIFIER)) {

                        productJsonObject.remove(DOWNLOADS_IDENTIFIER);
                    }

                    for(JsonElement variableDownload : variableProductDownloads) {

                        JsonObject o = variableDownload.getAsJsonObject();

                        String filePath = o.get(DOWNLOAD_FILE_IDENTIFIER).getAsString();

                        if(filePath.contains(Constants.SAMPLE_TEXT)) {

                            if(productJsonObject.has(SAMPLE_DOWNLOAD_URL_IDENTIFIER)) {

                                productJsonObject.remove(SAMPLE_DOWNLOAD_URL_IDENTIFIER);
                            }

                            productJsonObject.addProperty(SAMPLE_DOWNLOAD_URL_IDENTIFIER, filePath);

                        } else {

                            if(productJsonObject.has(DOWNLOAD_URL_IDENTIFIER)) {

                                productJsonObject.remove(DOWNLOAD_URL_IDENTIFIER);
                            }

                            productJsonObject.addProperty(DOWNLOAD_URL_IDENTIFIER, filePath);
                        }
                    }

                    productJsonObject.add(DOWNLOADABLE_VARIATION_ID, variableProduct.get(PRODUCT_ID_IDENTIFIER));
                }
                else{
                    productJsonObject.add(DOWNLOADABLE_VARIATION_ID, null);
                }

            }
        }

        /*if(priceElement == null) {

            productJsonObject.remove(PRICE_IDENTIFIER);

            productJsonObject.addProperty(PRICE_IDENTIFIER, 0);

        } else {

            try {

                priceElement.getAsDouble();

            } catch (NumberFormatException e) {

                productJsonObject.remove(PRICE_IDENTIFIER);

                productJsonObject.addProperty(PRICE_IDENTIFIER, 0);
            }
        }*/

        productJsonObject.remove(VARIATION_IDENTIFIER);
    }
}
