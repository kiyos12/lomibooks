package com.a360ground.lomibook.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.domain.woocommerce.Product;

import java.util.List;

public class SellRecyclerAdapter extends RecyclerView.Adapter<SellRecyclerAdapter.SellViewHolder> {

    List<Product> productList;

    private boolean international;

    public SellRecyclerAdapter(List<Product> productList, boolean international) {

        this.productList = productList;

        this.international = international;
    }

    @Override
    public SellViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_sell_view, parent, false);

        return new SellViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SellViewHolder holder, int position) {

        Product product = productList.get(position);

        holder.bookTitleInSell.setText(product.getName());

        holder.bookTotalSell.setText(international ? "" + product.getTotalUsdSales() : "" + product.getTotalEtbSales());
    }

    @Override
    public int getItemCount() {
        return this.productList.size();
    }

    class SellViewHolder extends RecyclerView.ViewHolder {

        TextView bookTitleInSell;

        TextView bookTotalSell;

        public SellViewHolder(View itemView) {

            super(itemView);

            bookTitleInSell = (TextView) itemView.findViewById(R.id.title);

            bookTotalSell = (TextView) itemView.findViewById(R.id.total_sales);
        }
    }

    public void setInternational(boolean international) {
        this.international = international;
    }
}
