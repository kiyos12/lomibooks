package com.a360ground.lomibook.adapter;

import com.a360ground.lomibook.domain.woocommerce.Image;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class ImageDeserilizer implements JsonDeserializer<Image> {

    @Override
    public Image deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        Image image = null;

        if(json.isJsonArray()) {
            // image is empty
        } else if(json.isJsonObject()) {

            JsonObject jsonObject = json.getAsJsonObject();

            return new Gson().fromJson(jsonObject, Image.class);

            // The line below causes recursive loop. I am keeping it for documentation purposes
            // The line above is the solution. src: http://stackoverflow.com/a/33889122/4688650
            // image = context.deserialize(json.getAsJsonObject(), Image.class);
        }

        return image;
    }
}
