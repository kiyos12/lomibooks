package com.a360ground.lomibook.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.application.LomiApplication;
import com.a360ground.lomibook.domain.woocommerce.Category;
import com.a360ground.lomibook.domain.woocommerce.Post;
import com.a360ground.lomibook.utils.PreferenceHelper;
import com.a360ground.lomibook.view.CarouselViewListener;
import com.a360ground.lomibook.view.LomiCategoryView;
import com.synnapps.carouselview.CarouselView;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class HomeCategoryRecyclerAdapter extends RealmRecyclerViewAdapter<Category, RecyclerView.ViewHolder> implements PreferenceHelper.OnLocaleChangedListener {

    private static final int TYPE_HEADER = 0;

    private static final int TYPE_ITEM = 1;

    RealmResults<Post> ads;

    Context context;

    LomiApplication application;

    private String TAG = HomeCategoryRecyclerAdapter.class.getSimpleName();

    public HomeCategoryRecyclerAdapter(final Context context, final RealmResults<Category> data) {

        super(data, true);

        this.context = context;

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            ads = realm.where(Post.class).findAll();

            ads.addChangeListener(new RealmChangeListener<RealmResults<Post>>() {

                @Override
                public void onChange(RealmResults<Post> element) {

                    notifyItemChanged(1);
                }
            });

        } finally {

        }

        application = (LomiApplication) context.getApplicationContext();

        PreferenceHelper.addListener(this);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {

            case TYPE_ITEM: {

                View view = LayoutInflater.from(context).inflate(R.layout.home_category_recycler_single_row, parent, false);

                return new CustomItemHolder(view);
            }

            case TYPE_HEADER: {

                View view = LayoutInflater.from(context).inflate(R.layout.header_fragment_home, parent, false);

                return new CarouselHolder(view);
            }

        }

        throw new RuntimeException("There is no type that matches the type: " + viewType + ". Make sure you are using the types correctly.");
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0) {

            return TYPE_HEADER;
        }

        return TYPE_ITEM;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof CustomItemHolder) {

            final CustomItemHolder customItemHolder = (CustomItemHolder) holder;

            final Category category = getData().get(position - 1);

           /* if(position -1 == 0){
                customItemHolder.categoryView.setCategory(category, true);
            }
            else{*/
            customItemHolder.categoryView.setCategory(category, false);


        } else if (holder instanceof CarouselHolder) {

            final CarouselHolder carouselHolder = (CarouselHolder) holder;

            carouselHolder.carouselView.setPageCount(ads.size());

            carouselHolder.carouselView.setViewListener(new CarouselViewListener(context, ads));
        }
    }

    @Override
    public int getItemCount() {

        return super.getItemCount() + 1;
    }

    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {

        super.onViewDetachedFromWindow(holder);
    }

    @Override
    public void localeChanged(String language) {
        Log.d(TAG, "Locale changed in HomeCategoryRecyclerAdapter");
    }

    public class CustomItemHolder extends RecyclerView.ViewHolder {

        LomiCategoryView categoryView;

        public CustomItemHolder(View view) {

            super(view);

            categoryView = (LomiCategoryView) view.findViewById(R.id.category_view);
        }
    }

    public class CarouselHolder extends RecyclerView.ViewHolder {

        View customView;

        CarouselView carouselView;

        public CarouselHolder(View view) {

            super(view);

            customView = view;

            carouselView = (CarouselView) view.findViewById(R.id.slider);
        }
    }
}