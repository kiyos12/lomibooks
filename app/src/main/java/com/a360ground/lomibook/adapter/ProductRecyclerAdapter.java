package com.a360ground.lomibook.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.a360ground.lomibook.application.LomiApplication;
import com.a360ground.lomibook.R;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.view.LomiProductView;

import io.realm.RealmResults;

public class ProductRecyclerAdapter extends RealmRecyclerViewAdapter<Product, ProductRecyclerAdapter.RecommendedRecyclerViewHolder> {

    Context mContext;

    LomiApplication application;

    public ProductRecyclerAdapter(Context mContext, RealmResults<Product> data) {

        super(data, false);

        this.mContext = mContext;

        application = (LomiApplication) mContext.getApplicationContext ();
    }

    @Override
    public RecommendedRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from (mContext).inflate (R.layout.single_book_in_recycler, parent, false);

        return new RecommendedRecyclerViewHolder (view);
    }

    @Override
    public void onBindViewHolder(RecommendedRecyclerViewHolder holder, int position) {

        final Product product = getData().get(position);

        holder.productView.setProduct(product);

//        Log.d(ProductRecyclerAdapter.class.getSimpleName(), "The book author is: " + product.getAuthor());
    }

    class RecommendedRecyclerViewHolder extends RecyclerView.ViewHolder {

        LomiProductView productView;

        public RecommendedRecyclerViewHolder(View view) {

            super (view);

            productView = (LomiProductView) view.findViewById(R.id.product_view);
        }
    }
}