package com.a360ground.lomibook.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.a360ground.lomibook.application.LomiApplication;
import com.a360ground.lomibook.R;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.view.LomiProductView;

import java.util.List;

public class MyBooksAdapter extends BaseAdapter {

    LayoutInflater inflater;

    LomiApplication application;

    private Context context;

    private List<Product> products;

    public MyBooksAdapter(Context context, List<Product> products) {

        this.context = context;

        this.products = products;

        application = (LomiApplication) context.getApplicationContext ();
    }

    @Override
    public Product getItem(int position) {
        return products.get (position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return products.size ();
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        inflater = (LayoutInflater) this.context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);

        if (view == null) {

            final Product product = products.get (position);

            view = inflater.inflate (R.layout.my_books_single_item, null);

            LomiProductView productView = (LomiProductView) view.findViewById(R.id.product_view);

            productView.setProduct(product);
        }

        return view;
    }
}
