package com.a360ground.lomibook.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.a360ground.lomibook.fragment.Intro1;

/**
 * Created by Kiyos on 3/15/2017.
 */

public class IntroFragmentPageAdapter extends FragmentPagerAdapter{


    public IntroFragmentPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        if(position == 0){
            fragment = new Intro1();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
