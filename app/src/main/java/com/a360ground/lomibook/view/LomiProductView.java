package com.a360ground.lomibook.view;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.activity.BookDetailsActivity;
import com.a360ground.lomibook.domain.woocommerce.Author;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.utils.DimensionUtils;
import com.a360ground.lomibook.utils.LomiFileUtils;
import com.a360ground.lomibook.utils.LomiReaderUtills;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import io.realm.Realm;

public class LomiProductView extends RelativeLayout {

    @IdRes
    private static final int THUMBNAIL_ID = 1;

    @IdRes
    private static final int TITLE_ID = 2;

    @IdRes
    private static final int AUTHOR_ID = 3;

    @IdRes
    private static final int CATEGORY_ID = 4;

    @IdRes
    private static final int PUBLICATION_YEAR_ID = 5;

    boolean bookDetail;
    private Product product;
    private ImageView thumbnail;
    private TextView titleTextView;
    private TextView authorTextView;
    private TextView categoryTextView;
    private TextView publicationYearTextView;
    private Context context;

    public LomiProductView(Context context) {
        this(context, null);
    }

    public LomiProductView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LomiProductView(final Context context, AttributeSet attrs, int defStyleAttr) {

        super(context, attrs, defStyleAttr);

        this.context = context;

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.LomiProductView, defStyleAttr, 0);

        boolean floatImage = a.getBoolean(R.styleable.LomiProductView_floatImage, false);

        boolean showExtra = a.getBoolean(R.styleable.LomiProductView_showExtra, false);

        boolean hideDetails = a.getBoolean(R.styleable.LomiProductView_hideDetails, false);

        bookDetail = a.getBoolean(R.styleable.LomiProductView_bookdetail, false);

        final boolean openBook = a.getBoolean(R.styleable.LomiProductView_openBook, false);

        boolean clickable = a.getBoolean(R.styleable.LomiProductView_clickable, true);

        RelativeLayout container = new RelativeLayout(context);

        LayoutParams rootLayoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        thumbnail = new ImageView(context);

        thumbnail.setId(THUMBNAIL_ID);

        setPadding(0, 0, 0, DimensionUtils.dpToPx(10));

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            final Customer customer = realm.where(Customer.class).findFirst();

            if (clickable) {

                thumbnail.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        try {
                            Bundle b = null;

                            b = ActivityOptions.makeThumbnailScaleUpAnimation(v, ((BitmapDrawable) thumbnail.getDrawable()).getBitmap(), 0, 0).toBundle();

                            if (openBook && customer.getBooks().contains(product) && product.fileExistsLocally(false)) {

                                /*Intent intent = new Intent(context, ReaderActivity.class);

                                intent.putExtra(TAGS.BOOK_COVER_PATH, product.getThumbnailSrc());

                                intent.putExtra(TAGS.BOOKNAME, product.getName());

                                Author author = product.getAuthor();

                                if (author != null) {

                                    intent.putExtra(TAGS.BOOK_AUTHOR_NAME, author.getName());
                                }

                                intent.putExtra(TAGS.FILE_PATH, );

                                if (b != null) {
                                    context.startActivity(intent, b);
                                } else {
                                    context.startActivity(intent);
                                }*/

                                LomiReaderUtills.openBookEvent(context, LomiFileUtils.LOMI_FOLDER_PATH + File.separator + product.getId() + Constants.LOMI_FILE_EXTENSION, false);



                            } else {

                                Intent intent = new Intent(context, BookDetailsActivity.class);

                                intent.putExtra(Constants.PRODUCT_ID_IDENTIFIER, product.getId());

                                if (b != null) {
                                    context.startActivity(intent, b);
                                } else {
                                    context.startActivity(intent);
                                }
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                            ExtendedToast.showToast(context, ExtendedToast.FAILED, "There is an error");
                        }
                    }
                });
            }

        } finally {

            if (realm != null) {

                realm.close();
            }
        }

        int imageWidth = DimensionUtils.dpToPx(100);

        int imageHeight = DimensionUtils.dpToPx(110);

        LayoutParams thumbnailParams = new LayoutParams(imageWidth, imageHeight);

        titleTextView = new TextView(context);

        titleTextView.setId(TITLE_ID);

        RelativeLayout.LayoutParams titleParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        if (showExtra) titleParams.width = LayoutParams.WRAP_CONTENT;

        titleTextView.setTextSize(12);

        titleTextView.setTextColor(ContextCompat.getColor(context, R.color.black));

        titleTextView.setMaxLines(2);

        RelativeLayout.LayoutParams authorParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        authorParams.addRule(RelativeLayout.BELOW, TITLE_ID);

        authorTextView = new TextView(context);

        authorTextView.setId(AUTHOR_ID);

        authorTextView.setTextSize(10);

        authorTextView.setTextColor(ContextCompat.getColor(context, R.color.gray_text));

        authorTextView.setMaxLines(1);

        authorTextView.setMaxWidth(imageWidth);

        categoryTextView = new TextView(context);

        categoryTextView.setId(CATEGORY_ID);

        categoryTextView.setTextColor(ContextCompat.getColor(context, R.color.gray_text));

        categoryTextView.setTextSize(11);

        categoryTextView.setMaxLines(1);

        RelativeLayout.LayoutParams categoryParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        categoryParams.addRule(RelativeLayout.BELOW, AUTHOR_ID);

        publicationYearTextView = new TextView(context);

        publicationYearTextView.setId(PUBLICATION_YEAR_ID);

        publicationYearTextView.setTextColor(ContextCompat.getColor(context, R.color.gray_text));

        publicationYearTextView.setMaxLines(1);

        publicationYearTextView.setTextSize(11);

        RelativeLayout.LayoutParams publicationYearParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        publicationYearParams.addRule(RelativeLayout.BELOW, CATEGORY_ID);

        RelativeLayout textContainer = new RelativeLayout(context);

        LayoutParams textContainerParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        int margin = DimensionUtils.dpToPx(5);

        if (floatImage) {

            titleTextView.setGravity(Gravity.LEFT);

            textContainerParams.addRule(RelativeLayout.CENTER_VERTICAL);

            textContainerParams.setMargins(margin, 0, 0, 0);

            thumbnailParams.width = DimensionUtils.dpToPx(70);

            thumbnailParams.height = DimensionUtils.dpToPx(104);

            textContainerParams.addRule(RelativeLayout.RIGHT_OF, THUMBNAIL_ID);

            authorParams.addRule(RelativeLayout.RIGHT_OF, THUMBNAIL_ID);

            categoryParams.addRule(RelativeLayout.RIGHT_OF, THUMBNAIL_ID);

            publicationYearParams.addRule(RelativeLayout.RIGHT_OF, THUMBNAIL_ID);

        } else if (bookDetail) {

            thumbnailParams.width = DimensionUtils.dpToPx(120);

            thumbnailParams.height = DimensionUtils.dpToPx(150);

            thumbnailParams.topMargin = DimensionUtils.dpToPx(40);

            thumbnailParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

            thumbnailParams.bottomMargin = DimensionUtils.dpToPx(20);

            titleTextView.setTypeface(null, Typeface.BOLD);

            textContainerParams.addRule(RelativeLayout.BELOW, THUMBNAIL_ID);

            textContainerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

            titleParams.addRule(RelativeLayout.BELOW, THUMBNAIL_ID);

            titleParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

            titleTextView.setTextSize(22);

            authorTextView.setTextColor(ContextCompat.getColor(context, R.color.black));

            authorTextView.setTextSize(13);

            authorTextView.setTypeface(null, Typeface.BOLD);

            titleTextView.setGravity(Gravity.CENTER_HORIZONTAL);

            authorTextView.setTextSize(15);

            authorParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

            publicationYearParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        } else {

            thumbnailParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

            textContainerParams.addRule(RelativeLayout.BELOW, THUMBNAIL_ID);

            textContainerParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

            titleParams.addRule(RelativeLayout.BELOW, THUMBNAIL_ID);

            titleParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

            authorParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

            publicationYearParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        }


        container.addView(thumbnail, thumbnailParams);

        if (!hideDetails) {

            textContainer.addView(titleTextView, titleParams);

            textContainer.addView(authorTextView, authorParams);

            if (showExtra) {

                textContainer.addView(categoryTextView, categoryParams);
            }

            container.addView(textContainer, textContainerParams);
        }

        if (hideDetails) {

            setPadding(0, DimensionUtils.dpToPx(5), 0, 0);
        }
        addView(container, rootLayoutParams);

        a.recycle();
    }


    public void setProduct(final Product p) {

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            product = p;

            if (product != null) {

                titleTextView.setText(product.getName());

                Author author = product.getAuthor();

                if (author != null) {

                    authorTextView.setText(author.getName());
                }

                publicationYearTextView.setText(product.getPublicationYear());

                categoryTextView.setText(product.getCategory());

                if (new File(context.getDir("image", Context.MODE_PRIVATE).getAbsolutePath() + File.separator + product.getId() + ".jpg").exists()) {

                    Picasso.with(context).load(new File(context.getDir("image", Context.MODE_PRIVATE).getAbsolutePath() + File.separator + product.getId() + ".jpg"))

                            .placeholder(R.drawable.placeholder)

                            .error(R.drawable.placeholder)

                            .resize(100, 135)

                            .into(thumbnail);

                } else if (!product.getThumbnailSrc().isEmpty()) {
                    Picasso.with(context)

                            .load(product.getThumbnailSrc())

                            .placeholder(R.drawable.placeholder)

                            .error(R.drawable.placeholder)

                            .resize(100, 135)

                            .into(new Target() {

                                @Override
                                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {

                                    thumbnail.setImageBitmap(bitmap);

                                    try {

                                        FileOutputStream fileOutputStream = new FileOutputStream(new File(context.getDir("image", Context.MODE_PRIVATE).getAbsolutePath() + File.separator + product.getId() + ".jpg"));

                                        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, fileOutputStream);

                                    } catch (FileNotFoundException e) {

                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onPrepareLoad(Drawable placeHolderDrawable) {
                                }

                                @Override
                                public void onBitmapFailed(Drawable errorDrawable) {
                                }
                            });
                }
                if (bookDetail) {

                    Picasso.with(context).load(new File(context.getDir("image", Context.MODE_PRIVATE).getAbsolutePath() + File.separator + product.getId() + ".jpg"))

                            .transform(new BlurTransformation(context, 10))

                            .into(new Target() {

                                @Override
                                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                                    setBackground(new BitmapDrawable(getResources(), bitmap));

                                    getBackground().setAlpha(99);
                                }

                                @Override
                                public void onBitmapFailed(Drawable errorDrawable) {

                                }

                                @Override
                                public void onPrepareLoad(Drawable placeHolderDrawable) {

                                }
                            });
                }
            }

        } finally {

            if (realm != null) {

                realm.close();
            }
        }
    }
}
