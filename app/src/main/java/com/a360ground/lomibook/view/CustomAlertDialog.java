package com.a360ground.lomibook.view;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.a360ground.lomibook.BuildConfig;
import com.a360ground.lomibook.R;
import com.a360ground.lomibook.domain.woocommerce.Apk;
import com.a360ground.lomibook.service.DownloadTrackingInfoInterface;
import com.a360ground.lomibook.utils.LomiFileUtils;
import com.a360ground.lomibook.utils.PreferenceHelper;

import java.io.File;
import java.util.ArrayList;

public class CustomAlertDialog extends DialogFragment implements DownloadTrackingInfoInterface {

    private static final String UPDATE_VERSION = "updateVersion";
    private static String versionName;
    private static String title;
    private static String content;
    private static String updateType;
    private TextView messageTextView;
    private Button updateButton;
    private Button cancelButton;

    private Button remindMeLaterButton;

    private DownloadManager dm;

    private ArrayList<Long> downloadIds = new ArrayList<>();

    public CustomAlertDialog() {
    }

    public static CustomAlertDialog newInstance(int[] version) {

        Bundle args = new Bundle();

        args.putIntArray(UPDATE_VERSION, version);

        CustomAlertDialog fragment = new CustomAlertDialog();

        fragment.setArguments(args);

        fragment.setCancelable(false);

        return fragment;
    }

    public static CustomAlertDialog updateInstance(Apk apk) {

        Bundle args = new Bundle();

        title = apk.getTitle().getRendered();

        updateType = apk.getMeta_box().getUpdate_type();

        content = apk.getContent().getRendered();

        versionName = apk.getMeta_box().getLomi_vcode();

        CustomAlertDialog fragment = new CustomAlertDialog();

        fragment.setArguments(args);

        fragment.setCancelable(false);

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.update_dialogue, null);

        TextView update_title = (TextView) view.findViewById(R.id.update_title);

        WebView update_desc = (WebView) view.findViewById(R.id.update_desc);

        TextView update_type = (TextView) view.findViewById(R.id.update_type);

        ImageView back = (ImageView) view.findViewById(R.id.updateDialogBack);

        update_title.setText(title);

        update_desc.loadData(content, "text/html; charset=UTF-8", null);

        builder.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                final String appPackageName = getActivity().getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        if (updateType.equals("recommended")) {

            update_type.setText(
                    R.string.recommended_update);

            builder.setNegativeButton(R.string.later, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    CustomAlertDialog.this.dismiss();

                    PreferenceHelper.saveVersion(getActivity(), versionName);
                }
            });

            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CustomAlertDialog.this.dismiss();
                }
            });
        } else {
            update_type.setText(R.string.update_required);

            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().finishAffinity();
                }
            });
        }


        builder.setView(view);

        return builder.create();
    }

    public void hideButtons() {

        if (remindMeLaterButton != null) {

            remindMeLaterButton.setVisibility(View.GONE);
        }

        if (cancelButton != null) {

            cancelButton.setVisibility(View.GONE);
        }
    }

    public void dismissDownload() {

        if (!downloadIds.isEmpty()) {

            for (long download : downloadIds) {

                dm.remove(download);
            }
        }
    }

    @Override
    public void onDownloadStarted(int productId) {
        hideButtons();
        if (updateButton != null) {
            updateButton.setText(getString(R.string.wait_update));
            updateButton.setOnClickListener(null);
            updateButton.setClickable(false);
        }
    }

    @Override
    public void onPreviewDownloadStarted(int productId) {
    }

    @Override
    public void onDownloadSuccess(int productId, String fileUrl) {

        if (getDialog() != null) {

            getDialog().setTitle("Download completed");

            if (updateButton != null) {

                updateButton.setVisibility(View.VISIBLE);

                updateButton.setText(getContext().getString(R.string.install_the_update));

                updateButton.setClickable(true);

                updateButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        File apkFile = new File(LomiFileUtils.LOMI_FOLDER_PATH + File.separator + "lomi.apk");

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                            Uri apkFileUri = FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".provider", apkFile);

                            Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);

                            intent.setData(apkFileUri);

                            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                            startActivity(intent);

                        } else {

                            Uri apkUri = Uri.fromFile(apkFile);

                            Intent intent = new Intent(Intent.ACTION_VIEW);

                            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");

                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                            startActivity(intent);
                        }
                    }
                });
            }
        }
    }

    @Override
    public void onPreviewDownloadSuccess(int productId) {
    }

    @Override
    public void onDownloadProgress(int productId, int progress) {
        if (getDialog() != null) {
            String progressText = String.format(getContext().getString(R.string.downloading_update), progress);
            getDialog().setTitle(progressText);
        }
    }

    @Override
    public void onPreviewDownloadProgress(int productId, int progress) {
    }

    @Override
    public void onDownloadFailure(int productId) {
        getDialog().setTitle("Download failed");
    }

    @Override
    public void onPreviewDownloadFailure(int productId) {
    }

    @Override
    public void onFailedBeforeDownloadStarted(int productId) {
    }

    @Override
    public void onDownloadPaused(int productId) {
        getDialog().setTitle("Download paused");
    }

    @Override
    public void onPreviewDownloadPaused(int productId) {
    }
}