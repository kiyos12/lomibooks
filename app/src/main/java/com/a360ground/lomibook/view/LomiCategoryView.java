package com.a360ground.lomibook.view;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.a360ground.lomibook.activity.CategoryBooksActivity;
import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.R;
import com.a360ground.lomibook.adapter.ProductRecyclerAdapter;
import com.a360ground.lomibook.domain.woocommerce.Category;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.utils.DimensionUtils;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class LomiCategoryView extends RelativeLayout {

    @IdRes
    private static final int TITLE_CONTAINER_IDENTIFIER = 1;

    @IdRes
    private static final int PRODUCT_RECYCLER_IDENTIFIER = 2;

    private TextView titleTextView;

    private RecyclerView productRecyclerView;

    private Category category;

    private Context context;

    public LomiCategoryView(Context context) {
        this(context, null);
    }

    public LomiCategoryView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LomiCategoryView(final Context context, AttributeSet attrs, int defStyleAttr) {

        super(context, attrs, defStyleAttr);

        this.context = context;

        int margin = DimensionUtils.dpToPx(5);

        LayoutParams rootLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        int rootMarginTop = DimensionUtils.dpToPx(50);

        rootLayoutParams.setMargins(0, rootMarginTop, 0, margin);

        setPadding(0, DimensionUtils.dpToPx(5), 0, 0);

        setLayoutParams(rootLayoutParams);

        ImageView backgroundImageView = new ImageView(context);

        LayoutParams backgroundImageViewLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        backgroundImageViewLayoutParams.addRule(CENTER_IN_PARENT);

        backgroundImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        RelativeLayout titleContainer = new RelativeLayout(context);

        titleTextView = new TextView(context);

        LayoutParams titleTextViewParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        titleTextViewParams.addRule(ALIGN_PARENT_LEFT);

        titleTextView.setTextColor(ContextCompat.getColor(context, R.color.dark_gray));

        titleTextView.setTextSize(15);

        titleTextView.setPadding(DimensionUtils.dpToPx(10), DimensionUtils.dpToPx(10), DimensionUtils.dpToPx(2), 0);

        TextView seeAllTextView = new TextView(context);

        seeAllTextView.setText(context.getString(R.string.see_all));

        seeAllTextView.setTextColor(ContextCompat.getColor(context, R.color.splash_blue));

        seeAllTextView.setPadding(0, DimensionUtils.dpToPx(10), DimensionUtils.dpToPx(2), 0);

        LayoutParams seeAllTextViewParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        seeAllTextViewParams.addRule(ALIGN_PARENT_RIGHT);

        seeAllTextViewParams.addRule(ALIGN_PARENT_END);

        seeAllTextView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, CategoryBooksActivity.class);

                intent.putExtra(Constants.CATEGORY_ID_IDENTIFIER, category.getId());

                intent.putExtra(Constants.CATEGORY_NAME_IDENTIFIER, category.getName());

                context.startActivity(intent);
            }
        });

        titleContainer.addView(titleTextView, titleTextViewParams);

        titleContainer.addView(seeAllTextView, seeAllTextViewParams);

        titleContainer.setId(TITLE_CONTAINER_IDENTIFIER);

        productRecyclerView = new RecyclerView(context);

        productRecyclerView.setId(PRODUCT_RECYCLER_IDENTIFIER);

        productRecyclerView.setPadding(DimensionUtils.dpToPx(10), 0, DimensionUtils.dpToPx(2), 0);

        LayoutParams productRecyclerViewParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        productRecyclerViewParams.addRule(BELOW, TITLE_CONTAINER_IDENTIFIER);

        productRecyclerViewParams.setMargins(0, margin, 0, 0);

        View separatorView = new View(context);

        separatorView.setBackground(ContextCompat.getDrawable(context, R.drawable.shadow_back));

        LayoutParams separatorViewParams = new LayoutParams(LayoutParams.MATCH_PARENT, DimensionUtils.dpToPx(6));

        separatorViewParams.addRule(BELOW, PRODUCT_RECYCLER_IDENTIFIER);

        addView(backgroundImageView);

        addView(titleContainer);

        addView(productRecyclerView, productRecyclerViewParams);

        addView(separatorView, separatorViewParams);
    }

    public void setCategory(Category category, boolean isLiked) {

        Realm realm = null;

        try {

            RealmResults<Product> products;

            realm = Realm.getDefaultInstance();

            this.category = category;

            if(isLiked){
                titleTextView.setText("የሚወዷቸው");
                products = realm.where(Product.class).equalTo("liked", true).findAll();
            }
            else{
                titleTextView.setText(category.getName());

                products = realm.where(Product.class).equalTo("categories.id", category.getId()).findAll();
            }


            final ProductRecyclerAdapter adapter = new ProductRecyclerAdapter(context, products);

            productRecyclerView.setAdapter(adapter);

            productRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

            productRecyclerView.setHasFixedSize(true);

            products.addChangeListener(new RealmChangeListener<RealmResults<Product>>() {

                @Override
                public void onChange(RealmResults<Product> element) {

                    LinearLayoutManager layoutManager = (LinearLayoutManager) productRecyclerView.getLayoutManager();

                    int visibleStartPosition = layoutManager.findFirstVisibleItemPosition();

                    int visibleEndPosition = layoutManager.findLastVisibleItemPosition();

                    adapter.notifyItemRangeChanged(visibleStartPosition, visibleEndPosition);
                }
            });

        } finally {

            if(realm != null) {

                realm.close();
            }
        }
    }
}
