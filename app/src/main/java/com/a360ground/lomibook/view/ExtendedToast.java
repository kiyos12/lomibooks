package com.a360ground.lomibook.view;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.a360ground.lomibook.R;

/**
 * Created by Kiyos on 2/16/2017.
 */

public class ExtendedToast {

    public static String SUCCESS = "success";
    public static String INFO = "info";
    public static String FAILED = "failed";

    public static void showToast(Context mContext,String type, String message){

        View view = null;

        if(type.equals (SUCCESS)){
            view = LayoutInflater.from (mContext).inflate (R.layout.toast_success,null,false);
            TextView textView = (TextView) view.findViewById (R.id.success_message);
            textView.setText (message);
        }
        if(type.equals (INFO)){
            view = LayoutInflater.from (mContext).inflate (R.layout.toast_info,null,false);
            TextView textView = (TextView) view.findViewById (R.id.info_message);
            textView.setText (message);
        }
        if(type.equals (FAILED)){
            view = LayoutInflater.from (mContext).inflate (R.layout.toast_error,null,false);
            TextView textView = (TextView) view.findViewById (R.id.error_message);
            textView.setText (message);
        }

        Toast toast = new Toast (mContext);

        toast.setView (view);
        toast.setGravity (Gravity.BOTTOM,0,200);
        toast.setDuration (Toast.LENGTH_SHORT);
        toast.show ();
    }

}
