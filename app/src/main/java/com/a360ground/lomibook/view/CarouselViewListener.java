package com.a360ground.lomibook.view;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.activity.BookDetailsActivity;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.Post;
import com.a360ground.lomibook.utils.Constants;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.ViewListener;

import io.realm.Realm;
import io.realm.RealmResults;

public class CarouselViewListener implements ViewListener {

    private final Context context;
    private final RealmResults<Post> ads;


    public CarouselViewListener(Context context, RealmResults<Post> ads) {

        this.context = context;

        this.ads = ads;
    }

    @Override
    public View setViewForPosition(int position) {

        View view = null;
        try {
            final Post ad = ads.get(position);

            view = LayoutInflater.from(context).inflate(R.layout.single_carousel, null);

            final ImageView adImageView = (ImageView) view.findViewById(R.id.carousel_background_image_view);

            final TextView carouselActionButton = (TextView) view.findViewById(R.id.carousel_action_button);

            Picasso.with(context)

                    .load(ad.getPostThumbnailUrl())

                    .centerCrop()

                    .fit()

                    .into(adImageView);


            if (Post.getActivityNameEnum(ad).getActivity().toString().equals("class com.a360ground.lomibook.activity.SignupActivity")) {

                if (Realm.getDefaultInstance().where(Customer.class).findFirst() != null) {
                    carouselActionButton.setText("Enjoy Lomi");
                } else {
                    carouselActionButton.setText(ad.getButtonText());
                }
            } else {
                carouselActionButton.setText(ad.getButtonText());
            }

            if(ad.isDisableButton()){
                carouselActionButton.setVisibility(View.GONE);
            }



            carouselActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    switch (Post.getButtonEndpointTypeEnum(ad)) {

                        case Activity:

                            context.startActivity(new Intent(context, Post.getActivityNameEnum(ad).getActivity()));

                            break;

                        case Book:

                            Intent intent = new Intent(context, BookDetailsActivity.class);

                            intent.putExtra(Constants.PRODUCT_ID_IDENTIFIER, ad.getBookId());

                            context.startActivity(intent);

                            break;

                        case Fragment:

                            FragmentTransaction transaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();

                            try {

                                transaction.replace(R.id.home_fragment_container, (Fragment) Post.getFragmentNameEnum(ad).getFragment().newInstance());

                            } catch (InstantiationException | IllegalAccessException e) {

                                e.printStackTrace();

                            } catch (IllegalArgumentException e) {

                                if (context.getApplicationContext() != null) {

                                    Toast.makeText(context, context.getString(R.string.ad_not_configured_properly), Toast.LENGTH_SHORT).show();
                                }
                            }

                            transaction.commitAllowingStateLoss();

                            break;
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }
}
