package com.a360ground.lomibook.view;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDelegate;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.application.LomiApplication;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.utils.DimensionUtils;
import com.a360ground.lomibook.utils.SIMUtils;

import io.realm.Realm;
import mp.MpUtils;
import mp.PaymentRequest;

public class LomiCoinView extends android.support.v7.widget.AppCompatTextView {

    public final static String SERVICE_ID = "ed1ececc773134dd71dc2731a97467ec";
    public final static String APP_SECRET = "3427533b629bff1546f6699949aabae0";
    public static final int REQUEST_CODE_FORTUMO = 1221;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    boolean coinClick = false;

    private CoinType type = CoinType.PRICE;

    private Context context;

    public LomiCoinView(Context context) {
        this(context, null);
    }

    public LomiCoinView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LomiCoinView(final Context context, AttributeSet attrs, int defStyleAttr) {

        super(context, attrs, defStyleAttr);

        this.context = context;

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.LomiCoinView, defStyleAttr, 0);

        int coinTypeIndex = a.getInt(R.styleable.LomiCoinView_type, CoinType.PRICE.ordinal());

        a.recycle();

        type = CoinType.values()[coinTypeIndex];

        setType(type);

        setWidth(DimensionUtils.dpToPx(40));

        setHeight(DimensionUtils.dpToPx(40));

        setGravity(Gravity.CENTER);

        setTypeface(null, Typeface.BOLD);

        setTextColor(ContextCompat.getColor(context, R.color.gray_text));

        setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {

                onCoinClick();
            }
        });
    }

    public void onCoinClick() {

        switch (type) {

            case PRICE:

                LomiToolTip.showToolTip(LomiCoinView.this, "1 Coin == 5 ETB", Gravity.BOTTOM, R.color.white, R.color.mainColor);

                break;

            case BALANCE:

                launchFortumo();

                /*if (GeneralUtills.isNetworkAvailable(context)) {

                    if (!coinClick) {

                        if (!SIMUtils.isSimReady(context).equals(context.getString(R.string.sim_ready))) {
                            ExtendedToast.showToast(context, ExtendedToast.INFO, SIMUtils.isSimReady(context));
                            return;
                        }



                        coinClick = true;

                    } else {

                        coinClick = false;
                    }

                } else {

                    ExtendedToast.showToast(context, ExtendedToast.INFO, context.getString(R.string.connect_to_internet));
                }*/

                break;
        }
    }

    private void launchFortumo() {

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            Customer customer = realm.where(Customer.class).findFirst();

            if (customer != null) {

                PaymentRequest.PaymentRequestBuilder paymentRequestBuilder = new PaymentRequest.PaymentRequestBuilder();

                paymentRequestBuilder.setService(SERVICE_ID, APP_SECRET);

                paymentRequestBuilder.setDisplayString("Buy one Lomi Coin");

                paymentRequestBuilder.setCreditsMultiplier(2.1d);

                paymentRequestBuilder.setPriceCurrency("ETB");

                paymentRequestBuilder.setProductName("" + customer.getId() + "-" + customer.getLomiCoins());

                paymentRequestBuilder.setType(MpUtils.PRODUCT_TYPE_CONSUMABLE);

                PaymentRequest pr = paymentRequestBuilder.build();

                ((Activity) context).startActivityForResult(pr.toIntent(context), REQUEST_CODE_FORTUMO);
            }

        } finally {

            if (realm != null) {

                realm.close();
            }
        }
    }

    public CoinType getType() {
        return type;
    }

    public void setType(CoinType type) {

        this.type = type;

        setBackground(type == CoinType.PRICE

                ? ContextCompat.getDrawable(context, R.drawable.lomi_coin)

                : ContextCompat.getDrawable(context, R.drawable.coin_buy_png));
    }

    public void flip() {

        AnimatorSet animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(context, R.animator.coin_flip);

        animatorSet.setTarget(this);

        animatorSet.start();

        MediaPlayer mPlayer = MediaPlayer.create(context, R.raw.coin_flip);

        mPlayer.start();

        setType(CoinType.PRICE);
    }

    public void bounce() {

        Animation animation = AnimationUtils.loadAnimation(context, R.anim.bounce);

        setAnimation(animation);

        MediaPlayer mPlayer = MediaPlayer.create(context, R.raw.coin_flip);

        mPlayer.start();

        ExtendedToast.showToast(context, ExtendedToast.SUCCESS, "Successfully Bought one lomi coin");
    }

    public void shake() {

        ExtendedToast.showToast(context, ExtendedToast.FAILED, "Payment Failed may be you dont have sufficient balance");

        Animation animationShake = AnimationUtils.loadAnimation(context, R.anim.grow_from_bottom);

        setAnimation(animationShake);

        animationShake.start();
    }

    public enum CoinType {

        BALANCE,

        PRICE
    }
}
