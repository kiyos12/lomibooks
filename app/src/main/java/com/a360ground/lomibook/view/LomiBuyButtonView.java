package com.a360ground.lomibook.view;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.a360ground.lomibook.R;
import com.a360ground.lomibook.application.LomiApplication;
import com.a360ground.lomibook.client.ProductClient;
import com.a360ground.lomibook.domain.woocommerce.Author;
import com.a360ground.lomibook.domain.woocommerce.Category;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.downloadManager.LomiDownloadManager;
import com.a360ground.lomibook.fragment.PaymentDialogFragment;
import com.a360ground.lomibook.service.DownloadTrackerBroadcastReceiver;
import com.a360ground.lomibook.service.DownloadTrackingInfoInterface;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.utils.CheckDevice;
import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.utils.LomiFileUtils;
import com.a360ground.lomibook.utils.LomiReaderUtills;
import com.a360ground.lomibook.utils.PreferenceHelper;
import com.koolearn.klibrary.core.drm.Encryption;
import com.koolearn.klibrary.core.util.Native;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LomiBuyButtonView extends RelativeLayout implements DownloadTrackingInfoInterface {

    @IdRes
    private static final int BUTTON_TEXT_VIEW_ID = 1;

    @IdRes
    private static final int LOADING_PROGRESS_BAR_ID = 2;

    private boolean isPreview = false;

    private TextView textView;

    private ProgressBar loadingProgressBar;

    private Context context;

    private LomiApplication application;

    private Call<Product> confirmWithRemoteCall;

    private StateEnum stateEnum;

    private Product product;

    boolean valid = true;

    public LomiBuyButtonView(Context context) {
        this(context, null);
    }

    public LomiBuyButtonView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LomiBuyButtonView(final Context context, AttributeSet attrs, int defStyleAttr) {

        super(context, attrs, defStyleAttr);

        this.context = context;

        LocalBroadcastManager.getInstance(context).registerReceiver(new DownloadTrackerBroadcastReceiver(this), new IntentFilter(LomiDownloadManager.LOMIDOWNLOAD_INTENT_FILTER));

        this.application = (LomiApplication) context.getApplicationContext();

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.LomiBuyButtonView, defStyleAttr, 0);

        isPreview = a.getBoolean(R.styleable.LomiBuyButtonView_preview, false);

        a.recycle();

        final float scale = context.getResources().getDisplayMetrics().density;

        int viewWidthInPixels = (int) (10 * scale + 0.5f);

        LayoutParams layoutParams = new LayoutParams(viewWidthInPixels, LayoutParams.WRAP_CONTENT);

        this.setLayoutParams(layoutParams);

        LayoutParams textViewLayoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        textViewLayoutParams.addRule(CENTER_VERTICAL);

        textView = new TextView(context);

        textView.setId(BUTTON_TEXT_VIEW_ID);

        textView.setMaxLines(1);

        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);

        textView.setTextColor(ContextCompat.getColor(context, R.color.white));

        loadingProgressBar = new ProgressBar(context);

        loadingProgressBar.setIndeterminate(true);

        int imageWidthInPixels = (int) (16 * scale + 0.5f);

        LayoutParams additionalLayoutParams = new LayoutParams(imageWidthInPixels, imageWidthInPixels);

        additionalLayoutParams.addRule(RelativeLayout.RIGHT_OF, BUTTON_TEXT_VIEW_ID);

        int leftMarginInPixels = (int) (5 * scale + 0.5f);

        additionalLayoutParams.setMargins(leftMarginInPixels, 0, 0, 0);

        loadingProgressBar.setId(LOADING_PROGRESS_BAR_ID);

        this.addView(textView, textViewLayoutParams);

        this.addView(loadingProgressBar, additionalLayoutParams);

        loadingProgressBar.setVisibility(GONE);

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonClicked();
            }
        });
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {

        this.product = product;

        setupButton();
    }

    public void setState(Context context, StateEnum state) {

        this.context = context;

        setState(state);
    }

    public StateEnum getState() {
        return this.stateEnum;
    }

    private void setState(StateEnum state) {

        if (context == null) return;

        if (state != StateEnum.DOWNLOADING && state != StateEnum.PENDING) {

            resetLoading();
        }

        if (state != null) {

            switch (state) {

                case FREE:

                    textView.setText(context.getString(R.string.free));

                    break;

                case PRICE:

                    boolean inEthiopia = PreferenceHelper.inEthiopia(context);

                    String priceText = inEthiopia

                            ? String.format(context.getString(R.string.coin_text), product.getPrice(context))

                            : String.format(context.getString(R.string.price_text), product.getPrice(context));

                    textView.setText(priceText);

                    break;

                case DOWNLOAD:

                    if (isPreview) textView.setText(context.getString(R.string.download_preview));

                    else textView.setText(context.getString(R.string.download));


                    break;

                case OPEN:

                    textView.setText(context.getString(R.string.open_book));

                    if (isPreview) textView.setText(context.getString(R.string.open_preview));

                    break;

                case CONNECTING:

                    textView.setText(context.getString(R.string.connecting));

                    ;

                    setLoading();

                    break;

                case OPENING:

                    if (isPreview) textView.setText(context.getString(R.string.opening_preview));

                    else textView.setText(context.getString(R.string.opening));

                    ;

                    setLoading();

                    break;

                case DOWNLOADING:

                    ;

                    setLoading();

                    break;

                case PENDING:

                    textView.setText(context.getString(R.string.pending));

                case CHECKING_REMOTE:

                    setLoading();
            }

        } else {

            setVisibility(GONE);

            valid = false;
        }

        this.stateEnum = state;
    }

    private void setLoading() {

        if (loadingProgressBar.getVisibility() != VISIBLE)
            loadingProgressBar.setVisibility(VISIBLE);

        if (isPreview) {

            setBackground(ContextCompat.getDrawable(context, R.drawable.preview_downloading));

            textView.setTextColor(ContextCompat.getColor(context, R.color.app_green));

        } else {

            textView.setTextColor(ContextCompat.getColor(context, R.color.mainColor));

            setBackground(ContextCompat.getDrawable(context, R.drawable.button_downloading));
        }
    }

    private void resetLoading() {

        textView.setTextColor(ContextCompat.getColor(context, R.color.white));

        if (isPreview) {

            setBackground(ContextCompat.getDrawable(context, R.drawable.small_green_background_rounded_button));

        } else {

            setBackground(ContextCompat.getDrawable(context, R.drawable.small_blue_background_rounded_button));
        }

        loadingProgressBar.setVisibility(GONE);
    }

    public void onButtonClicked() {

        Customer customer = null;

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            customer = realm.where(Customer.class).findFirst();

            if (customer == null && !isPreview) {

                application.launchSignupWithError();

                return;
            }

            if (product == null) return;

            if (!application.isNetworkAvailable() && !product.isInMyBooks()) {

                ExtendedToast.showToast(context, ExtendedToast.INFO, context.getString(R.string.connect_to_internet));

                return;
            }

            if (!isPreview && product.getDownloadUrl() == null) {

                ExtendedToast.showToast(context, ExtendedToast.INFO, context.getString(R.string.no_associated_file));

                return;
            }

            if (isPreview && product.getSampleDownloadUrl() == null) {

                ExtendedToast.showToast(context, ExtendedToast.INFO, context.getString(R.string.sample_file_does_not_exist));

                return;
            }

            switch (stateEnum) {

                case OPENING:

                case DOWNLOADING:

                    return;

                case FREE:

                    if (!application.isNetworkAvailable()) {

                        ExtendedToast.showToast(context, ExtendedToast.INFO, context.getString(R.string.connect_to_internet));

                        return;
                    }

                    onFinishPayment(false);

                    break;

                case CONFIRMED_WITH_REMOTE:

                    PaymentDialogFragment paymentDialogFragment = PaymentDialogFragment.newInstance(this);

                    paymentDialogFragment.show(((AppCompatActivity) context).getSupportFragmentManager(), LomiBuyButtonView.class.getSimpleName());

                    break;

                case PRICE:

                    if (!application.isNetworkAvailable()) {

                        ExtendedToast.showToast(context, ExtendedToast.INFO, context.getString(R.string.connect_to_internet));

                        return;
                    }

                    confirmWithRemote(realm);

                    break;

                case OPEN:

                    setState(LomiBuyButtonView.StateEnum.OPENING);

                    if (isPreview) {

                        openPreviewBook();

                    } else {

                        openBook();
                    }

                    break;

                case DOWNLOAD:

                    if (!application.isNetworkAvailable()) {

                        ExtendedToast.showToast(context, ExtendedToast.INFO, context.getString(R.string.connect_to_internet));

                        return;
                    }

                    setState(StateEnum.CONNECTING);

                    download();

                    break;

                case CHECKING_REMOTE:

                    if (!application.isNetworkAvailable()) {

                        ExtendedToast.showToast(context, ExtendedToast.INFO, context.getString(R.string.connect_to_internet));

                        return;
                    }

                    ExtendedToast.showToast(context, ExtendedToast.INFO, context.getString(R.string.checking_book_availability));

                    break;

                case PENDING:

                    break;

                default:

            }

        } finally {

            if (realm != null) {

                realm.close();
            }
        }
    }

    private void download() {

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            LomiDownloadManager lomiDownloadManager = new LomiDownloadManager(getContext(), product, realm);

            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                setState(StateEnum.DOWNLOAD);

                if (ActivityCompat.shouldShowRequestPermissionRationale(((Activity) getContext()), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    //todo: show explanation
                } else {

                    ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.MY_PERMISSION_WRITE_TO_EXTERNAL_STORAGE);
                }

            } else {

                try {

                    if (isPreview) {

                        lomiDownloadManager.downloadPreview();

                    } else {

                        lomiDownloadManager.download();
                    }

                } catch (LomiDownloadManager.LomiDownloadError lomiDownloadError) {

                    ExtendedToast.showToast(context, ExtendedToast.FAILED, lomiDownloadError.getMessage());

                    lomiDownloadError.printStackTrace();
                }
            }

        } finally {

            if (realm != null) {

                realm.close();
            }
        }
    }

    public void onFinishPayment(boolean pay) throws RuntimeException {

        //todo: assumes that customer is logged in by this time
        // check to see if the customer is not null

        setState(StateEnum.CONNECTING);

        Realm realm = null;

        if (pay && PreferenceHelper.inEthiopia(context)) {

            try {

                realm = Realm.getDefaultInstance();

                final Customer customer = realm.where(Customer.class).findFirst();

                if (customer != null) {

                    realm.executeTransaction(new Realm.Transaction() {

                        @Override
                        public void execute(Realm realm) {
                            if(!product.isOrderSuccesful()) {
                                customer.setLomiCoins(customer.getLomiCoins() - product.getPrice(context));
                            }
                        }
                    });
                }

            } finally {

                if (realm != null) {

                    realm.close();
                }
            }
        }

        download();
    }

    public void confirmWithRemote(final Realm realm) {

        if (stateEnum != LomiBuyButtonView.StateEnum.CHECKING_REMOTE) {

            final LomiBuyButtonView.StateEnum savedBuyButtonState = stateEnum;

            setState(LomiBuyButtonView.StateEnum.CHECKING_REMOTE);

            WooCommerceServiceGenerator generator = new WooCommerceServiceGenerator(context);

            ProductClient client = generator.createService(ProductClient.class);

            try {
                confirmWithRemoteCall = client.productById(new String(Native.getProductsById(product.getId() + "/"), "cp1252"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            confirmWithRemoteCall.enqueue(new Callback<Product>() {

                @Override
                public void onResponse(Call<Product> call, Response<Product> response) {

                    setState(savedBuyButtonState);

                    if (response.isSuccessful()) {

                        Product remoteProduct = response.body();

                        if (remoteProduct.getStatusEnum() == Product.StatusEnum.publish && remoteProduct.getDownloadUrl() != null) {

                            setState(LomiBuyButtonView.StateEnum.CONFIRMED_WITH_REMOTE);

                            performClick();

                        } else {

                            setState(savedBuyButtonState);

                            ExtendedToast.showToast(context, ExtendedToast.FAILED, context.getString(R.string.book_removed));

                        }

                    } else {

                        setState(savedBuyButtonState);

                        try {

                            Error error = application.getErrorResponse(response.errorBody());

                            ExtendedToast.showToast(context, ExtendedToast.FAILED, error.getMessage());

                        } catch (IOException e) {

                            e.printStackTrace();
                        }

                        ((Activity) context).finish();
                    }
                }

                @Override
                public void onFailure(Call<Product> call, Throwable t) {

                    setState(savedBuyButtonState);

                    if (context != null) {

                        ExtendedToast.showToast(context, ExtendedToast.FAILED, context.getString(R.string.something_wrong));
                    }
                }
            });
        }
    }

    private void openPreviewBook() {

        if (product.getSampleDownloadUrl() == null) {

            ExtendedToast.showToast(context, ExtendedToast.INFO,context.getString(R.string.sample_file_does_not_exist));

            return;
        }

        if (product.fileExistsLocally(true)) {

            setState(LomiBuyButtonView.StateEnum.OPENING);

            /*Intent intent = new Intent(context, ReaderActivity.class);

            intent.putExtra(TAGS.BOOK_COVER_PATH, product.getThumbnailSrc());

            intent.putExtra(TAGS.BOOKNAME, product.getName()+" Sample");

            Author author = product.getAuthor();

            if (author != null) {

                intent.putExtra(TAGS.BOOK_AUTHOR_NAME, author.getName());

                intent.putExtra(TAGS.BOOK_AUTHOR_NAME, author.getName());
            }

            intent.putExtra(TAGS.FILE_PATH, LomiFileUtils.LOMI_FOLDER_PATH + File.separator + product.getId() + Constants.SAMPLE_FILE_EXTENSION);

            context.startActivity(intent);*/

            LomiReaderUtills.openBookEvent(context, LomiFileUtils.LOMI_FOLDER_PATH + File.separator + product.getId() + Constants.SAMPLE_FILE_EXTENSION, false);


        } else {

            setState(LomiBuyButtonView.StateEnum.DOWNLOADING);

            download();
        }
    }

    public void openBook() {

        if (product.fileExistsLocally(false)) {

           /* Intent intent = new Intent(context, ReaderActivity.class);

            intent.putExtra(TAGS.BOOK_COVER_PATH, product.getThumbnailSrc());

            Author author = product.getAuthor();

            if (author != null) {

                intent.putExtra(TAGS.BOOK_AUTHOR_NAME, author.getName());
            }

            intent.putExtra(TAGS.BOOKNAME, product.getName());

            intent.putExtra(TAGS.FILE_PATH, LomiFileUtils.LOMI_FOLDER_PATH + File.separator + product.getId() + Constants.LOMI_FILE_EXTENSION);

            context.startActivity(intent);
            */
            LomiReaderUtills.openBookEvent(context, LomiFileUtils.LOMI_FOLDER_PATH + File.separator + product.getId() + Constants.LOMI_FILE_EXTENSION, false);

        } else {

            onFinishPayment(false);
        }
    }

    @Override
    protected void onVisibilityChanged(@NonNull View changedView, int visibility) {

        super.onVisibilityChanged(changedView, visibility);

        if (visibility == VISIBLE) { // onResume

            if (stateEnum == StateEnum.OPENING) {

                setState(StateEnum.OPEN);
            }
        }
    }

    private void setupButton() {

        // setProduct gets called several times
        // causing setupButton to be called the same amount of times
        // the button switches between DOWNLOADING and DOWNLOAD states
        // while in progress
        // the next line prevents that
        if (stateEnum == StateEnum.DOWNLOADING) return;

        if (isPreview) {

            if (product.fileExistsLocally(true)) {

                setState(StateEnum.OPEN);

            } else {

                setState(StateEnum.DOWNLOAD);
            }

            if (product.isInMyBooks() || product.isFree() || product.getSampleDownloadUrl() == null) {

                setState(null);
            }
        } else {

            if (product.isInMyBooks()) {

                if (product.fileExistsLocally(false)) {

                    setState(StateEnum.OPEN);

                } else {

                    setState(StateEnum.DOWNLOAD);
                }

            } else {

                if (product.isFree()) {

                    setState(StateEnum.FREE);

                } else {

                    setState(StateEnum.PRICE);
                }
            }
        }
    }

    public enum StateEnum {

        FREE,

        PRICE,

        DOWNLOAD,

        OPENING,

        DOWNLOADING,

        CHECKING_REMOTE,

        CONNECTING,

        OPEN,

        CONFIRMED_WITH_REMOTE,

        PENDING
    }

    public boolean isValid() {
        return valid;
    }

    @Override
    public void onDownloadStarted(int productId) {

        if (!isPreview && product.isValid() && productId == product.getId()) {

            setState(LomiBuyButtonView.StateEnum.DOWNLOADING);
        }
    }

    @Override
    public void onPreviewDownloadStarted(int productId) {

        if (isPreview && product.isValid() && productId == product.getId()) {

            setState(LomiBuyButtonView.StateEnum.DOWNLOADING);
        }
    }

    @Override
    public void onDownloadSuccess(int productId, String fileUrl) {

        if (product.isValid() && productId == product.getId()) {

            Encryption encryption = new Encryption(context);

            encryption.encrypt(new File(fileUrl));

            setState(LomiBuyButtonView.StateEnum.OPEN);
        }
    }

    @Override
    public void onPreviewDownloadSuccess(int productId) {

        if (isPreview && product.isValid() && productId == product.getId()) {

            setState(LomiBuyButtonView.StateEnum.OPEN);
        }
    }

    @Override
    public void onDownloadProgress(int productId, int progress) {

        if (progress != -1 && product.isValid() && productId == product.getId()) {

            setState(LomiBuyButtonView.StateEnum.DOWNLOADING);

            textView.setText(String.format(context.getString(R.string.downloading), progress));
        }
    }

    @Override
    public void onPreviewDownloadProgress(int productId, int progress) {

        if (isPreview && product.isValid() && productId == product.getId()) {

            if (progress != -1) {

                setState(LomiBuyButtonView.StateEnum.DOWNLOADING);

                textView.setText(String.format(context.getString(R.string.downloading_preview), progress));
            }
        }
    }

    @Override
    public void onDownloadFailure(int productId) {

        if (product.isValid() && productId == product.getId()) {

            ExtendedToast.showToast(context, ExtendedToast.FAILED, context.getString(R.string.download_failed));

            setState(LomiBuyButtonView.StateEnum.DOWNLOAD);
        }
    }

    @Override
    public void onPreviewDownloadFailure(int productId) {

        if (isPreview && product.isValid() && productId == product.getId()) {

            ExtendedToast.showToast(context, ExtendedToast.FAILED, context.getString(R.string.download_failed));

            setState(LomiBuyButtonView.StateEnum.DOWNLOAD);
        }
    }

    @Override
    public void onFailedBeforeDownloadStarted(int productId) {

        if (product.isValid() && productId == product.getId()) {

            setState(LomiBuyButtonView.StateEnum.DOWNLOAD);
        }
    }

    @Override
    public void onDownloadPaused(int productId) {

        if (product.isValid() && productId == product.getId()) {

            setState(LomiBuyButtonView.StateEnum.PENDING);
        }
    }

    @Override
    public void onPreviewDownloadPaused(int productId) {

        if (isPreview && product.isValid() && productId == product.getId()) {

            textView.setText(context.getString(R.string.pending));

            textView.setTextColor(ContextCompat.getColor(context, R.color.mainColor));

            setBackground(ContextCompat.getDrawable(context, R.drawable.preview_downloading));
        }
    }
}
