package com.a360ground.lomibook.view;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.domain.woocommerce.Customer;

import a360ground.com.lomipay.logic.SMS;

/**
 * Created by Kiyos on 8/26/2017.
 */

public class ProgressButton extends RelativeLayout implements View.OnClickListener {

    private RelativeLayout payCompletePurchareWrapper;
    private RelativeLayout payAddBirrWrapper;
    private TextView button_text;
    private TextView button_option;
    private ProgressBar progressBar;
    private LoadingSpinner success_image;
    private Context context;
    private onProgressFinished onProgressFinished;
    private int maxProgress;
    private Customer customer;
    private OnClickListener listener;

    public ProgressButton(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public ProgressButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public ProgressButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ProgressButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        init();
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getMaxProgress() {
        return maxProgress;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (listener != null) listener.onClick(this);
        }
        return super.dispatchTouchEvent(event);
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP && (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER || event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
            if (listener != null) listener.onClick(this);
        }
        return super.dispatchKeyEvent(event);
    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    public void init() {
        inflate(context, R.layout.progress_button, this);
        this.payAddBirrWrapper = (RelativeLayout) findViewById(R.id.pay_add_birr_wrapper);
        this.payCompletePurchareWrapper = (RelativeLayout) findViewById(R.id.pay_complete_purchase_wrapper);
        this.button_text = (TextView) findViewById(R.id.pay_Progress_txt);
        this.button_option = (TextView) findViewById(R.id.pay_progress_option);
        this.progressBar = (ProgressBar) findViewById(R.id.pay_progressBar);
        this.success_image = (LoadingSpinner) findViewById(R.id.pay_Progress_status);
        this.setOnClickListener(this);
    }

    public int getMax() {
        return this.progressBar.getMax();
    }

    public void setMax(int max) {
        this.progressBar.setMax(max);
    }

    public int getProgress() {
        return progressBar.getProgress();
    }

    public void setProgress(int progress) {

        this.success_image.clearAnimation();
        progressBar.incrementProgressBy(progress);
        if (onProgressFinished != null) {
            setOnClickListener(this);
            if (getProgress() == getMax()) {
                this.success_image.setImageResource(R.drawable.ic_download);
                this.payAddBirrWrapper.setVisibility(GONE);
                this.payCompletePurchareWrapper.setVisibility(VISIBLE);
                this.button_text.setTextColor(ContextCompat.getColor(context, R.color.white));
                this.onProgressFinished.onFinished(false, true);
            } else {
                button_text.setText(context.getString(R.string.pay_10_birr));
                button_option.setVisibility(VISIBLE);
                this.onProgressFinished.onFinished(false, false);
            }
        }
    }

    public void setOnProgressFinished(ProgressButton.onProgressFinished onProgressFinished) {
        this.onProgressFinished = onProgressFinished;
    }

    public void paymentConfirmation(String title, String message, final boolean isFinished) {

        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);

        alertBuilder.setCancelable(false);

        alertBuilder.setTitle(title);

        alertBuilder.setMessage(message);

        alertBuilder.setPositiveButton(context.getString(R.string.pay), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (!isFinished) {

                    success_image.startAnimation();

                    button_text.setText(context.getString(R.string.wait));

                    button_option.setVisibility(GONE);

                    setOnClickListener(null);

                    SMS.send(context, new String(Base64.encode((customer.getFirstName() + ":" + customer.getId()).getBytes(), 1)), new SMS.IPaymentStatus() {
                        @Override
                        public void onPaymentSuccessfull(int id) {
                            onProgressFinished.onPayment(true);
                        }

                        @Override
                        public void onPaymentUnSuccessfull(int id, String message) {
                            onProgressFinished.onPayment(false);
                        }
                    });
                }
            }
        });
        alertBuilder.setNegativeButton(context.getString(R.string.cancel_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        alertBuilder.show();

    }

    @Override
    public void onClick(View view) {
        if (getProgress() < getMax()) {
            paymentConfirmation(context.getString(R.string.pay_10_birr), context.getString(R.string.payments_will_be_charged_to_your_mobile_phone), false);

        } else {
            onProgressFinished.onFinished(true, true);
        }
    }

    public interface onProgressFinished {
        void onFinished(boolean clicked, boolean isFinished);

        void onPayment(boolean isSuccessful);

    }
}
