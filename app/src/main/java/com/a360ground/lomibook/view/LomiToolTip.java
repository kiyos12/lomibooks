package com.a360ground.lomibook.view;

import android.view.View;

import com.a360ground.lomibook.R;
import com.tooltip.Tooltip;

/**
 * Created by Kiyos on 2/17/2017.
 */

public class LomiToolTip {

    public static void showToolTip(View v , String message, int gravity, int color, int bgcolor){
        Tooltip.Builder builder = new Tooltip.Builder (v, R.style.Tooltip2)

                .setCancelable (true)

                .setBackgroundColor (v.getContext ().getResources ().getColor (bgcolor))

                .setTextColor (v.getContext ().getResources ().getColor (color))

                .setCornerRadius (20f)

                .setGravity (gravity)

                .setText (message);

        builder.show ();

    }
}
