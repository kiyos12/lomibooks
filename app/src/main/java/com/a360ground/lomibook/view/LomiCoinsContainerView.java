package com.a360ground.lomibook.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.utils.DimensionUtils;
import com.a360ground.lomibook.utils.SIMUtils;

import io.realm.Realm;

public class LomiCoinsContainerView extends RelativeLayout {

    private final Realm realm;

    private final Customer customer;

    Context context;

    public LomiCoinsContainerView(Context context) {
        this(context, null);
    }

    public LomiCoinsContainerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LomiCoinsContainerView(Context context, AttributeSet attrs, int defStyleAttr) {

        super(context, attrs, defStyleAttr);

        this.context = context;

        this.realm = Realm.getDefaultInstance();

        this.customer = realm.where(Customer.class).findFirst();

        int padding = DimensionUtils.dpToPx(10);

        setPadding(0, padding, 0, padding);
    }

    public void addCoins(final double balance, final boolean isPriceCoin, boolean animate, long lomiCoins) {

        removeAllViews();

        int height = DimensionUtils.dpToPx(40);

        int width = DimensionUtils.dpToPx(40);

        int maxCoins = getMaxCoins();

        for (int i = 0; i < balance; i++) {

            if (i == maxCoins) {

                LomiCoinView lastPriceCoin = getLastPriceCoin();

                lastPriceCoin.setText("+" + (int) (balance - maxCoins + 1));

                break;
            }

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, height);

            final LomiCoinView coinView = new LomiCoinView(context);

            if (!isPriceCoin && i >= lomiCoins) {

                coinView.setType(LomiCoinView.CoinType.BALANCE);
            }

            coinView.setId(i + 2);

            layoutParams.addRule(RelativeLayout.RIGHT_OF, (i + 2) - 1);

            layoutParams.setMargins(0, 0, DimensionUtils.dpToPx(7), 0);

            addView(coinView, layoutParams);

            if (i == (balance - 1) && animate) {

                coinView.bounce();
            }
        }
    }

    public void purchased() {

        LomiCoinView coin = getFirstBalanceCoin();

        if (coin != null) {

            coin.flip();
        }
    }

    public void purchaseFailed() {

        LomiCoinView coin = getFirstBalanceCoin();

        if (coin != null) {

            coin.shake();
        }
    }

    public LomiCoinView getLastPriceCoin() {

        LomiCoinView lastPriceCoin = null;

        for (int i = 0; i < getChildCount(); i++) {

            LomiCoinView coin = ((LomiCoinView) getChildAt(i));

            if (coin.getType() == LomiCoinView.CoinType.PRICE) {

                lastPriceCoin = coin;
            }
        }

        return lastPriceCoin;
    }

    public LomiCoinView getFirstBalanceCoin() {

        for (int i = 0; i < getChildCount(); i++) {

            LomiCoinView coin = ((LomiCoinView) getChildAt(i));

            if (coin.getType() == LomiCoinView.CoinType.BALANCE) {

                return coin;
            }
        }

        return null;
    }

    public int getMaxCoins() {

        DisplayMetrics metrics = new DisplayMetrics();

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        wm.getDefaultDisplay().getMetrics(metrics);

        int screenWidthInDP = DimensionUtils.pxTodp(metrics.widthPixels);

        return ((screenWidthInDP - 77) / 47);
    }
}
