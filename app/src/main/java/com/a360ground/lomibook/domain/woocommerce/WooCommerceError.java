package com.a360ground.lomibook.domain.woocommerce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WooCommerceError extends Error {

    public WooCommerceError(ErrorCode code) {
        super(code.toString());
        this.errorCode = code;
        this.message = code.toString();
    }

    public enum ErrorCode {
        REGISTRATION_ERROR_EMAIL_EXISTS {
            @Override
            public String toString() {
                return "registration-error-email-exists";
            }
        },

        FILE_NOT_FOUND {
            @Override
            public String toString() {
                return "file-not-found-on-server";
            }
        },

        CONNECTION_ERROR {
            @Override
            public String toString() {
                return "connection-error";
            }
        },

        DOWNLOAD_LIMIT_EXCEEDED {

            @Override
            public String toString() {
                return "download-limit-exceeded";
            }
        },

        PRODUCT_REMOVED {
            @Override
            public String toString() {
                return "product-removed";
            }
        },

        METHOD_NOT_IMPLEMENTED {
            @Override
            public String toString() {
                return "method-not-implemented";
            }
        },

        BOOK_REMOVED_ERROR {
            @Override
            public String toString() {
                return "book-removed-from-remote";
            }
        },

        REMOTE_URL_ERROR {

            @Override
            public String toString() {
                return "remote-url-error";
            }
        }
    }

    @SerializedName("code")
    @Expose
    ErrorCode errorCode;

    @SerializedName("message")
    @Expose
    String message;

    @Override
    public String getMessage() {
        return message;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
