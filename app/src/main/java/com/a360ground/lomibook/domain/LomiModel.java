package com.a360ground.lomibook.domain;

import android.content.Context;

public interface LomiModel {

    long getId();

    void setId(long id);

    LomiModel beforeSave(Context context);
}
