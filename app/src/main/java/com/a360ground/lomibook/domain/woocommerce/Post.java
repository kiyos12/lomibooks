package com.a360ground.lomibook.domain.woocommerce;

import android.content.Context;

import com.a360ground.lomibook.activity.MainActivity;
import com.a360ground.lomibook.activity.SearchActivity;
import com.a360ground.lomibook.activity.SettingsActivity;
import com.a360ground.lomibook.domain.LomiModel;
import com.a360ground.lomibook.fragment.SelectedFragment;
import com.a360ground.lomibook.fragment.HomeFragment;
import com.a360ground.lomibook.fragment.MyBooksFragment;
import com.a360ground.lomibook.fragment.TopSellerFragment;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.paypal.android.sdk.payments.LoginActivity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Post extends RealmObject implements LomiModel {

    @PrimaryKey
    @SerializedName(value="id", alternate = {"ID"})
    @Expose
    protected long id;

    @SerializedName("post_title")
    @Expose
    private String postTitle;

    @SerializedName("post_content")
    @Expose
    private String postContent;

    @SerializedName("post_thumbnail_url")
    @Expose
    private String postThumbnailUrl;

    @SerializedName("button_text")
    @Expose
    private String buttonText;

    @SerializedName("button_endpoint_type")
    @Expose
    private String buttonEndpointType;

    @SerializedName("book_id")
    @Expose
    private long bookId;

    @SerializedName("disable_button")
    @Expose
    private boolean disableButton;

    @SerializedName("activity_name")
    @Expose
    private String activityName;

    @SerializedName("fragment_name")
    @Expose
    private String fragmentName;

    public String getPostTitle() {
        return postTitle;
    }

    public String getPostThumbnailUrl() {
        return postThumbnailUrl;
    }

    public String getPostContent() {
        return postContent;
    }

    public String getButtonEndpointType() {
        return buttonEndpointType;
    }

    public void setButtonEndpointType(String buttonEndpointType) {
        this.buttonEndpointType = buttonEndpointType;
    }

    public boolean isDisableButton() {
        return disableButton;
    }

    public void setDisableButton(boolean disableButton) {
        this.disableButton = disableButton;
    }

    public long getBookId() {
        return bookId;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getFragmentName() {
        return fragmentName;
    }

    public void setFragmentName(String fragmentName) {
        this.fragmentName = fragmentName;
    }

    // Helpers to be able to add Enums to RealmObject
    public static Post.ButtonEndPointTypeEnum getButtonEndpointTypeEnum(Post post) {
        return Post.ButtonEndPointTypeEnum.valueOf(post.getButtonEndpointType());
    }

    public static void setButtonEndpointTypeEnum(Post post, Post.ButtonEndPointTypeEnum buttonEndpointType) {
        post.setButtonEndpointType(buttonEndpointType.toString());
    }

    public static Post.ActivityEnum getActivityNameEnum(Post post) {
        return Post.ActivityEnum.valueOf(post.getActivityName());
    }

    public static void setActivityNameEnum(Post post, Post.ActivityEnum activityEnum) {
        post.setActivityName(activityEnum.toString());
    }

    public static Post.FragmentEnum getFragmentNameEnum(Post post) {
        return Post.FragmentEnum.valueOf(post.getFragmentName());
    }

    public static void setFragmentNameEnum(Post post, Post.FragmentEnum fragmentEnum) {
        post.setFragmentName(fragmentEnum.toString());
    }

    public String getButtonText() {
        return buttonText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Post)) return false;

        Post post = (Post) o;

        return id != 0 && id == post.id;
    }

    @Override
    public String toString() {
        return this.postTitle;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public enum ButtonEndPointTypeEnum {
        Book,
        Activity,
        Fragment
    }

    public enum ActivityEnum implements Post.HasActivity {
        Signup {
            @Override
            public  Class getActivity() {
                return MainActivity.class;
            }
        },
        Settings {
            @Override
            public Class getActivity() {
                return SettingsActivity.class;
            }
        },
        Login {
            @Override
            public Class getActivity() {
                return LoginActivity.class;
            }
        },
        Search {
            @Override
            public Class getActivity() {
                return SearchActivity.class;
            }
        },
        Home {
            @Override
            public Class getActivity() {
                return MainActivity.class;
            }
        }
    }

    public enum FragmentEnum implements Post.HasFragment {
        Home {
            @Override
            public Class getFragment() {
                return HomeFragment.class;
            }

            @Override
            public String toString() {

                return HomeFragment.class.getName();
            }
        },
        Favorites {
            @Override
            public Class getFragment() {
                return SelectedFragment.class;
            }

            @Override
            public String toString() {

                return SelectedFragment.class.getName();
            }
        },
        TopSellers {
            @Override
            public Class getFragment() {
                return TopSellerFragment.class;
            }

            @Override
            public String toString() {

                return TopSellerFragment.class.getName();
            }
        },
        MyBooks {
            @Override
            public Class getFragment() {
                return MyBooksFragment.class;
            }

            @Override
            public String toString() {
                return MyBooksFragment.class.getName();
            }
        }
    }

    public interface HasActivity {
        Class getActivity();
    }

    public interface HasFragment {
        Class getFragment();
    }

    @Override
    public Post beforeSave(Context context) {
        return this;
    }
}
