package com.a360ground.lomibook.domain.woocommerce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDownloadFile {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("file")
    @Expose
    private String file;

    public String getName() {
        return name;
    }

    public String getFile() {
        return file;
    }
}
