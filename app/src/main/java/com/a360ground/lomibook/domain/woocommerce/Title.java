package com.a360ground.lomibook.domain.woocommerce;

import io.realm.RealmObject;

/**
 * Created by Kiyos on 6/3/2017.
 */
public class Title extends RealmObject {
    /**
     * rendered : Aniversary Update
     */

    private String rendered;

    public String getRendered() {
        return rendered;
    }

    public void setRendered(String rendered) {
        this.rendered = rendered;
    }
}