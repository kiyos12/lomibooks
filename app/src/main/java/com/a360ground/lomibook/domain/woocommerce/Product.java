package com.a360ground.lomibook.domain.woocommerce;

import android.content.Context;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.adapter.ProductTypeAdapter;
import com.a360ground.lomibook.domain.LomiModel;
import com.a360ground.lomibook.utils.LomiFileUtils;
import com.a360ground.lomibook.utils.PreferenceHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class Product extends RealmObject implements LomiModel {

    @PrimaryKey
    @SerializedName(value=ProductTypeAdapter.PRODUCT_ID_IDENTIFIER, alternate = {"product_id", "ID"})
    @Expose
    protected long id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("slug")
    @Expose
    private String slug;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("short_description")
    @Expose
    private String shortDescription;

    @SerializedName("permalink")
    @Expose
    private String permalink;
    @SerializedName("date_created")
    @Expose
    private Date dateCreated;
    @SerializedName("date_modified")
    @Expose
    private Date dateModified;

    @SerializedName("categories")
    @Expose
    private RealmList<Category> categories;

    @SerializedName("images")
    @Expose
    private RealmList<Image> images = new RealmList<>();

    @SerializedName("default_attributes")
    @Expose
    private RealmList<Attribute> defaultAttributes;

    @SerializedName("downloads")
    @Expose
    private RealmList<Download> downloads;

    @SerializedName(ProductTypeAdapter.DOWNLOAD_URL_IDENTIFIER)
    @Expose
    private String downloadUrl;

    @SerializedName(ProductTypeAdapter.SAMPLE_DOWNLOAD_URL_IDENTIFIER)
    @Expose
    private String sampleDownloadUrl;

    @SerializedName(ProductTypeAdapter.PRICE_IDENTIFIER)
    @Expose
    private int price = 0;

    @SerializedName("quantity")
    @Expose
    private long quantity;

    @Expose
    @SerializedName("author")
    private Author author;

    @SerializedName(ProductTypeAdapter.ATTRIBUTE_PUBLICATION_YEAR_IDENTIFIER)
    @Expose
    private String publicationYear;

    @SerializedName(ProductTypeAdapter.ATTRIBUTE_ISBN_IDENTIFIER)
    @Expose
    private String ISBN;

    @SerializedName(ProductTypeAdapter.ATTRIBUTE_PUBLISHER_IDENTIFIER)
    @Expose
    private String publisher;

    @SerializedName(ProductTypeAdapter.DOLLAR_PRICE_IDENTIFIER)
    @Expose
    private int dollarPrice = 0;

    private long bytesDownloaded;

    private long previewBytesDownloaded;

    private long bytesTotal;

    private long previewBytesTotal;

    private long downloadId;

    private long previewDownloadId;

    private String downloadingUrl;

    private String previewDownloadingUrl;

    private String thumbnailSrc;

    private boolean isOrderSuccesful;

    private boolean liked;

    @SerializedName(ProductTypeAdapter.USD_VARIATION_ID_IDENTIFIER)
    @Expose
    private int usdVariationId;

    @SerializedName(ProductTypeAdapter.ETB_VARIATION_ID_IDENTIFIER)
    @Expose
    private int etbVariationId;

    @SerializedName(ProductTypeAdapter.VARIATION_IDENTIFIER)
    @Expose
    @Ignore
    private List<Product> variations;

    @SerializedName("total_usd_sales")
    private  long totalUsdSales;

    @SerializedName("total_etb_sales")
    private long totalEtbSales;

    @Override
    public long getId() {
        return id;
    }

    public int likeNumbers;

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Product beforeSave(final Context context) {

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            if (categories != null) {

                for (Category category : categories) {

                    category.addProduct(this);
                }
            }

            Product cachedProduct = realm.where(Product.class).equalTo("id", id).findFirst();

            if (getDownloadUrl() != null && getStatusEnum() == Product.StatusEnum.publish) {

                if (cachedProduct != null) {

                    if (getQuantity() == 0) {

                        setQuantity(cachedProduct.getQuantity());
                    }

                    if (downloadId == 0) {

                        setDownloadId(cachedProduct.getDownloadId());

                        setLikeNumbers();

                        setDownloadingUrl(cachedProduct.getDownloadingUrl());

                        setBytesDownloaded(cachedProduct.getBytesDownloaded());

                        setBytesTotal(cachedProduct.getBytesTotal());

                        setOrderSuccesful(cachedProduct.isOrderSuccesful);

                        setPreviewDownloadId(cachedProduct.getPreviewDownloadId());

                        setPreviewDownloadingUrl(cachedProduct.getPreviewDownloadingUrl());

                        setPreviewBytesDownloaded(cachedProduct.getPreviewBytesDownloaded());

                        setPreviewBytesTotal(cachedProduct.getPreviewBytesTotal());
                    }
                }

                return this;

            } else if (quantity != 0) {

                if (cachedProduct != null) {

                    cachedProduct.setQuantity(quantity);

                    return cachedProduct;
                }
            }

        } finally {

            if (realm != null) {

                realm.close();
            }
        }

        return null;
    }

    public String getThumbnailSrc() {
        return images != null && !images.isEmpty() ? images.get(0).getSrc() : "";
    }

    public int getLikeNumbers() {
        return likeNumbers;
    }

    public void setLikeNumbers() {
        Random r = new Random();
        int Low = 500;
        int High = 3000;
        this.likeNumbers = r.nextInt(High-Low) + Low;
    }

    public boolean isOrderSuccesful() {
        return isOrderSuccesful;
    }

    public void setOrderSuccesful(boolean orderSuccesful) {
        isOrderSuccesful = orderSuccesful;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public void setThumbnailSrc(String thumbnailSrc) {
        this.thumbnailSrc = thumbnailSrc;
    }

    public long getBytesDownloaded() {
        return bytesDownloaded;
    }

    public long getPreviewBytesDownloaded() {
        return previewBytesDownloaded;
    }

    public long getPreviewBytesTotal() {
        return previewBytesTotal;
    }

    public void setPreviewBytesTotal(long previewBytesTotal) {
        this.previewBytesTotal = previewBytesTotal;
    }

    public enum StatusEnum {

        publish,

        draft,

        pending_preview,

        trash
    }

    @SerializedName("status")
    @Expose
    private String status;

    public Product() {}

    public Product(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(RealmList<Category> categories) {
        this.categories = categories;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(RealmList<Image> images) {
        this.images = images;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;

        Product product = (Product) o;

        return id != 0 && id == product.id;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public boolean isFree() {
        return price == 0 && dollarPrice == 0;
    }

    /**
     * @param context
     * @return coins if in Ethiopia and dollarPrice if not
     */
    public int getPrice(Context context) {

        return PreferenceHelper.inEthiopia(context) ? this.price : this.dollarPrice;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public String getSampleDownloadUrl() {
        return sampleDownloadUrl;
    }

    public boolean fileExistsLocally(boolean preview) {

        String path = LomiFileUtils.LOMI_FOLDER_PATH + File.separator + id + (preview ? Constants.SAMPLE_FILE_EXTENSION : Constants.LOMI_FILE_EXTENSION);

        File file = new File(path);

        if(preview) {

            return file.getAbsoluteFile().exists() && previewBytesDownloaded != 0 && previewBytesDownloaded == previewBytesTotal;

        } else {

            return file.getAbsoluteFile().exists() && bytesDownloaded != 0 && bytesDownloaded == bytesTotal;
        }
    }

    public long getQuantity() {
        return quantity;
    }

    public StatusEnum getStatusEnum() {
        return StatusEnum.valueOf(this.status);
    }

    public void setStatusEnum(StatusEnum status) {
        this.status = status.name();
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(String publicationYear) {
        this.publicationYear = publicationYear;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public boolean isInMyBooks() {

        boolean isInMyBooks = false;

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            Customer customer = realm.where(Customer.class).findFirst();

            if(customer != null && customer.getBooks().contains(this)) {

                isInMyBooks = true;
            }

        } finally {

            if(realm != null) {

                realm.close();
            }
        }

        return isInMyBooks;
    }

    public boolean isPreviewDownloading() {
        return previewBytesDownloaded < previewBytesTotal;
    }

    public boolean isDownloading() {
        return bytesTotal != 0 && bytesDownloaded < bytesTotal;
    }

    public String getPreviewDownloadingUrl() {
        return previewDownloadingUrl;
    }

    public void setPreviewDownloadingUrl(String previewDownloadingUrl) {
        this.previewDownloadingUrl = previewDownloadingUrl;
    }

    public String getDownloadingUrl() {
        return downloadingUrl;
    }

    public void setDownloadingUrl(String downloadingUrl) {
        this.downloadingUrl = downloadingUrl;
    }

    public void setPreviewDownloadId(long previewDownloadId) {
        this.previewDownloadId = previewDownloadId;
    }

    public long getPreviewDownloadId() {
        return previewDownloadId;
    }

    public void setDownloadId(long downloadId) {
        this.downloadId = downloadId;
    }

    public long getDownloadId() {
        return downloadId;
    }

    public void setBytesDownloaded(long bytesDownloaded) {
        this.bytesDownloaded = bytesDownloaded;
    }

    public long getBytesTotal() {
        return bytesTotal;
    }

    public void setBytesTotal(long bytesTotal) {
        this.bytesTotal = bytesTotal;
    }

    public void setPreviewBytesDownloaded(long previewBytesDownloaded) {
        this.previewBytesDownloaded = previewBytesDownloaded;
    }

    public String getCategory() {
        return categories != null && !categories.isEmpty() ? categories.get(0).getName() : "";
    }

    public long getEtbVariationId() {
        return etbVariationId;
    }

    public long getUsdVariationId() {
        return usdVariationId;
    }

    public long getTotalEtbSales() {
        return totalEtbSales;
    }

    public long getTotalUsdSales() {
        return totalUsdSales;
    }
}