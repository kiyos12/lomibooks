package com.a360ground.lomibook.domain;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class LomiDownload extends RealmObject implements LomiModel {

    @SerializedName(value="id", alternate = {"product_id", "ID"})
    @Expose
    protected long id;

    @PrimaryKey
    private long productId;

    private boolean preview = false;

    private long downloadId;

    private long bytesDownloaded = 0;

    private int bytesTotal;

    private long downloadStarted;

    String downloadURI;

    String name;

    String filePath;

    public LomiDownload() {}

    public LomiDownload(long productId, long downloadId, int bytesDownloaded, long downloadStarted) {

        this.productId = productId;

        this.downloadId = downloadId;

        this.bytesDownloaded = bytesDownloaded;

        this.downloadStarted = downloadStarted;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public void setDownloadId(long downloadId) {
        this.downloadId = downloadId;
    }

    public void setDownloadStarted(long time) {
        this.downloadStarted = time;
    }

    public long getBytesDownloaded() {
        return bytesDownloaded;
    }

    public int getBytesTotal() {
        return bytesTotal;
    }

    public void setBytesDownloaded(long bytesDownloaded) {
        this.bytesDownloaded = bytesDownloaded;
    }

    public boolean isCompleted() {
        return bytesDownloaded == bytesTotal;
    }

    public void setBytesTotal(int bytesTotal) {
        this.bytesTotal = bytesTotal;
    }

    public String getDownloadURI() {
        return downloadURI;
    }

    public void setDownloadURI(String downloadURI) {
        this.downloadURI = downloadURI;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public void setPreview(boolean preview) {
        this.preview = preview;
    }

    public boolean isPreview() {
        return this.preview;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public LomiDownload beforeSave(Context context) {
        return this;
    }
}
