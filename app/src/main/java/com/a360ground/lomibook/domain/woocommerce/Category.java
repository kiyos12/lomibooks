package com.a360ground.lomibook.domain.woocommerce;

import android.content.Context;

import com.a360ground.lomibook.domain.LomiModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Category extends RealmObject implements LomiModel {

    @PrimaryKey
    @SerializedName(value="id", alternate = {"ID"})
    @Expose
    protected long id;

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("parent")
    @Expose
    private Integer parent;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("display")
    @Expose
    private String display;
    @SerializedName("image")
    @Expose
    private Image image;

    @SerializedName("count")
    @Expose
    private Integer count;

    private RealmList<Product> products = new RealmList<>();

    @SerializedName("menu_order")
    @Expose
    private int menuOrder;

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The slug
     */
    public String getSlug() {
        return slug;
    }

    /**
     *
     * @param slug
     * The slug
     */
    public void setSlug(String slug) {
        this.slug = slug;
    }

    /**
     *
     * @return
     * The parent
     */
    public Integer getParent() {
        return parent;
    }

    /**
     *
     * @param parent
     * The parent
     */
    public void setParent(Integer parent) {
        this.parent = parent;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     *
     * @param count
     * The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }


    /**
     * equals normally compares objects by their memory locations
     * This causes a problem in {@Link MainActivity}
     * after GetCategoriesAsyncHttpTask has fetched new categories
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Category)) return false;

        Category category = (Category) o;

        return id != 0 && id == category.id;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public Image getImage() {
        return image;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public int getMenuOrder() {
        return menuOrder;
    }

    public RealmList<Product> getProducts() {
        return products;
    }

    public void addProduct(Product product) {
        this.products.add(product);
    }

    @Override
    public Category beforeSave(final Context context) {
        if(count > 0) {
            this.products.addAll(Realm.getDefaultInstance().where(Product.class).equalTo("categories.id", id).findAll());
            return this;
        }
        return null;
    }
}