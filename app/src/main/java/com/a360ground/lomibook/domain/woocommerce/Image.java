package com.a360ground.lomibook.domain.woocommerce;

import android.content.Context;

import com.a360ground.lomibook.domain.LomiModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Image extends RealmObject implements LomiModel {

    @PrimaryKey
    @SerializedName(value="id", alternate = {"ID"})
    @Expose
    protected long id;

    @SerializedName("src")
    @Expose
    private String src;

    public Image() {}

    public Image(Integer id, String src) {
        this.id = id;
        this.src = src;
    }

    /**
     *
     * @return
     * The src
     */
    public String getSrc() {
        return src;
    }

    /**
     *
     * @param src
     * The src
     */
    public void setSrc(String src) {
        this.src = src;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Image beforeSave(Context context) {
        return this;
    }
}