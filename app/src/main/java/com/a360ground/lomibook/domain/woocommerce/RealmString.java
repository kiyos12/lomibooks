package com.a360ground.lomibook.domain.woocommerce;

import io.realm.RealmObject;

public class RealmString extends RealmObject {

    private String option;

    public RealmString() {
    }

    public RealmString(String s) {
        this.option = s;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }
}