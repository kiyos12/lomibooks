package com.a360ground.lomibook.domain.woocommerce;

import io.realm.RealmObject;

/**
 * Created by Kiyos on 6/3/2017.
 */

public class MetaBox extends RealmObject {
    /**
     * lomi_vcode : 12
     * update_type : recommended
     * lomi_apk_url : https://support.google.com/chrome/?p=help&ctx=keyboard
     */

    private String lomi_vcode;
    private String update_type;
    private String lomi_apk_url;

    public String getLomi_vcode() {
        return lomi_vcode;
    }

    public void setLomi_vcode(String lomi_vcode) {
        this.lomi_vcode = lomi_vcode;
    }

    public String getUpdate_type() {
        return update_type;
    }

    public void setUpdate_type(String update_type) {
        this.update_type = update_type;
    }

    public String getLomi_apk_url() {
        return lomi_apk_url;
    }

    public void setLomi_apk_url(String lomi_apk_url) {
        this.lomi_apk_url = lomi_apk_url;
    }
}
