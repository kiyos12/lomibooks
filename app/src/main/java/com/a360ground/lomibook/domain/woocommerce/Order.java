package com.a360ground.lomibook.domain.woocommerce;

import android.content.Context;

import com.a360ground.lomibook.domain.LomiModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Order extends RealmObject implements LomiModel {

    @PrimaryKey
    @SerializedName(value = "id")
    @Expose
    protected long id;
    @SerializedName("total_price")
    @Expose
    private int totalPrice;
    @SerializedName("order_number")
    @Expose
    private Integer orderNumber;
    @SerializedName("order_key")
    @Expose
    private String orderKey;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("customer_id")
    @Expose
    private Long customerId;
    @SerializedName("line_items")
    @Expose
    private RealmList<LineItem> lineItems;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("payment_method_title")
    @Expose
    private String paymentMethodTitle;
    @SerializedName("set_paid")
    @Expose
    private boolean setPaid;
    @SerializedName("coins_spent")
    @Expose
    private long coinsSpent;

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The customerId
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId The customer_id
     */
    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    /**
     * @return The lineItems
     */
    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(RealmList<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    @Override
    public long getId() {
        return id;
    }

    public long getCoinsSpent() {
        return coinsSpent;
    }

    public void setCoinsSpent(long coinsSpent) {
        this.coinsSpent = coinsSpent;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public Order beforeSave(Context context) {
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;

        Product product = (Product) o;

        return id != 0 && id == product.id;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public void setPaymentMethodTitle(String paymentMethodTitle) {
        this.paymentMethodTitle = paymentMethodTitle;
    }

    public void setSetPaid(boolean setPaid) {
        this.setPaid = setPaid;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public enum StatusEnum {
        PENDING {
            @Override
            public String toString() {
                return "pending";
            }
        },
        PROCESSING {
            @Override
            public String toString() {
                return "processing";
            }
        },
        ON_HOLD {
            @Override
            public String toString() {
                return "on-hold";
            }
        },
        COMPLETED {
            @Override
            public String toString() {
                return "completed";
            }
        },
        CANCELED {
            @Override
            public String toString() {
                return "canceled";
            }
        },
        REFUNDED {
            @Override
            public String toString() {
                return "refunded";
            }
        },
        FAILED {
            @Override
            public String toString() {
                return "failed";
            }
        }
    }

    public enum PaymentMethod {

        DIRECT_BANK_TRANSFER,

        PAYPAL,

        ETHIO_TELECOM,

        HELLO_CASH
    }
}