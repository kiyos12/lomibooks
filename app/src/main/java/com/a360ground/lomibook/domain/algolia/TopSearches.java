package com.a360ground.lomibook.domain.algolia;

import android.content.Context;
import android.os.Parcel;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.a360ground.lomibook.adapter.TopSearchAdapter;
import com.a360ground.lomibook.client.SearchesClient;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.koolearn.klibrary.core.util.Native;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopSearches implements SearchSuggestion{
    /**
     * query : ሸክም
     */

    private String query;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @Override
    public String getBody() {
        return query;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public TopSearches(Parcel in){
        query = in.readString();
    }

    public TopSearches(){

    }

    public static final Creator<TopSearches> CREATOR = new Creator<TopSearches>() {
        @Override
        public TopSearches createFromParcel(Parcel in) {
            return new TopSearches(in);
        }

        @Override
        public TopSearches[] newArray(int size) {
            return new TopSearches[size];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(query);
    }
}
