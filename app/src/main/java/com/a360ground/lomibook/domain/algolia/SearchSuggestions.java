package com.a360ground.lomibook.domain.algolia;

import android.os.Parcel;

import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kiyos on 9/29/2017.
 */

public class SearchSuggestions implements SearchSuggestion {

    @SerializedName("title")
    private
    String suggestion;

    @SerializedName("id")
    int id;

    public String getsuggestion() {
        return suggestion;
    }

    public void setsuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String getBody() {
        return suggestion;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(suggestion);
    }

    public static final Creator<TopSearches> CREATOR = new Creator<TopSearches>() {
        @Override
        public TopSearches createFromParcel(Parcel in) {
            return new TopSearches(in);
        }

        @Override
        public TopSearches[] newArray(int size) {
            return new TopSearches[size];
        }
    };
}
