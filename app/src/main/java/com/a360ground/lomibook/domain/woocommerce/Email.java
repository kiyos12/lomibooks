package com.a360ground.lomibook.domain.woocommerce;

/**
 * Created by Kiyos on 6/23/2017.
 */

public class Email {

    private String subject;
    private String message;
    private String emailAdd;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmailAdd() {
        return emailAdd;
    }

    public void setEmailAdd(String emailAdd) {
        this.emailAdd = emailAdd;
    }
}
