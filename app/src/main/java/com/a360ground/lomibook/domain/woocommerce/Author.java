package com.a360ground.lomibook.domain.woocommerce;

import android.content.Context;
import com.a360ground.lomibook.domain.LomiModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Author extends RealmObject implements LomiModel {

    @PrimaryKey
    @SerializedName(value="id", alternate = {"ID"})
    @Expose
    protected long id;

    @SerializedName("name")
    @Expose
    private String name;

    private RealmList<Product> products;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Author beforeSave(Context context) {
        return this;
    }
}