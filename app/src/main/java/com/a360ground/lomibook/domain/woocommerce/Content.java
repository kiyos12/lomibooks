package com.a360ground.lomibook.domain.woocommerce;

import io.realm.RealmObject;

/**
 * Created by Kiyos on 6/3/2017.
 */

public class Content extends RealmObject {
    /**
     * rendered : <ol>
     <li>Updated Crash</li>
     <li>You can login in an other device Most of the time, you will primarily be concerned with <code>onResponderMove</code> and <code>onResponderRelease</code>.All of these methods receive a synthetic touch event object, which adheres to the following format (again, excerpted from the documentation):
     <ul>
     <li><code>changedTouches</code> &#8211; Array of all touch events that have changed since the last event</li>
     <li><code>identifier</code> &#8211; The ID of the touch</li>
     <li><code>locationX</code> &#8211; The X position of the touch, relative to the element</li>
     <li><code>locationY</code> &#8211; The Y position of the touch, relative to the element</li>
     <li><code>pageX</code> &#8211; The X position of the touch, relative to the screen</li>
     <li><code>pageY</code> &#8211; The Y position of the touch, relative to the screen</li>
     <li><code>target</code> &#8211; The node id of the element receiving the touch event</li>
     <li><code>timestamp</code> &#8211; A time identifier for the touch, useful for velocity calculation</li>
     <li><code>touches</code> &#8211; Array of all current touches on the screen</li>
     </ul>
     </li>
     </ol>

     * protected : false
     */

    private String rendered;
    @com.google.gson.annotations.SerializedName("protected")
    private boolean protectedX;

    public String getRendered() {
        return rendered;
    }

    public void setRendered(String rendered) {
        this.rendered = rendered;
    }

    public boolean isProtectedX() {
        return protectedX;
    }

    public void setProtectedX(boolean protectedX) {
        this.protectedX = protectedX;
    }
}