package com.a360ground.lomibook.domain.woocommerce;

import android.content.Context;

import com.a360ground.lomibook.domain.LomiModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Customer extends RealmObject implements LomiModel, Serializable {

    public static final String LOMI_COINS_JSON_LABEL = "lomi_coins";

    @PrimaryKey
    @SerializedName(value="id", alternate = {"ID"})
    @Expose
    protected long id;

    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("last_update")
    @Expose
    private String lastUpdate;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("last_order_id")
    @Expose
    private String lastOrderId;
    @SerializedName("last_order_date")
    @Expose
    private String lastOrderDate;
    @SerializedName("orders_count")
    @Expose
    private Integer ordersCount;

    @SerializedName("avatar_url")
    @Expose
    private String avatarUrl;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName(LOMI_COINS_JSON_LABEL)
    @Expose
    private long lomiCoins = 0;

    @SerializedName("is_author")
    private boolean isAuthor;

    @SerializedName("coins_spent")
    private long coinsSpent;

    private long usdSpent;

    // code and message are here only to support errors
    // on duplicate phone number refer to the WooCommerce
    // theme implementation
    // file: wp-content/themes/shopping/lib/custom_functions.php
    @SerializedName("code")
    private String code;

    @SerializedName("message")
    private String message;

    @SerializedName("device_id")
    private String deviceId;

    RealmList<Product> books = new RealmList<>();

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return this.username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public void setLomiCoins(long lomiCoins) {
        this.lomiCoins = lomiCoins;
    }

    public long getLomiCoins() {
        return lomiCoins;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void addBook(Product product) {
        this.books.add(product);
    }
    public void addBooks(List<Product> products) {
        this.books.addAll(products);
    }

    public RealmList<Product> getBooks() {
        return books;
    }

    public void setBooks(RealmList<Product> books) {
        this.books = books;
    }

    @Override
    public Customer beforeSave(Context context) {

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            Customer customer = realm.where(Customer.class).findFirst();

            if(customer != null) {

                lomiCoins = customer.getLomiCoins();

                books.addAll(customer.getBooks());

                coinsSpent = customer.getCoinsSpent();

                usdSpent = customer.getUsdSpent();
            }

        } finally {

            if(realm != null) {

                realm.close();
            }
        }

        return this;
    }

    public long getUsdSpent() {
        return usdSpent;
    }

    public long getCoinsSpent() {
        return coinsSpent;
    }

    public void setUsdSpent(long usdSpent) {
        this.usdSpent = usdSpent;
    }

    public void setCoinsSpent(long coinsSpent) {
        this.coinsSpent = coinsSpent;
    }

    public boolean isAuthor() {
        return isAuthor;
    }

    public float getTotalRevenue(boolean international) {

        float totalRevenue = 0;

        if(isAuthor) {

            Realm realm = null;

            try {

                realm = Realm.getDefaultInstance();

                List<Product> authorBooks = realm.where(Product.class).equalTo("author.id", id).findAll();

                for(Product product : authorBooks) {

                    if(international) {

                        totalRevenue += product.getTotalUsdSales();

                    } else {

                        totalRevenue += product.getTotalEtbSales();
                    }
                }

            } finally {

                if(realm != null) {

                    realm.close();
                }
            }
        }

        return totalRevenue;
    }
}