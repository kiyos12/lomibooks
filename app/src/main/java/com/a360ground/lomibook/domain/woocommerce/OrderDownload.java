package com.a360ground.lomibook.domain.woocommerce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDownload {

    @SerializedName("download_url")
    @Expose
    private String downloadUrl;

    @SerializedName("download_id")
    @Expose
    private String downloadId;

    @SerializedName("product_id")
    @Expose
    private long productId;

    @SerializedName("download_name")
    @Expose
    private String downloadName;

    @SerializedName("order_id")
    @Expose
    private long orderId;

    @SerializedName("order_key")
    @Expose
    private String orderKey;

    @SerializedName("downloadsRemaining")
    @Expose
    private int downloadsRemaining;

    @SerializedName("access_expires")
    @Expose
    private String accessExpires;

    @SerializedName("file")
    @Expose
    private OrderDownloadFile file;

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public String getDownloadId() {
        return downloadId;
    }

    public long getProductId() {
        return productId;
    }

    public String getDownloadName() {
        return downloadName;
    }

    public long getOrderId() {
        return orderId;
    }

    public String getOrderKey() {
        return orderKey;
    }

    public int getDownloadsRemaining() {
        return downloadsRemaining;
    }

    public String getAccessExpires() {
        return accessExpires;
    }

    public OrderDownloadFile getFile() {
        return file;
    }
}
