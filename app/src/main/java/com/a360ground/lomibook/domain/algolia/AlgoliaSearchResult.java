package com.a360ground.lomibook.domain.algolia;

import com.a360ground.lomibook.domain.woocommerce.Author;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AlgoliaSearchResult {


    /**
     * hits : [{"post_id":92234,"post_type":"product","post_type_label":"Products","post_title":"የሕይወት ጉዞዬ እና ትዝታዎቼ - በሀገር ቤት  ክፍል ሁለት","post_excerpt":"<p>yehiwot, hiwot, yhiwot<br />\nguzoyena tizetawoche<br />\ngetachew<br />\ndr doctor <\/p>\n","post_date":1495979309,"post_date_formatted":"May 28, 2017","post_modified":1497471641,"comment_count":0,"menu_order":0,"post_author":{"user_id":145,"display_name":"Biruk Hailu","user_url":"http://360ground.com","user_login":"360_admin"},"images":{"thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-150x150.jpg","width":150,"height":150},"medium":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-234x300.jpg","width":234,"height":300},"medium_large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7.jpg","width":569,"height":730},"large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7.jpg","width":569,"height":730},"custom-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-569x300.jpg","width":569,"height":300},"custom-small-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-100x135.jpg","width":100,"height":135},"post-thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-100x135.jpg","width":100,"height":135},"shop_thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-180x180.jpg","width":180,"height":180},"shop_catalog":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-300x300.jpg","width":300,"height":300},"shop_single":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-569x600.jpg","width":569,"height":600}},"permalink":"https://api.lomistore.com/product/%e1%8b%a8%e1%88%95%e1%8b%ad%e1%8b%88%e1%89%b5-%e1%8c%89%e1%8b%9e%e1%8b%ac-%e1%8a%a5%e1%8a%93-%e1%89%b5%e1%8b%9d%e1%89%b3%e1%8b%8e%e1%89%bc-%e1%89%a0%e1%88%80%e1%8c%88%e1%88%ad-%e1%89%a4%e1%89%b5/","post_mime_type":"","taxonomies":{"product_type":["variable"],"product_cat":["እንዲያነቡት የሚመከሩ","የህይወት ታሪኮች"],"product_tag":[],"product_shipping_class":[],"pa_author-name":["ሀዲስ ዓለማየሁ"],"pa_currency":["etb","usd"],"pa_dollar-price":[],"pa_publication-year":["2000"],"pa_publisher":["Lomi Publishing"]},"taxonomies_hierarchical":{"product_cat":{"lvl0":["እንዲያነቡት የሚመከሩ","የህይወት ታሪኮች"]},"pa_author-name":{"lvl0":["ሀዲስ ዓለማየሁ"]},"pa_currency":{"lvl0":["etb","usd"]},"pa_dollar-price":[],"pa_publication-year":{"lvl0":["2000"]},"pa_publisher":{"lvl0":["Lomi Publishing"]}},"is_sticky":0,"title1":"","title2":"","title3":"","title4":"","title5":"","title6":"","content":"በመጀመሪያ የሕይወት ጉዞዬና ትዝታዎቼ በሚል ርዕስ ለኅትመት ያበቃሁት መጽሐፍ በጠቅላላ የሚተርከው የውጭ ዓለም ኑሮዬንና ጉዞዬን ነው፡፡ ይህ መጽሐፍ ብዙ ሰዎች ተወያይተውበታል፣ በመጽሔትና በጋዜጣ፣ በሬዲዮና በቴሌቪዥን ብዙ ሽፋን አግኝቷል፡፡ ከብዙ ግለ ሰቦችም ከማስበው በላይ ምስጋና አግኝቼበታለሁ፡፡ አበጀህ፣ ያበርታህ ተብዬበታለሁ፡፡ ዕውቀት አግኝተንበታል ብለውኛል፡፡ በዚህም በጣም ደስ ብሎኛል፡፡ አሁን ደግሞ የሕይወት ጉዞዬና ትዝታዎቼ፣ ክፍል ሁለት &#8211; በሀገር ቤት ይኸው ለኅትመት አበቃሁ፡፡ የዚህ ጽሑፍ ዋናዋ ተወናዋይ እኔው እራሴ ነኝ፡፡ ወደ እምወዳት ሀገሬ ከተመለስኩ በኋላ ያጋጠሙኝን የደስታ፣ የሀዘን፣ የሚያናድድ፣ የሚያስደስት፣ አልፎ አልፎም በጣም የሚያስቅ ሕይወት መርቻለሁ፡፡ ሁሉንም እንደወረደ አስቀምጨዋለሁ፡፡ ቤት ሥሰራ የነበረው ምልልስ፣ የመሬት ካርታ ለማስለወጥ የነበረውን ውጣ ውረድ፣ ባሕር ማዶ ያሉት ጓደኞቼ ለመጀመሪያ ጊዜ ኢትዮጵን ለመጎብኘትና እኔንም ለመጠየቅ ሲመጡ የነበረውን ገጠመኞች፣ ከበጎ አድራጎት ድርጅቶች ጋር ሥራ፣ የሠፈር መንገድ ለማሠራት፣ ሲያቀብጠኝ ኃላፊነቱን ተቀብዬ፣ ጎረቤቶቼ ራሳቸው መርጠውኝ &#8211; መራጮቹ አንከፍልም ሲሉኝ፣ ኧረ ስንቱ? በሀገር ውስጥ ከደራሲያን ጋር ያደረኩትን ጉብኝት፣ ሀዘኑን &#8211; ለቅሶውን፣ ሠርጉን &#8211; ጭፈራውን ወዘተርፈ. በሰፊውም ባይሆን አጠር መጠን አድርጌ በትረካ መልክ አቅርቤዋለሁ፡፡ ምንም እንኳን ርዕሱ በሀገር ቤት ቢልም፣ ከዚሁ ከሀገር ቤት ወደ ውጭ አገር ሔጄ የጎበኘኋቸውን የባዕድ አገሮችንም አብሬ ተርኬ አውጥቼዋለሁ፡፡ ቻይናዎች \u201cአንድ ፎቶግራፍ አንድ ሺ ቃላት ያወራል\u201d ይላሉ፡፡ እኔም አምንበታለሁ፡፡ ስለዚህ ከትረካው ጋር የሚዛመዱ ብዙ ፎቶግራፎች አብሬ አክየበታለሁ፡፡","record_index":0,"objectID":"92234-0","_snippetResult":{"post_title":{"value":"የሕይወት ጉዞዬ <em>እ<\/em>ና ትዝታዎቼ - በሀገር ቤት  ክፍል ሁለት","matchLevel":"full"},"title1":{"value":"","matchLevel":"none"},"title2":{"value":"","matchLevel":"none"},"title3":{"value":"","matchLevel":"none"},"title4":{"value":"","matchLevel":"none"},"title5":{"value":"","matchLevel":"none"},"title6":{"value":"","matchLevel":"none"},"content":{"value":"\u2026 ደግሞ የሕይወት ጉዞዬና ትዝታዎቼ፣ ክፍል ሁለት \u2013 በሀገር ቤት ይኸው ለኅትመት አበቃሁ፡፡ የዚህ ጽሑፍ ዋናዋ ተወናዋይ <em>እ<\/em>ኔው <em>እ<\/em>ራሴ ነኝ፡፡ ወደ <em>እ<\/em>ምወዳት ሀገሬ ከተመለስኩ በኋላ ያጋጠሙኝን የደስታ፣ የሀዘን፣ የሚያናድድ፣ የሚያስደስት፣ አልፎ አልፎም \u2026","matchLevel":"full"}},"_highlightResult":{"post_title":{"value":"የሕይወት ጉዞዬ <em>እ<\/em>ና ትዝታዎቼ - በሀገር ቤት  ክፍል ሁለት","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]},"taxonomies":{"product_type":[{"value":"variable","matchLevel":"none","matchedWords":[]}],"product_cat":[{"value":"<em>እ<\/em>ንዲያነቡት የሚመከሩ","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]},{"value":"የህይወት ታሪኮች","matchLevel":"none","matchedWords":[]}],"pa_author-name":[{"value":"ሀዲስ ዓለማየሁ","matchLevel":"none","matchedWords":[]}],"pa_currency":[{"value":"etb","matchLevel":"none","matchedWords":[]},{"value":"usd","matchLevel":"none","matchedWords":[]}],"pa_publication-year":[{"value":"2000","matchLevel":"none","matchedWords":[]}],"pa_publisher":[{"value":"Lomi Publishing","matchLevel":"none","matchedWords":[]}]},"title1":{"value":"","matchLevel":"none","matchedWords":[]},"title2":{"value":"","matchLevel":"none","matchedWords":[]},"title3":{"value":"","matchLevel":"none","matchedWords":[]},"title4":{"value":"","matchLevel":"none","matchedWords":[]},"title5":{"value":"","matchLevel":"none","matchedWords":[]},"title6":{"value":"","matchLevel":"none","matchedWords":[]},"content":{"value":"በመጀመሪያ የሕይወት ጉዞዬና ትዝታዎቼ በሚል ርዕስ ለኅትመት ያበቃሁት መጽሐፍ በጠቅላላ የሚተርከው የውጭ ዓለም ኑሮዬንና ጉዞዬን ነው፡፡ ይህ መጽሐፍ ብዙ ሰዎች ተወያይተውበታል፣ በመጽሔትና በጋዜጣ፣ በሬዲዮና በቴሌቪዥን ብዙ ሽፋን አግኝቷል፡፡ ከብዙ ግለ ሰቦችም ከማስበው በላይ ምስጋና አግኝቼበታለሁ፡፡ አበጀህ፣ ያበርታህ ተብዬበታለሁ፡፡ ዕውቀት አግኝተንበታል ብለውኛል፡፡ በዚህም በጣም ደስ ብሎኛል፡፡ አሁን ደግሞ የሕይወት ጉዞዬና ትዝታዎቼ፣ ክፍል ሁለት \u2013 በሀገር ቤት ይኸው ለኅትመት አበቃሁ፡፡ የዚህ ጽሑፍ ዋናዋ ተወናዋይ <em>እ<\/em>ኔው <em>እ<\/em>ራሴ ነኝ፡፡ ወደ <em>እ<\/em>ምወዳት ሀገሬ ከተመለስኩ በኋላ ያጋጠሙኝን የደስታ፣ የሀዘን፣ የሚያናድድ፣ የሚያስደስት፣ አልፎ አልፎም በጣም የሚያስቅ ሕይወት መርቻለሁ፡፡ ሁሉንም <em>እ<\/em>ንደወረደ አስቀምጨዋለሁ፡፡ ቤት ሥሰራ የነበረው ምልልስ፣ የመሬት ካርታ ለማስለወጥ የነበረውን ውጣ ውረድ፣ ባሕር ማዶ ያሉት ጓደኞቼ ለመጀመሪያ ጊዜ ኢትዮጵን ለመጎብኘትና <em>እ<\/em>ኔንም ለመጠየቅ ሲመጡ የነበረውን ገጠመኞች፣ ከበጎ አድራጎት ድርጅቶች ጋር ሥራ፣ የሠፈር መንገድ ለማሠራት፣ ሲያቀብጠኝ ኃላፊነቱን ተቀብዬ፣ ጎረቤቶቼ ራሳቸው መርጠውኝ \u2013 መራጮቹ አንከፍልም ሲሉኝ፣ ኧረ ስንቱ? በሀገር ውስጥ ከደራሲያን ጋር ያደረኩትን ጉብኝት፣ ሀዘኑን \u2013 ለቅሶውን፣ ሠርጉን \u2013 ጭፈራውን ወዘተርፈ. በሰፊውም ባይሆን አጠር መጠን አድርጌ በትረካ መልክ አቅርቤዋለሁ፡፡ ምንም <em>እ<\/em>ንኳን ርዕሱ በሀገር ቤት ቢልም፣ ከዚሁ ከሀገር ቤት ወደ ውጭ አገር ሔጄ የጎበኘኋቸውን የባዕድ አገሮችንም አብሬ ተርኬ አውጥቼዋለሁ፡፡ ቻይናዎች \u201cአንድ ፎቶግራፍ አንድ ሺ ቃላት ያወራል\u201d ይላሉ፡፡ <em>እ<\/em>ኔም አምንበታለሁ፡፡ ስለዚህ ከትረካው ጋር የሚዛመዱ ብዙ ፎቶግራፎች አብሬ አክየበታለሁ፡፡","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]}}},{"post_id":92010,"post_type":"product","post_type_label":"Products","post_title":"እምቢታ","post_excerpt":"","post_date":1495901064,"post_date_formatted":"May 27, 2017","post_modified":1497467140,"comment_count":0,"menu_order":0,"post_author":{"user_id":145,"display_name":"Biruk Hailu","user_url":"http://360ground.com","user_login":"360_admin"},"images":{"thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/Embita-150x150.jpg","width":150,"height":150},"medium":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/Embita-234x300.jpg","width":234,"height":300},"medium_large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/Embita.jpg","width":569,"height":730},"large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/Embita.jpg","width":569,"height":730},"custom-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/Embita-569x300.jpg","width":569,"height":300},"custom-small-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/Embita-100x135.jpg","width":100,"height":135},"post-thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/Embita-100x135.jpg","width":100,"height":135},"shop_thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/Embita-180x180.jpg","width":180,"height":180},"shop_catalog":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/Embita-300x300.jpg","width":300,"height":300},"shop_single":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/Embita-569x600.jpg","width":569,"height":600}},"permalink":"https://api.lomistore.com/product/%e1%8a%a5%e1%88%9d%e1%89%a2%e1%89%b3/","post_mime_type":"","taxonomies":{"product_type":["variable"],"product_cat":["ፎክሎራዊ ልቦለድ"],"product_tag":[],"product_shipping_class":[],"pa_author-name":["ሀዲስ ዓለማየሁ"],"pa_currency":["etb","usd"],"pa_dollar-price":[],"pa_publication-year":["2000"],"pa_publisher":["Lomi Publishing"]},"taxonomies_hierarchical":{"product_cat":{"lvl0":["ፎክሎራዊ ልቦለድ"]},"pa_author-name":{"lvl0":["ሀዲስ ዓለማየሁ"]},"pa_currency":{"lvl0":["etb","usd"]},"pa_dollar-price":[],"pa_publication-year":{"lvl0":["2000"]},"pa_publisher":{"lvl0":["Lomi Publishing"]}},"is_sticky":0,"title1":"","title2":"","title3":"","title4":"","title5":"","title6":"","content":"ሐ. ይህች ድርሰት መጀመርያ በልቦለድ ሳይሆን በተውኔት መልክ ተጽፋ፣ የአዲስ አበባ ዩኒቨርሲቲ፣ የቲያትር ጥበባት ትምህርት ክፍል ተማሪ ሳለሁ (በ1990ዎቹ መጨረሻ) ለኮርስ ማሟያ ትሆን ዘንድ አቅርቤያት ነበር፡፡ በዓመቱ በመምህር ነብዩ ባዬ አዘጋጅነት፣ በተማሪዎች ተዋናይነት እዚያው ለእይታ በቅታ ነበር፡፡ አሁን ደግሞ በልቦለድ መልክ ቀረበች፡፡","record_index":3,"objectID":"92010-3","_snippetResult":{"post_title":{"value":"<em>እ<\/em>ምቢታ","matchLevel":"full"},"title1":{"value":"","matchLevel":"none"},"title2":{"value":"","matchLevel":"none"},"title3":{"value":"","matchLevel":"none"},"title4":{"value":"","matchLevel":"none"},"title5":{"value":"","matchLevel":"none"},"title6":{"value":"","matchLevel":"none"},"content":{"value":"\u2026 የቲያትር ጥበባት ትምህርት ክፍል ተማሪ ሳለሁ (በ1990ዎቹ መጨረሻ) ለኮርስ ማሟያ ትሆን ዘንድ አቅርቤያት ነበር፡፡ በዓመቱ በመምህር ነብዩ ባዬ አዘጋጅነት፣ በተማሪዎች ተዋናይነት <em>እ<\/em>ዚያው ለእይታ በቅታ ነበር፡፡ አሁን ደግሞ በልቦለድ መልክ ቀረበች፡፡","matchLevel":"full"}},"_highlightResult":{"post_title":{"value":"<em>እ<\/em>ምቢታ","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]},"taxonomies":{"product_type":[{"value":"variable","matchLevel":"none","matchedWords":[]}],"product_cat":[{"value":"ፎክሎራዊ ልቦለድ","matchLevel":"none","matchedWords":[]}],"pa_author-name":[{"value":"ሀዲስ ዓለማየሁ","matchLevel":"none","matchedWords":[]}],"pa_currency":[{"value":"etb","matchLevel":"none","matchedWords":[]},{"value":"usd","matchLevel":"none","matchedWords":[]}],"pa_publication-year":[{"value":"2000","matchLevel":"none","matchedWords":[]}],"pa_publisher":[{"value":"Lomi Publishing","matchLevel":"none","matchedWords":[]}]},"title1":{"value":"","matchLevel":"none","matchedWords":[]},"title2":{"value":"","matchLevel":"none","matchedWords":[]},"title3":{"value":"","matchLevel":"none","matchedWords":[]},"title4":{"value":"","matchLevel":"none","matchedWords":[]},"title5":{"value":"","matchLevel":"none","matchedWords":[]},"title6":{"value":"","matchLevel":"none","matchedWords":[]},"content":{"value":"ሐ. ይህች ድርሰት መጀመርያ በልቦለድ ሳይሆን በተውኔት መልክ ተጽፋ፣ የአዲስ አበባ ዩኒቨርሲቲ፣ የቲያትር ጥበባት ትምህርት ክፍል ተማሪ ሳለሁ (በ1990ዎቹ መጨረሻ) ለኮርስ ማሟያ ትሆን ዘንድ አቅርቤያት ነበር፡፡ በዓመቱ በመምህር ነብዩ ባዬ አዘጋጅነት፣ በተማሪዎች ተዋናይነት <em>እ<\/em>ዚያው ለእይታ በቅታ ነበር፡፡ አሁን ደግሞ በልቦለድ መልክ ቀረበች፡፡","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]}}},{"post_id":91933,"post_type":"product","post_type_label":"Products","post_title":"ትዝታዎቼ  እና ገጠመኞቼ","post_excerpt":"","post_date":1495741354,"post_date_formatted":"May 25, 2017","post_modified":1497466971,"comment_count":0,"menu_order":0,"post_author":{"user_id":145,"display_name":"Biruk Hailu","user_url":"http://360ground.com","user_login":"360_admin"},"images":{"thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-1-150x150.jpg","width":150,"height":150},"medium":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-1-234x300.jpg","width":234,"height":300},"medium_large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-1.jpg","width":569,"height":730},"large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-1.jpg","width":569,"height":730},"custom-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-1-569x300.jpg","width":569,"height":300},"custom-small-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-1-100x135.jpg","width":100,"height":135},"post-thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-1-100x135.jpg","width":100,"height":135},"shop_thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-1-180x180.jpg","width":180,"height":180},"shop_catalog":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-1-300x300.jpg","width":300,"height":300},"shop_single":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-1-569x600.jpg","width":569,"height":600}},"permalink":"https://api.lomistore.com/product/%e1%89%b5%e1%8b%9d%e1%89%b3%e1%8b%8e%e1%89%bc-%e1%8a%a5%e1%8a%93-%e1%8c%88%e1%8c%a0%e1%88%98%e1%8a%9e%e1%89%bc/","post_mime_type":"","taxonomies":{"product_type":["variable"],"product_cat":["እንዲያነቡት የሚመከሩ","የህይወት ታሪኮች"],"product_tag":[],"product_shipping_class":[],"pa_author-name":["ሀዲስ ዓለማየሁ"],"pa_currency":["etb","usd"],"pa_dollar-price":[],"pa_publication-year":["2000"],"pa_publisher":["Lomi Publishing"]},"taxonomies_hierarchical":{"product_cat":{"lvl0":["እንዲያነቡት የሚመከሩ","የህይወት ታሪኮች"]},"pa_author-name":{"lvl0":["ሀዲስ ዓለማየሁ"]},"pa_currency":{"lvl0":["etb","usd"]},"pa_dollar-price":[],"pa_publication-year":{"lvl0":["2000"]},"pa_publisher":{"lvl0":["Lomi Publishing"]}},"is_sticky":0,"title1":"","title2":"","title3":"","title4":"","title5":"","title6":"","content":"ዓመተ ምህረቶቹ በሙሉ የተመዘገቡት እ.ኤ.አ (እንደ አውሮጳ አቆጣጠር) ነው፡","record_index":6,"objectID":"91933-6","_snippetResult":{"post_title":{"value":"ትዝታዎቼ  <em>እ<\/em>ና ገጠመኞቼ","matchLevel":"full"},"title1":{"value":"","matchLevel":"none"},"title2":{"value":"","matchLevel":"none"},"title3":{"value":"","matchLevel":"none"},"title4":{"value":"","matchLevel":"none"},"title5":{"value":"","matchLevel":"none"},"title6":{"value":"","matchLevel":"none"},"content":{"value":"ዓመተ ምህረቶቹ በሙሉ የተመዘገቡት <em>እ<\/em>.ኤ.አ (<em>እ<\/em>ንደ አውሮጳ አቆጣጠር) ነው፡","matchLevel":"full"}},"_highlightResult":{"post_title":{"value":"ትዝታዎቼ  <em>እ<\/em>ና ገጠመኞቼ","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]},"taxonomies":{"product_type":[{"value":"variable","matchLevel":"none","matchedWords":[]}],"product_cat":[{"value":"<em>እ<\/em>ንዲያነቡት የሚመከሩ","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]},{"value":"የህይወት ታሪኮች","matchLevel":"none","matchedWords":[]}],"pa_author-name":[{"value":"ሀዲስ ዓለማየሁ","matchLevel":"none","matchedWords":[]}],"pa_currency":[{"value":"etb","matchLevel":"none","matchedWords":[]},{"value":"usd","matchLevel":"none","matchedWords":[]}],"pa_publication-year":[{"value":"2000","matchLevel":"none","matchedWords":[]}],"pa_publisher":[{"value":"Lomi Publishing","matchLevel":"none","matchedWords":[]}]},"title1":{"value":"","matchLevel":"none","matchedWords":[]},"title2":{"value":"","matchLevel":"none","matchedWords":[]},"title3":{"value":"","matchLevel":"none","matchedWords":[]},"title4":{"value":"","matchLevel":"none","matchedWords":[]},"title5":{"value":"","matchLevel":"none","matchedWords":[]},"title6":{"value":"","matchLevel":"none","matchedWords":[]},"content":{"value":"ዓመተ ምህረቶቹ በሙሉ የተመዘገቡት <em>እ<\/em>.ኤ.አ (<em>እ<\/em>ንደ አውሮጳ አቆጣጠር) ነው፡","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]}}},{"post_id":91904,"post_type":"product","post_type_label":"Products","post_title":"እውነት ማለት የእኔ ልጅ","post_excerpt":"","post_date":1495739461,"post_date_formatted":"May 25, 2017","post_modified":1497521283,"comment_count":0,"menu_order":0,"post_author":{"user_id":145,"display_name":"Biruk Hailu","user_url":"http://360ground.com","user_login":"360_admin"},"images":{"thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/ewnet-150x150.jpg","width":150,"height":150},"medium":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/ewnet-234x300.jpg","width":234,"height":300},"medium_large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/ewnet.jpg","width":569,"height":730},"large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/ewnet.jpg","width":569,"height":730},"custom-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/ewnet-569x300.jpg","width":569,"height":300},"custom-small-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/ewnet-100x135.jpg","width":100,"height":135},"post-thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/ewnet-100x135.jpg","width":100,"height":135},"shop_thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/ewnet-180x180.jpg","width":180,"height":180},"shop_catalog":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/ewnet-300x300.jpg","width":300,"height":300},"shop_single":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/ewnet-569x600.jpg","width":569,"height":600}},"permalink":"https://api.lomistore.com/product/%e1%8a%a5%e1%8b%8d%e1%8a%90%e1%89%b5-%e1%88%9b%e1%88%88%e1%89%b5-%e1%8b%a8%e1%8a%a5%e1%8a%94-%e1%88%8d%e1%8c%85/","post_mime_type":"","taxonomies":{"product_type":["variable"],"product_cat":["እንዲያነቡት የሚመከሩ","ሥነ ግጥም"],"product_tag":[],"product_shipping_class":[],"pa_author-name":[],"pa_currency":["etb","usd"],"pa_dollar-price":[],"pa_publication-year":[],"pa_publisher":[]},"taxonomies_hierarchical":{"product_cat":{"lvl0":["እንዲያነቡት የሚመከሩ","ሥነ ግጥም"]},"pa_author-name":[],"pa_currency":{"lvl0":["etb","usd"]},"pa_dollar-price":[],"pa_publication-year":[],"pa_publisher":[]},"is_sticky":0,"title1":"","title2":"","title3":"","title4":"","title5":"","title6":"","content":"በድሉ ጥቂት ለማይባሉ ዓመታት ከሀገሩ ውጪ ቢኖርም የግጥሞቹ ርእሰ ጉዳዮች የሀገሩ ጉዳዮች ናቸው፡፡ በውጪ ሀገር ሲኖር የሚያጋጥምን ባይተዋርነትና ናፍቆት እንኳን ሲገልጽ የሀገሩን ልጅ በሰው ሀገር መኖርን አትመኝ እያለ የሚለምን የሚያባብልበት ነው &#8211; \u2039\u2039አንተንስ እኔን አያርግህ\u203a\u203a እያለ፡፡ ይህ ግጥም በስደት የሚኖር ሰው የሚሰማውን ባይተዋርነት፣ ባእድነት፣ ብቸኝነት የሚያሳይ፣ \u2039\u2039ለአገሩ እንግዳ፣ ለሰው ባዳ\u203a\u203a ብቻ ሳይኮን ለራስም እንግዳና ባዳ እንደሚያደርግ የሚገልጽ ግጥም ነው፡፡ እንደተራኪው በአካል ሳይሆን በኅሊና በምናብ የተሰደደ ወንድሙን የስደት ጣዕም ምሬት በመሆኑ ተው ይቅርብህ ሲል የመከረበት ነው፡፡","record_index":9,"objectID":"91904-9","_snippetResult":{"post_title":{"value":"<em>እ<\/em>ውነት ማለት የእኔ ልጅ","matchLevel":"full"},"title1":{"value":"","matchLevel":"none"},"title2":{"value":"","matchLevel":"none"},"title3":{"value":"","matchLevel":"none"},"title4":{"value":"","matchLevel":"none"},"title5":{"value":"","matchLevel":"none"},"title6":{"value":"","matchLevel":"none"},"content":{"value":"\u2026 አንተንስ <em>እ<\/em>ኔን አያርግህ\u203a\u203a <em>እ<\/em>ያለ፡፡ ይህ ግጥም በስደት የሚኖር ሰው የሚሰማውን ባይተዋርነት፣ ባእድነት፣ ብቸኝነት የሚያሳይ፣ \u2039\u2039ለአገሩ <em>እ<\/em>ንግዳ፣ ለሰው ባዳ\u203a\u203a ብቻ ሳይኮን ለራስም <em>እ<\/em>ንግዳና ባዳ <em>እ<\/em>ንደሚያደርግ የሚገልጽ ግጥም ነው፡፡ <em>እ<\/em>ንደተራኪው በአካል ሳይሆን \u2026","matchLevel":"full"}},"_highlightResult":{"post_title":{"value":"<em>እ<\/em>ውነት ማለት የእኔ ልጅ","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]},"taxonomies":{"product_type":[{"value":"variable","matchLevel":"none","matchedWords":[]}],"product_cat":[{"value":"<em>እ<\/em>ንዲያነቡት የሚመከሩ","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]},{"value":"ሥነ ግጥም","matchLevel":"none","matchedWords":[]}],"pa_currency":[{"value":"etb","matchLevel":"none","matchedWords":[]},{"value":"usd","matchLevel":"none","matchedWords":[]}]},"title1":{"value":"","matchLevel":"none","matchedWords":[]},"title2":{"value":"","matchLevel":"none","matchedWords":[]},"title3":{"value":"","matchLevel":"none","matchedWords":[]},"title4":{"value":"","matchLevel":"none","matchedWords":[]},"title5":{"value":"","matchLevel":"none","matchedWords":[]},"title6":{"value":"","matchLevel":"none","matchedWords":[]},"content":{"value":"በድሉ ጥቂት ለማይባሉ ዓመታት ከሀገሩ ውጪ ቢኖርም የግጥሞቹ ርእሰ ጉዳዮች የሀገሩ ጉዳዮች ናቸው፡፡ በውጪ ሀገር ሲኖር የሚያጋጥምን ባይተዋርነትና ናፍቆት <em>እ<\/em>ንኳን ሲገልጽ የሀገሩን ልጅ በሰው ሀገር መኖርን አትመኝ <em>እ<\/em>ያለ የሚለምን የሚያባብልበት ነው \u2013 \u2039\u2039አንተንስ <em>እ<\/em>ኔን አያርግህ\u203a\u203a <em>እ<\/em>ያለ፡፡ ይህ ግጥም በስደት የሚኖር ሰው የሚሰማውን ባይተዋርነት፣ ባእድነት፣ ብቸኝነት የሚያሳይ፣ \u2039\u2039ለአገሩ <em>እ<\/em>ንግዳ፣ ለሰው ባዳ\u203a\u203a ብቻ ሳይኮን ለራስም <em>እ<\/em>ንግዳና ባዳ <em>እ<\/em>ንደሚያደርግ የሚገልጽ ግጥም ነው፡፡ <em>እ<\/em>ንደተራኪው በአካል ሳይሆን በኅሊና በምናብ የተሰደደ ወንድሙን የስደት ጣዕም ምሬት በመሆኑ ተው ይቅርብህ ሲል የመከረበት ነው፡፡","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]}}},{"post_id":92052,"post_type":"product","post_type_label":"Products","post_title":"ዛጎል","post_excerpt":"","post_date":1495903146,"post_date_formatted":"May 27, 2017","post_modified":1497467423,"comment_count":0,"menu_order":0,"post_author":{"user_id":145,"display_name":"Biruk Hailu","user_url":"http://360ground.com","user_login":"360_admin"},"images":{"thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/zagol-150x150.jpg","width":150,"height":150},"medium":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/zagol-234x300.jpg","width":234,"height":300},"medium_large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/zagol.jpg","width":569,"height":730},"large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/zagol.jpg","width":569,"height":730},"custom-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/zagol-569x300.jpg","width":569,"height":300},"custom-small-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/zagol-100x135.jpg","width":100,"height":135},"post-thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/zagol-100x135.jpg","width":100,"height":135},"shop_thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/zagol-180x180.jpg","width":180,"height":180},"shop_catalog":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/zagol-300x300.jpg","width":300,"height":300},"shop_single":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/zagol-569x600.jpg","width":569,"height":600}},"permalink":"https://api.lomistore.com/product/%e1%8b%9b%e1%8c%8e%e1%88%8d/","post_mime_type":"","taxonomies":{"product_type":["variable"],"product_cat":["አዲስ አና መታለፍ ያሌለባቸው","እንዲያነቡት የሚመከሩ","ልብ ወለድ"],"product_tag":[],"product_shipping_class":[],"pa_author-name":["ሀዲስ ዓለማየሁ"],"pa_currency":["etb","usd"],"pa_dollar-price":[],"pa_publication-year":["2000"],"pa_publisher":["Lomi Publishing"]},"taxonomies_hierarchical":{"product_cat":{"lvl0":["አዲስ አና መታለፍ ያሌለባቸው","እንዲያነቡት የሚመከሩ","ልብ ወለድ"]},"pa_author-name":{"lvl0":["ሀዲስ ዓለማየሁ"]},"pa_currency":{"lvl0":["etb","usd"]},"pa_dollar-price":[],"pa_publication-year":{"lvl0":["2000"]},"pa_publisher":{"lvl0":["Lomi Publishing"]}},"is_sticky":0,"title1":"","title2":"","title3":"","title4":"","title5":"","title6":"","content":"ሙዚቃው ጠጪዎችን ለማማሟቅ ትግል ይዟል፡፡ ቤቱ ሰው እየጠገበ ነው፡፡ ትኩረቱን ወደ \u2018ጋርዱ\u2019 አዙሮ፣ \u201cእኔ ምልህ ማን ነው የሚጣራው? . . . ከቅድም ጀምሮ \u2018መስፍን\u2019 ሲባል እሰማለሁ\u201d አለው፤ ያ ባለዜማ፤ በስኮር የተኮላተፈ ድምጽ ከጆሮው ሽው ብሎ፡፡ \u201cተዋት ባክህ፡፡ ሰሞኑን እንዲህ ይሰራራታል\u201d አለው \u2018ጋርዱ\u2019 በሰለቸ ሁኔታ፡፡","record_index":9,"objectID":"92052-9","_snippetResult":{"post_title":{"value":"ዛጎል","matchLevel":"none"},"title1":{"value":"","matchLevel":"none"},"title2":{"value":"","matchLevel":"none"},"title3":{"value":"","matchLevel":"none"},"title4":{"value":"","matchLevel":"none"},"title5":{"value":"","matchLevel":"none"},"title6":{"value":"","matchLevel":"none"},"content":{"value":"\u2026 <em>እ<\/em>የጠገበ ነው፡፡ ትኩረቱን ወደ \u2018ጋርዱ\u2019 አዙሮ፣ \u201c<em>እ<\/em>ኔ ምልህ ማን ነው የሚጣራው? . . . ከቅድም ጀምሮ \u2018መስፍን\u2019 ሲባል <em>እ<\/em>ሰማለሁ\u201d አለው፤ ያ ባለዜማ፤ በስኮር የተኮላተፈ ድምጽ ከጆሮው ሽው ብሎ፡፡ \u201cተዋት ባክህ፡፡ ሰሞኑን <em>እ<\/em>ንዲህ ይሰራራታል \u2026","matchLevel":"full"}},"_highlightResult":{"post_title":{"value":"ዛጎል","matchLevel":"none","matchedWords":[]},"taxonomies":{"product_type":[{"value":"variable","matchLevel":"none","matchedWords":[]}],"product_cat":[{"value":"አዲስ አና መታለፍ ያሌለባቸው","matchLevel":"none","matchedWords":[]},{"value":"<em>እ<\/em>ንዲያነቡት የሚመከሩ","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]},{"value":"ልብ ወለድ","matchLevel":"none","matchedWords":[]}],"pa_author-name":[{"value":"ሀዲስ ዓለማየሁ","matchLevel":"none","matchedWords":[]}],"pa_currency":[{"value":"etb","matchLevel":"none","matchedWords":[]},{"value":"usd","matchLevel":"none","matchedWords":[]}],"pa_publication-year":[{"value":"2000","matchLevel":"none","matchedWords":[]}],"pa_publisher":[{"value":"Lomi Publishing","matchLevel":"none","matchedWords":[]}]},"title1":{"value":"","matchLevel":"none","matchedWords":[]},"title2":{"value":"","matchLevel":"none","matchedWords":[]},"title3":{"value":"","matchLevel":"none","matchedWords":[]},"title4":{"value":"","matchLevel":"none","matchedWords":[]},"title5":{"value":"","matchLevel":"none","matchedWords":[]},"title6":{"value":"","matchLevel":"none","matchedWords":[]},"content":{"value":"ሙዚቃው ጠጪዎችን ለማማሟቅ ትግል ይዟል፡፡ ቤቱ ሰው <em>እ<\/em>የጠገበ ነው፡፡ ትኩረቱን ወደ \u2018ጋርዱ\u2019 አዙሮ፣ \u201c<em>እ<\/em>ኔ ምልህ ማን ነው የሚጣራው? . . . ከቅድም ጀምሮ \u2018መስፍን\u2019 ሲባል <em>እ<\/em>ሰማለሁ\u201d አለው፤ ያ ባለዜማ፤ በስኮር የተኮላተፈ ድምጽ ከጆሮው ሽው ብሎ፡፡ \u201cተዋት ባክህ፡፡ ሰሞኑን <em>እ<\/em>ንዲህ ይሰራራታል\u201d አለው \u2018ጋርዱ\u2019 በሰለቸ ሁኔታ፡፡","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]}}},{"post_id":92038,"post_type":"product","post_type_label":"Products","post_title":"ማዕቀብ","post_excerpt":"","post_date":1495902752,"post_date_formatted":"May 27, 2017","post_modified":1497467327,"comment_count":0,"menu_order":0,"post_author":{"user_id":145,"display_name":"Biruk Hailu","user_url":"http://360ground.com","user_login":"360_admin"},"images":{"thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-2-150x150.jpg","width":150,"height":150},"medium":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-2-234x300.jpg","width":234,"height":300},"medium_large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-2.jpg","width":569,"height":730},"large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-2.jpg","width":569,"height":730},"custom-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-2-569x300.jpg","width":569,"height":300},"custom-small-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-2-100x135.jpg","width":100,"height":135},"post-thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-2-100x135.jpg","width":100,"height":135},"shop_thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-2-180x180.jpg","width":180,"height":180},"shop_catalog":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-2-300x300.jpg","width":300,"height":300},"shop_single":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-2-569x600.jpg","width":569,"height":600}},"permalink":"https://api.lomistore.com/product/%e1%88%9b%e1%8b%95%e1%89%80%e1%89%a5/","post_mime_type":"","taxonomies":{"product_type":["variable"],"product_cat":["እንዲያነቡት የሚመከሩ","ኢ-ልቦለድ"],"product_tag":[],"product_shipping_class":[],"pa_author-name":["ሀዲስ ዓለማየሁ"],"pa_currency":["etb","usd"],"pa_dollar-price":[],"pa_publication-year":["2000"],"pa_publisher":["Lomi Publishing"]},"taxonomies_hierarchical":{"product_cat":{"lvl0":["እንዲያነቡት የሚመከሩ","ኢ-ልቦለድ"]},"pa_author-name":{"lvl0":["ሀዲስ ዓለማየሁ"]},"pa_currency":{"lvl0":["etb","usd"]},"pa_dollar-price":[],"pa_publication-year":{"lvl0":["2000"]},"pa_publisher":{"lvl0":["Lomi Publishing"]}},"is_sticky":0,"title1":"","title2":"","title3":"","title4":"","title5":"","title6":"","content":"\u2026 በሚቀጥሉት ገጾች የምታገኙዋቸው ተረኮች፣ ትዝታዎች፣ ገጠመኞችና ትዝብቶች ብዙዎቹ አዳዲሶች ናቸው፤ ጥቂቶቹ ግን በልዩ ልዩ መንገድ ተነግረው ይሆናል፤ ዳግም መተረካቸውና በዚህ ቅፅ መካተታቸው ግን ጉዳዩን ዘላለማዊ ያደርገዋል ብዬ አምናለሁ፤ ዛሬ ንቀን የወረወርነው ድንጋይ አንድ ቀን &#8211; በባለሙያ እጅ ሲገባ &#8211; ተጠርቦና ተሽሞንሙኖ ለማዕዘን ራስነት ይታጭ ይሆናል፡፡","record_index":4,"objectID":"92038-4","_snippetResult":{"post_title":{"value":"ማዕቀብ","matchLevel":"none"},"title1":{"value":"","matchLevel":"none"},"title2":{"value":"","matchLevel":"none"},"title3":{"value":"","matchLevel":"none"},"title4":{"value":"","matchLevel":"none"},"title5":{"value":"","matchLevel":"none"},"title6":{"value":"","matchLevel":"none"},"content":{"value":"\u2026 ልዩ መንገድ ተነግረው ይሆናል፤ ዳግም መተረካቸውና በዚህ ቅፅ መካተታቸው ግን ጉዳዩን ዘላለማዊ ያደርገዋል ብዬ አምናለሁ፤ ዛሬ ንቀን የወረወርነው ድንጋይ አንድ ቀን \u2013 በባለሙያ <em>እ<\/em>ጅ ሲገባ \u2013 ተጠርቦና ተሽሞንሙኖ ለማዕዘን ራስነት ይታጭ ይሆናል፡፡","matchLevel":"full"}},"_highlightResult":{"post_title":{"value":"ማዕቀብ","matchLevel":"none","matchedWords":[]},"taxonomies":{"product_type":[{"value":"variable","matchLevel":"none","matchedWords":[]}],"product_cat":[{"value":"<em>እ<\/em>ንዲያነቡት የሚመከሩ","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]},{"value":"ኢ-ልቦለድ","matchLevel":"none","matchedWords":[]}],"pa_author-name":[{"value":"ሀዲስ ዓለማየሁ","matchLevel":"none","matchedWords":[]}],"pa_currency":[{"value":"etb","matchLevel":"none","matchedWords":[]},{"value":"usd","matchLevel":"none","matchedWords":[]}],"pa_publication-year":[{"value":"2000","matchLevel":"none","matchedWords":[]}],"pa_publisher":[{"value":"Lomi Publishing","matchLevel":"none","matchedWords":[]}]},"title1":{"value":"","matchLevel":"none","matchedWords":[]},"title2":{"value":"","matchLevel":"none","matchedWords":[]},"title3":{"value":"","matchLevel":"none","matchedWords":[]},"title4":{"value":"","matchLevel":"none","matchedWords":[]},"title5":{"value":"","matchLevel":"none","matchedWords":[]},"title6":{"value":"","matchLevel":"none","matchedWords":[]},"content":{"value":"\u2026 በሚቀጥሉት ገጾች የምታገኙዋቸው ተረኮች፣ ትዝታዎች፣ ገጠመኞችና ትዝብቶች ብዙዎቹ አዳዲሶች ናቸው፤ ጥቂቶቹ ግን በልዩ ልዩ መንገድ ተነግረው ይሆናል፤ ዳግም መተረካቸውና በዚህ ቅፅ መካተታቸው ግን ጉዳዩን ዘላለማዊ ያደርገዋል ብዬ አምናለሁ፤ ዛሬ ንቀን የወረወርነው ድንጋይ አንድ ቀን \u2013 በባለሙያ <em>እ<\/em>ጅ ሲገባ \u2013 ተጠርቦና ተሽሞንሙኖ ለማዕዘን ራስነት ይታጭ ይሆናል፡፡","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]}}},{"post_id":91924,"post_type":"product","post_type_label":"Products","post_title":"ሀገሬ ገመናሽ","post_excerpt":"","post_date":1495740523,"post_date_formatted":"May 25, 2017","post_modified":1497466951,"comment_count":0,"menu_order":0,"post_author":{"user_id":145,"display_name":"Biruk Hailu","user_url":"http://360ground.com","user_login":"360_admin"},"images":{"thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-3-150x150.jpg","width":150,"height":150},"medium":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-3-234x300.jpg","width":234,"height":300},"medium_large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-3.jpg","width":569,"height":730},"large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-3.jpg","width":569,"height":730},"custom-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-3-569x300.jpg","width":569,"height":300},"custom-small-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-3-100x135.jpg","width":100,"height":135},"post-thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-3-100x135.jpg","width":100,"height":135},"shop_thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-3-180x180.jpg","width":180,"height":180},"shop_catalog":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-3-300x300.jpg","width":300,"height":300},"shop_single":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-3-569x600.jpg","width":569,"height":600}},"permalink":"https://api.lomistore.com/product/%e1%88%80%e1%8c%88%e1%88%ac-%e1%8c%88%e1%88%98%e1%8a%93%e1%88%bd/","post_mime_type":"","taxonomies":{"product_type":["variable"],"product_cat":["አዲስ አና መታለፍ ያሌለባቸው","እንዲያነቡት የሚመከሩ","ወጎች"],"product_tag":[],"product_shipping_class":[],"pa_author-name":["ሀዲስ ዓለማየሁ"],"pa_currency":["etb","usd"],"pa_dollar-price":[],"pa_publication-year":["2000"],"pa_publisher":["Lomi Publishing"]},"taxonomies_hierarchical":{"product_cat":{"lvl0":["አዲስ አና መታለፍ ያሌለባቸው","እንዲያነቡት የሚመከሩ","ወጎች"]},"pa_author-name":{"lvl0":["ሀዲስ ዓለማየሁ"]},"pa_currency":{"lvl0":["etb","usd"]},"pa_dollar-price":[],"pa_publication-year":{"lvl0":["2000"]},"pa_publisher":{"lvl0":["Lomi Publishing"]}},"is_sticky":0,"title1":"","title2":"","title3":"","title4":"","title5":"","title6":"","content":"ይህን መጽሀፍ ለማሳተም ማሰብ ከጀመርኩ ትንሽ ቆየት ብያለሁ &#8211; መንፈቅ ያህል፡፡ ይሁን እንጂ የሀገራችን ሁኔታ ለጊዜ ትርጉም ሁን ሲለው፣ እነሆ ዛሬን አደለውና ከነጻው ፕሬስ መካነ መቃብረር ላይ፣ መነበራቸውን ዘካሪ የአበባ ጉንጉን ሆኖ ተቀመጠ፡፡","record_index":4,"objectID":"91924-4","_snippetResult":{"post_title":{"value":"ሀገሬ ገመናሽ","matchLevel":"none"},"title1":{"value":"","matchLevel":"none"},"title2":{"value":"","matchLevel":"none"},"title3":{"value":"","matchLevel":"none"},"title4":{"value":"","matchLevel":"none"},"title5":{"value":"","matchLevel":"none"},"title6":{"value":"","matchLevel":"none"},"content":{"value":"ይህን መጽሀፍ ለማሳተም ማሰብ ከጀመርኩ ትንሽ ቆየት ብያለሁ \u2013 መንፈቅ ያህል፡፡ ይሁን <em>እ<\/em>ንጂ የሀገራችን ሁኔታ ለጊዜ ትርጉም ሁን ሲለው፣ <em>እ<\/em>ነሆ ዛሬን አደለውና ከነጻው ፕሬስ መካነ መቃብረር ላይ፣ መነበራቸውን ዘካሪ የአበባ ጉንጉን \u2026","matchLevel":"full"}},"_highlightResult":{"post_title":{"value":"ሀገሬ ገመናሽ","matchLevel":"none","matchedWords":[]},"taxonomies":{"product_type":[{"value":"variable","matchLevel":"none","matchedWords":[]}],"product_cat":[{"value":"አዲስ አና መታለፍ ያሌለባቸው","matchLevel":"none","matchedWords":[]},{"value":"<em>እ<\/em>ንዲያነቡት የሚመከሩ","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]},{"value":"ወጎች","matchLevel":"none","matchedWords":[]}],"pa_author-name":[{"value":"ሀዲስ ዓለማየሁ","matchLevel":"none","matchedWords":[]}],"pa_currency":[{"value":"etb","matchLevel":"none","matchedWords":[]},{"value":"usd","matchLevel":"none","matchedWords":[]}],"pa_publication-year":[{"value":"2000","matchLevel":"none","matchedWords":[]}],"pa_publisher":[{"value":"Lomi Publishing","matchLevel":"none","matchedWords":[]}]},"title1":{"value":"","matchLevel":"none","matchedWords":[]},"title2":{"value":"","matchLevel":"none","matchedWords":[]},"title3":{"value":"","matchLevel":"none","matchedWords":[]},"title4":{"value":"","matchLevel":"none","matchedWords":[]},"title5":{"value":"","matchLevel":"none","matchedWords":[]},"title6":{"value":"","matchLevel":"none","matchedWords":[]},"content":{"value":"ይህን መጽሀፍ ለማሳተም ማሰብ ከጀመርኩ ትንሽ ቆየት ብያለሁ \u2013 መንፈቅ ያህል፡፡ ይሁን <em>እ<\/em>ንጂ የሀገራችን ሁኔታ ለጊዜ ትርጉም ሁን ሲለው፣ <em>እ<\/em>ነሆ ዛሬን አደለውና ከነጻው ፕሬስ መካነ መቃብረር ላይ፣ መነበራቸውን ዘካሪ የአበባ ጉንጉን ሆኖ ተቀመጠ፡፡","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]}}},{"post_id":91918,"post_type":"product","post_type_label":"Products","post_title":"ፍካት ናፋቂዎች","post_excerpt":"","post_date":1495740285,"post_date_formatted":"May 25, 2017","post_modified":1497466893,"comment_count":0,"menu_order":0,"post_author":{"user_id":145,"display_name":"Biruk Hailu","user_url":"http://360ground.com","user_login":"360_admin"},"images":{"thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-150x150.jpg","width":150,"height":150},"medium":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-204x300.jpg","width":204,"height":300},"medium_large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-768x1127.jpg","width":768,"height":1127},"large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-698x1024.jpg","width":698,"height":1024},"custom-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-600x300.jpg","width":600,"height":300},"custom-small-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-100x135.jpg","width":100,"height":135},"post-thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-100x135.jpg","width":100,"height":135},"shop_thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-180x180.jpg","width":180,"height":180},"shop_catalog":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-300x300.jpg","width":300,"height":300},"shop_single":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-600x600.jpg","width":600,"height":600}},"permalink":"https://api.lomistore.com/product/%e1%8d%8d%e1%8a%ab%e1%89%b5-%e1%8a%93%e1%8d%8b%e1%89%82%e1%8b%8e%e1%89%bd/","post_mime_type":"","taxonomies":{"product_type":["variable"],"product_cat":["እንዲያነቡት የሚመከሩ","ሥነ ግጥም"],"product_tag":[],"product_shipping_class":[],"pa_author-name":["ሀዲስ ዓለማየሁ"],"pa_currency":["etb","usd"],"pa_dollar-price":[],"pa_publication-year":["2000"],"pa_publisher":["Lomi Publishing"]},"taxonomies_hierarchical":{"product_cat":{"lvl0":["እንዲያነቡት የሚመከሩ","ሥነ ግጥም"]},"pa_author-name":{"lvl0":["ሀዲስ ዓለማየሁ"]},"pa_currency":{"lvl0":["etb","usd"]},"pa_dollar-price":[],"pa_publication-year":{"lvl0":["2000"]},"pa_publisher":{"lvl0":["Lomi Publishing"]}},"is_sticky":0,"title1":"","title2":"","title3":"","title4":"","title5":"","title6":"","content":"ይህን መሰለቹ ግጥሞች ጊዜን ያቆማሉ፤ እርቃንን ያሳያሉ፡፡ እኮ ለምን? ብሎ መጠየቅ ባልተለመደበት፤ ቸልታ፣ ግዴለሽነትና ዝንጋኤ በነገሰበት የድንቁርና ቤት \u2039\u2039አባትህ አንተን ወለደ!\u203a\u203aን የመሰሉት ከብራቅ የፈጠኑ፣ ከብራቅ የገነኑ ድምፆች አገርን ከእንቅልፍ ያነቃሉ፡፡ አንዳንዱም \u2039\u2039በስመአብ\u203a\u203a ብሎ፣ ከትውልድ ወደ ወረሰው የእንቅልፍ ታሪኩ ሊመለስ ይችላል፡፡ ግን መንቃት እስከፈለገ ድረስ ለእያንዳንዱ የዘመኑ ደወል &#8211; ብራቅ &#8211; ብርሃን ከእነዚህ አንድ ወይም ሁሉም አሉለት፡፡","record_index":7,"objectID":"91918-7","_snippetResult":{"post_title":{"value":"ፍካት ናፋቂዎች","matchLevel":"none"},"title1":{"value":"","matchLevel":"none"},"title2":{"value":"","matchLevel":"none"},"title3":{"value":"","matchLevel":"none"},"title4":{"value":"","matchLevel":"none"},"title5":{"value":"","matchLevel":"none"},"title6":{"value":"","matchLevel":"none"},"content":{"value":"ይህን መሰለቹ ግጥሞች ጊዜን ያቆማሉ፤ <em>እ<\/em>ርቃንን ያሳያሉ፡፡ <em>እ<\/em>ኮ ለምን? ብሎ መጠየቅ ባልተለመደበት፤ ቸልታ፣ ግዴለሽነትና ዝንጋኤ በነገሰበት የድንቁርና ቤት \u2039\u2039አባትህ አንተን ወለደ!\u203a\u203aን የመሰሉት ከብራቅ የፈጠኑ፣ ከብራቅ የገነኑ ድምፆች አገርን ከእንቅልፍ \u2026","matchLevel":"full"}},"_highlightResult":{"post_title":{"value":"ፍካት ናፋቂዎች","matchLevel":"none","matchedWords":[]},"taxonomies":{"product_type":[{"value":"variable","matchLevel":"none","matchedWords":[]}],"product_cat":[{"value":"<em>እ<\/em>ንዲያነቡት የሚመከሩ","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]},{"value":"ሥነ ግጥም","matchLevel":"none","matchedWords":[]}],"pa_author-name":[{"value":"ሀዲስ ዓለማየሁ","matchLevel":"none","matchedWords":[]}],"pa_currency":[{"value":"etb","matchLevel":"none","matchedWords":[]},{"value":"usd","matchLevel":"none","matchedWords":[]}],"pa_publication-year":[{"value":"2000","matchLevel":"none","matchedWords":[]}],"pa_publisher":[{"value":"Lomi Publishing","matchLevel":"none","matchedWords":[]}]},"title1":{"value":"","matchLevel":"none","matchedWords":[]},"title2":{"value":"","matchLevel":"none","matchedWords":[]},"title3":{"value":"","matchLevel":"none","matchedWords":[]},"title4":{"value":"","matchLevel":"none","matchedWords":[]},"title5":{"value":"","matchLevel":"none","matchedWords":[]},"title6":{"value":"","matchLevel":"none","matchedWords":[]},"content":{"value":"ይህን መሰለቹ ግጥሞች ጊዜን ያቆማሉ፤ <em>እ<\/em>ርቃንን ያሳያሉ፡፡ <em>እ<\/em>ኮ ለምን? ብሎ መጠየቅ ባልተለመደበት፤ ቸልታ፣ ግዴለሽነትና ዝንጋኤ በነገሰበት የድንቁርና ቤት \u2039\u2039አባትህ አንተን ወለደ!\u203a\u203aን የመሰሉት ከብራቅ የፈጠኑ፣ ከብራቅ የገነኑ ድምፆች አገርን ከእንቅልፍ ያነቃሉ፡፡ አንዳንዱም \u2039\u2039በስመአብ\u203a\u203a ብሎ፣ ከትውልድ ወደ ወረሰው የእንቅልፍ ታሪኩ ሊመለስ ይችላል፡፡ ግን መንቃት <em>እ<\/em>ስከፈለገ ድረስ ለእያንዳንዱ የዘመኑ ደወል \u2013 ብራቅ \u2013 ብርሃን ከእነዚህ አንድ ወይም ሁሉም አሉለት፡፡","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]}}},{"post_id":91970,"post_type":"product","post_type_label":"Products","post_title":"ኬር ሻዶ","post_excerpt":"","post_date":1495899910,"post_date_formatted":"May 27, 2017","post_modified":1497529122,"comment_count":0,"menu_order":0,"post_author":{"user_id":145,"display_name":"Biruk Hailu","user_url":"http://360ground.com","user_login":"360_admin"},"images":{"thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/care-shado-1-150x150.jpg","width":150,"height":150},"medium":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/care-shado-1-234x300.jpg","width":234,"height":300},"medium_large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/care-shado-1.jpg","width":569,"height":730},"large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/care-shado-1.jpg","width":569,"height":730},"custom-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/care-shado-1-569x300.jpg","width":569,"height":300},"custom-small-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/care-shado-1-100x135.jpg","width":100,"height":135},"post-thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/care-shado-1-100x135.jpg","width":100,"height":135},"shop_thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/care-shado-1-180x180.jpg","width":180,"height":180},"shop_catalog":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/care-shado-1-300x300.jpg","width":300,"height":300},"shop_single":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/care-shado-1-569x600.jpg","width":569,"height":600}},"permalink":"https://api.lomistore.com/product/%e1%8a%ac%e1%88%ad-%e1%88%bb%e1%8b%b6/","post_mime_type":"","taxonomies":{"product_type":["variable"],"product_cat":["አዲስ አና መታለፍ ያሌለባቸው","ፎክሎራዊ ልቦለድ"],"product_tag":[],"product_shipping_class":[],"pa_author-name":["ሀዲስ ዓለማየሁ"],"pa_currency":["etb","usd"],"pa_dollar-price":[],"pa_publication-year":["2000"],"pa_publisher":["Lomi Publishing"]},"taxonomies_hierarchical":{"product_cat":{"lvl0":["አዲስ አና መታለፍ ያሌለባቸው","ፎክሎራዊ ልቦለድ"]},"pa_author-name":{"lvl0":["ሀዲስ ዓለማየሁ"]},"pa_currency":{"lvl0":["etb","usd"]},"pa_dollar-price":[],"pa_publication-year":{"lvl0":["2000"]},"pa_publisher":{"lvl0":["Lomi Publishing"]}},"is_sticky":0,"title1":"","title2":"","title3":"","title4":"","title5":"","title6":"","content":"ከጉራጊኛ ወደ አማርኛ ቋንቋ ሲመለስ፤ ኬር &#8211; ሰላም ነው፤ ፍቅር ነው &#8211; \u201cአገር አማን ይሁን ኬር ይላል ጉራጌ\u201d እንዲል ቴዲ አፍሮ፡፡ ሻዶ &#8211; ደግሞ አካፍሉ ነው፤ ስጡ፤ እጃችሁም ልባችሁም ቸር ይሁን፡፡ ኬር ሻዶ &#8211; ሰላምን፣ ፍቅርንና ሰናይ ሀሳቦችን ተቀባበሉ እንደማለት፡፡ አለን ብለን የምናምነው ማንኛውንም ማለፊያ ተሞክሮ፣ የሕይወት ልምድን ጨምሮ ለመሰጣጣት ራስን ማዘጋጀትን ያካትታል፡፡ ኬር ሻዶ!","record_index":0,"objectID":"91970-0","_snippetResult":{"post_title":{"value":"ኬር ሻዶ","matchLevel":"none"},"title1":{"value":"","matchLevel":"none"},"title2":{"value":"","matchLevel":"none"},"title3":{"value":"","matchLevel":"none"},"title4":{"value":"","matchLevel":"none"},"title5":{"value":"","matchLevel":"none"},"title6":{"value":"","matchLevel":"none"},"content":{"value":"\u2026 ነው \u2013 \u201cአገር አማን ይሁን ኬር ይላል ጉራጌ\u201d <em>እ<\/em>ንዲል ቴዲ አፍሮ፡፡ ሻዶ \u2013 ደግሞ አካፍሉ ነው፤ ስጡ፤ <em>እ<\/em>ጃችሁም ልባችሁም ቸር ይሁን፡፡ ኬር ሻዶ \u2013 ሰላምን፣ ፍቅርንና ሰናይ ሀሳቦችን ተቀባበሉ <em>እ<\/em>ንደማለት፡፡ አለን ብለን የምናምነው \u2026","matchLevel":"full"}},"_highlightResult":{"post_title":{"value":"ኬር ሻዶ","matchLevel":"none","matchedWords":[]},"taxonomies":{"product_type":[{"value":"variable","matchLevel":"none","matchedWords":[]}],"product_cat":[{"value":"አዲስ አና መታለፍ ያሌለባቸው","matchLevel":"none","matchedWords":[]},{"value":"ፎክሎራዊ ልቦለድ","matchLevel":"none","matchedWords":[]}],"pa_author-name":[{"value":"ሀዲስ ዓለማየሁ","matchLevel":"none","matchedWords":[]}],"pa_currency":[{"value":"etb","matchLevel":"none","matchedWords":[]},{"value":"usd","matchLevel":"none","matchedWords":[]}],"pa_publication-year":[{"value":"2000","matchLevel":"none","matchedWords":[]}],"pa_publisher":[{"value":"Lomi Publishing","matchLevel":"none","matchedWords":[]}]},"title1":{"value":"","matchLevel":"none","matchedWords":[]},"title2":{"value":"","matchLevel":"none","matchedWords":[]},"title3":{"value":"","matchLevel":"none","matchedWords":[]},"title4":{"value":"","matchLevel":"none","matchedWords":[]},"title5":{"value":"","matchLevel":"none","matchedWords":[]},"title6":{"value":"","matchLevel":"none","matchedWords":[]},"content":{"value":"ከጉራጊኛ ወደ አማርኛ ቋንቋ ሲመለስ፤ ኬር \u2013 ሰላም ነው፤ ፍቅር ነው \u2013 \u201cአገር አማን ይሁን ኬር ይላል ጉራጌ\u201d <em>እ<\/em>ንዲል ቴዲ አፍሮ፡፡ ሻዶ \u2013 ደግሞ አካፍሉ ነው፤ ስጡ፤ <em>እ<\/em>ጃችሁም ልባችሁም ቸር ይሁን፡፡ ኬር ሻዶ \u2013 ሰላምን፣ ፍቅርንና ሰናይ ሀሳቦችን ተቀባበሉ <em>እ<\/em>ንደማለት፡፡ አለን ብለን የምናምነው ማንኛውንም ማለፊያ ተሞክሮ፣ የሕይወት ልምድን ጨምሮ ለመሰጣጣት ራስን ማዘጋጀትን ያካትታል፡፡ ኬር ሻዶ!","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]}}}]
     * nbHits : 9
     * page : 0
     * nbPages : 1
     * hitsPerPage : 50
     * processingTimeMS : 1
     * exhaustiveNbHits : true
     * query : እ
     * params : hitsPerPage=50&query=%E1%8A%A5
     */

    private int nbHits;
    private int page;
    private int nbPages;
    private int hitsPerPage;
    private int processingTimeMS;
    private boolean exhaustiveNbHits;
    private String query;
    private String params;
    private List<HitsBean> hits;

    public int getNbHits() {
        return nbHits;
    }

    public void setNbHits(int nbHits) {
        this.nbHits = nbHits;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getNbPages() {
        return nbPages;
    }

    public void setNbPages(int nbPages) {
        this.nbPages = nbPages;
    }

    public int getHitsPerPage() {
        return hitsPerPage;
    }

    public void setHitsPerPage(int hitsPerPage) {
        this.hitsPerPage = hitsPerPage;
    }

    public int getProcessingTimeMS() {
        return processingTimeMS;
    }

    public void setProcessingTimeMS(int processingTimeMS) {
        this.processingTimeMS = processingTimeMS;
    }

    public boolean isExhaustiveNbHits() {
        return exhaustiveNbHits;
    }

    public void setExhaustiveNbHits(boolean exhaustiveNbHits) {
        this.exhaustiveNbHits = exhaustiveNbHits;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public List<HitsBean> getHits() {
        return hits;
    }

    public void setHits(List<HitsBean> hits) {
        this.hits = hits;
    }

    public List<Product> getProducts() {

        List<Product> products = new ArrayList<>();

        for(HitsBean result : hits) {

            products.add(result.toProduct());
        }

        return products;
    }

    public static class HitsBean {
        /**
         * post_id : 92234
         * post_type : product
         * post_type_label : Products
         * post_title : የሕይወት ጉዞዬ እና ትዝታዎቼ - በሀገር ቤት  ክፍል ሁለት
         * post_excerpt : <p>yehiwot, hiwot, yhiwot<br />
         guzoyena tizetawoche<br />
         getachew<br />
         dr doctor </p>

         * post_date : 1495979309
         * post_date_formatted : May 28, 2017
         * post_modified : 1497471641
         * comment_count : 0
         * menu_order : 0
         * post_author : {"user_id":145,"display_name":"Biruk Hailu","user_url":"http://360ground.com","user_login":"360_admin"}
         * images : {"thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-150x150.jpg","width":150,"height":150},"medium":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-234x300.jpg","width":234,"height":300},"medium_large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7.jpg","width":569,"height":730},"large":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7.jpg","width":569,"height":730},"custom-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-569x300.jpg","width":569,"height":300},"custom-small-thumbnail-size":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-100x135.jpg","width":100,"height":135},"post-thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-100x135.jpg","width":100,"height":135},"shop_thumbnail":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-180x180.jpg","width":180,"height":180},"shop_catalog":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-300x300.jpg","width":300,"height":300},"shop_single":{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-569x600.jpg","width":569,"height":600}}
         * permalink : https://api.lomistore.com/product/%e1%8b%a8%e1%88%95%e1%8b%ad%e1%8b%88%e1%89%b5-%e1%8c%89%e1%8b%9e%e1%8b%ac-%e1%8a%a5%e1%8a%93-%e1%89%b5%e1%8b%9d%e1%89%b3%e1%8b%8e%e1%89%bc-%e1%89%a0%e1%88%80%e1%8c%88%e1%88%ad-%e1%89%a4%e1%89%b5/
         * post_mime_type :
         * taxonomies : {"product_type":["variable"],"product_cat":["እንዲያነቡት የሚመከሩ","የህይወት ታሪኮች"],"product_tag":[],"product_shipping_class":[],"pa_author-name":["ሀዲስ ዓለማየሁ"],"pa_currency":["etb","usd"],"pa_dollar-price":[],"pa_publication-year":["2000"],"pa_publisher":["Lomi Publishing"]}
         * taxonomies_hierarchical : {"product_cat":{"lvl0":["እንዲያነቡት የሚመከሩ","የህይወት ታሪኮች"]},"pa_author-name":{"lvl0":["ሀዲስ ዓለማየሁ"]},"pa_currency":{"lvl0":["etb","usd"]},"pa_dollar-price":[],"pa_publication-year":{"lvl0":["2000"]},"pa_publisher":{"lvl0":["Lomi Publishing"]}}
         * is_sticky : 0
         * title1 :
         * title2 :
         * title3 :
         * title4 :
         * title5 :
         * title6 :
         * content : በመጀመሪያ የሕይወት ጉዞዬና ትዝታዎቼ በሚል ርዕስ ለኅትመት ያበቃሁት መጽሐፍ በጠቅላላ የሚተርከው የውጭ ዓለም ኑሮዬንና ጉዞዬን ነው፡፡ ይህ መጽሐፍ ብዙ ሰዎች ተወያይተውበታል፣ በመጽሔትና በጋዜጣ፣ በሬዲዮና በቴሌቪዥን ብዙ ሽፋን አግኝቷል፡፡ ከብዙ ግለ ሰቦችም ከማስበው በላይ ምስጋና አግኝቼበታለሁ፡፡ አበጀህ፣ ያበርታህ ተብዬበታለሁ፡፡ ዕውቀት አግኝተንበታል ብለውኛል፡፡ በዚህም በጣም ደስ ብሎኛል፡፡ አሁን ደግሞ የሕይወት ጉዞዬና ትዝታዎቼ፣ ክፍል ሁለት &#8211; በሀገር ቤት ይኸው ለኅትመት አበቃሁ፡፡ የዚህ ጽሑፍ ዋናዋ ተወናዋይ እኔው እራሴ ነኝ፡፡ ወደ እምወዳት ሀገሬ ከተመለስኩ በኋላ ያጋጠሙኝን የደስታ፣ የሀዘን፣ የሚያናድድ፣ የሚያስደስት፣ አልፎ አልፎም በጣም የሚያስቅ ሕይወት መርቻለሁ፡፡ ሁሉንም እንደወረደ አስቀምጨዋለሁ፡፡ ቤት ሥሰራ የነበረው ምልልስ፣ የመሬት ካርታ ለማስለወጥ የነበረውን ውጣ ውረድ፣ ባሕር ማዶ ያሉት ጓደኞቼ ለመጀመሪያ ጊዜ ኢትዮጵን ለመጎብኘትና እኔንም ለመጠየቅ ሲመጡ የነበረውን ገጠመኞች፣ ከበጎ አድራጎት ድርጅቶች ጋር ሥራ፣ የሠፈር መንገድ ለማሠራት፣ ሲያቀብጠኝ ኃላፊነቱን ተቀብዬ፣ ጎረቤቶቼ ራሳቸው መርጠውኝ &#8211; መራጮቹ አንከፍልም ሲሉኝ፣ ኧረ ስንቱ? በሀገር ውስጥ ከደራሲያን ጋር ያደረኩትን ጉብኝት፣ ሀዘኑን &#8211; ለቅሶውን፣ ሠርጉን &#8211; ጭፈራውን ወዘተርፈ. በሰፊውም ባይሆን አጠር መጠን አድርጌ በትረካ መልክ አቅርቤዋለሁ፡፡ ምንም እንኳን ርዕሱ በሀገር ቤት ቢልም፣ ከዚሁ ከሀገር ቤት ወደ ውጭ አገር ሔጄ የጎበኘኋቸውን የባዕድ አገሮችንም አብሬ ተርኬ አውጥቼዋለሁ፡፡ ቻይናዎች “አንድ ፎቶግራፍ አንድ ሺ ቃላት ያወራል” ይላሉ፡፡ እኔም አምንበታለሁ፡፡ ስለዚህ ከትረካው ጋር የሚዛመዱ ብዙ ፎቶግራፎች አብሬ አክየበታለሁ፡፡
         * record_index : 0
         * objectID : 92234-0
         * _snippetResult : {"post_title":{"value":"የሕይወት ጉዞዬ <em>እ<\/em>ና ትዝታዎቼ - በሀገር ቤት  ክፍል ሁለት","matchLevel":"full"},"title1":{"value":"","matchLevel":"none"},"title2":{"value":"","matchLevel":"none"},"title3":{"value":"","matchLevel":"none"},"title4":{"value":"","matchLevel":"none"},"title5":{"value":"","matchLevel":"none"},"title6":{"value":"","matchLevel":"none"},"content":{"value":"\u2026 ደግሞ የሕይወት ጉዞዬና ትዝታዎቼ፣ ክፍል ሁለት \u2013 በሀገር ቤት ይኸው ለኅትመት አበቃሁ፡፡ የዚህ ጽሑፍ ዋናዋ ተወናዋይ <em>እ<\/em>ኔው <em>እ<\/em>ራሴ ነኝ፡፡ ወደ <em>እ<\/em>ምወዳት ሀገሬ ከተመለስኩ በኋላ ያጋጠሙኝን የደስታ፣ የሀዘን፣ የሚያናድድ፣ የሚያስደስት፣ አልፎ አልፎም \u2026","matchLevel":"full"}}
         * _highlightResult : {"post_title":{"value":"የሕይወት ጉዞዬ <em>እ<\/em>ና ትዝታዎቼ - በሀገር ቤት  ክፍል ሁለት","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]},"taxonomies":{"product_type":[{"value":"variable","matchLevel":"none","matchedWords":[]}],"product_cat":[{"value":"<em>እ<\/em>ንዲያነቡት የሚመከሩ","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]},{"value":"የህይወት ታሪኮች","matchLevel":"none","matchedWords":[]}],"pa_author-name":[{"value":"ሀዲስ ዓለማየሁ","matchLevel":"none","matchedWords":[]}],"pa_currency":[{"value":"etb","matchLevel":"none","matchedWords":[]},{"value":"usd","matchLevel":"none","matchedWords":[]}],"pa_publication-year":[{"value":"2000","matchLevel":"none","matchedWords":[]}],"pa_publisher":[{"value":"Lomi Publishing","matchLevel":"none","matchedWords":[]}]},"title1":{"value":"","matchLevel":"none","matchedWords":[]},"title2":{"value":"","matchLevel":"none","matchedWords":[]},"title3":{"value":"","matchLevel":"none","matchedWords":[]},"title4":{"value":"","matchLevel":"none","matchedWords":[]},"title5":{"value":"","matchLevel":"none","matchedWords":[]},"title6":{"value":"","matchLevel":"none","matchedWords":[]},"content":{"value":"በመጀመሪያ የሕይወት ጉዞዬና ትዝታዎቼ በሚል ርዕስ ለኅትመት ያበቃሁት መጽሐፍ በጠቅላላ የሚተርከው የውጭ ዓለም ኑሮዬንና ጉዞዬን ነው፡፡ ይህ መጽሐፍ ብዙ ሰዎች ተወያይተውበታል፣ በመጽሔትና በጋዜጣ፣ በሬዲዮና በቴሌቪዥን ብዙ ሽፋን አግኝቷል፡፡ ከብዙ ግለ ሰቦችም ከማስበው በላይ ምስጋና አግኝቼበታለሁ፡፡ አበጀህ፣ ያበርታህ ተብዬበታለሁ፡፡ ዕውቀት አግኝተንበታል ብለውኛል፡፡ በዚህም በጣም ደስ ብሎኛል፡፡ አሁን ደግሞ የሕይወት ጉዞዬና ትዝታዎቼ፣ ክፍል ሁለት \u2013 በሀገር ቤት ይኸው ለኅትመት አበቃሁ፡፡ የዚህ ጽሑፍ ዋናዋ ተወናዋይ <em>እ<\/em>ኔው <em>እ<\/em>ራሴ ነኝ፡፡ ወደ <em>እ<\/em>ምወዳት ሀገሬ ከተመለስኩ በኋላ ያጋጠሙኝን የደስታ፣ የሀዘን፣ የሚያናድድ፣ የሚያስደስት፣ አልፎ አልፎም በጣም የሚያስቅ ሕይወት መርቻለሁ፡፡ ሁሉንም <em>እ<\/em>ንደወረደ አስቀምጨዋለሁ፡፡ ቤት ሥሰራ የነበረው ምልልስ፣ የመሬት ካርታ ለማስለወጥ የነበረውን ውጣ ውረድ፣ ባሕር ማዶ ያሉት ጓደኞቼ ለመጀመሪያ ጊዜ ኢትዮጵን ለመጎብኘትና <em>እ<\/em>ኔንም ለመጠየቅ ሲመጡ የነበረውን ገጠመኞች፣ ከበጎ አድራጎት ድርጅቶች ጋር ሥራ፣ የሠፈር መንገድ ለማሠራት፣ ሲያቀብጠኝ ኃላፊነቱን ተቀብዬ፣ ጎረቤቶቼ ራሳቸው መርጠውኝ \u2013 መራጮቹ አንከፍልም ሲሉኝ፣ ኧረ ስንቱ? በሀገር ውስጥ ከደራሲያን ጋር ያደረኩትን ጉብኝት፣ ሀዘኑን \u2013 ለቅሶውን፣ ሠርጉን \u2013 ጭፈራውን ወዘተርፈ. በሰፊውም ባይሆን አጠር መጠን አድርጌ በትረካ መልክ አቅርቤዋለሁ፡፡ ምንም <em>እ<\/em>ንኳን ርዕሱ በሀገር ቤት ቢልም፣ ከዚሁ ከሀገር ቤት ወደ ውጭ አገር ሔጄ የጎበኘኋቸውን የባዕድ አገሮችንም አብሬ ተርኬ አውጥቼዋለሁ፡፡ ቻይናዎች \u201cአንድ ፎቶግራፍ አንድ ሺ ቃላት ያወራል\u201d ይላሉ፡፡ <em>እ<\/em>ኔም አምንበታለሁ፡፡ ስለዚህ ከትረካው ጋር የሚዛመዱ ብዙ ፎቶግራፎች አብሬ አክየበታለሁ፡፡","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]}}
         */

        private int post_id;
        private String post_type;
        private String post_type_label;
        private String post_title;
        private String post_excerpt;
        private int post_date;
        private String post_date_formatted;
        private int post_modified;
        private int comment_count;
        private int menu_order;
        private PostAuthorBean post_author;
        private ImagesBean images;
        private String permalink;
        private String post_mime_type;
        private TaxonomiesBean taxonomies;
        private int is_sticky;
        private String title1;
        private String title2;
        private String title3;
        private String title4;
        private String title5;
        private String title6;
        private String content;
        private int record_index;
        private String objectID;
        private SnippetResultBean _snippetResult;
        private HighlightResultBean _highlightResult;

        public Product toProduct() {

            Product product = new Product();

            product.setId(post_id);

            product.setThumbnailSrc(images.getThumbnail().getUrl());

            product.setName(post_title);

            product.setAuthor(new PostAuthorBean().getAuthor());

            return product;
        }

        public int getPost_id() {
            return post_id;
        }

        public void setPost_id(int post_id) {
            this.post_id = post_id;
        }

        public String getPost_type() {
            return post_type;
        }

        public void setPost_type(String post_type) {
            this.post_type = post_type;
        }

        public String getPost_type_label() {
            return post_type_label;
        }

        public void setPost_type_label(String post_type_label) {
            this.post_type_label = post_type_label;
        }

        public String getPost_title() {
            return post_title;
        }

        public void setPost_title(String post_title) {
            this.post_title = post_title;
        }

        public String getPost_excerpt() {
            return post_excerpt;
        }

        public void setPost_excerpt(String post_excerpt) {
            this.post_excerpt = post_excerpt;
        }

        public int getPost_date() {
            return post_date;
        }

        public void setPost_date(int post_date) {
            this.post_date = post_date;
        }

        public String getPost_date_formatted() {
            return post_date_formatted;
        }

        public void setPost_date_formatted(String post_date_formatted) {
            this.post_date_formatted = post_date_formatted;
        }

        public int getPost_modified() {
            return post_modified;
        }

        public void setPost_modified(int post_modified) {
            this.post_modified = post_modified;
        }

        public int getComment_count() {
            return comment_count;
        }

        public void setComment_count(int comment_count) {
            this.comment_count = comment_count;
        }

        public int getMenu_order() {
            return menu_order;
        }

        public void setMenu_order(int menu_order) {
            this.menu_order = menu_order;
        }

        public PostAuthorBean getPost_author() {
            return post_author;
        }

        public void setPost_author(PostAuthorBean post_author) {
            this.post_author = post_author;
        }

        public ImagesBean getImages() {
            return images;
        }

        public void setImages(ImagesBean images) {
            this.images = images;
        }

        public String getPermalink() {
            return permalink;
        }

        public void setPermalink(String permalink) {
            this.permalink = permalink;
        }

        public String getPost_mime_type() {
            return post_mime_type;
        }

        public void setPost_mime_type(String post_mime_type) {
            this.post_mime_type = post_mime_type;
        }

        public TaxonomiesBean getTaxonomies() {
            return taxonomies;
        }

        public void setTaxonomies(TaxonomiesBean taxonomies) {
            this.taxonomies = taxonomies;
        }

        public int getIs_sticky() {
            return is_sticky;
        }

        public void setIs_sticky(int is_sticky) {
            this.is_sticky = is_sticky;
        }

        public String getTitle1() {
            return title1;
        }

        public void setTitle1(String title1) {
            this.title1 = title1;
        }

        public String getTitle2() {
            return title2;
        }

        public void setTitle2(String title2) {
            this.title2 = title2;
        }

        public String getTitle3() {
            return title3;
        }

        public void setTitle3(String title3) {
            this.title3 = title3;
        }

        public String getTitle4() {
            return title4;
        }

        public void setTitle4(String title4) {
            this.title4 = title4;
        }

        public String getTitle5() {
            return title5;
        }

        public void setTitle5(String title5) {
            this.title5 = title5;
        }

        public String getTitle6() {
            return title6;
        }

        public void setTitle6(String title6) {
            this.title6 = title6;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public int getRecord_index() {
            return record_index;
        }

        public void setRecord_index(int record_index) {
            this.record_index = record_index;
        }

        public String getObjectID() {
            return objectID;
        }

        public void setObjectID(String objectID) {
            this.objectID = objectID;
        }

        public SnippetResultBean get_snippetResult() {
            return _snippetResult;
        }

        public void set_snippetResult(SnippetResultBean _snippetResult) {
            this._snippetResult = _snippetResult;
        }

        public HighlightResultBean get_highlightResult() {
            return _highlightResult;
        }

        public void set_highlightResult(HighlightResultBean _highlightResult) {
            this._highlightResult = _highlightResult;
        }

        public static class PostAuthorBean {
            /**
             * user_id : 145
             * display_name : Biruk Hailu
             * user_url : http://360ground.com
             * user_login : 360_admin
             */

                @SerializedName("user_id")
                private int userId;

                @SerializedName("display_name")
                private String displayName;

                @SerializedName("user_url")
                private String userURL;

                @SerializedName("user_login")
                private String userLogin;

                public Author getAuthor() {

                    Author author = new Author();

                    author.setId(userId);

                    author.setName(displayName);

                    return author;
                }
        }

        public static class ImagesBean {
            /**
             * thumbnail : {"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-150x150.jpg","width":150,"height":150}
             * medium : {"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-234x300.jpg","width":234,"height":300}
             * medium_large : {"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7.jpg","width":569,"height":730}
             * large : {"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7.jpg","width":569,"height":730}
             * custom-thumbnail-size : {"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-569x300.jpg","width":569,"height":300}
             * custom-small-thumbnail-size : {"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-100x135.jpg","width":100,"height":135}
             * post-thumbnail : {"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-100x135.jpg","width":100,"height":135}
             * shop_thumbnail :{"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-180x180.jpg","width":180,"height":180}
             * shop_catalog : {"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-300x300.jpg","width":300,"height":300}
             * shop_single : {"url":"https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-569x600.jpg","width":569,"height":600}
             */

            private ThumbnailBean thumbnail;
            private MediumBean medium;
            private MediumLargeBean medium_large;
            private LargeBean large;
            @SerializedName("custom-thumbnail-size")
            private CustomthumbnailsizeBean customthumbnailsize;
            @SerializedName("custom-small-thumbnail-size")
            private CustomsmallthumbnailsizeBean customsmallthumbnailsize;
            @SerializedName("post-thumbnail")
            private PostthumbnailBean postthumbnail;
            private ShopThumbnailBean shop_thumbnail;
            private ShopCatalogBean shop_catalog;
            private ShopSingleBean shop_single;

            public ThumbnailBean getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(ThumbnailBean thumbnail) {
                this.thumbnail = thumbnail;
            }

            public MediumBean getMedium() {
                return medium;
            }

            public void setMedium(MediumBean medium) {
                this.medium = medium;
            }

            public MediumLargeBean getMedium_large() {
                return medium_large;
            }

            public void setMedium_large(MediumLargeBean medium_large) {
                this.medium_large = medium_large;
            }

            public LargeBean getLarge() {
                return large;
            }

            public void setLarge(LargeBean large) {
                this.large = large;
            }

            public CustomthumbnailsizeBean getCustomthumbnailsize() {
                return customthumbnailsize;
            }

            public void setCustomthumbnailsize(CustomthumbnailsizeBean customthumbnailsize) {
                this.customthumbnailsize = customthumbnailsize;
            }

            public CustomsmallthumbnailsizeBean getCustomsmallthumbnailsize() {
                return customsmallthumbnailsize;
            }

            public void setCustomsmallthumbnailsize(CustomsmallthumbnailsizeBean customsmallthumbnailsize) {
                this.customsmallthumbnailsize = customsmallthumbnailsize;
            }

            public PostthumbnailBean getPostthumbnail() {
                return postthumbnail;
            }

            public void setPostthumbnail(PostthumbnailBean postthumbnail) {
                this.postthumbnail = postthumbnail;
            }

            public ShopThumbnailBean getShop_thumbnail() {
                return shop_thumbnail;
            }

            public void setShop_thumbnail(ShopThumbnailBean shop_thumbnail) {
                this.shop_thumbnail = shop_thumbnail;
            }

            public ShopCatalogBean getShop_catalog() {
                return shop_catalog;
            }

            public void setShop_catalog(ShopCatalogBean shop_catalog) {
                this.shop_catalog = shop_catalog;
            }

            public ShopSingleBean getShop_single() {
                return shop_single;
            }

            public void setShop_single(ShopSingleBean shop_single) {
                this.shop_single = shop_single;
            }

            public static class ThumbnailBean {
                /**
                 * url : https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-150x150.jpg
                 * width : 150
                 * height : 150
                 */

                private String url;
                private int width;
                private int height;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }
            }

            public static class MediumBean {
                /**
                 * url : https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-234x300.jpg
                 * width : 234
                 * height : 300
                 */

                private String url;
                private int width;
                private int height;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }
            }

            public static class MediumLargeBean {
                /**
                 * url : https://api.lomistore.com/wp-content/uploads/2017/05/cover-7.jpg
                 * width : 569
                 * height : 730
                 */

                private String url;
                private int width;
                private int height;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }
            }

            public static class LargeBean {
                /**
                 * url : https://api.lomistore.com/wp-content/uploads/2017/05/cover-7.jpg
                 * width : 569
                 * height : 730
                 */

                private String url;
                private int width;
                private int height;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }
            }

            public static class CustomthumbnailsizeBean {
                /**
                 * url : https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-569x300.jpg
                 * width : 569
                 * height : 300
                 */

                private String url;
                private int width;
                private int height;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }
            }

            public static class CustomsmallthumbnailsizeBean {
                /**
                 * url : https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-100x135.jpg
                 * width : 100
                 * height : 135
                 */

                private String url;
                private int width;
                private int height;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }
            }

            public static class PostthumbnailBean {
                /**
                 * url : https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-100x135.jpg
                 * width : 100
                 * height : 135
                 */

                private String url;
                private int width;
                private int height;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }
            }

            public static class ShopThumbnailBean {
                /**
                 * url : https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-180x180.jpg
                 * width : 180
                 * height : 180
                 */

                private String url;
                private int width;
                private int height;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }
            }

            public static class ShopCatalogBean {
                /**
                 * url : https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-300x300.jpg
                 * width : 300
                 * height : 300
                 */

                private String url;
                private int width;
                private int height;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }
            }

            public static class ShopSingleBean {
                /**
                 * url : https://api.lomistore.com/wp-content/uploads/2017/05/cover-7-569x600.jpg
                 * width : 569
                 * height : 600
                 */

                private String url;
                private int width;
                private int height;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }
            }
        }

        public static class TaxonomiesBean {
            private List<String> product_type;
            private List<String> product_cat;
            private List<?> product_tag;
            private List<?> product_shipping_class;
            @SerializedName("pa_author-name")
            private List<String> pa_authorname;
            private List<String> pa_currency;
            @SerializedName("pa_dollar-price")
            private List<?> pa_dollarprice;
            @SerializedName("pa_publication-year")
            private List<String> pa_publicationyear;
            private List<String> pa_publisher;

            public List<String> getProduct_type() {
                return product_type;
            }

            public void setProduct_type(List<String> product_type) {
                this.product_type = product_type;
            }

            public List<String> getProduct_cat() {
                return product_cat;
            }

            public void setProduct_cat(List<String> product_cat) {
                this.product_cat = product_cat;
            }

            public List<?> getProduct_tag() {
                return product_tag;
            }

            public void setProduct_tag(List<?> product_tag) {
                this.product_tag = product_tag;
            }

            public List<?> getProduct_shipping_class() {
                return product_shipping_class;
            }

            public void setProduct_shipping_class(List<?> product_shipping_class) {
                this.product_shipping_class = product_shipping_class;
            }

            public List<String> getPa_authorname() {
                return pa_authorname;
            }

            public void setPa_authorname(List<String> pa_authorname) {
                this.pa_authorname = pa_authorname;
            }

            public List<String> getPa_currency() {
                return pa_currency;
            }

            public void setPa_currency(List<String> pa_currency) {
                this.pa_currency = pa_currency;
            }

            public List<?> getPa_dollarprice() {
                return pa_dollarprice;
            }

            public void setPa_dollarprice(List<?> pa_dollarprice) {
                this.pa_dollarprice = pa_dollarprice;
            }

            public List<String> getPa_publicationyear() {
                return pa_publicationyear;
            }

            public void setPa_publicationyear(List<String> pa_publicationyear) {
                this.pa_publicationyear = pa_publicationyear;
            }

            public List<String> getPa_publisher() {
                return pa_publisher;
            }

            public void setPa_publisher(List<String> pa_publisher) {
                this.pa_publisher = pa_publisher;
            }
        }

        public static class TaxonomiesHierarchicalBean {
            /**
             * product_cat : {"lvl0":["እንዲያነቡት የሚመከሩ","የህይወት ታሪኮች"]}
             * pa_author-name : {"lvl0":["ሀዲስ ዓለማየሁ"]}
             * pa_currency : {"lvl0":["etb","usd"]}
             * pa_dollar-price : []
             * pa_publication-year : {"lvl0":["2000"]}
             * pa_publisher : {"lvl0":["Lomi Publishing"]}
             */

            private ProductCatBean product_cat;
            @SerializedName("pa_author-name")
            private PaAuthornameBean pa_authorname;
            private PaCurrencyBean pa_currency;
            @SerializedName("pa_publication-year")
            private PaPublicationyearBean pa_publicationyear;
            private PaPublisherBean pa_publisher;
            @SerializedName("pa_dollar-price")
            private List<?> pa_dollarprice;

            public ProductCatBean getProduct_cat() {
                return product_cat;
            }

            public void setProduct_cat(ProductCatBean product_cat) {
                this.product_cat = product_cat;
            }

            public PaAuthornameBean getPa_authorname() {
                return pa_authorname;
            }

            public void setPa_authorname(PaAuthornameBean pa_authorname) {
                this.pa_authorname = pa_authorname;
            }

            public PaCurrencyBean getPa_currency() {
                return pa_currency;
            }

            public void setPa_currency(PaCurrencyBean pa_currency) {
                this.pa_currency = pa_currency;
            }

            public PaPublicationyearBean getPa_publicationyear() {
                return pa_publicationyear;
            }

            public void setPa_publicationyear(PaPublicationyearBean pa_publicationyear) {
                this.pa_publicationyear = pa_publicationyear;
            }

            public PaPublisherBean getPa_publisher() {
                return pa_publisher;
            }

            public void setPa_publisher(PaPublisherBean pa_publisher) {
                this.pa_publisher = pa_publisher;
            }

            public List<?> getPa_dollarprice() {
                return pa_dollarprice;
            }

            public void setPa_dollarprice(List<?> pa_dollarprice) {
                this.pa_dollarprice = pa_dollarprice;
            }

            public static class ProductCatBean {
                private List<String> lvl0;

                public List<String> getLvl0() {
                    return lvl0;
                }

                public void setLvl0(List<String> lvl0) {
                    this.lvl0 = lvl0;
                }
            }

            public static class PaAuthornameBean {
                private List<String> lvl0;

                public List<String> getLvl0() {
                    return lvl0;
                }

                public void setLvl0(List<String> lvl0) {
                    this.lvl0 = lvl0;
                }
            }

            public static class PaCurrencyBean {
                private List<String> lvl0;

                public List<String> getLvl0() {
                    return lvl0;
                }

                public void setLvl0(List<String> lvl0) {
                    this.lvl0 = lvl0;
                }
            }

            public static class PaPublicationyearBean {
                private List<String> lvl0;

                public List<String> getLvl0() {
                    return lvl0;
                }

                public void setLvl0(List<String> lvl0) {
                    this.lvl0 = lvl0;
                }
            }

            public static class PaPublisherBean {
                private List<String> lvl0;

                public List<String> getLvl0() {
                    return lvl0;
                }

                public void setLvl0(List<String> lvl0) {
                    this.lvl0 = lvl0;
                }
            }
        }

        public static class SnippetResultBean {
            /**
             * post_title : {"value":"የሕይወት ጉዞዬ <em>እ<\/em>ና ትዝታዎቼ - በሀገር ቤት  ክፍል ሁለት","matchLevel":"full"}
             * title1 : {"value":"","matchLevel":"none"}
             * title2 : {"value":"","matchLevel":"none"}
             * title3 : {"value":"","matchLevel":"none"}
             * title4 : {"value":"","matchLevel":"none"}
             * title5 : {"value":"","matchLevel":"none"}
             * title6 : {"value":"","matchLevel":"none"}
             * content : {"value":"\u2026 ደግሞ የሕይወት ጉዞዬና ትዝታዎቼ፣ ክፍል ሁለት \u2013 በሀገር ቤት ይኸው ለኅትመት አበቃሁ፡፡ የዚህ ጽሑፍ ዋናዋ ተወናዋይ <em>እ<\/em>ኔው <em>እ<\/em>ራሴ ነኝ፡፡ ወደ <em>እ<\/em>ምወዳት ሀገሬ ከተመለስኩ በኋላ ያጋጠሙኝን የደስታ፣ የሀዘን፣ የሚያናድድ፣ የሚያስደስት፣ አልፎ አልፎም \u2026","matchLevel":"full"}
             */

            private PostTitleBean post_title;
            private Title1Bean title1;
            private Title2Bean title2;
            private Title3Bean title3;
            private Title4Bean title4;
            private Title5Bean title5;
            private Title6Bean title6;
            private ContentBean content;

            public PostTitleBean getPost_title() {
                return post_title;
            }

            public void setPost_title(PostTitleBean post_title) {
                this.post_title = post_title;
            }

            public Title1Bean getTitle1() {
                return title1;
            }

            public void setTitle1(Title1Bean title1) {
                this.title1 = title1;
            }

            public Title2Bean getTitle2() {
                return title2;
            }

            public void setTitle2(Title2Bean title2) {
                this.title2 = title2;
            }

            public Title3Bean getTitle3() {
                return title3;
            }

            public void setTitle3(Title3Bean title3) {
                this.title3 = title3;
            }

            public Title4Bean getTitle4() {
                return title4;
            }

            public void setTitle4(Title4Bean title4) {
                this.title4 = title4;
            }

            public Title5Bean getTitle5() {
                return title5;
            }

            public void setTitle5(Title5Bean title5) {
                this.title5 = title5;
            }

            public Title6Bean getTitle6() {
                return title6;
            }

            public void setTitle6(Title6Bean title6) {
                this.title6 = title6;
            }

            public ContentBean getContent() {
                return content;
            }

            public void setContent(ContentBean content) {
                this.content = content;
            }

            public static class PostTitleBean {
                /**
                 * value : የሕይወት ጉዞዬ <em>እ</em>ና ትዝታዎቼ - በሀገር ቤት  ክፍል ሁለት
                 * matchLevel : full
                 */

                private String value;
                private String matchLevel;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getMatchLevel() {
                    return matchLevel;
                }

                public void setMatchLevel(String matchLevel) {
                    this.matchLevel = matchLevel;
                }
            }

            public static class Title1Bean {
                /**
                 * value :
                 * matchLevel : none
                 */

                private String value;
                private String matchLevel;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getMatchLevel() {
                    return matchLevel;
                }

                public void setMatchLevel(String matchLevel) {
                    this.matchLevel = matchLevel;
                }
            }

            public static class Title2Bean {
                /**
                 * value :
                 * matchLevel : none
                 */

                private String value;
                private String matchLevel;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getMatchLevel() {
                    return matchLevel;
                }

                public void setMatchLevel(String matchLevel) {
                    this.matchLevel = matchLevel;
                }
            }

            public static class Title3Bean {
                /**
                 * value :
                 * matchLevel : none
                 */

                private String value;
                private String matchLevel;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getMatchLevel() {
                    return matchLevel;
                }

                public void setMatchLevel(String matchLevel) {
                    this.matchLevel = matchLevel;
                }
            }

            public static class Title4Bean {
                /**
                 * value :
                 * matchLevel : none
                 */

                private String value;
                private String matchLevel;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getMatchLevel() {
                    return matchLevel;
                }

                public void setMatchLevel(String matchLevel) {
                    this.matchLevel = matchLevel;
                }
            }

            public static class Title5Bean {
                /**
                 * value :
                 * matchLevel : none
                 */

                private String value;
                private String matchLevel;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getMatchLevel() {
                    return matchLevel;
                }

                public void setMatchLevel(String matchLevel) {
                    this.matchLevel = matchLevel;
                }
            }

            public static class Title6Bean {
                /**
                 * value :
                 * matchLevel : none
                 */

                private String value;
                private String matchLevel;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getMatchLevel() {
                    return matchLevel;
                }

                public void setMatchLevel(String matchLevel) {
                    this.matchLevel = matchLevel;
                }
            }

            public static class ContentBean {
                /**
                 * value : … ደግሞ የሕይወት ጉዞዬና ትዝታዎቼ፣ ክፍል ሁለት – በሀገር ቤት ይኸው ለኅትመት አበቃሁ፡፡ የዚህ ጽሑፍ ዋናዋ ተወናዋይ <em>እ</em>ኔው <em>እ</em>ራሴ ነኝ፡፡ ወደ <em>እ</em>ምወዳት ሀገሬ ከተመለስኩ በኋላ ያጋጠሙኝን የደስታ፣ የሀዘን፣ የሚያናድድ፣ የሚያስደስት፣ አልፎ አልፎም …
                 * matchLevel : full
                 */

                private String value;
                private String matchLevel;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getMatchLevel() {
                    return matchLevel;
                }

                public void setMatchLevel(String matchLevel) {
                    this.matchLevel = matchLevel;
                }
            }
        }

        public static class HighlightResultBean {
            /**
             * post_title : {"value":"የሕይወት ጉዞዬ <em>እ<\/em>ና ትዝታዎቼ - በሀገር ቤት  ክፍል ሁለት","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]}
             * taxonomies : {"product_type":[{"value":"variable","matchLevel":"none","matchedWords":[]}],"product_cat":[{"value":"<em>እ<\/em>ንዲያነቡት የሚመከሩ","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]},{"value":"የህይወት ታሪኮች","matchLevel":"none","matchedWords":[]}],"pa_author-name":[{"value":"ሀዲስ ዓለማየሁ","matchLevel":"none","matchedWords":[]}],"pa_currency":[{"value":"etb","matchLevel":"none","matchedWords":[]},{"value":"usd","matchLevel":"none","matchedWords":[]}],"pa_publication-year":[{"value":"2000","matchLevel":"none","matchedWords":[]}],"pa_publisher":[{"value":"Lomi Publishing","matchLevel":"none","matchedWords":[]}]}
             * title1 : {"value":"","matchLevel":"none","matchedWords":[]}
             * title2 : {"value":"","matchLevel":"none","matchedWords":[]}
             * title3 : {"value":"","matchLevel":"none","matchedWords":[]}
             * title4 : {"value":"","matchLevel":"none","matchedWords":[]}
             * title5 : {"value":"","matchLevel":"none","matchedWords":[]}
             * title6 : {"value":"","matchLevel":"none","matchedWords":[]}
             * content : {"value":"በመጀመሪያ የሕይወት ጉዞዬና ትዝታዎቼ በሚል ርዕስ ለኅትመት ያበቃሁት መጽሐፍ በጠቅላላ የሚተርከው የውጭ ዓለም ኑሮዬንና ጉዞዬን ነው፡፡ ይህ መጽሐፍ ብዙ ሰዎች ተወያይተውበታል፣ በመጽሔትና በጋዜጣ፣ በሬዲዮና በቴሌቪዥን ብዙ ሽፋን አግኝቷል፡፡ ከብዙ ግለ ሰቦችም ከማስበው በላይ ምስጋና አግኝቼበታለሁ፡፡ አበጀህ፣ ያበርታህ ተብዬበታለሁ፡፡ ዕውቀት አግኝተንበታል ብለውኛል፡፡ በዚህም በጣም ደስ ብሎኛል፡፡ አሁን ደግሞ የሕይወት ጉዞዬና ትዝታዎቼ፣ ክፍል ሁለት \u2013 በሀገር ቤት ይኸው ለኅትመት አበቃሁ፡፡ የዚህ ጽሑፍ ዋናዋ ተወናዋይ <em>እ<\/em>ኔው <em>እ<\/em>ራሴ ነኝ፡፡ ወደ <em>እ<\/em>ምወዳት ሀገሬ ከተመለስኩ በኋላ ያጋጠሙኝን የደስታ፣ የሀዘን፣ የሚያናድድ፣ የሚያስደስት፣ አልፎ አልፎም በጣም የሚያስቅ ሕይወት መርቻለሁ፡፡ ሁሉንም <em>እ<\/em>ንደወረደ አስቀምጨዋለሁ፡፡ ቤት ሥሰራ የነበረው ምልልስ፣ የመሬት ካርታ ለማስለወጥ የነበረውን ውጣ ውረድ፣ ባሕር ማዶ ያሉት ጓደኞቼ ለመጀመሪያ ጊዜ ኢትዮጵን ለመጎብኘትና <em>እ<\/em>ኔንም ለመጠየቅ ሲመጡ የነበረውን ገጠመኞች፣ ከበጎ አድራጎት ድርጅቶች ጋር ሥራ፣ የሠፈር መንገድ ለማሠራት፣ ሲያቀብጠኝ ኃላፊነቱን ተቀብዬ፣ ጎረቤቶቼ ራሳቸው መርጠውኝ \u2013 መራጮቹ አንከፍልም ሲሉኝ፣ ኧረ ስንቱ? በሀገር ውስጥ ከደራሲያን ጋር ያደረኩትን ጉብኝት፣ ሀዘኑን \u2013 ለቅሶውን፣ ሠርጉን \u2013 ጭፈራውን ወዘተርፈ. በሰፊውም ባይሆን አጠር መጠን አድርጌ በትረካ መልክ አቅርቤዋለሁ፡፡ ምንም <em>እ<\/em>ንኳን ርዕሱ በሀገር ቤት ቢልም፣ ከዚሁ ከሀገር ቤት ወደ ውጭ አገር ሔጄ የጎበኘኋቸውን የባዕድ አገሮችንም አብሬ ተርኬ አውጥቼዋለሁ፡፡ ቻይናዎች \u201cአንድ ፎቶግራፍ አንድ ሺ ቃላት ያወራል\u201d ይላሉ፡፡ <em>እ<\/em>ኔም አምንበታለሁ፡፡ ስለዚህ ከትረካው ጋር የሚዛመዱ ብዙ ፎቶግራፎች አብሬ አክየበታለሁ፡፡","matchLevel":"full","fullyHighlighted":false,"matchedWords":["እ"]}
             */

            private PostTitleBeanX post_title;
            private TaxonomiesBeanX taxonomies;
            private Title1BeanX title1;
            private Title2BeanX title2;
            private Title3BeanX title3;
            private Title4BeanX title4;
            private Title5BeanX title5;
            private Title6BeanX title6;
            private ContentBeanX content;

            public PostTitleBeanX getPost_title() {
                return post_title;
            }

            public void setPost_title(PostTitleBeanX post_title) {
                this.post_title = post_title;
            }

            public TaxonomiesBeanX getTaxonomies() {
                return taxonomies;
            }

            public void setTaxonomies(TaxonomiesBeanX taxonomies) {
                this.taxonomies = taxonomies;
            }

            public Title1BeanX getTitle1() {
                return title1;
            }

            public void setTitle1(Title1BeanX title1) {
                this.title1 = title1;
            }

            public Title2BeanX getTitle2() {
                return title2;
            }

            public void setTitle2(Title2BeanX title2) {
                this.title2 = title2;
            }

            public Title3BeanX getTitle3() {
                return title3;
            }

            public void setTitle3(Title3BeanX title3) {
                this.title3 = title3;
            }

            public Title4BeanX getTitle4() {
                return title4;
            }

            public void setTitle4(Title4BeanX title4) {
                this.title4 = title4;
            }

            public Title5BeanX getTitle5() {
                return title5;
            }

            public void setTitle5(Title5BeanX title5) {
                this.title5 = title5;
            }

            public Title6BeanX getTitle6() {
                return title6;
            }

            public void setTitle6(Title6BeanX title6) {
                this.title6 = title6;
            }

            public ContentBeanX getContent() {
                return content;
            }

            public void setContent(ContentBeanX content) {
                this.content = content;
            }

            public static class PostTitleBeanX {
                /**
                 * value : የሕይወት ጉዞዬ <em>እ</em>ና ትዝታዎቼ - በሀገር ቤት  ክፍል ሁለት
                 * matchLevel : full
                 * fullyHighlighted : false
                 * matchedWords : ["እ"]
                 */

                private String value;
                private String matchLevel;
                private boolean fullyHighlighted;
                private List<String> matchedWords;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getMatchLevel() {
                    return matchLevel;
                }

                public void setMatchLevel(String matchLevel) {
                    this.matchLevel = matchLevel;
                }

                public boolean isFullyHighlighted() {
                    return fullyHighlighted;
                }

                public void setFullyHighlighted(boolean fullyHighlighted) {
                    this.fullyHighlighted = fullyHighlighted;
                }

                public List<String> getMatchedWords() {
                    return matchedWords;
                }

                public void setMatchedWords(List<String> matchedWords) {
                    this.matchedWords = matchedWords;
                }
            }

            public static class TaxonomiesBeanX {
                private List<ProductTypeBean> product_type;
                private List<ProductCatBeanX> product_cat;
                @SerializedName("pa_author-name")
                private List<PaAuthornameBeanX> pa_authorname;
                private List<PaCurrencyBeanX> pa_currency;
                @SerializedName("pa_publication-year")
                private List<PaPublicationyearBeanX> pa_publicationyear;
                private List<PaPublisherBeanX> pa_publisher;

                public List<ProductTypeBean> getProduct_type() {
                    return product_type;
                }

                public void setProduct_type(List<ProductTypeBean> product_type) {
                    this.product_type = product_type;
                }

                public List<ProductCatBeanX> getProduct_cat() {
                    return product_cat;
                }

                public void setProduct_cat(List<ProductCatBeanX> product_cat) {
                    this.product_cat = product_cat;
                }

                public List<PaAuthornameBeanX> getPa_authorname() {
                    return pa_authorname;
                }

                public void setPa_authorname(List<PaAuthornameBeanX> pa_authorname) {
                    this.pa_authorname = pa_authorname;
                }

                public List<PaCurrencyBeanX> getPa_currency() {
                    return pa_currency;
                }

                public void setPa_currency(List<PaCurrencyBeanX> pa_currency) {
                    this.pa_currency = pa_currency;
                }

                public List<PaPublicationyearBeanX> getPa_publicationyear() {
                    return pa_publicationyear;
                }

                public void setPa_publicationyear(List<PaPublicationyearBeanX> pa_publicationyear) {
                    this.pa_publicationyear = pa_publicationyear;
                }

                public List<PaPublisherBeanX> getPa_publisher() {
                    return pa_publisher;
                }

                public void setPa_publisher(List<PaPublisherBeanX> pa_publisher) {
                    this.pa_publisher = pa_publisher;
                }

                public static class ProductTypeBean {
                    /**
                     * value : variable
                     * matchLevel : none
                     * matchedWords : []
                     */

                    private String value;
                    private String matchLevel;
                    private List<?> matchedWords;

                    public String getValue() {
                        return value;
                    }

                    public void setValue(String value) {
                        this.value = value;
                    }

                    public String getMatchLevel() {
                        return matchLevel;
                    }

                    public void setMatchLevel(String matchLevel) {
                        this.matchLevel = matchLevel;
                    }

                    public List<?> getMatchedWords() {
                        return matchedWords;
                    }

                    public void setMatchedWords(List<?> matchedWords) {
                        this.matchedWords = matchedWords;
                    }
                }

                public static class ProductCatBeanX {
                    /**
                     * value : <em>እ</em>ንዲያነቡት የሚመከሩ
                     * matchLevel : full
                     * fullyHighlighted : false
                     * matchedWords : ["እ"]
                     */

                    private String value;
                    private String matchLevel;
                    private boolean fullyHighlighted;
                    private List<String> matchedWords;

                    public String getValue() {
                        return value;
                    }

                    public void setValue(String value) {
                        this.value = value;
                    }

                    public String getMatchLevel() {
                        return matchLevel;
                    }

                    public void setMatchLevel(String matchLevel) {
                        this.matchLevel = matchLevel;
                    }

                    public boolean isFullyHighlighted() {
                        return fullyHighlighted;
                    }

                    public void setFullyHighlighted(boolean fullyHighlighted) {
                        this.fullyHighlighted = fullyHighlighted;
                    }

                    public List<String> getMatchedWords() {
                        return matchedWords;
                    }

                    public void setMatchedWords(List<String> matchedWords) {
                        this.matchedWords = matchedWords;
                    }
                }

                public static class PaAuthornameBeanX {
                    /**
                     * value : ሀዲስ ዓለማየሁ
                     * matchLevel : none
                     * matchedWords : []
                     */

                    private String value;
                    private String matchLevel;
                    private List<?> matchedWords;

                    public String getValue() {
                        return value;
                    }

                    public void setValue(String value) {
                        this.value = value;
                    }

                    public String getMatchLevel() {
                        return matchLevel;
                    }

                    public void setMatchLevel(String matchLevel) {
                        this.matchLevel = matchLevel;
                    }

                    public List<?> getMatchedWords() {
                        return matchedWords;
                    }

                    public void setMatchedWords(List<?> matchedWords) {
                        this.matchedWords = matchedWords;
                    }
                }

                public static class PaCurrencyBeanX {
                    /**
                     * value : etb
                     * matchLevel : none
                     * matchedWords : []
                     */

                    private String value;
                    private String matchLevel;
                    private List<?> matchedWords;

                    public String getValue() {
                        return value;
                    }

                    public void setValue(String value) {
                        this.value = value;
                    }

                    public String getMatchLevel() {
                        return matchLevel;
                    }

                    public void setMatchLevel(String matchLevel) {
                        this.matchLevel = matchLevel;
                    }

                    public List<?> getMatchedWords() {
                        return matchedWords;
                    }

                    public void setMatchedWords(List<?> matchedWords) {
                        this.matchedWords = matchedWords;
                    }
                }

                public static class PaPublicationyearBeanX {
                    /**
                     * value : 2000
                     * matchLevel : none
                     * matchedWords : []
                     */

                    private String value;
                    private String matchLevel;
                    private List<?> matchedWords;

                    public String getValue() {
                        return value;
                    }

                    public void setValue(String value) {
                        this.value = value;
                    }

                    public String getMatchLevel() {
                        return matchLevel;
                    }

                    public void setMatchLevel(String matchLevel) {
                        this.matchLevel = matchLevel;
                    }

                    public List<?> getMatchedWords() {
                        return matchedWords;
                    }

                    public void setMatchedWords(List<?> matchedWords) {
                        this.matchedWords = matchedWords;
                    }
                }

                public static class PaPublisherBeanX {
                    /**
                     * value : Lomi Publishing
                     * matchLevel : none
                     * matchedWords : []
                     */

                    private String value;
                    private String matchLevel;
                    private List<?> matchedWords;

                    public String getValue() {
                        return value;
                    }

                    public void setValue(String value) {
                        this.value = value;
                    }

                    public String getMatchLevel() {
                        return matchLevel;
                    }

                    public void setMatchLevel(String matchLevel) {
                        this.matchLevel = matchLevel;
                    }

                    public List<?> getMatchedWords() {
                        return matchedWords;
                    }

                    public void setMatchedWords(List<?> matchedWords) {
                        this.matchedWords = matchedWords;
                    }
                }
            }

            public static class Title1BeanX {
                /**
                 * value :
                 * matchLevel : none
                 * matchedWords : []
                 */

                private String value;
                private String matchLevel;
                private List<?> matchedWords;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getMatchLevel() {
                    return matchLevel;
                }

                public void setMatchLevel(String matchLevel) {
                    this.matchLevel = matchLevel;
                }

                public List<?> getMatchedWords() {
                    return matchedWords;
                }

                public void setMatchedWords(List<?> matchedWords) {
                    this.matchedWords = matchedWords;
                }
            }

            public static class Title2BeanX {
                /**
                 * value :
                 * matchLevel : none
                 * matchedWords : []
                 */

                private String value;
                private String matchLevel;
                private List<?> matchedWords;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getMatchLevel() {
                    return matchLevel;
                }

                public void setMatchLevel(String matchLevel) {
                    this.matchLevel = matchLevel;
                }

                public List<?> getMatchedWords() {
                    return matchedWords;
                }

                public void setMatchedWords(List<?> matchedWords) {
                    this.matchedWords = matchedWords;
                }
            }

            public static class Title3BeanX {
                /**
                 * value :
                 * matchLevel : none
                 * matchedWords : []
                 */

                private String value;
                private String matchLevel;
                private List<?> matchedWords;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getMatchLevel() {
                    return matchLevel;
                }

                public void setMatchLevel(String matchLevel) {
                    this.matchLevel = matchLevel;
                }

                public List<?> getMatchedWords() {
                    return matchedWords;
                }

                public void setMatchedWords(List<?> matchedWords) {
                    this.matchedWords = matchedWords;
                }
            }

            public static class Title4BeanX {
                /**
                 * value :
                 * matchLevel : none
                 * matchedWords : []
                 */

                private String value;
                private String matchLevel;
                private List<?> matchedWords;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getMatchLevel() {
                    return matchLevel;
                }

                public void setMatchLevel(String matchLevel) {
                    this.matchLevel = matchLevel;
                }

                public List<?> getMatchedWords() {
                    return matchedWords;
                }

                public void setMatchedWords(List<?> matchedWords) {
                    this.matchedWords = matchedWords;
                }
            }

            public static class Title5BeanX {
                /**
                 * value :
                 * matchLevel : none
                 * matchedWords : []
                 */

                private String value;
                private String matchLevel;
                private List<?> matchedWords;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getMatchLevel() {
                    return matchLevel;
                }

                public void setMatchLevel(String matchLevel) {
                    this.matchLevel = matchLevel;
                }

                public List<?> getMatchedWords() {
                    return matchedWords;
                }

                public void setMatchedWords(List<?> matchedWords) {
                    this.matchedWords = matchedWords;
                }
            }

            public static class Title6BeanX {
                /**
                 * value :
                 * matchLevel : none
                 * matchedWords : []
                 */

                private String value;
                private String matchLevel;
                private List<?> matchedWords;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getMatchLevel() {
                    return matchLevel;
                }

                public void setMatchLevel(String matchLevel) {
                    this.matchLevel = matchLevel;
                }

                public List<?> getMatchedWords() {
                    return matchedWords;
                }

                public void setMatchedWords(List<?> matchedWords) {
                    this.matchedWords = matchedWords;
                }
            }

            public static class ContentBeanX {
                /**
                 * value : በመጀመሪያ የሕይወት ጉዞዬና ትዝታዎቼ በሚል ርዕስ ለኅትመት ያበቃሁት መጽሐፍ በጠቅላላ የሚተርከው የውጭ ዓለም ኑሮዬንና ጉዞዬን ነው፡፡ ይህ መጽሐፍ ብዙ ሰዎች ተወያይተውበታል፣ በመጽሔትና በጋዜጣ፣ በሬዲዮና በቴሌቪዥን ብዙ ሽፋን አግኝቷል፡፡ ከብዙ ግለ ሰቦችም ከማስበው በላይ ምስጋና አግኝቼበታለሁ፡፡ አበጀህ፣ ያበርታህ ተብዬበታለሁ፡፡ ዕውቀት አግኝተንበታል ብለውኛል፡፡ በዚህም በጣም ደስ ብሎኛል፡፡ አሁን ደግሞ የሕይወት ጉዞዬና ትዝታዎቼ፣ ክፍል ሁለት – በሀገር ቤት ይኸው ለኅትመት አበቃሁ፡፡ የዚህ ጽሑፍ ዋናዋ ተወናዋይ <em>እ</em>ኔው <em>እ</em>ራሴ ነኝ፡፡ ወደ <em>እ</em>ምወዳት ሀገሬ ከተመለስኩ በኋላ ያጋጠሙኝን የደስታ፣ የሀዘን፣ የሚያናድድ፣ የሚያስደስት፣ አልፎ አልፎም በጣም የሚያስቅ ሕይወት መርቻለሁ፡፡ ሁሉንም <em>እ</em>ንደወረደ አስቀምጨዋለሁ፡፡ ቤት ሥሰራ የነበረው ምልልስ፣ የመሬት ካርታ ለማስለወጥ የነበረውን ውጣ ውረድ፣ ባሕር ማዶ ያሉት ጓደኞቼ ለመጀመሪያ ጊዜ ኢትዮጵን ለመጎብኘትና <em>እ</em>ኔንም ለመጠየቅ ሲመጡ የነበረውን ገጠመኞች፣ ከበጎ አድራጎት ድርጅቶች ጋር ሥራ፣ የሠፈር መንገድ ለማሠራት፣ ሲያቀብጠኝ ኃላፊነቱን ተቀብዬ፣ ጎረቤቶቼ ራሳቸው መርጠውኝ – መራጮቹ አንከፍልም ሲሉኝ፣ ኧረ ስንቱ? በሀገር ውስጥ ከደራሲያን ጋር ያደረኩትን ጉብኝት፣ ሀዘኑን – ለቅሶውን፣ ሠርጉን – ጭፈራውን ወዘተርፈ. በሰፊውም ባይሆን አጠር መጠን አድርጌ በትረካ መልክ አቅርቤዋለሁ፡፡ ምንም <em>እ</em>ንኳን ርዕሱ በሀገር ቤት ቢልም፣ ከዚሁ ከሀገር ቤት ወደ ውጭ አገር ሔጄ የጎበኘኋቸውን የባዕድ አገሮችንም አብሬ ተርኬ አውጥቼዋለሁ፡፡ ቻይናዎች “አንድ ፎቶግራፍ አንድ ሺ ቃላት ያወራል” ይላሉ፡፡ <em>እ</em>ኔም አምንበታለሁ፡፡ ስለዚህ ከትረካው ጋር የሚዛመዱ ብዙ ፎቶግራፎች አብሬ አክየበታለሁ፡፡
                 * matchLevel : full
                 * fullyHighlighted : false
                 * matchedWords : ["እ"]
                 */

                private String value;
                private String matchLevel;
                private boolean fullyHighlighted;
                private List<String> matchedWords;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getMatchLevel() {
                    return matchLevel;
                }

                public void setMatchLevel(String matchLevel) {
                    this.matchLevel = matchLevel;
                }

                public boolean isFullyHighlighted() {
                    return fullyHighlighted;
                }

                public void setFullyHighlighted(boolean fullyHighlighted) {
                    this.fullyHighlighted = fullyHighlighted;
                }

                public List<String> getMatchedWords() {
                    return matchedWords;
                }

                public void setMatchedWords(List<String> matchedWords) {
                    this.matchedWords = matchedWords;
                }
            }
        }
    }
}