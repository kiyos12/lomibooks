package com.a360ground.lomibook.domain.woocommerce;


import com.a360ground.lomibook.BuildConfig;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Kiyos on 6/3/2017.
 */

public class Apk extends RealmObject {

    @PrimaryKey
    private int id;
    private String status;
    private String type;
    private Title title;
    private Content content;
    private String template;


    private MetaBox meta_box;
    private boolean later;


    public boolean isLater() {
        return later;
    }

    public void setLater(boolean later) {
        this.later = later;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public MetaBox getMeta_box() {
        return meta_box;
    }

    public void setMeta_box(MetaBox meta_box) {
        this.meta_box = meta_box;
    }

    public boolean isLatest() {

        /*String[] lomi_code_online = meta_box.getLomi_vcode().split("\\.");

        Log.d(TAG, "isLatest: " + meta_box.getLomi_vcode());

        String[] lomi_code_current = BuildConfig.VERSION_NAME.split("\\.");

        Log.d(TAG, "isLatest: " + BuildConfig.VERSION_NAME);

        int a1 = Integer.parseInt(lomi_code_online[0]);
        int a2 = Integer.parseInt(lomi_code_online[1]);
        int a3 = Integer.parseInt(lomi_code_online[2]);

        int b1 = Integer.parseInt(lomi_code_current[0]);
        int b2 = Integer.parseInt(lomi_code_current[1]);
        int b3 = Integer.parseInt(lomi_code_current[2]);

        if (a1 > b1) {
            Log.d(TAG, "isLatest: 111 " + a1 + " " + b1);
            return true;
        } else if (a1 == b1) {
            if (a2 > b2) {
                Log.d(TAG, "isLatest: 115 " + a2 + " " + b2);
                return true;
            } else if (a2 == b2) {
                if (a3 > b3) {
                    Log.d(TAG, "isLatest: 120 " + a3 + " " + b3);
                    return true;
                }
            }
        } else {
            return false;
        }
        return false;*/

        if (BuildConfig.VERSION_CODE < Integer.parseInt(meta_box.getLomi_vcode())){
            return true;
        }
        else{
            return false;
        }
    }
}
