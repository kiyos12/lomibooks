package com.a360ground.lomibook.domain.woocommerce;

/**
 * Created by kiyos on 9/30/2017.
 */

public class CustomerError {


    /**
     * id : 5336
     * code : 34
     * device_id : 1021
     * message : Accessing account in multiple device
     */

    private int id;
    private int code;
    private String device_id;
    private String message;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
