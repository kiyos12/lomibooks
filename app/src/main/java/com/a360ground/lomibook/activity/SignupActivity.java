package com.a360ground.lomibook.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.fragment.SignupFragment;
import com.facebook.FacebookSdk;

import io.realm.Realm;

public class SignupActivity extends AppCompatActivity {

    Realm realm;

    Customer customer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        realm = Realm.getDefaultInstance();

        customer = realm.where(Customer.class).findFirst();

        FacebookSdk.sdkInitialize (getApplicationContext ());

        setContentView(R.layout.activity_signup);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Join");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        Fragment fragment = new SignupFragment();

        transaction.add(R.id.signup_root, fragment);

        transaction.commit();
    }

    @Override
    protected void onResume() {

        super.onResume();

        if(customer != null) {

            finish();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId () == android.R.id.home){
            finish ();
        }

        return true;
    }
}
