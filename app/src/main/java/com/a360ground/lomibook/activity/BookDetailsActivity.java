package com.a360ground.lomibook.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.fragment.BookDetailsFragment;
import com.a360ground.lomibook.utils.Constants;

public class BookDetailsActivity extends AppCompatActivity {

    BookDetailsFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Intent in = getIntent();

        Uri data = in.getData();

        fragment = new BookDetailsFragment();

        if (data != null) {

            long id = Integer.parseInt(data.toString().replaceAll("\\D+",""));

            Log.d(BookDetailsActivity.class.getSimpleName(), "onCreate: " + id);

            Bundle bundle = new Bundle();

            bundle.putLong(Constants.PRODUCT_ID_IDENTIFIER, id);

            fragment.setArguments(bundle);
        }

        setContentView(R.layout.activity_book_details);

        FragmentManager manager = getSupportFragmentManager();

        FragmentTransaction transaction = manager.beginTransaction();

        transaction.add(R.id.fragment_container, fragment);

        transaction.commit();
    }
}