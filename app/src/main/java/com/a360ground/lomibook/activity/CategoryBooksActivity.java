package com.a360ground.lomibook.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.R;
import com.a360ground.lomibook.fragment.CategoryBooksFragment;

public class CategoryBooksActivity extends AppCompatActivity {

    CategoryBooksFragment fragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        String categoryName = getIntent().getStringExtra(Constants.CATEGORY_NAME_IDENTIFIER);

        setContentView(R.layout.activity_category_books);

        setSupportActionBar ((Toolbar) findViewById (R.id.category_activity_toolbar));

        getSupportActionBar ().setDisplayHomeAsUpEnabled (true);

        getSupportActionBar ().setTitle (categoryName);

        fragment = new CategoryBooksFragment();

        FragmentManager manager = getSupportFragmentManager();

        FragmentTransaction transaction = manager.beginTransaction();

        transaction.add(R.id.fragment_container, fragment);

        transaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId () == android.R.id.home){
            finish ();
        }

        return true;
    }
}
