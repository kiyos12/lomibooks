package com.a360ground.lomibook.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.domain.woocommerce.Customer;

import io.realm.Realm;

public class SplashActivity extends AppCompatActivity {

    private static final int COUNTRY_REQUEST_CODE = 592;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_splash);

        final ImageView flying_phone = (ImageView) findViewById(R.id.flying_phone);
        final TextView logo_title = (TextView) findViewById(R.id.logo_title);
        final TextView splash_title = (TextView) findViewById(R.id.splash_title);
        final TextView splash_desc = (TextView) findViewById(R.id.splash_desc);
        final Button begin_journey = (Button) findViewById(R.id.begin_journey);

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                Realm realm = Realm.getDefaultInstance();
                try {

                    Customer customer = realm.where(Customer.class).findFirst();

                    if (customer == null) {
                        logo_title.setVisibility(View.VISIBLE);
                        splash_title.setVisibility(View.VISIBLE);
                        splash_desc.setVisibility(View.VISIBLE);
                        begin_journey.setVisibility(View.VISIBLE);
                        flying_phone.setVisibility(View.VISIBLE);
                        flying_phone.setAnimation(AnimationUtils.loadAnimation(SplashActivity.this, R.anim.grow_from_top));
                        splash_title.setAnimation(AnimationUtils.loadAnimation(SplashActivity.this, R.anim.grow_from_bottom));
                        splash_desc.setAnimation(AnimationUtils.loadAnimation(SplashActivity.this, R.anim.grow_from_bottom));
                        begin_journey.setAnimation(AnimationUtils.loadAnimation(SplashActivity.this, R.anim.grow_from_bottom));

                    } else {

                        startActivity(new Intent(SplashActivity.this, MainActivity.class));

                    }
                } finally {

                }

            }
        }, 3000);
        begin_journey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SplashActivity.this, ChooseCountryActivity.class));
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        startActivity(new Intent(SplashActivity.this, MainActivity.class));
    }
}
