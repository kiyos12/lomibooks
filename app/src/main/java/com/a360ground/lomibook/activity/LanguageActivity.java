package com.a360ground.lomibook.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.adapter.LanguageRecyclerAdapter;
import com.a360ground.lomibook.utils.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;

public class LanguageActivity extends AppCompatActivity {

    RecyclerView languageRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_language);

        setSupportActionBar((Toolbar) findViewById(R.id.language_activity_toolbar));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setTitle(getString(R.string.choose_language));

        languageRecyclerView = (RecyclerView) findViewById(R.id.language_recycler);

        List<PreferenceHelper.LomiLanguage> languages = new ArrayList<>();

        languages.add(new PreferenceHelper.LomiLanguage(this, "en"));

        languages.add(new PreferenceHelper.LomiLanguage(this, "am"));

        LanguageRecyclerAdapter adapter = new LanguageRecyclerAdapter(LanguageActivity.this, languages);

        languageRecyclerView.setAdapter(adapter);

        languageRecyclerView.setLayoutManager(new LinearLayoutManager(LanguageActivity.this, LinearLayoutManager.VERTICAL, false));
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId () == android.R.id.home){
            finish ();
        }

        return true;
    }
}
