package com.a360ground.lomibook.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.fragment.Intro1;
import com.a360ground.lomibook.fragment.Intro2Login;
import com.a360ground.lomibook.utils.PreferenceHelper;
import com.a360ground.lomibook.view.LomiToolTip;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public class ChooseCountryActivity extends AppCompatActivity {

    ViewPager viewPager;
    Button next;
    Customer customer;
    ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_choose_country);

        viewPager = (ViewPager) findViewById(R.id.viewpager_indicator);

        next = (Button) findViewById(R.id.next);

        IntroFragmentPageAdapter introFragmentPageAdapter = new IntroFragmentPageAdapter(getSupportFragmentManager());

        viewPager.setAdapter(introFragmentPageAdapter);

        checkAndRequestPermissions();

        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            customer = realm.where(Customer.class).findFirst();

            next.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (PreferenceHelper.getCountryCode(ChooseCountryActivity.this) == null) {
                        LomiToolTip.showToolTip(v, "Please choose country", Gravity.TOP, R.color.white, R.color.red);
                    }
                    else if (customer != null) {
                        startActivity(new Intent(ChooseCountryActivity.this, MainActivity.class));
                        finish();
                    }
                    else {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    }
                }
            });
        } finally {
            if (realm != null) {
                realm.close();
            }
        }



        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {


                if (position == 0) {
                    next.setText(R.string.begin);
                } else {

                    next.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {

            finish();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class IntroFragmentPageAdapter extends FragmentPagerAdapter {


        IntroFragmentPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {
                Intro1 intro1 = new Intro1();


                return intro1;
            }
            if (position == 1) {
                Intro2Login intro2 = new Intro2Login();
                return intro2;
            }
            throw new RuntimeException("Error in returning");

        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    private boolean checkAndRequestPermissions() {

        int storagePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        int settingPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_SETTINGS);

        int smsPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_SETTINGS);

        int phoneStatePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (settingPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_SETTINGS);
        }

        if (smsPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (phoneStatePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 12);
            return false;
        }

        return true;
    }
}
