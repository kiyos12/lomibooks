package com.a360ground.lomibook.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.application.LomiApplication;
import com.a360ground.lomibook.client.CustomerClient;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.utils.LomiNetworkOperation;
import com.a360ground.lomibook.utils.PhotoHelper;
import com.a360ground.lomibook.utils.PreferenceHelper;
import com.a360ground.lomibook.view.ExtendedToast;
import com.a360ground.lomibook.view.LomiCoinView;
import com.a360ground.lomibook.view.LomiCoinsContainerView;
import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.koolearn.klibrary.core.util.Native;
import com.squareup.picasso.Picasso;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import mp.MpUtils;
import mp.PaymentResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsActivity extends AppCompatActivity implements PreferenceHelper.OnLocaleChangedListener {

    private static final int REQUEST_CODE_FORTUMO = 1221;

    private static final int REQUEST_CODE_COUNTRY = 7643;

    private ImageView profilePictureImageView;

    private TextView usernameTextView;

    private TextView emailTextView;

    private TextView myBooksCountTextView;

    private TextView countryTextView;

    private TextView coinAmountTextView;

    private TextView coinsSpent;

    private TextView usdSpent;

    private RelativeLayout coinsContainer;

    private RelativeLayout usdContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        final DisplayMetrics displayMetrics = new DisplayMetrics();

        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        setContentView(R.layout.activity_settings);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbarMyAccount));

        getSupportActionBar().setTitle(R.string.myaccount);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        PreferenceHelper.addListener(this);

        setTitle(getString(R.string.settings_activity_title));

        coinAmountTextView = (TextView) findViewById(R.id.coin_amount);

        profilePictureImageView = (ImageView) findViewById(R.id.drawer_avatar);

        usernameTextView = (TextView) findViewById(R.id.username);

        emailTextView = (TextView) findViewById(R.id.user_email);

        myBooksCountTextView = (TextView) findViewById(R.id.my_books_count);

        coinsSpent = (TextView) findViewById(R.id.coin_count);

        usdSpent = (TextView) findViewById(R.id.usd_count);

        coinsContainer = (RelativeLayout) findViewById(R.id.coins_container);

        usdContainer = (RelativeLayout) findViewById(R.id.usd_container);

        RelativeLayout languagePickerContainer = (RelativeLayout) findViewById(R.id.language_container);

        RelativeLayout publicationContainer = (RelativeLayout) findViewById(R.id.my_publications_container);

        publicationContainer.setVisibility(View.GONE);

        RelativeLayout countryContainer = (RelativeLayout) findViewById(R.id.country_container);

        TextView currentLanguageTextView = (TextView) findViewById(R.id.current_language);

        TextView deactivateTextView = (TextView) findViewById(R.id.deactivate);

        TextView deleteAccountTextView = (TextView) findViewById(R.id.delete_account);

        countryContainer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivityForResult(new Intent(SettingsActivity.this, ChooseCountryActivity.class), REQUEST_CODE_COUNTRY);
            }
        });

        countryTextView = (TextView) findViewById(R.id.country);

        languagePickerContainer.setClickable(true);

        languagePickerContainer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivityForResult(new Intent(SettingsActivity.this, LanguageActivity.class), Constants.LANGUAGE_REQUEST_CODE);
            }
        });

        publicationContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SettingsActivity.this, AuthorActivity.class));
            }
        });

        currentLanguageTextView.setText((new PreferenceHelper.LomiLanguage(this, PreferenceHelper.getLanguage(this)).getLanguage()));

        deactivateTextView.setClickable(true);

        deleteAccountTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(new Intent(SettingsActivity.this, DeactivationActivity.class));
            }
        });

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            final Customer customer = realm.where(Customer.class).findFirst();

            if (customer != null) {
                if (customer.isAuthor()) {
                    publicationContainer.setVisibility(View.VISIBLE);
                }

                //coinAmountTextView.setText(String.valueOf(customer.getLomiCoins()));

                CustomerClient customerClient = new WooCommerceServiceGenerator(SettingsActivity.this).createService(CustomerClient.class);

                Call<Customer> customerCall = customerClient.getCustomerById(new String(Native.getCustomerById(String.valueOf(customer.getId())), Charset.defaultCharset()) + "/");

                final Realm finalRealm = realm;
                customerCall.enqueue(new Callback<Customer>() {
                    @Override
                    public void onResponse(Call<Customer> call, final Response<Customer> response) {
                        finalRealm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                customer.setLomiCoins(response.body().getLomiCoins());
                            }
                        });
                        coinAmountTextView.setText(response.body().getLomiCoins()+" "+getResources().getString(R.string.birr));
                    }

                    @Override
                    public void onFailure(Call<Customer> call, Throwable t) {

                    }
                });

                updateUI(customer);

            } else {

                ExtendedToast.showToast(this, ExtendedToast.INFO, getString(R.string.login_required));

                Intent intent = new Intent(this, MainActivity.class);

                intent.putExtra(Constants.ERROR_MESSAGE_IDENTIFIER, "must_login");

                startActivity(intent);

                finish();
            }

        } finally {

            if (realm != null) {

                realm.close();
            }
        }
    }

    @Override
    public void localeChanged(String language) {

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            Customer customer = realm.where(Customer.class).findFirst();

            if (customer != null) {

                updateUI(customer);
            }

        } finally {

            if (realm != null) {

                realm.close();
            }
        }
    }

    private void updateUI(Customer customer) {

        if (customer == null) return;

        String pathToProfilePicture = PhotoHelper.getPersistedData(SettingsActivity.this);

        if (PreferenceHelper.getProfilePicFromFB(this) != null) {

            LomiApplication app = (LomiApplication) getApplication();

            Picasso.with(SettingsActivity.this).load(PreferenceHelper.getProfilePicFromFB(this)).into(profilePictureImageView);

        } else if (pathToProfilePicture != null) {

            Uri fileUri = Uri.parse("file://" + pathToProfilePicture);

            Picasso.with(SettingsActivity.this).load(fileUri).resize(150, 150).into(profilePictureImageView);

        } else {

            ColorGenerator colorGenerator = ColorGenerator.MATERIAL;

            String name = customer.getFirstName();

            TextDrawable drawable = TextDrawable.builder()

                    .beginConfig()

                    .width(100)

                    .height(100)

                    .endConfig()

                    .buildRoundRect((name.substring(0, 1) + name.substring(name.lastIndexOf(" ") + 1, name.lastIndexOf(" ") + 2)).toUpperCase(), colorGenerator.getColor(name), 10);

            profilePictureImageView.setImageDrawable(drawable);
        }

        usernameTextView.setText(customer.getFirstName());

        emailTextView.setText(customer.getEmail());

        long coinsSpentAmount = customer.getCoinsSpent();

        long usdSpentAmount = customer.getUsdSpent();

        if (coinsSpentAmount > 0) {

            coinsSpent.setText(String.format(getString(R.string.coin_text), coinsSpentAmount+""));

        } else {

            coinsContainer.setVisibility(View.GONE);
        }

        if (usdSpentAmount > 0) {

            usdSpent.setText(String.format(getString(R.string.usd_spent), usdSpentAmount+""));

        } else {

            usdContainer.setVisibility(View.GONE);
        }

        int myBooksCount = customer.getBooks().size();

        myBooksCountTextView.setText(String.format(getResources().getQuantityString(R.plurals.my_books_counter, myBooksCount), myBooksCount));

        String countryCode = PreferenceHelper.getCountryCode(SettingsActivity.this);

        if (countryCode != null) {

            String country = (new Locale("", countryCode)).getDisplayCountry();

            if (country.equals("EN")) {
                countryTextView.setText(R.string.abroad);
            } else {
                countryTextView.setText(R.string.ethiopia);
            }

        } else {

            countryTextView.setText(getString(R.string.choose_country));
        }

        setTitle(getString(R.string.settings_activity_title));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_COUNTRY) {

            if (resultCode == RESULT_OK) {


            }
        }
    }

    @Override
    protected void onResume() {

        super.onResume();

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            Customer customer = realm.where(Customer.class).findFirst();

            updateUI(customer);

        } finally {

            if (realm != null) {

                realm.close();
            }
        }
    }
}
