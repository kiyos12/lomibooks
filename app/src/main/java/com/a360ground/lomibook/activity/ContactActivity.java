package com.a360ground.lomibook.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.client.EmailClient;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.Email;
import com.a360ground.lomibook.domain.woocommerce.WooCommerceError;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactActivity extends AppCompatActivity {

    private EditText nameEditText;

    private EditText mobileNumberEditText;

    private EditText messageEditText;

    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_contact);

        realm = Realm.getDefaultInstance();

        final Customer customer = realm.where(Customer.class).findFirst();

        nameEditText = (EditText) findViewById(R.id.contact_name);

        mobileNumberEditText = (EditText) findViewById(R.id.contact_mobile_number);

        messageEditText = (EditText) findViewById(R.id.contact_message);

        setSupportActionBar((Toolbar) findViewById(R.id.toolBar));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(R.string.question);

        TextView sendButton = (TextView) findViewById(R.id.send_button);

        sendButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String name = nameEditText.getText().toString();

                String mobileNumber = mobileNumberEditText.getText().toString();

                String message = messageEditText.getText().toString();

                EmailClient emailClient = new WooCommerceServiceGenerator(ContactActivity.this).createService(EmailClient.class);

                Call<String> emailCall = emailClient.sendEmail("https://api.lomistore.com/mail/email.php", customer.getEmail(), name, message);

                emailCall.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        
                        if(response.isSuccessful()){
                            Toast.makeText(ContactActivity.this, "Successfully Sent", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(ContactActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {

                    }
                });

                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
