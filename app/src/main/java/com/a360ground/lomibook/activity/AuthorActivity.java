package com.a360ground.lomibook.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.adapter.SellRecyclerAdapter;
import com.a360ground.lomibook.client.CustomerClient;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.utils.LomiNetworkOperation;
import com.a360ground.lomibook.utils.PreferenceHelper;
import com.a360ground.lomibook.view.ExtendedToast;
import com.koolearn.klibrary.core.util.Native;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;

public class AuthorActivity extends AppCompatActivity {

    private SellRecyclerAdapter adapter;

    Customer customer;

    Realm realm = null;

    WebView authorReport;

    SwipeRefreshLayout authorRefresh;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_author);

        setSupportActionBar((Toolbar) findViewById(R.id.your_sell_toolbar));

        authorReport = (WebView) findViewById(R.id.author_report);

        authorRefresh = (SwipeRefreshLayout) findViewById(R.id.author_swipe_refresh);

        authorRefresh.setColorSchemeColors(ContextCompat.getColor(this, R.color.mainColor));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(getSupportActionBar() != null) {

            getSupportActionBar().setTitle(R.string.your_sell);
        }

        authorRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                authorReport.reload();
            }
        });

        try {

            realm = Realm.getDefaultInstance();

            customer = realm.where(Customer.class).findFirst();

            if (customer != null) {

                String id = new String(Base64.encode(String.valueOf(customer.getId()).getBytes(), 1));

                authorReport.loadUrl("https://api.lomistore.com/yytzvsht/?_zxftys="+id);

                authorReport.getSettings().setJavaScriptEnabled(true);

                authorReport.setWebViewClient(new MyWebiewClient());

            }

        } finally {

            if(realm != null) {

                realm.close();
            }
        }
        //todo send request to check author books remotely
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    class MyWebiewClient extends WebViewClient{

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            authorRefresh.setRefreshing(true);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if(authorRefresh.isRefreshing()){
                authorRefresh.setRefreshing(false);
            }
        }
    }
}