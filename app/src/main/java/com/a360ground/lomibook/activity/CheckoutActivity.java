package com.a360ground.lomibook.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.client.CustomerClient;
import com.a360ground.lomibook.domain.woocommerce.Author;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.fragment.CheckoutLoadingFragment;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.view.ExtendedToast;
import com.a360ground.lomibook.view.LomiCoinView;
import com.a360ground.lomibook.view.LomiCoinsContainerView;
import com.a360ground.lomibook.view.LomiToolTip;
import com.koolearn.klibrary.core.util.Native;
import com.squareup.picasso.Picasso;

import java.nio.charset.Charset;

import io.realm.Realm;
import mp.MpUtils;
import mp.PaymentResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutActivity extends AppCompatActivity {

    private int coins;

    private int balance;

    private TextView numOfCoinsTextView;

    private LomiCoinsContainerView balanceCoinWrapper;

    private LomiCoinsContainerView priceCoinWrapper;

    private Button completePurchase;

    private TextView balanceInsufficient;

    private ImageView selectedBookImage;

    private TextView selectedBookTitle;

    private TextView selectedBookCoins;


    private TextView selectedBookAuthor;

    private long productId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_checkout);

        final Intent intent = getIntent();

        productId = intent.getLongExtra(Constants.PRODUCT_ID_IDENTIFIER, -1);

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            if (productId != -1) {

                final Customer customer = login(realm);

                final Product product = realm.where(Product.class).equalTo("id", productId).findFirst();

                if (customer != null && product != null) {

                    coins = product.getPrice(CheckoutActivity.this);

                    numOfCoinsTextView = (TextView) findViewById(R.id.num_of_coins);

                    balanceCoinWrapper = (LomiCoinsContainerView) findViewById(R.id.balance_coin_wrapper);

                    priceCoinWrapper = (LomiCoinsContainerView) findViewById(R.id.price_coin_wrapper);

                    completePurchase = (Button) findViewById(R.id.complete_purchase);

                    balanceInsufficient = (TextView) findViewById(R.id.balance_insufficient);

                    selectedBookImage = (ImageView) findViewById(R.id.selected_book_img_checkout);

                    selectedBookTitle = (TextView) findViewById(R.id.selected_book_title_checkout);

                    selectedBookCoins = (TextView) findViewById(R.id.checkout_price_coins);

                    selectedBookAuthor = (TextView) findViewById(R.id.selected_book_author_checkout);

                    priceCoinWrapper.addCoins(coins, true, false, 0);

                    final CheckoutLoadingFragment loadingFragment = new CheckoutLoadingFragment(CheckoutActivity.this);

                    loadingFragment.showDialog();

                    CustomerClient customerClient = new WooCommerceServiceGenerator(CheckoutActivity.this).createService(CustomerClient.class);

                    Call<Customer> customerCall = customerClient.getCustomerById(new String(Native.getCustomerById(String.valueOf(customer.getId())), Charset.defaultCharset()) + "/");

                    customerCall.enqueue(new Callback<Customer>() {

                        @Override
                        public void onResponse(Call<Customer> call, Response<Customer> response) {

                            loadingFragment.dismissDialog();

                            if(response.isSuccessful()){

                                balanceCoinWrapper.addCoins(coins, false, false, response.body().getLomiCoins());

                                updateUI(response.body(), product);
                            }
                            else{
                                finish();

                                ExtendedToast.showToast(CheckoutActivity.this, ExtendedToast.INFO, getString(R.string.sorry_error));
                            }

                        }

                        @Override
                        public void onFailure(Call<Customer> call, Throwable t) {
                            loadingFragment.dismissDialog();
                            finish();
                            ExtendedToast.showToast(CheckoutActivity.this, ExtendedToast.INFO, getString(R.string.sorry_error));
                        }
                    });


                    setSupportActionBar((Toolbar) findViewById(R.id.toolbar_checkout));

                    ActionBar actionBar = getSupportActionBar();

                    if (actionBar != null) {

                        actionBar.setTitle(getString(R.string.checkout));

                        actionBar.setDisplayHomeAsUpEnabled(true);
                    }

                }
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    private void updateUI(final Customer customer, final Product product) {

        if (customer != null && product != null) {

            numOfCoinsTextView.setText(String.valueOf(customer.getLomiCoins()));

            selectedBookCoins.setText(String.format(getString(R.string.coin_text), coins));

            selectedBookTitle.setText(product.getName());

            Author author = product.getAuthor();

            if (author != null) {

                selectedBookAuthor.setText(author.getName());
            }

            Picasso.with(this)

                    .load(product.getThumbnailSrc())

                    .placeholder(R.drawable.placeholder)

                    .into(selectedBookImage);

            if (customer.getLomiCoins() >= product.getPrice(CheckoutActivity.this)) {

                completePurchase.setAlpha(1.0f);

                completePurchase.setEnabled(true);

                balanceInsufficient.setText(getString(R.string.sufficient_balance));

                balanceInsufficient.setTextColor(ContextCompat.getColor(CheckoutActivity.this, R.color.app_green));

            } else {

                balanceInsufficient.setText(String.format(getString(R.string.you_don_t_have_sufficient_balance_to_buy_this_book_please_buy_1_lomi_coin), coins - customer.getLomiCoins()));
            }


            completePurchase.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (customer.getLomiCoins() < product.getPrice(CheckoutActivity.this)) {
                        LomiToolTip.showToolTip(v, "Please add more coins", Gravity.TOP, R.color.white, R.color.red);
                        return;
                    }



                    ExtendedToast.showToast(CheckoutActivity.this, ExtendedToast.SUCCESS, String.format(getString(R.string.successfully_bought), coins));

                    setResult(Activity.RESULT_OK, new Intent());

                    finish();
                }
            });
        }
    }

    private Customer login(Realm realm) {

        Customer customer = realm.where(Customer.class).findFirst();

        if (customer == null) {

            ExtendedToast.showToast(CheckoutActivity.this, ExtendedToast.FAILED, getString(R.string.login_required));

            Intent mainActivityIntent = new Intent(CheckoutActivity.this, MainActivity.class);

            mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            startActivity(mainActivityIntent);
        }

        return customer;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LomiCoinView.REQUEST_CODE_FORTUMO) {

            if (data == null) {

                return;
            }

            if (resultCode == Activity.RESULT_OK) {

                PaymentResponse paymentResponse = new PaymentResponse(data);

                Realm realm = null;

                try {

                    realm = Realm.getDefaultInstance();

                    Customer customer = realm.where(Customer.class).findFirst();

                    Product product = realm.where(Product.class).equalTo("id", productId).findFirst();

                    switch (paymentResponse.getBillingStatus()) {

                        case MpUtils.MESSAGE_STATUS_FAILED:

                            balanceCoinWrapper.purchaseFailed();

                            break;

                        case MpUtils.MESSAGE_STATUS_BILLED:

                        case MpUtils.MESSAGE_STATUS_PENDING:

                            balanceCoinWrapper.purchased();

                            realm.executeTransaction(new Realm.Transaction() {

                                @Override
                                public void execute(Realm realm) {

                                    Customer customer = realm.where(Customer.class).findFirst();

                                    if (customer != null) {

                                        customer.setLomiCoins(customer.getLomiCoins() + 1);
                                    }
                                }
                            });

                            addCoinOnline(customer, CheckoutActivity.this,1);

                            break;
                    }

                    updateUI(customer, product);

                } finally {

                    if (realm != null) {

                        realm.close();
                    }
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return true;
    }

    public static void addCoinOnline(Customer customer, final Context context, int numCoins){

        final ProgressDialog progressDialog = new ProgressDialog(context);

        progressDialog.setMessage(context.getString(R.string.wait));

        progressDialog.show();

        WooCommerceServiceGenerator generator = new WooCommerceServiceGenerator(context);

        CustomerClient client = generator.createService(CustomerClient.class);

        Call<Customer> customerCall = client.putCustomerById(new String(Native.putCustomerById(String.valueOf(customer.getId())), Charset.defaultCharset()) + "/"+numCoins+"/");

        customerCall.enqueue(new Callback<Customer>() {
            @Override
            public void onResponse(Call<Customer> call, Response<Customer> response) {
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Customer> call, Throwable t) {

            }
        });
    }

}
