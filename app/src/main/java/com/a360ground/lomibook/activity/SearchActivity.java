package com.a360ground.lomibook.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.fragment.SearchFragment;

public class SearchActivity extends AppCompatActivity {

    SearchFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search);

        fragment = new SearchFragment();

        FragmentManager manager = getSupportFragmentManager();

        FragmentTransaction transaction = manager.beginTransaction();

        transaction.add(R.id.fragment_container, fragment);

        transaction.commit();
   }
}
