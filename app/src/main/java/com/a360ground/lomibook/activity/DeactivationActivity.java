package com.a360ground.lomibook.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.client.CustomerClient;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.utils.LomiFileUtils;
import com.a360ground.lomibook.utils.PhotoHelper;
import com.a360ground.lomibook.view.ExtendedToast;
import com.facebook.login.LoginManager;
import com.koolearn.klibrary.core.util.Native;

import java.io.File;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeactivationActivity extends AppCompatActivity {

    TextView deactivationDesc;
    private Realm realm;
    private Customer customer;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.deactivate1);

        realm = Realm.getDefaultInstance();

        customer = realm.where(Customer.class).findFirst();

        if (customer == null) {

            Toast.makeText(DeactivationActivity.this, "You are not logged in", Toast.LENGTH_SHORT).show();

            finish();
        }

        findViewById(R.id.deactivation1_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.deactivation1_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(R.layout.deactivate2);

                findViewById(R.id.deactivation2_button).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setContentView(R.layout.deactivate3);
                        deactivationDesc = (TextView) findViewById(R.id.deactivation3_desc);
                        findViewById(R.id.deactivation3_button).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                deactivate();
                            }
                        });
                    }
                });
            }
        });

    }

    public void cancelAndGoBack(View cancel) {
        finish();
    }

    public void deactivate() {

        final ProgressDialog progressDialog = new ProgressDialog(this);

        PhotoHelper.setProfilePicture(DeactivationActivity.this, null);

        progressDialog.setTitle(R.string.deactivate);

        progressDialog.setCancelable(false);

        progressDialog.setMessage(getString(R.string.deleting_user));

        progressDialog.show();

        WooCommerceServiceGenerator generator = new WooCommerceServiceGenerator(DeactivationActivity.this);

        CustomerClient client = generator.createService(CustomerClient.class);

        Call<Customer> call = client.deleteCustomer(new String(Native.getCustomerById(String.valueOf(customer.getId()))) + "/");

        call.enqueue(new Callback<Customer>() {

            @Override
            public void onResponse(Call<Customer> call, Response<Customer> response) {

                try {

                    if (response.isSuccessful()) {

                        Realm realm = Realm.getDefaultInstance();

                        realm.executeTransaction(new Realm.Transaction() {

                            @Override
                            public void execute(Realm realm) {
                                realm.where(Customer.class).findFirst().getBooks().deleteAllFromRealm();
                                realm.delete(Customer.class);
                            }
                        });

                        progressDialog.dismiss();

                        LoginManager.getInstance().logOut();

                        PreferenceManager.getDefaultSharedPreferences(getBaseContext()).
                                edit().clear().apply();

                        File fileNames = new File(LomiFileUtils.LOMI_FOLDER_PATH);

                        for(String fileName : fileNames.list()){
                            new File(fileNames, fileName).delete();
                        }

                        startActivity(new Intent(DeactivationActivity.this, ChooseCountryActivity.class));

                    } else {

                        progressDialog.dismiss();

                        ExtendedToast.showToast(DeactivationActivity.this, ExtendedToast.FAILED, getString(R.string.not_delete_user));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Customer> call, Throwable t) {

                progressDialog.dismiss();

                ExtendedToast.showToast(DeactivationActivity.this, ExtendedToast.FAILED, getString(R.string.not_delete_user));
            }
        });
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

    }
}
