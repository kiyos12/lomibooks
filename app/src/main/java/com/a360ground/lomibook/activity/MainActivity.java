package com.a360ground.lomibook.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.application.LomiApplication;
import com.a360ground.lomibook.client.Update;
import com.a360ground.lomibook.domain.algolia.SearchSuggestions;
import com.a360ground.lomibook.domain.algolia.TopSearches;
import com.a360ground.lomibook.domain.woocommerce.Apk;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.fragment.HomeFragment;
import com.a360ground.lomibook.fragment.MyBooksFragment;
import com.a360ground.lomibook.fragment.SearchFragment;
import com.a360ground.lomibook.fragment.SelectedFragment;
import com.a360ground.lomibook.fragment.TopSellerFragment;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.utils.PhotoHelper;
import com.a360ground.lomibook.utils.PreferenceHelper;
import com.a360ground.lomibook.utils.SearchHelper;
import com.a360ground.lomibook.view.CustomAlertDialog;
import com.a360ground.lomibook.view.ExtendedToast;
import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.koolearn.klibrary.core.util.Native;
import com.koolearn.klibrary.core.util.PreferenceUtill;
import com.koolearn.klibrary.ui.android.view.AHBottomNavigation;
import com.koolearn.klibrary.ui.android.view.AHBottomNavigationItem;
import com.scottyab.rootbeer.RootBeer;
import com.scottyab.rootbeer.RootBeerNative;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements PreferenceHelper.OnLocaleChangedListener {

    private static String TAG = MainActivity.class.getSimpleName();
    Fragment homeFragment = new HomeFragment();
    Fragment selectedFragment = new SelectedFragment();
    Fragment topSellersFragment = new TopSellerFragment();
    Fragment myBooksFragment = new MyBooksFragment();
    LomiApplication application;
    Intent starterIntent;
    boolean exit;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private AHBottomNavigation bottomNavigation;
    private RelativeLayout headerLayout;
    private TextView drawerUsernameTextView;
    private TextView drawerPhoneNumberTextView;
    private ImageView drawerProfileImageView;
    private MenuItem myBooksMenuItem;
    private MenuItem accountMenuItem;
    private MenuItem helpAndFeedbackMenuItem;
    private MenuItem inviteFriendMenuItem;
    private TextView myBooksCountTextView;
    private TextView updateRecommendation;
    private Customer customer;
    private CustomAlertDialog dialog;
    private FloatingSearchView searchView;
    private Toolbar mainToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ConnectivityManager mgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo bgData = mgr.getActiveNetworkInfo();

        RootBeer r = new RootBeer(this);

        Log.d(TAG, "onCreate: is Rooted " + r.isRootedWithoutBusyBoxCheck());

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            customer = realm.where(Customer.class).findFirst();

            drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

            application = (LomiApplication) getApplicationContext();

            mainToolbar = (Toolbar) findViewById(R.id.mainToolbar);

            setSupportActionBar(mainToolbar);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ham);

            getSupportActionBar().setTitle(R.string.my_books);

            PreferenceHelper.onCreate(this);

            starterIntent = getIntent();

            searchView = (FloatingSearchView) findViewById(R.id.home_activity_toolbar);

            searchView.attachNavigationDrawerToMenuButton(drawerLayout);

            searchView.setSearchHint("Search Lomi");

            searchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {

                @Override
                public void onSuggestionClicked(SearchSuggestion searchSuggestion) {

                    Intent intent = new Intent(MainActivity.this, BookDetailsActivity.class);

                    intent.putExtra(Constants.PRODUCT_ID_IDENTIFIER, searchSuggestion.getBody());
                }

                @Override
                public void onSearchAction(String s) {

                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                    transaction.setCustomAnimations(R.anim.fadein, R.anim.fadeout);

                    transaction.replace(R.id.home_fragment_container, SearchFragment.newInstance(s));

                    transaction.commit();

                }
            });

            searchView.setOnFocusChangeListener(new FloatingSearchView.OnFocusChangeListener() {

                @Override
                public void onFocus() {

                    SearchHelper.getTopSearchesCall(MainActivity.this, new SearchHelper.ITopSearches() {
                        @Override
                        public void onResponse(List<TopSearches> topSearches) {
                            searchView.swapSuggestions(topSearches);
                        }
                    });
                }

                @Override
                public void onFocusCleared() {

                }
            });

            searchView.setOnHomeActionClickListener(new FloatingSearchView.OnHomeActionClickListener() {

                @Override
                public void onHomeClicked() {
                    if (!homeFragment.isVisible()) {
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.home_fragment_container, homeFragment);
                        transaction.commit();
                    }
                }
            });

            searchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {

                @Override
                public void onSearchTextChanged(String s, String s1) {

                    if (s1.length() >= 2) {
                        searchView.showProgress();
                        SearchHelper.getSearchSuggestions(MainActivity.this, s1, new SearchHelper.ISearchSuggestions() {
                            @Override
                            public void onResponse(List<SearchSuggestions> searchSuggestions) {
                                searchView.hideProgress();
                                searchView.swapSuggestions(searchSuggestions);
                            }
                        });
                    }
                }
            });

            searchView.setOnClearSearchActionListener(new FloatingSearchView.OnClearSearchActionListener() {
                @Override
                public void onClearSearchClicked() {
                    searchView.clearSuggestions();
                }
            });

            navigationView = (NavigationView) findViewById(R.id.navigation_view);

            updateRecommendation = (TextView) findViewById(R.id.update_recommendation);

            updateRecommendation.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    final String appPackageName = MainActivity.this.getPackageName();
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }
            });

            headerLayout = (RelativeLayout) navigationView.getHeaderView(0);

            drawerUsernameTextView = (TextView) headerLayout.findViewById(R.id.drawer_username);

            drawerPhoneNumberTextView = (TextView) headerLayout.findViewById(R.id.drawer_phone_number);

            drawerProfileImageView = (ImageView) headerLayout.findViewById(R.id.drawer_avatar);

            final Menu menu = navigationView.getMenu();

            setupMenu(menu);

            bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);

            setupBottomNavigation();

            initializeFragment();

            PreferenceHelper.addListener(this);

            updateUI(realm);

            if (PreferenceHelper.getVersion(MainActivity.this, Constants.VERSION) != null) {

                updateRecommendation.setVisibility(View.VISIBLE);
            }


            WooCommerceServiceGenerator wooCommerceServiceGenerator = new WooCommerceServiceGenerator(this);

            Update update = wooCommerceServiceGenerator.createService(Update.class);

            Call<List<Apk>> apListCall = update.getApk(Native.getApk());

            apListCall.enqueue(new Callback<List<Apk>>() {

                @Override
                public void onResponse(Call<List<Apk>> call, Response<List<Apk>> response) {

                    if (response != null) {
                        if (response.body().size() > 0) {

                            final Apk apk = response.body().get(0);

                            if (PreferenceHelper.getVersion(MainActivity.this, Constants.VERSION) != null) {
                                if (apk.isLatest() &&
                                        !PreferenceHelper.getVersion(MainActivity.this, Constants.VERSION).equals(apk.getMeta_box().getLomi_vcode())) {

                                    Log.d(TAG, "onResponse: line 332");
                                    dialog = CustomAlertDialog.updateInstance(apk);

                                    dialog.show(getSupportFragmentManager(), MainActivity.class.getSimpleName());
                                }
                            } else {
                                if (apk.isLatest()) {

                                    Log.d(TAG, "onResponse: line 340");

                                    dialog = CustomAlertDialog.updateInstance(apk);

                                    dialog.show(getSupportFragmentManager(), MainActivity.class.getSimpleName());
                                }
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<Apk>> call, Throwable t) {

                }
            });

        } finally {
            if (realm != null) {
                realm.close();
            }
        }

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {

    }

    public void setActiveBottomNavigationItem(int position) {
        if (bottomNavigation != null)
            bottomNavigation.setCurrentItem(position, false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {

            drawerLayout.openDrawer(GravityCompat.START);

            return true;
        }

        if (id == R.id.launch_search_activity_button) {

            if (application.isNetworkAvailable()) {

                startActivity(new Intent(MainActivity.this, SearchActivity.class));

            } else {

                ExtendedToast.showToast(MainActivity.this, ExtendedToast.INFO, getString(R.string.connect_to_internet));
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {


        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {

            drawerLayout.closeDrawers();

        } else {

            if (exit) {
                finishAffinity(); // finish activity
            } else {
                ExtendedToast.showToast(this, ExtendedToast.INFO, "Press Back again to Exit.");
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 3 * 1000);

            }
        }
    }

    @Override
    protected void onResume() {

        super.onResume();
        updateUI();

    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    private void updateUI() {
        Picasso.with(this).load(PreferenceUtill.readFromPreferences(this, "ProfilePic", null)).resize(150, 150).into(drawerProfileImageView);

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            updateUI(realm);

        } finally {

            if (realm != null) {

                realm.close();
            }
        }
    }

    private void updateUI(Realm realm) {

        customer = realm.where(Customer.class).findFirst();

        myBooksMenuItem.setTitle(getString(R.string.my_books));

        accountMenuItem.setTitle(getString(R.string.account));

        helpAndFeedbackMenuItem.setTitle(getString(R.string.help));

        inviteFriendMenuItem.setTitle(getString(R.string.invite_friend));

        bottomNavigation.getItem(0).setTitle(getString(R.string.home));

        bottomNavigation.getItem(1).setTitle(getString(R.string.selected));

        bottomNavigation.getItem(2).setTitle(getString(R.string.top_seller));

        bottomNavigation.getItem(3).setTitle(getString(R.string.my_books));

        bottomNavigation.refresh();

        setTitle(getString(R.string.app_name));

        if (customer == null) {

            drawerUsernameTextView.setVisibility(View.GONE);

            drawerPhoneNumberTextView.setVisibility(View.GONE);

            drawerProfileImageView.setVisibility(View.GONE);

            accountMenuItem.setVisible(false);

            myBooksMenuItem.setVisible(false);

        } else {

            drawerUsernameTextView.setVisibility(View.VISIBLE);

            drawerPhoneNumberTextView.setVisibility(View.VISIBLE);

            drawerProfileImageView.setVisibility(View.VISIBLE);

            String pathToProfilePicture = PhotoHelper.getPersistedData(MainActivity.this);

            if (PreferenceHelper.getProfilePicFromFB(this) != null) {

                Picasso.with(MainActivity.this).load(PreferenceHelper.getProfilePicFromFB(MainActivity.this)).into(drawerProfileImageView);
            } else if (pathToProfilePicture != null) {

                Uri fileUri = Uri.parse("file://" + pathToProfilePicture);

                Picasso.with(MainActivity.this).load(fileUri).resize(150, 150).into(drawerProfileImageView);

            } else {

                ColorGenerator colorGenerator = ColorGenerator.MATERIAL;

                String name = customer.getFirstName();

                TextDrawable drawable = TextDrawable.builder()

                        .beginConfig()

                        .width(100)

                        .height(100)

                        .endConfig()

                        .buildRoundRect((name.substring(0, 1) + name.substring(name.lastIndexOf(" ") + 1, name.lastIndexOf(" ") + 2)).toUpperCase(), colorGenerator.getColor(name), 10);

                drawerProfileImageView.setImageDrawable(drawable);
            }

            accountMenuItem.setVisible(true);

            myBooksMenuItem.setVisible(true);

            drawerUsernameTextView.setText(customer.getFirstName());

            drawerPhoneNumberTextView.setText(customer.getEmail());

            int myBooksCount = customer.getBooks().size();

            myBooksCountTextView.setText(String.format(getResources().getQuantityString(R.plurals.my_books_counter, myBooksCount), myBooksCount));

        }

        ActionBar actionBar = getSupportActionBar();

        String actionBarTitle = getString(R.string.home);

        if (actionBar != null) {

            if (selectedFragment != null && selectedFragment.isVisible()) {

                actionBarTitle = getString(R.string.selected);

            } else if (topSellersFragment != null && topSellersFragment.isVisible()) {

                actionBarTitle = getString(R.string.top_seller);

            } else if (myBooksFragment != null && myBooksFragment.isVisible()) {

                actionBarTitle = getString(R.string.my_books);
            }

            actionBar.setTitle(actionBarTitle);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);


        switch (requestCode) {

            case Constants.GALLERY_REQUEST:

                if (resultCode == RESULT_OK) {

                    Uri selectedImage = data.getData();

                    drawerProfileImageView.setImageURI(selectedImage);

                    PhotoHelper.setProfilePicture(this, getRealPathFromUri(selectedImage));
                }

                break;

            case Constants.CAMERA_REQUEST:

                if (resultCode == RESULT_OK) {

                    Bundle extras = data.getExtras();

                    Bitmap imageBitmap = (Bitmap) extras.get("data");

                    Uri tempUri = getImageUri(getApplicationContext(), imageBitmap);

                    PhotoHelper.setProfilePicture(this, getRealPathFromUri(tempUri));
                }

                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private Uri getImageUri(Context applicationContext, Bitmap imageBitmap) {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();

        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        String path = MediaStore.Images.Media.insertImage(applicationContext.getContentResolver(), imageBitmap, "Title", null);

        return Uri.parse(path);
    }

    private String getRealPathFromUri(Uri uri) {

        Cursor cursor = getContentResolver().query(uri, null, null, null, null);

        cursor.moveToFirst();

        int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);

        return cursor.getString(index);
    }

    @Override
    public void localeChanged(String language) {

        Realm realm = null;

        try {

            realm = Realm.getDefaultInstance();

            updateUI(realm);

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public void setupMenu(Menu menu) {

        myBooksMenuItem = menu.findItem(R.id.nav_mybooks);

        helpAndFeedbackMenuItem = menu.findItem(R.id.nav_help);

        inviteFriendMenuItem = menu.findItem(R.id.nav_invite);

        myBooksCountTextView = (TextView) myBooksMenuItem.getActionView();

        myBooksMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                searchView.setVisibility(View.GONE);

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                transaction.replace(R.id.home_fragment_container, myBooksFragment);

                transaction.commitAllowingStateLoss();

                drawerLayout.closeDrawer(Gravity.LEFT);

                drawerLayout.closeDrawers();

                return true;
            }
        });

        accountMenuItem = menu.findItem(R.id.nav_account);

        accountMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                startActivity(new Intent(MainActivity.this, SettingsActivity.class));

                drawerLayout.closeDrawers();

                return true;
            }
        });

        MenuItem helpMenuItem = menu.findItem(R.id.nav_help);

        helpMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                startActivity(new Intent(MainActivity.this, HelpAndFeedbackActivity.class));

                drawerLayout.closeDrawers();

                return true;
            }
        });

        final MenuItem inviteFriendMenuItem = menu.findItem(R.id.nav_invite);

        inviteFriendMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                Intent sendIntent = new Intent();

                sendIntent.setAction(Intent.ACTION_SEND);

                sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.invitation_message));

                sendIntent.setType("text/plain");

                startActivity(sendIntent);

                drawerLayout.closeDrawers();

                return true;
            }
        });
    }

    public void setupBottomNavigation() {

        bottomNavigation.addItem(new AHBottomNavigationItem(getString(R.string.home), R.drawable.home_active));

        bottomNavigation.addItem(new AHBottomNavigationItem(getString(R.string.selected), R.drawable.favorite));

        bottomNavigation.addItem(new AHBottomNavigationItem(getString(R.string.top_seller), R.drawable.top_seller_active));

        bottomNavigation.addItem(new AHBottomNavigationItem(getString(R.string.my_books), R.drawable.book_active));

        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);

        bottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.bottomNavigation));

        bottomNavigation.setBackgroundColor(getResources().getColor(R.color.bottomNavigation));

        bottomNavigation.setBehaviorTranslationEnabled(false);

        bottomNavigation.setInactiveColor(Color.parseColor("#A4C7F0"));

        bottomNavigation.setAccentColor(Color.parseColor("white"));

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {

            @Override
            public boolean onTabSelected(final int position, boolean wasSelected) {

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                transaction.setCustomAnimations(R.anim.fadein, R.anim.fadeout);

                myBooksMenuItem.setChecked(false);

                accountMenuItem.setChecked(false);

                helpAndFeedbackMenuItem.setChecked(false);

                inviteFriendMenuItem.setChecked(false);

                switch (position) {

                    case 0: {

                        searchView.setVisibility(View.VISIBLE);

                        searchView.clearQuery();

                        transaction.replace(R.id.home_fragment_container, homeFragment);

                        break;

                    }
                    case 1:

                        searchView.setVisibility(View.VISIBLE);

                        searchView.clearQuery();

                        transaction.replace(R.id.home_fragment_container, selectedFragment);

                        break;

                    case 2:

                        searchView.setVisibility(View.VISIBLE);

                        searchView.clearQuery();

                        transaction.replace(R.id.home_fragment_container, topSellersFragment);

                        break;

                    case 3:

                        myBooksMenuItem.setChecked(true);

                        searchView.setVisibility(View.GONE);

                        // if user is not logged in, direct to the login screen
                        if (customer != null) {

                            transaction.replace(R.id.home_fragment_container, myBooksFragment);

                        } else {
                            ExtendedToast.showToast(MainActivity.this, ExtendedToast.INFO, getString(R.string.login_required));

                            openDrawer();
                        }
                        break;
                }

                transaction.commit();

                return true;
            }
        });
    }

    public void initializeFragment() {

        if (findViewById(R.id.home_fragment_container) != null) {

            HomeFragment homeFragment = new HomeFragment();

            homeFragment.setArguments(getIntent().getExtras());

            getSupportFragmentManager()

                    .beginTransaction()

                    .add(R.id.home_fragment_container, homeFragment)

                    .commit();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    public void openDrawer() {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public void closeDrawer() {
        if (drawerLayout.isShown()) {
            drawerLayout.closeDrawers();
        }
    }

}