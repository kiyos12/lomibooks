package com.a360ground.lomibook.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.adapter.TermsAndConditionsAdapter;
import java.util.ArrayList;

public class TermsAndConditions extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_terms_and_conditions);

        setSupportActionBar((Toolbar) findViewById(R.id.terms_and_conditions_activity_toolbar));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setTitle(R.string.terms_and_conditions);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        ArrayList<String> indices = new ArrayList<>();

        ArrayList<String> articles = new ArrayList<>();

        indices.add("Article 1");

        articles.add("ግጭቱ በተቀሰቀሰበት በዚያው ዕለት ከሰዓት በኋላ የፀጥታ ኃይሎች በአካባቢው በመሰማራት በግጭቱ ላይ ተሳትፈዋል በሚል የተጠረጠሩ ከ2000 በላይ ነዋሪዎችን በቁጥጥር ስር ማዋሉን ፖሊስ ገለጸ።");

        indices.add("Article 2");

        articles.add("ግጭቱ በተቀሰቀሰበት በዚያው ዕለት ከሰዓት በኋላ የፀጥታ ኃይሎች በአካባቢው በመሰማራት በግጭቱ ላይ ተሳትፈዋል በሚል የተጠረጠሩ ከ2000 በላይ ነዋሪዎችን በቁጥጥር ስር ማዋሉን ፖሊስ ገለጸ።");

        TermsAndConditionsAdapter adapter = new TermsAndConditionsAdapter(TermsAndConditions.this, indices, articles);

        recyclerView.setAdapter(adapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(TermsAndConditions.this, LinearLayoutManager.VERTICAL, false));
    }
    @Override
    public void onBackPressed() {
        finish ();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId () == android.R.id.home){
            finish ();
        }

        return true;
    }
}
