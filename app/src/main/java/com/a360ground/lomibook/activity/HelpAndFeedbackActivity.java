package com.a360ground.lomibook.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.adapter.ExpandableListAdapter;

import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HelpAndFeedbackActivity extends AppCompatActivity {

    private int lastExpandedPosition;

    private ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_help_and_feedback);

        setTitle(getString(R.string.help_and_feedback_title));

        setSupportActionBar ((Toolbar) findViewById (R.id.help_feedback_activity_toolbar));

        getSupportActionBar ().setDisplayHomeAsUpEnabled (true);

        TextView sayHelloTextView = (TextView) findViewById(R.id.say_hello_button);

        final ExpandableListView expandableListView = (ExpandableListView) findViewById(R.id.list);

        List<String> helpItems = new ArrayList<>();

        helpItems.add("How do I change my language settings?");

        helpItems.add("Can I share book with friends?");

        helpItems.add("How do I pay for books?");

        helpItems.add("How do I signup?");

        helpItems.add("How do I reset my password?");

        HashMap<String, String> answersMap = new HashMap<>();

        List<String> answers = new ArrayList<>();

        answers.add("Coming Soon");

        answers.add("Coming Soon");

        answers.add("Coming Soon");

        answers.add("Coming Soon");

        answers.add("Coming Soon");

        for(int i = 0; i < helpItems.size(); i++) {

            if(i < answers.size()) {

                answersMap.put(helpItems.get(i), answers.get(i));

            } else {

                answersMap.put(helpItems.get(i), answers.get(answers.size() - 1));
            }
        }

        final ExpandableListAdapter listAdapter = new ExpandableListAdapter(this, helpItems, answersMap);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {

                if(lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {

                    expandableListView.collapseGroup(lastExpandedPosition);
                }

                lastExpandedPosition = groupPosition;
            }
        });

        /*ArrayAdapter adapter = new ArrayAdapter<>(this, R.layout.help_and_feedback_list_item, R.id.title, helpItems);*/
        expandableListView.setAdapter(listAdapter);

        sayHelloTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(new Intent(HelpAndFeedbackActivity.this, ContactActivity.class));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId () == android.R.id.home){
            finish ();
        }

        return true;
    }
}
