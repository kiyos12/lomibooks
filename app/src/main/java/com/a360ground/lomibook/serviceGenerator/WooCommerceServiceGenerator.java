package com.a360ground.lomibook.serviceGenerator;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Toast;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.adapter.ImageDeserilizer;
import com.a360ground.lomibook.adapter.ProductTypeAdapter;
import com.a360ground.lomibook.client.CustomerClient;
import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.CustomerError;
import com.a360ground.lomibook.domain.woocommerce.Image;
import com.a360ground.lomibook.domain.woocommerce.RealmString;
import com.a360ground.lomibook.domain.woocommerce.WooCommerceError;
import com.a360ground.lomibook.gson.RealmObjectExclusionStrategy;
import com.a360ground.lomibook.gson.RealmStringTypeAdapter;
import com.a360ground.lomibook.utils.CheckDevice;
import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.utils.ErrorUtils;
import com.a360ground.lomibook.view.ExtendedToast;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.koolearn.klibrary.core.util.Native;

import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmList;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WooCommerceServiceGenerator {

    private static Gson gson = new GsonBuilder()

            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")

            .setExclusionStrategies(new RealmObjectExclusionStrategy())

            .registerTypeAdapter(new TypeToken<RealmList<RealmString>>() {
            }.getType(), new RealmStringTypeAdapter())

            .registerTypeAdapter(Image.class, new ImageDeserilizer())

            .registerTypeAdapterFactory(new ProductTypeAdapter())

            .create();

    private OkHttpClient.Builder httpClient;

    private Retrofit retrofit;

    private Context context;

    public WooCommerceServiceGenerator(Context context) {

        httpClient = new OkHttpClient.Builder();

        this.context = context;

        httpClient.addInterceptor(new Interceptor() {

            @Override
            public Response intercept(Chain chain) throws IOException {

                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder()

                        .header("Accept", "application/json")

                        .header("header1", Native.getHeaders()[0].toString())

                        .header("header2", Native.getHeaders()[1].toString())

                        .header("header3", Native.getHeaders()[2].toString())

                        .method(original.method(), original.body());

                Request request = requestBuilder.build();

                return chain.proceed(request);
            }
        });

      /*  HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();

        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        httpClient.addInterceptor(loggingInterceptor);*/

        OkHttpClient client = httpClient.build();

        String remoteUrl = Constants.DEFAULT_REMOTE_URI;

        Retrofit.Builder builder =

                new Retrofit.Builder()

                        .baseUrl(remoteUrl)

                        .addConverterFactory(GsonConverterFactory.create(gson));

        retrofit = builder.client(client).build();
    }

    public void createCustomer(final Context context, final String email, String fullName, final OnSignupCompleted signupCompleted) {

        final Customer customer = new Customer();

        final String deviceID = CheckDevice.getDeviceId(context);

        customer.setEmail(email);

        customer.setPassword("test");

        customer.setFirstName(fullName);

        customer.setUsername(email);

        customer.setLomiCoins(0);

        customer.setDeviceId(deviceID);

        WooCommerceServiceGenerator generator = new WooCommerceServiceGenerator(context);

        final CustomerClient client = generator.createService(CustomerClient.class);

        Call<Customer> call = client.createCustomer(Native.getCustomer(), customer);

        final ProgressDialog progressDialog = new ProgressDialog(context);

        progressDialog.setMessage(context.getString(R.string.creating_customer));

        progressDialog.show();

        progressDialog.setCancelable(false);

        call.enqueue(new Callback<Customer>() {

            Error error;

            @Override
            public void onResponse(Call<Customer> call, retrofit2.Response<Customer> response) {

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (response.isSuccessful()) {

                    final Customer customer = response.body();

                    // A successful response may actually be an error object
                    // to learn more: check the WooCommerce theme customization
                    // file: wp-content/themes/shopping/lib/custom_functions.php
                    if (customer.getCode() != null) {
                        // Phone number is duplicate and customer is not created
                        // Now customer is like an error object
                        Toast.makeText(context, customer.getMessage(), Toast.LENGTH_SHORT).show();

                        return;
                    }

                    saveCustomer(customer, signupCompleted);

                    ExtendedToast.showToast(context, ExtendedToast.SUCCESS, context.getString(R.string.registered_successfully));

                } else {
                    final CustomerError customerError = ErrorUtils.parseError(response, getRetrofit());
                    if(customerError.getCode() == 34){
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setCustomTitle(LayoutInflater.from(context).inflate(R.layout.error_view, null));
                        builder.setCancelable(false);
                        builder.setMessage("እየተጠቀሙበት በሚገኙት አካውንት እየተጠቀመ ያለ ሌላ ስልክ አለ ወደ እዚህ ስልክ ማዞር ከፈለጉ እዛኛው ስልክ ላይ ያለው መጽሐፍ በሙሉ ጠፍቶ ወደዚህ ይጫናል።ይስማማሉ？");
                        builder.setIcon(R.drawable.ic_error_black_24dp);
                        builder.setPositiveButton("አዎ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                final ProgressDialog progressDialog1 = new ProgressDialog(context);
                                progressDialog1.setMessage(context.getString(R.string.wait));
                                progressDialog1.show();
                                Call<Customer> customerCall = client.putCustomerID(new String(Native.putCustomerID(String.valueOf(customerError.getId())))+"/"+deviceID+"/");
                                customerCall.enqueue(new Callback<Customer>() {
                                    @Override
                                    public void onResponse(Call<Customer> call, retrofit2.Response<Customer> response) {
                                        progressDialog1.dismiss();
                                        saveCustomer(response.body(), signupCompleted);
                                    }

                                    @Override
                                    public void onFailure(Call<Customer> call, Throwable throwable) {

                                    }
                                });
                            }
                        });
                        builder.setNegativeButton("አይ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                        AlertDialog a = builder.create();
                        a.show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Customer> call, Throwable t) {

                progressDialog.dismiss();

                call.cancel();

                error = new WooCommerceError(WooCommerceError.ErrorCode.CONNECTION_ERROR);
            }
        });
    }

    public <S> S createService(Class<S> serviceClass) {

        return retrofit.create(serviceClass);
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public interface OnSignupCompleted {
        void isSignupFinished(Customer customer);
    }

    private void saveCustomer(final Customer customer,final OnSignupCompleted signupCompleted){

        Realm realm  = Realm.getDefaultInstance();

            realm.executeTransaction(new Realm.Transaction() {

                @Override
                public void execute(Realm realm) {

                    realm.delete(Customer.class);

                    realm.insertOrUpdate(customer);

                    signupCompleted.isSignupFinished(realm.where(Customer.class).findFirst());
                }
            });
    }
}
