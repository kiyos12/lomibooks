package com.a360ground.lomibook.client;

import com.a360ground.lomibook.domain.woocommerce.Category;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface CategoryClient {

    @GET
    Call<List<Category>> categories(@Url String categoryUrl);
}
