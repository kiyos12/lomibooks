package com.a360ground.lomibook.client;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface CouponClient {
    @GET
    Call<Long> getCouponByCode(@Url String url);
}
