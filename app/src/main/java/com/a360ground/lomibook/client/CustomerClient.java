package com.a360ground.lomibook.client;

import com.a360ground.lomibook.domain.woocommerce.Customer;
import com.a360ground.lomibook.domain.woocommerce.OrderDownload;
import com.a360ground.lomibook.domain.woocommerce.Product;
import com.a360ground.lomibook.utils.Constants;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface CustomerClient {

    String urlPrefix = Constants.WOOCOMMERCE_REST_API_PREFIX + "customers";

    @POST
    Call<Customer> createCustomer(@Url String url, @Body Customer body);

    @GET
    Call<List<Product>> getDownloads(@Url String url);

    @POST
    Call<Customer> payLomiCoins(@Url String url, @Body HashMap<String, Integer> body);

    @GET
    Call<Customer> getCustomerById(@Url String url);

    @GET
    Call<Customer> getCustomerByEmail(@Url String url);

    @PUT
    Call<Customer> putCustomerById(@Url String url);

    @PUT
    Call<Customer> putCustomerID(@Url String url);

    @GET
    Call<String> checkDevice(@Url String url);

    @GET
    Call<String> getBookUrl(@Url String url);

    @DELETE
    Call<Customer> deleteCustomer(@Url String url);

    @GET
    Call<List<Product>> getAuthorBooks(@Url String url, @Query("start_date") String fromDate, @Query("to_date") String toDate);
}
