package com.a360ground.lomibook.client;

import com.a360ground.lomibook.domain.woocommerce.Order;
import com.a360ground.lomibook.domain.woocommerce.OrderDownload;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface OrderClient {

    @POST
    Call<OrderDownload> createOrder(@Url String url, @Body Order order);
}
