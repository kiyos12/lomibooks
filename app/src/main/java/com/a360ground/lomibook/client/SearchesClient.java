package com.a360ground.lomibook.client;

import com.a360ground.lomibook.domain.algolia.SearchSuggestions;
import com.a360ground.lomibook.domain.algolia.TopSearches;
import com.a360ground.lomibook.domain.woocommerce.Product;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface SearchesClient {

    @GET
    Call<List<TopSearches>> topSearches(@Url String url);

    @GET
    Call<List<Product>> search(@Url String url);

    @GET
    Call<List<SearchSuggestions>> searchSuggestions(@Url String url);
}
