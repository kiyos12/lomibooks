package com.a360ground.lomibook.client;

import com.a360ground.lomibook.domain.woocommerce.Apk;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by Kiyos on 6/12/2017.
 */

public interface Update {

    @GET
    Call<List<Apk>> getApk(@Url String url);

}
