package com.a360ground.lomibook.client;

import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.domain.woocommerce.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PostClient {

    String urlPrefix = Constants.LOMI_REST_API_PREFIX + "ads";

    @GET(urlPrefix)
    Call<List<Post>> getAds();
}
