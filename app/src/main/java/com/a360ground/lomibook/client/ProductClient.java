package com.a360ground.lomibook.client;

import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.domain.woocommerce.Product;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ProductClient {

    String urlPrefix = Constants.WOOCOMMERCE_REST_API_PREFIX + "products";

    @GET
    Call<List<Product>> products(@Url String url, @Query(value = "page", encoded = true) int page);

    @GET
    Call<List<Product>> productsByCategory(@Url String url, @Query("category") long id);

    @GET
    Call<List<Product>> productsByCategoryWithPagination(@Url String url,
                                                         @Query(value = "category", encoded = true) long id,
                                                         @Query(value = "per_page", encoded = true) int page);

    @GET
    Call<Product> productById(@Url String url);

    @GET
    Call<List<Product>> productsByIds(@Url String Url ,@Query(value="include", encoded = true) String ids);

    @GET
    Call<List<Product>> getTopSellers(@Url String url,@Query(value="period", encoded = true) String period);
}
