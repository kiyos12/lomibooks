package com.a360ground.lomibook.client;

import com.a360ground.lomibook.domain.woocommerce.Email;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by Kiyos on 6/23/2017.
 */

public interface EmailClient {

    @FormUrlEncoded
    @POST
    Call<String> sendEmail(@Url String url, @Field("from") String from,@Field("subject") String subject, @Field("message") String message);

}
