package com.a360ground.lomibook.application;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;

import com.a360ground.lomibook.R;
import com.a360ground.lomibook.activity.MainActivity;
import com.a360ground.lomibook.domain.LomiModel;
import com.a360ground.lomibook.domain.woocommerce.WooCommerceError;
import com.a360ground.lomibook.serviceGenerator.WooCommerceServiceGenerator;
import com.a360ground.lomibook.utils.Constants;
import com.a360ground.lomibook.utils.LomiFileUtils;
import com.a360ground.lomibook.view.ExtendedToast;
import com.jaychang.slm.SocialLoginManager;
import com.koolearn.klibrary.ui.android.library.ZLAndroidApplication;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import okhttp3.ResponseBody;
import retrofit2.Converter;

public class LomiApplication extends ZLAndroidApplication {

    private static final String PATH = Environment.getExternalStorageDirectory() + "/download/lomi.apk";

    WooCommerceServiceGenerator generator;

    public static List<Long> getIds(List<LomiModel> models) {

        List<Long> ids = new ArrayList<>();

        for (LomiModel model : models) {

            ids.add(model.getId());
        }

        return ids;
    }

    @Override
    public void onCreate() {

        generator = new WooCommerceServiceGenerator(getApplicationContext());

        SocialLoginManager.init(this);

        super.onCreate();

        LomiFileUtils.createLomiFolder();

        Realm.init(this);

        byte[] key = new byte[64];

        new SecureRandom().nextBytes(key);

        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .name("app.realm")
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(config);

        // Picasso caching
        // src: http://stackoverflow.com/a/30686992/4688650
        //todo wait till the next Picasso release to make this work.
        // It's not working with OkHttp3
        /*Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttp3Downloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(true);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);*/
    }

    public boolean isNetworkAvailable() {

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    /*public void update(Context context) {

        ProgressDialog progressDialog = new ProgressDialog(context);

        progressDialog.setTitle(getString(R.string.update_available));

        progressDialog.setMessage(getString(R.string.updating));

        progressDialog.setCancelable(false);

        progressDialog.show();

        final Uri uri = Uri.parse("file://" + PATH);

        File apkFile = new File(PATH);

        if (apkFile.exists()) {

            apkFile.delete();
        }

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(Constants.APK_URL));

        request.setDescription(getString(R.string.downloading_update));

        request.setTitle(getString(R.string.app_name));

        request.setDestinationUri(uri);

        final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        final long downloadId = manager.enqueue(request);

        BroadcastReceiver onComplete = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                Intent broadcastIntent = new Intent("message");

                broadcastIntent.putExtra("success", "Completed download");

                LocalBroadcastManager.getInstance(LomiApplication.this).sendBroadcast(broadcastIntent);

                Intent install = new Intent(Intent.ACTION_VIEW);

                install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                install.setDataAndType(uri, manager.getMimeTypeForDownloadedFile(downloadId));

                startActivity(install);

                unregisterReceiver(this);
            }
        };

        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }*/

    public Error getErrorResponse(ResponseBody responseBody) throws IOException {

        Converter<ResponseBody, WooCommerceError> converter = generator.getRetrofit().responseBodyConverter(WooCommerceError.class, new Annotation[0]);

        return converter.convert(responseBody);
    }

    public void launchSignupWithError() {

        ExtendedToast.showToast(this, ExtendedToast.INFO, getString(R.string.login_required));

        Intent intent = new Intent(this, MainActivity.class);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        intent.putExtra(Constants.ERROR_MESSAGE_IDENTIFIER, "must_login");

        startActivity(intent);
    }
}