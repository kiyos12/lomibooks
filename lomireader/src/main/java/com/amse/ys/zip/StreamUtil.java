package com.amse.ys.zip;

import android.content.Context;
import android.util.Log;

import com.koolearn.klibrary.core.drm.Encryption;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Kiyos on 8/14/2017.
 */

public class StreamUtil {

    public static final String PREFIX = "stream2file";
    public static final String SUFFIX = ".epub";

    public static File stream2file(Context context, InputStream in, String fileName) throws IOException {

        final File tempFile = new File(context.getCacheDir().getAbsolutePath()+File.separator+fileName+SUFFIX);

        tempFile.deleteOnExit();

        FileOutputStream out = new FileOutputStream(tempFile);

        IOUtils.copy(in, out);

        return tempFile;
    }



    public static String getDecrypted(Context context, File file, boolean isOnline) throws IOException {
        String path = "";
        if (file.getAbsolutePath().endsWith(".sample.lomi")) {
            File file2 = new File(context.getCacheDir().getAbsolutePath()+File.separator+file.getName()+SUFFIX);
            FileUtils.copyFile(file, file2);
            if(file2.exists()){
                path =  context.getCacheDir().getAbsolutePath()+File.separator+file.getName()+SUFFIX;
            }
            else{
                Log.d(StreamUtil.class.getSimpleName(), "getDecrypted: else");
            }
        } else {
            path = new Encryption(context).getFile(context, file, isOnline, file.getName()).getAbsolutePath();
        }

        return path;
    }

}