package com.koolearn.klibrary.core.drm;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;
import android.util.Log;

import com.amse.ys.zip.StreamUtil;
import com.koolearn.klibrary.core.util.Native;
import com.koolearn.klibrary.core.util.PreferenceUtill;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import static android.content.ContentValues.TAG;

/**
 * Created by Kiyos Solomon on 10/17/2016.
 */
public class Encryption {
    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES";
    private static final String PrefName = "DeviceInfo";
    private static final String AndroidKeyStore = "AndroidKeyStore";
    private static final String AES_MODE = "AES/GCM/NoPadding";

    private Context context;
    private String encEncKey;

    public Encryption(Context context) {
        this.context = context;
        encEncKey = Native.storePassBookEnc();
    }

    public static void getSha1(Context context) {
        PackageInfo info;
        try {

            info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                System.out.println("Hash key" + something);
            }

        } catch (Exception e) {

        }
    }

    public String getEncEncKey() {
        return encEncKey;
    }

    public String keyGen() {
        String key;
        if (PreferenceUtill.readFromPreferences(context, PrefName, null) == null) {
            key = UUID.randomUUID().toString().replace("-", "").substring(0, 16);
            PreferenceUtill.saveToPreferences(context, PrefName, crypto(key, false));
        } else {
            key = crypto(PreferenceUtill.readFromPreferences(context, PrefName, null), true);
        }
        return key;
    }

    public void encrypt(File file) {
        try {
            Key secretKey = new SecretKeySpec(keyGen().getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            InputStream inputStream = decrypt(file, true);
            byte[] inputBytes = new byte[(int) file.length()];
            inputStream.read(inputBytes);
            byte[] outputBytes = cipher.doFinal(inputBytes);
            FileOutputStream outputStream = new FileOutputStream(file.getParent() + File.separator + file.getName().substring(0, file.getName().length() - 5) + ".lomi");
            outputStream.write(outputBytes);
            inputStream.close();
            outputStream.close();
        } catch (Exception e) {

        }
    }

    private String doEncEncrypto(String string, int mode) {
        try {
            Key secretKey = new SecretKeySpec(encEncKey.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(mode, secretKey);
            byte[] outputBytes = cipher.doFinal(string.getBytes());
            return IOUtils.toString(outputBytes);
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    public String crypto(String inString, boolean decrypt) {
        try {
            Key secretKey = new SecretKeySpec(encEncKey.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            byte[] inputByte = inString.getBytes("UTF-8");
            if (decrypt) {
                cipher.init(Cipher.DECRYPT_MODE, secretKey);
                return new String(cipher.doFinal(Base64.decode(inputByte, Base64.DEFAULT)));
            } else {
                cipher.init(Cipher.ENCRYPT_MODE, secretKey);
                return new String(Base64.encode(cipher.doFinal(inputByte), Base64.DEFAULT));
            }
        } catch (Exception e) {
            return null;
        }
    }

    public InputStream decrypt(File file, boolean isOnline) {
        byte[] outputBytes = null;
        String d = "";
        Key secretKey;
        try {
            if (isOnline) {
                secretKey = new SecretKeySpec(Native.storePassBookEnc().getBytes(), ALGORITHM);
                d = "online";
            } else {
                secretKey = new SecretKeySpec(keyGen().getBytes(), ALGORITHM);
                d = "offline";
            }
            Log.d(TAG, "decrypt: "+d);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            FileInputStream fileinputStream = new FileInputStream(file);
            byte[] inputBytes = new byte[(int) file.length()];
            fileinputStream.read(inputBytes);
            outputBytes = cipher.doFinal(inputBytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ByteArrayInputStream(outputBytes);
    }

    public File getFile(Context context, File file, boolean isOnline, String fileName) throws IOException {
        return StreamUtil.stream2file(context, decrypt(file, isOnline), fileName);
    }

    private SecretKey getSecretKeyM(final String alias) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, KeyStoreException, IOException, CertificateException, UnrecoverableKeyException {

        KeyStore keyStore = KeyStore.getInstance(AndroidKeyStore);
        keyStore.load(null);
        KeyGenerator keyGenerator = null;
        if (!keyStore.containsAlias(alias)) {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, AndroidKeyStore);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                keyGenerator.init(
                        new KeyGenParameterSpec.Builder(alias,
                                KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                                .setBlockModes(KeyProperties.BLOCK_MODE_GCM).setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                                .setRandomizedEncryptionRequired(false)
                                .build());
            } else {

            }
        }
        assert keyGenerator != null;
        return keyGenerator.generateKey();
    }

    private byte[] toByteArray(File file) throws IOException {
        InputStream is = new FileInputStream(file);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[16384];

        while ((nRead = is.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }

        buffer.flush();

        return buffer.toByteArray();
    }

}