package com.koolearn.klibrary.core.util;

/**
 * Created by Kiyos on 4/20/2017.
 */

public class Native {

    public static native String storePassBookEnc();

    public static native String getTopSearchs();

    public static native String keyGen2();

    public static native Object[] getHeaders();

    public static native String getProducts();

    public static native String getCategories();

    public static native byte[] getAuthorBooks(String id);

    public static native String getApk();

    public static native byte[] getProductsByCategoryId(String id);

    public static native String getCustomer();

    public static native String getOrder();

    public static native byte[] getCustomerDownloads(String id);

    public static native byte[] putCustomerID(String id);

    public static native byte[] checkDevice(String id);

    public static native byte[] getProductsById(String id);

    public static native byte[] getSearchProducts(String query);

    public static native byte[] getCustomerById(String id);

    public static native byte[] getCustomerByEmail(String email);

    public static native byte[] getCouponByCode(String code);

    public static native String getTopSellers();

    public static native byte[] putCustomerById(String id);

    public static native byte[] getDownloadUrl(String cid, String pid);

    static {
        System.loadLibrary("store-imp");
    }


}