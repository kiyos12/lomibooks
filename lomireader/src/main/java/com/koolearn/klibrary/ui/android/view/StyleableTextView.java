package com.koolearn.klibrary.ui.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.koolearn.klibrary.ui.android.util.UiUtil;
import com.ninestars.android.R;


public class StyleableTextView extends android.support.v7.widget.AppCompatTextView {

    public StyleableTextView(Context context) {
        super(context);
    }

    public StyleableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.StyleableTextView,
                R.styleable.StyleableTextView_font);
    }

    public StyleableTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.StyleableTextView,
                R.styleable.StyleableTextView_font);
    }

}
