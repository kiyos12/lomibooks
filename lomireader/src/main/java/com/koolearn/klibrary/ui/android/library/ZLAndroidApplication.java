package com.koolearn.klibrary.ui.android.library;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.koolearn.android.kooreader.config.ConfigShadow;
import com.koolearn.klibrary.ui.android.image.ZLAndroidImageManager;

public abstract class ZLAndroidApplication extends MultiDexApplication {
	private ZLAndroidLibrary myLibrary;
	private ConfigShadow myConfig;

	static private Context context;
	@Override
	public void onCreate() {
		super.onCreate();
		// this is a workaround for strange issue on some devices:
		//    NoClassDefFoundError for android.os.AsyncTask
		try {
			Class.forName("android.os.AsyncTask"); // 一个解决bug的奇怪的方法
		} catch (Throwable t) {
		}
		context = getApplicationContext();
		myConfig = new ConfigShadow(this); // 绑定服务,创建config.db
		new ZLAndroidImageManager();
		myLibrary = new ZLAndroidLibrary(this);
	}

	public final ZLAndroidLibrary library() {
		return myLibrary;
	}
	public static Context getContext() {
		return context;
	}

}