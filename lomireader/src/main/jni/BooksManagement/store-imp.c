#include <jni.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const char *subString(const char *someString, int n);

char *replace_str(char *string, char *search, char *replace);

const char *header1[] = {"deb3cdf5-3c63-4c4e-8cce-c7f9f5c99370",
                         "5f8ad0d9-126b-40d5-8548-a857a2f5ed6d",
                         "a88b2bea-27f7-43ad-9c78-861d2e755c4a",
                         "153fd9df-2966-4e9e-9e98-f56f48a77f9c",
                         "9f7ecb73-cab7-4152-b0ce-1f871d5efb3a",
                         "fbf6f4b9-9b4d-4073-8410-f967dc855966",
                         "3a7bd243-9a09-45e4-837a-610987880631",
                         "8d017f68-da5f-41e8-a768-d5dd4692af90",
                         "09b244aa-7ea1-48a5-8680-af6c4095eab9",
                         "d2fefa15-6cc1-4b0d-8d6b-da9aa97ad3dd",
                         "a416ca8e-6de1-4059-b942-df196a6b3aa7",
                         "4a39c151-b911-457e-969d-90c22631fec8",
                         "ddffda1e-2e6c-42b9-aa6f-52e050b0d342",
                         "3e805d8f-51b3-4b33-b303-3357d27372b5",
                         "e2725abc-221f-4ace-801b-9dd133636d9d",
                         "6467f778-8be7-43f4-a63e-b47606a9a1a4",};

const char *header2[] = {"94ad6901-1016-40f0-b419-93a540da3661",
                         "ca72d546-2e86-4d5c-b0ec-5fc54d771186",
                         "9a382c52-0b66-4a0a-8b6d-2e3a26755a30",
                         "c1068426-4ae9-4cd2-869d-3f79016addc1",
                         "4a15c9e0-ac3d-40f5-b5ea-a18b729b9280",
                         "4e56696f-b132-4ce4-b7b9-bded26295421",
                         "9c7510df-6ad7-49fb-bdf6-e686ce724da8",
                         "ae9d1e73-31aa-47fa-bb70-3fa8b07fcac5",
                         "165fa601-472e-484f-8acf-46b92a1a77ed",
                         "fd1a8912-74df-4576-b4ac-b310ebb1f156",
                         "b3ef1ded-bd5c-42bf-afce-fffb01ff4826",
                         "00744ff7-ea41-40c1-9f3e-8d60a4b81256",
                         "2ac9b88e-3db3-4888-8f86-37af227900b4",
                         "6756d2e7-4b47-41c4-ba30-964658046556",
                         "adf1ab85-163e-4b7e-a17b-a7f00c6a4be1",
                         "58792ba4-eb1b-42ef-83e4-b00c164242c1",};


const char *header3[] = {"04c83240-7de1-4e35-8edc-5b2e7a723d6e",
                         "8c13634d-0808-47f8-a024-d61bed1f82f6",
                         "d07bb48a-ad81-4082-803c-915f805c0850",
                         "146a0ef0-ac21-42e7-92da-b87ab20297e0",
                         "ba84f058-03fb-4cc1-b284-61d82104498a",
                         "ff67bd27-a879-43c6-8bc5-25cb189ab627",
                         "e375b878-99d8-4290-acee-80b76f5f130b",
                         "b4bf93fa-97db-4c97-97cb-a0ec3eb57420",
                         "080a4f31-2603-4648-8dba-5944b095c1ad",
                         "fdf79528-143a-48a0-8a74-9a62094c68b5",
                         "ae9295a4-64e7-44fb-8a66-a0ed3ecf0968",
                         "ca78eddb-2bc7-4350-8fcc-7782fea3072f",
                         "fba6f3d2-473c-455b-ae02-9a6702d9644f",
                         "1fa2ea55-76a2-4b67-8c98-b5a17e860fc4",
                         "dff98dcd-6094-476a-bd72-08c0b492612d",
                         "5ff8b2bf-19b8-42b1-8cb4-8382a356ef56"


};
const char *header4[] = {"04c83240-7de1-4e35-8edc-5b2e7a723d6e",
                         "8c13634d-0808-47f8-a024-d61bed1f82f6",
                         "d07bb48a-ad81-4082-803c-915f805c0850",
                         "146a0ef0-ac21-42e7-92da-b87ab20297e0",
                         "ba84f058-03fb-4cc1-b284-61d82104498a",
                         "ff67bd27-a879-43c6-8bc5-25cb189ab627",
                         "e375b878-99d8-4290-acee-80b76f5f130b",
                         "b4bf93fa-97db-4c97-97cb-a0ec3eb57420",
                         "080a4f31-2603-4648-8dba-5944b095c1ad",
                         "fdf79528-143a-48a0-8a74-9a62094c68b5",
                         "ae9295a4-64e7-44fb-8a66-a0ed3ecf0968",
                         "ca78eddb-2bc7-4350-8fcc-7782fea3072f",
                         "fba6f3d2-473c-455b-ae02-9a6702d9644f",
                         "1fa2ea55-76a2-4b67-8c98-b5a17e860fc4",
                         "dff98dcd-6094-476a-bd72-08c0b492612d",
                         "5ff8b2bf-19b8-42b1-8cb4-8382a356ef56"


};
const char *header5[] = {"04c83240-7de1-4e35-8edc-5b2e7a723d6e",
                         "8c13634d-0808-47f8-a024-d61bed1f82f6",
                         "d07bb48a-ad81-4082-803c-915f805c0850",
                         "146a0ef0-ac21-42e7-92da-b87ab20297e0",
                         "ba84f058-03fb-4cc1-b284-61d82104498a",
                         "ff67bd27-a879-43c6-8bc5-25cb189ab627",
                         "e375b878-99d8-4290-acee-80b76f5f130b",
                         "b4bf93fa-97db-4c97-97cb-a0ec3eb57420",
                         "080a4f31-2603-4648-8dba-5944b095c1ad",
                         "fdf79528-143a-48a0-8a74-9a62094c68b5",
                         "ae9295a4-64e7-44fb-8a66-a0ed3ecf0968",
                         "ca78eddb-2bc7-4350-8fcc-7782fea3072f",
                         "fba6f3d2-473c-455b-ae02-9a6702d9644f",
                         "1fa2ea55-76a2-4b67-8c98-b5a17e860fc4",
                         "dff98dcd-6094-476a-bd72-08c0b492612d",
                         "5ff8b2bf-19b8-42b1-8cb4-8382a356ef56"


};

const char *DEFAULT_REMOTE_URI = "https://api.lomistore.com";

const char *header6[] = {"04c83240-7de1-4e35-8edc-5b2e7a723d6e",
                         "8c13634d-0808-47f8-a024-d61bed1f82f6",
                         "d07bb48a-ad81-4082-803c-915f805c0850",
                         "146a0ef0-ac21-42e7-92da-b87ab20297e0",
                         "ba84f058-03fb-4cc1-b284-61d82104498a",
                         "ff67bd27-a879-43c6-8bc5-25cb189ab627",
                         "e375b878-99d8-4290-acee-80b76f5f130b",
                         "b4bf93fa-97db-4c97-97cb-a0ec3eb57420",
                         "080a4f31-2603-4648-8dba-5944b095c1ad",
                         "fdf79528-143a-48a0-8a74-9a62094c68b5",
                         "ae9295a4-64e7-44fb-8a66-a0ed3ecf0968",
                         "ca78eddb-2bc7-4350-8fcc-7782fea3072f",
                         "fba6f3d2-473c-455b-ae02-9a6702d9644f",
                         "1fa2ea55-76a2-4b67-8c98-b5a17e860fc4",
                         "dff98dcd-6094-476a-bd72-08c0b492612d",
                         "5ff8b2bf-19b8-42b1-8cb4-8382a356ef56"
};


JNIEXPORT jstring JNICALL

Java_com_koolearn_klibrary_core_util_Native_storePassBookEnc(
        JNIEnv *env, jobject instance) {

    jstring encEncKey = "qwertyuiopasdfgh";

    return (*env)->NewStringUTF(env, encEncKey);
}

Java_com_koolearn_klibrary_core_util_Native_getTopSearchs(
        JNIEnv *env, jobject instance) {

jstring encEncKey = "https://api.lomistore.com/proxy/topsearches/";

return (*env)->NewStringUTF(env, encEncKey);
}

JNIEXPORT jstring JNICALL
Java_com_koolearn_klibrary_core_util_Native_keyGen2(JNIEnv *env,
                                                       jobject instance) {

    jstring returnedKey = "";

    jclass jclass1 = (*env)->FindClass(env, "java/util/UUID");

    jmethodID randomUUID = (*env)->GetStaticMethodID(env, jclass1, "randomUUID",
                                                     "()Ljava/util/UUID;");

    jmethodID toString = (*env)->GetMethodID(env, jclass1, "toString",
                                             "()Ljava/lang/String;");

    if (randomUUID != NULL) {

        jobject jobject1 = (*env)->CallStaticObjectMethod(env, jclass1, randomUUID);

        returnedKey = (*env)->CallObjectMethod(env, jobject1, toString);

        const char *toConstChar = (*env)->GetStringUTFChars(env, returnedKey, 0);

        char *replacedHyphen = replace_str(toConstChar, "-", "");

        returnedKey = (*env)->NewStringUTF(env, subString(replacedHyphen, 16));
    } else {
        returnedKey = "Error in casting";
    }


    return returnedKey;
}

const char *subString(const char *someString, int n) {
    char *new = malloc(sizeof(char) * n + 1);

    strncpy(new, someString, n);

    new[n] = '\0';

    return new;
}

char *replace_str(char *string, char *search, char *replace) {
    char buffer[100];
    char *p = string;
    while ((p = strstr(p, search))) {
        strncpy(buffer, string, p - string);
        buffer[p - string] = '\0'; //EDIT: THIS WAS MISSING
        strcat(buffer, replace);
        strcat(buffer, p + strlen(search));
        strcpy(string, buffer);
        p++;
    }
    return buffer;
}

jstring merge_str(jstring string1, jstring string2) {

    char *str1;
    char *str2;
    str1 = string1;
    str2 = string2;
    char *str3 = (char *) malloc(1 + strlen(str1) + strlen(str2));
    strcpy(str3, str1);
    strcat(str3, str2);
    return str3;
}


JNIEXPORT jobjectArray JNICALL
Java_com_koolearn_klibrary_core_util_Native_getHeaders(JNIEnv *env, jobject instance) {

    jclass objectClass = (*env)->FindClass(env, "java/lang/Object");
    jobjectArray headers = (*env)->NewObjectArray(env, 3, objectClass, 0);

    int x1 = (rand() % 16);
    int x2 = (rand() % 16);
    int x3 = (rand() % 16);

    jstring headerJstring1 = (*env)->NewStringUTF(env, header1[x1]);
    jstring headerJstring2 = (*env)->NewStringUTF(env, header2[x2]);
    jstring headerJstring3 = (*env)->NewStringUTF(env, header3[x3]);

    (*env)->SetObjectArrayElement(env, headers, 0, headerJstring1);
    (*env)->SetObjectArrayElement(env, headers, 1, headerJstring2);
    (*env)->SetObjectArrayElement(env, headers, 2, headerJstring3);

    return headers;
}

JNIEXPORT jstring JNICALL
Java_com_koolearn_klibrary_core_util_Native_getProducts(JNIEnv *env, jobject instance) {

    return (*env)->NewStringUTF(env, merge_str(DEFAULT_REMOTE_URI, "/proxy/products/"));
}

JNIEXPORT jstring JNICALL
Java_com_koolearn_klibrary_core_util_Native_getCategories(JNIEnv *env, jobject instance) {

    return (*env)->NewStringUTF(env,
                                "https://api.lomistore.com/proxy/products/categories/?hide_empty=true");
}

JNIEXPORT jstring JNICALL
Java_com_koolearn_klibrary_core_util_Native_getCustomer(JNIEnv *env, jobject instance) {

    return (*env)->NewStringUTF(env, "https://api.lomistore.com/signup/");
}

JNIEXPORT jstring JNICALL
Java_com_koolearn_klibrary_core_util_Native_getOrder(JNIEnv *env, jobject instance) {

    return (*env)->NewStringUTF(env, "https://api.lomistore.com/order/");
}

JNIEXPORT jbyteArray JNICALL
Java_com_koolearn_klibrary_core_util_Native_getCustomerDownloads(JNIEnv *env, jobject instance,
                                                                    jstring id_) {

    const char *id = (*env)->GetStringUTFChars(env, id_, 0);

    // TODO

    const char *s1 = merge_str("https://api.lomistore.com/proxy/customers/", id);

    jstring s2 = merge_str(s1, "/downloads/");

    jbyteArray trackIDArray = (*env)->NewByteArray(env, strlen(s2));

    (*env)->SetByteArrayRegion(env, trackIDArray, 0, strlen(s2), (const jbyte *) s2);

    return trackIDArray;
}

JNIEXPORT jbyteArray JNICALL
Java_com_koolearn_klibrary_core_util_Native_getDownloadUrl(JNIEnv *env, jobject instance,
                                                              jstring cid_, jstring pid_) {

    const char *cid = (*env)->GetStringUTFChars(env, cid_, 0);

    const char *pid = (*env)->GetStringUTFChars(env, pid_, 0);

    // TODO

    const char *s1 = merge_str("https://api.lomistore.com/customers/", cid);

    jstring s2 = merge_str(s1, "/downloads/");

    jstring s3 = merge_str(s2, pid);

    jbyteArray trackIDArray = (*env)->NewByteArray(env, strlen(s3));

    (*env)->SetByteArrayRegion(env, trackIDArray, 0, strlen(s3), (const jbyte *) s3);

    return trackIDArray;
}

JNIEXPORT jbyteArray JNICALL
Java_com_koolearn_klibrary_core_util_Native_getProductsById(JNIEnv *env, jobject instance,
                                                               jstring id_) {
    const char *id = (*env)->GetStringUTFChars(env, id_, 0);

    const char *s1 = merge_str("https://api.lomistore.com/proxy/products/", id);

    jbyteArray trackIDArray = (*env)->NewByteArray(env, strlen(s1));

    (*env)->SetByteArrayRegion(env, trackIDArray, 0, strlen(s1), (const jbyte *) s1);

    return trackIDArray;
}

JNIEXPORT jbyteArray JNICALL
Java_com_koolearn_klibrary_core_util_Native_putCustomerID(JNIEnv *env, jobject instance,
                                                               jstring id_) {
    const char *id = (*env)->GetStringUTFChars(env, id_, 0);

    const char *s1 = merge_str("https://api.lomistore.com/signup/setDevice/", id);

    jbyteArray trackIDArray = (*env)->NewByteArray(env, strlen(s1));

    (*env)->SetByteArrayRegion(env, trackIDArray, 0, strlen(s1), (const jbyte *) s1);

    return trackIDArray;
}

JNIEXPORT jbyteArray JNICALL
Java_com_koolearn_klibrary_core_util_Native_checkDevice(JNIEnv *env, jobject instance,
                                                               jstring id_) {
    const char *id = (*env)->GetStringUTFChars(env, id_, 0);

    const char *s1 = merge_str("https://api.lomistore.com/signup/checkDevice/", id);

    jbyteArray trackIDArray = (*env)->NewByteArray(env, strlen(s1));

    (*env)->SetByteArrayRegion(env, trackIDArray, 0, strlen(s1), (const jbyte *) s1);

    return trackIDArray;
}

JNIEXPORT jbyteArray JNICALL
        Java_com_koolearn_klibrary_core_util_Native_getSearchProducts(JNIEnv *env, jobject instance,
jstring query_) {
const char *query = (*env)->GetStringUTFChars(env, query_, 0);

const char *s1 = merge_str("https://api.lomistore.com/proxy/search/?query=", query);

jbyteArray trackIDArray = (*env)->NewByteArray(env, strlen(s1));

(*env)->SetByteArrayRegion(env, trackIDArray, 0, strlen(s1), (const jbyte *) s1);

return trackIDArray;
}

JNIEXPORT jstring JNICALL
Java_com_koolearn_klibrary_core_util_Native_getTopSellers(JNIEnv *env, jobject instance) {

    // TODO
    jstring encEncKey = "https://api.lomistore.com/proxy/reports/top_sellers/";

    return (*env)->NewStringUTF(env, encEncKey);
}

JNIEXPORT jbyteArray JNICALL
Java_com_koolearn_klibrary_core_util_Native_getCustomerById(JNIEnv *env, jobject instance,
                                                               jstring id_) {

    const char *id = (*env)->GetStringUTFChars(env, id_, NULL);

    const char *s1 = merge_str("https://api.lomistore.com/proxy/customers/", id);

    jbyteArray trackIDArray = (*env)->NewByteArray(env, strlen(s1));

    (*env)->SetByteArrayRegion(env, trackIDArray, 0, strlen(s1), (const jbyte *) s1);

    return trackIDArray;
}

JNIEXPORT jbyteArray JNICALL
Java_com_koolearn_klibrary_core_util_Native_putCustomerById(JNIEnv *env, jobject instance,
                                                               jstring id_) {
    const char *id = (*env)->GetStringUTFChars(env, id_, NULL);

    const char *s1 = merge_str("https://api.lomistore.com/customers/", id);

    jbyteArray trackIDArray = (*env)->NewByteArray(env, strlen(s1));

    (*env)->SetByteArrayRegion(env, trackIDArray, 0, strlen(s1), (const jbyte *) s1);

    return trackIDArray;
}

JNIEXPORT jbyteArray JNICALL
Java_com_koolearn_klibrary_core_util_Native_getProductsByCategoryId(JNIEnv *env, jobject instance,
                                                                       jstring id_) {
    const char *id = (*env)->GetStringUTFChars(env, id_, 0);

    // TODO

    (*env)->ReleaseStringUTFChars(env, id_, id);
}

JNIEXPORT jstring JNICALL
Java_com_koolearn_klibrary_core_util_Native_getApk(JNIEnv *env, jobject instance) {

    // TODO


    return (*env)->NewStringUTF(env, "https://api.lomistore.com/wp-json/wp/v2/apks");
}

JNIEXPORT jbyteArray JNICALL
Java_com_koolearn_klibrary_core_util_Native_getAuthorBooks(JNIEnv *env, jobject instance,
                                                              jstring id_) {

    const char *id = (*env)->GetStringUTFChars(env, id_, NULL);

    const char *s1 = merge_str("https://api.lomistore.com/customers/author-books/", id);

    jbyteArray trackIDArray = (*env)->NewByteArray(env, strlen(s1));

    (*env)->SetByteArrayRegion(env, trackIDArray, 0, strlen(s1), (const jbyte *) s1);

    return trackIDArray;
}

JNIEXPORT jbyteArray JNICALL
Java_com_koolearn_klibrary_core_util_Native_getCustomerByEmail(JNIEnv *env, jobject instance,
                                                                  jstring email_) {

    const char *email = (*env)->GetStringUTFChars(env, email_, NULL);

    const char *s1 = merge_str("https://api.lomistore.com/customers/email/", email);

    jbyteArray trackIDArray = (*env)->NewByteArray(env, strlen(s1));

    (*env)->SetByteArrayRegion(env, trackIDArray, 0, strlen(s1), (const jbyte *) s1);

    return trackIDArray;
}

JNIEXPORT jbyteArray JNICALL
Java_com_koolearn_klibrary_core_util_Native_getCouponByCode(JNIEnv *env, jobject instance,
                                                               jstring code_) {

    const char *code = (*env)->GetStringUTFChars(env, code_, NULL);

    const char *s1 = merge_str("https://api.lomistore.com/proxy/coupon/", code);

    const char *s2 = merge_str(s1, "/");

    jbyteArray trackIDArray = (*env)->NewByteArray(env, strlen(s2));

    (*env)->SetByteArrayRegion(env, trackIDArray, 0, strlen(s2), (const jbyte *) s2);

    return trackIDArray;
}