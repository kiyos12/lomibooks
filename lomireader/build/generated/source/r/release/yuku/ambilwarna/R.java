/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package yuku.ambilwarna;

public final class R {
    public static final class attr {
        public static int supportsAlpha = 0x7f010029;
    }
    public static final class dimen {
        public static int ambilwarna_hsvHeight = 0x7f0a0005;
        public static int ambilwarna_hsvWidth = 0x7f0a0006;
        public static int ambilwarna_hueWidth = 0x7f0a0050;
        public static int ambilwarna_spacer = 0x7f0a0051;
    }
    public static final class drawable {
        public static int ambilwarna_alphacheckered = 0x7f02004d;
        public static int ambilwarna_alphacheckered_tiled = 0x7f02004e;
        public static int ambilwarna_arrow_down = 0x7f02004f;
        public static int ambilwarna_arrow_right = 0x7f020050;
        public static int ambilwarna_cursor = 0x7f020051;
        public static int ambilwarna_hue = 0x7f020052;
        public static int ambilwarna_target = 0x7f020053;
    }
    public static final class id {
        public static int ambilwarna_alphaCheckered = 0x7f0f0079;
        public static int ambilwarna_alphaCursor = 0x7f0f007c;
        public static int ambilwarna_cursor = 0x7f0f007b;
        public static int ambilwarna_dialogView = 0x7f0f0075;
        public static int ambilwarna_newColor = 0x7f0f0080;
        public static int ambilwarna_oldColor = 0x7f0f007f;
        public static int ambilwarna_overlay = 0x7f0f007a;
        public static int ambilwarna_pref_widget_box = 0x7f0f0081;
        public static int ambilwarna_state = 0x7f0f007e;
        public static int ambilwarna_target = 0x7f0f007d;
        public static int ambilwarna_viewContainer = 0x7f0f0076;
        public static int ambilwarna_viewHue = 0x7f0f0078;
        public static int ambilwarna_viewSatBri = 0x7f0f0077;
    }
    public static final class layout {
        public static int ambilwarna_dialog = 0x7f04001b;
        public static int ambilwarna_pref_widget = 0x7f04001c;
    }
    public static final class styleable {
        public static int[] AmbilWarnaPreference = { 0x7f010029 };
        public static int AmbilWarnaPreference_supportsAlpha = 0;
    }
}
